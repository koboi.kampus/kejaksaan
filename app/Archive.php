<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    protected $fillable = [
        'caase_id',
        'dictum',
        'tanggal_dictum',
        'dictum_pidana',
        'dictum_barangbukti',
        'nomor_putusan',
        'tanggal_putusan',
        'amar_pidana',
        'amar_barangbukti',
        'sikap_jpu',
        'sikap_terdakwa',
        'eksekusi_tuntas',
        'bp_arsip',
        'kasus_posisi',
        'keterangan'
    ];

    public function caase()
    {
        return $this->belongsTo('App\Caase');
    }
}
