<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Caase extends Model
{
    //protected $table = 'cases';

    protected $fillable = [
      'type_id', 
      'classification_id', 
      'nomor_spdp', 
      'tanggal_spdp', 
      'spdp_diterima', 
      'case_file_id', 
      'keterangan',
      'status',
      'tanggal_status',
      'pelanggaran'
    ];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function classification()
    {
      return $this->belongsTo('App\Classification');
    }

    public function casefile()
    {
      return $this->belongsTo('App\CaseFile');
    }

    public function suspects()
    {
      return $this->belongsToMany('App\Suspect');
    }

    public function lawbooks()
    {
      return $this->belongsToMany('App\Lawbook');
    }

    public function investigators()
    {
      return $this->belongsToMany('App\Investigator');
    }

    public function profiles()
    {
      return $this->belongsToMany('App\Profile')->withPivot('id');
    }

    public function sixteen()
    {
      return $this->hasMany('App\Sixteen');
    }

    public function sixtena()
    {
      return $this->hasMany('App\Sixtena');
    }

    public function seventeen()
    {
      return $this->hasOne('App\Seventeen');
    }

    public function eighteen()
    {
      return $this->hasOne('App\Eighteen');
    }

    public function twenty()
    {
      return $this->hasOne('App\Twenty');
    }

    public function twentyone()
    {
      return $this->hasOne('App\Twentyone');
    }

    public function document()
    {
      return $this->hasOne('App\Document');
    }

    public function handover()
    {
      return $this->hasOne('App\Handover');
    }

    public function court()
    {
      return $this->hasOne('App\Court');
    }

    public function execution()
    {
      return $this->hasOne('App\Execution');
    }

    public function archive()
    {
      return $this->hasOne('App\Archive');
    }

    public function scopeStatue($query)
    {
        return $query->where('status', '<', '10');
    }

    public function getStatusPerkaraAttribute()
    {
      $persidangan = \App\Court::where('caase_id', $this->id)->get();

      if ($this->status == '1') {
        return "P-16";
      } elseif ($this->status == '2') {
        return "P-17";
      } elseif ($this->status == '3') {
        return "P-18 / P-19";
      } elseif ($this->status == '4') {
        return "P-20";
      } elseif ($this->status == '5') {
        return "P-21";
      } elseif ($this->status == '6') {
        return "BP";
      } elseif ($this->status == '7') {
        return "P-21.A";
      } elseif ($this->status == '10') {
        return "P16.A";
      } elseif ($this->status == '11') {
        return "Pelimpahan";
      } elseif ($this->status == '12' AND $persidangan->first()->banding == '0' AND $persidangan->first()->kasasi == '0') {
        return "Persidangan";
      } elseif ($this->status == '12' AND $persidangan->first()->banding == '1' AND $persidangan->first()->kasasi == '1') {
        return "Persidangan";
      } elseif ($this->status == '12' AND $persidangan->first()->banding == '1') {
        return "Banding";
      } elseif ($this->status == '12' AND $persidangan->first()->kasasi == '1') {
        return "Kasasi";
      } elseif ($this->status == '20') {
        return "Eksekusi";
      } else {
        return "Belum Ada Status";
      }
    }

    public function getTimeLeftAttribute()
    {
        $now = Carbon::now();
        $terima_spdp = new Carbon($this->spdp_diterima);        
        $status_date = new Carbon($this->tanggal_status);

        $persidangan = \App\Court::where('caase_id', $this->id)->get();

        if($this->status == '1') 
        {
          $expire = $terima_spdp->addDays(30);
  
          if ($now->diffInDays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '2') {
          $expire = $status_date->addDays(30);
          
          if ($now->diffIndays($expire) > 0)
          {
            return $now->diffIndays($expire);
          }
        } elseif($this->status == '3') {
          $expire = $status_date->addDays(30);

          if ($now->diffIndays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '4') {
          $expire = $status_date->addDays(30);

          if ($now->diffInDays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '5') {
          $expire = $status_date->addDays(30);
          if ($now->diffInDays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '6') {
          $expire = $status_date->addDays(14);
          if ($now->diffInDays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '7') {
          $expire = $status_date->addDays(30);
          if ($now->diffInDays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '10' AND $this->type_id == '2') {
            $expire = $status_date->addDays(10);
            if ($now->diffInDays($expire) > 0)
            {
              return $now->diffInDays($expire);
            }
        } elseif($this->status == '10' AND $this->type_id == '1') {
            $expire = $status_date->addDays(3);
            if ($now->diffInDays($expire) > 0)
            {
              return $now->diffInDays($expire);
            }
        } elseif($this->status == '11') {
          $expire = $status_date->addDays(30);
          if ($now->diffInDays($expire) > 0)
          {
            return $now->diffInDays($expire);
          }
        } elseif($this->status == '12' AND $persidangan->first()->putusan == '1' AND $persidangan->first()->P44 == '0') {
            $expire = $status_date->addDays(2);
            if ($now->diffInDays($expire) > 0)
            {
              return $now->diffInDays($expire);
            }
        } else {
          return "0";
        }
    }

    public function getTimeReminderAttribute()
    {
        $now = Carbon::now();
        $terima_spdp = new Carbon($this->spdp_diterima);        
        $status_date = new Carbon($this->tanggal_status);

        $persidangan = \App\Court::where('caase_id', $this->id)->get();

        if($this->status == '1') 
        {
          $expire = $terima_spdp->addDays(30);
          return $expire->toDateString();
        } elseif($this->status == '2') {
            $expire = $status_date->addDays(30);
            return $expire->toDateString();
        } elseif($this->status == '3') {
            $expire = $status_date->addDays(30);
            return $expire->toDateString();
        } elseif($this->status == '4') {
            $expire = $status_date->addDays(30);
            return $expire->toDateString();
        } elseif($this->status == '5') {
            $expire = $status_date->addDays(30);
            return $expire->toDateString();
        } elseif($this->status == '6') {
            $expire = $status_date->addDays(14);
            return $expire->toDateString();
        } elseif($this->status == '7') {
            $expire = $status_date->addDays(30);
            return $expire->toDateString();
        } elseif($this->status == '10' AND $this->type_id == '2') {
            $expire = $status_date->addDays(10);
            return $expire->toDateString();
        } elseif($this->status == '10' AND $this->type_id == '1') {
            $expire = $status_date->addDays(3);
            return $expire->toDateString();
        } elseif($this->status == '11') {
            $expire = $status_date->addDays(30);
            return $expire->toDateString();
        } elseif($this->status == '12' AND $persidangan->first()->putusan == '1' AND $persidangan->first()->P44 == '0') {
            $expire = $status_date->addDays(2);
            return $expire->toDateString();
        } else {
          return "2018-12-31";
        }
    }

    public function getTimeRingingAttribute()
    {
        $now = Carbon::now();
        $terima_spdp = new Carbon($this->spdp_diterima);        
        $status_date = new Carbon($this->tanggal_status);

        $persidangan = \App\Court::where('caase_id', $this->id)->get();

        if($this->status == '1') 
        {
          return '10';
        } elseif($this->status == '2') {
          return '10';
        } elseif($this->status == '3') {
          return '10';
        } elseif($this->status == '4') {
          return '10';
        } elseif($this->status == '5') {
          return '10';
        } elseif($this->status == '6') {
          return '7';
        } elseif($this->status == '7') {
          return '10';
        } elseif($this->status == '10' AND $this->type_id == '2') {
          return '5';
        } elseif($this->status == '10' AND $this->type_id == '1') {
          return '2';
        } elseif($this->status == '11') {
          return '10';
        } elseif($this->status == '12' AND $persidangan->first()->putusan == '1' AND $persidangan->first()->P44 == '0') {
          return '1';
        } else {
          return "100";
        }
    }

    public function getTimeStartAttribute()
    {
        $now = Carbon::now();
        $terima_spdp = new Carbon($this->spdp_diterima);        
        $status_date = new Carbon($this->tanggal_status);

        $persidangan = \App\Court::where('caase_id', $this->id)->get();

        if($this->status == '1') 
        {
          $expire = $terima_spdp->addDays(20);
          return $expire->toDateString();
        } elseif($this->status == '2') {
            $expire = $status_date->addDays(20);
            return $expire->toDateString();
        } elseif($this->status == '3') {
            $expire = $status_date->addDays(20);
            return $expire->toDateString();
        } elseif($this->status == '4') {
            $expire = $status_date->addDays(20);
            return $expire->toDateString();
        } elseif($this->status == '5') {
            $expire = $status_date->addDays(20);
            return $expire->toDateString();
        } elseif($this->status == '6') {
            $expire = $status_date->addDays(7);
            return $expire->toDateString();
        } elseif($this->status == '7') {
            $expire = $status_date->addDays(20);
            return $expire->toDateString();
        } elseif($this->status == '10' AND $this->type_id == '2') {
            $expire = $status_date->addDays(5);
            return $expire->toDateString();
        } elseif($this->status == '10' AND $this->type_id == '1') {
            $expire = $status_date->addDays(1);
            return $expire->toDateString();
        } elseif($this->status == '11') {
            $expire = $status_date->addDays(20);
            return $expire->toDateString();
        } elseif($this->status == '12' AND $persidangan->first()->putusan == '1' AND $persidangan->first()->P44 == '0') {
            $expire = $status_date->addDays(1);
            return $expire->toDateString();
        } else {
          return "2018-12-31";
        }
    }
    
    /*public function peneliti()
    {
      return $this->belongsTo('App\Employee', 'jaksa_peneliti', 'id');
    }

    public function berkas()
    {
      return $this->belongsTo('App\Employee', 'jaksa_berkas', 'id');
    } */

/*    public function users()
    {
        return $this->belongsToMany('App\User');
    } */
}
