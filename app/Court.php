<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Court extends Model
{
    protected $fillable = [
        'caase_id', 
        'nomor', 
        'tanggal', 
        'keterangan',
        'dakwaan',
        'p37',
        'p38',
        'p42',
        'putusan',
        'P44',
        'banding',
        'kasasi'
    ];

    public function caase()
    {
        return $this->belongsTo('App\Caase');
    }

    public function getDakwaansAttribute()
    {
      if ($this->dakwaan == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
          return '<a href="'.url('updakwaan', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus37Attribute()
    {
      if ($this->p37 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
            return '<a href="'.url('up37', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus38Attribute()
    {
      if ($this->p38 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
            return '<a href="'.url('up38', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus42Attribute()
    {
      if ($this->p42 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
            return '<a href="'.url('up42', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatusPutusanAttribute()
    {
      if ($this->putusan == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
            return '<a href="'.url('upputusan', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus44Attribute()
    {
      if ($this->P44 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
            return '<a href="'.url('up44', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatusBandingAttribute()
    {
      if ($this->banding == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
            return '<a href="'.url('upbanding', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatusKasasiAttribute($id)
    {
      if ($this->kasasi == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
          return '<a href="'.url('upkasasi', $this->id).'" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getTimeLeftAttribute()
    {
        $now = Carbon::now();
        $counter = new Carbon($this->updated_at);        

        if($this->putusan == '1' AND $this->P44 == '0')
        {
            $expire = $counter->addDays(2);
  
            if ($now->diffInDays($expire) > 0)
            {
                return '<span class="badge badge-pill badge-danger"><strong>'.$now->diffInDays($expire)." Hari Lagi</strong></span>";
            }   
        } else {
            return '<span class="badge badge-pill badge-success"><strong>Tidak Counter</strong></span>';
        }
    }
}
