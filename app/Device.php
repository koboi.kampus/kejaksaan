<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['player_id', 'user_id'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
