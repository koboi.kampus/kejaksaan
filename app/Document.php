<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['caase_id', 'nomor', 'tanggal', 'keterangan'];

    public function caase()
    {
      return $this->belongsTo('App\Caase');
    }
}
