<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Execution extends Model
{
    protected $fillable = [
        'caase_id', 
        'nomor', 
        'tanggal', 
        'keterangan', 
        'ba17', 
        'ba20', 
        'ba23'
    ];

    public function caase()
    {
        return $this->belongsTo('App\Caase');
    }
}
