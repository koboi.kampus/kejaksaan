<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Handover extends Model
{
    protected $fillable = [
        'caase_id', 
        'nomor', 
        'tanggal', 
        'keterangan', 
        'p29', 
        'p31', 
        'p33', 
        'p34'
    ];

    public function caase()
    {
        return $this->belongsTo('App\Caase');
    }

    public function getStatus29Attribute()
    {
      if ($this->p29 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
          return '<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus31Attribute()
    {
      if ($this->p31 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
          return '<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus33Attribute()
    {
      if ($this->p33 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
          return '<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }

    public function getStatus34Attribute()
    {
      if ($this->p34 == '1') {
        return '<a href="#" class="btn btn-warning btn-xs"><i class="fa fa-check-circle"></i> </a>';
      } else {
          return '<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </a>';
      }
    }
}
