<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Device;
use App\Profile;

class DeviceController extends Controller
{
    public function store(Request $request)
    {
        $user = Auth::user()->id;
        $playerId = $request->playerId;

        $profile = Profile::where('user_id', $user)->get();

        //return $profile->first()->id;

        $device = Device::firstOrCreate(
          [
            'player_id' => $playerId,
            'user_id' => $profile->first()->id
          ]
        );

        return $device;
    }
}
