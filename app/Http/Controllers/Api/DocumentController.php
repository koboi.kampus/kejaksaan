<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Document;
use App\Caase;

class DocumentController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $doc = new Document([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $doc->save();

        $now = date('Y-m-d');

        $case->update(['status' => '6', 'tanggal_status' => $request->tanggal ]);

        return $doc;

    }
}
