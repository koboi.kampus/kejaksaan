<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Eighteen;

class EighteenController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $p18 = new Eighteen([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p18->save();

        $now = date('Y-m-d');

        $case->update(['status' => '3', 'tanggal_status' => $request->tanggal ]);

        return $p18;
    }
}
