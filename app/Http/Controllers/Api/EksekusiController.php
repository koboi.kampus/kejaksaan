<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Execution;

class EksekusiController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $eksekusi = new Execution([
            'caase_id' => $id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $eksekusi->save();

        $now = date('Y-m-d');

        $case->update(['status' => '20', 'tanggal_status' => $request->tanggal ]);

        return $eksekusi;
    }
}
