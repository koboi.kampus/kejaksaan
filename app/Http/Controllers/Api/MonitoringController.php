<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Caase;

use App\Http\Resources\ApiMonitoringResource;

class MonitoringController extends Controller
{
    public function index()
    {
      $monitor = Caase::all();

      return ApiMonitoringResource::collection($monitor); 
    }
}
