<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Handover;

class PelimpahanController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $pelimpahan = new Handover([
            'caase_id' => $id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan,
            'p29' => '1',
            'p31' => '1',
            'p33' => '1',
            'p34' => '1' 
        ]);

        $pelimpahan->save();

        $now = date('Y-m-d');

        $case->update(['status' => '11', 'tanggal_status' => $request->tanggal ]);

        return $pelimpahan;
    }

}
