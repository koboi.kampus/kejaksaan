<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Court;

class PersidanganController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $persidangan = new Court([
            'caase_id' => $id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $persidangan->save();

        $now = date('Y-m-d');

        $case->update(['status' => '12', 'tanggal_status' => $request->tanggal ]);

        return $persidangan;
    }

    public function updatedakwaan($id)
    {
        $dakwaan = Court::where('caase_id', $id);
        $dakwaan->update(['dakwaan' => '1']);

        return "Update Status Berhasil";
    }

    public function update37($id)
    {
        $p37 = Court::where('caase_id', $id);
        $p37->update(['p37' => '1']);

        return "Update Status Berhasil";
    }

    public function update38($id)
    {
        $p38 = Court::where('caase_id', $id);
        $p38->update(['p38' => '1']);

        return "Update Status Berhasil";
    }

    public function update42($id)
    {
        $p42 = Court::where('caase_id', $id);
        $p42->update(['p42' => '1']);

        return "Update Status Berhasil";
    }

    public function updateputusan($id)
    {
        $putusan = Court::where('caase_id', $id);
        $putusan->update(['putusan' => '1']);

        return "Update Status Berhasil";
    }

    public function update44($id)
    {
        $p44 = Court::where('caase_id', $id);
        $p44->update(['P44' => '1']);

        return "Update Status Berhasil";
    }

    public function updatebanding($id)
    {
        $banding = Court::where('caase_id', $id);
        $banding->update(['banding' => '1']);

        return "Update Status Berhasil";
    }

    public function updatekasasi($id)
    {
        $kasasi = Court::where('caase_id', $id);
        $kasasi->update(['kasasi' => '1']);

        return "Update Status Berhasil";
    }

}
