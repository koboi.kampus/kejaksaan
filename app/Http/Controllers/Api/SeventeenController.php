<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Seventeen;

class SeventeenController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $p17 = new Seventeen([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p17->save();

        $now = date('Y-m-d');

        $case->update(['status' => '2', 'tanggal_status' => $request->tanggal ]);

        return $p17;

    }
}
