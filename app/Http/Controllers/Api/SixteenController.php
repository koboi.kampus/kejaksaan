<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Profile;
use App\Caase;

use App\Http\Resources\CaaseResource;
use App\Http\Resources\ApiMonitoringResource;
use Auth;

class SixteenController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->id;

        $sixteen = Profile::where('user_id', $userId)->get();

        $six = CaaseResource::collection($sixteen);

        return $six;
    }

    public function detil($id)
    {
        $caase = Caase::find($id);

        //return ApiMonitoringResource::collection($caase);
        return new ApiMonitoringResource($caase);
    }
}
