<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Profile;
use App\Caase;
use App\Sixtena;

use App\Http\Resources\CaasaResource;
use App\Http\Resources\ApiMonitoringaDetilResource;
//use App\Http\Resources\ApiMonitoringResource;
use Auth;

class SixtenaController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->id;

        $sixteena = Profile::with('caases')->where('user_id', $userId)->get();

        return CaasaResource::collection($sixteena);
    }

    public function detil($id)
    {
        //$sixtena = Sixtena::find($id);
        $caase = Caase::find($id);

        return new ApiMonitoringaDetilResource($caase);
    }
}
