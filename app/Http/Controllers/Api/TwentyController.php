<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Twenty;

class TwentyController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $p20 = new Twenty([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p20->save();

        $now = date('Y-m-d');

        $case->update(['status' => '4', 'tanggal_status' => $request->tanggal ]);

        return $p20;

    }
}
