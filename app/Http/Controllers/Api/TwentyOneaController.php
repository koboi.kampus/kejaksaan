<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\TwentyoneA;

class TwentyOneaController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $p21a = new TwentyoneA([
            'caase_id' => $id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p21a->save();

        $now = date('Y-m-d');

        $case->update(['status' => '7', 'tanggal_status' => $request->tanggal ]);

        return $p21a;
    }
}
