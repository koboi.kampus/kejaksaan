<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Caase;
use App\Twentyone;

class TwentyoneController extends Controller
{
    public function store(Request $request, $id)
    {
        $case = Caase::find($id);

        $p21 = new Twentyone([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p21->save();

        $now = date('Y-m-d');

        $case->update(['status' => '5', 'tanggal_status' => $request->tanggal ]);

        return $p21;

    }
}
