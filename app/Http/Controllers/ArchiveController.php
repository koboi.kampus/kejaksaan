<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
Use App\Profile;
use App\Archive;

class ArchiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '21')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.arsip.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);

        return view('modules.arsip.create', compact('case'));
    }

    public function store(Request $request, $id)
    {

        $case = Caase::find($id);

        $arsip = new Archive([
            'caase_id' => $id,
            'dictum' => $request->dictum,
            'tanggal_dictum' => $request->tanggal_dictum,
            'dictum_pidana' => $request->dictum_pidana,
            'dictum_barangbukti' => $request->dictum_barangbukti,
            'nomor_putusan' => $request->nomor_putusan,
            'tanggal_putusan' => $request->tanggal_putusan,
            'amar_pidana' => $request->amar_pidana,
            'amar_barangbukti' => $request->amar_barangbukti,
            'sikap_jpu' => $request->sikap_jpu,
            'sikap_terdakwa' => $request->sikap_terdakwa,
            'eksekusi_tuntas' => $request->eksekusi_tuntas,
            'bp_arsip' => $request->bp_arsip,
            'kasus_posisi' => $request->kasus_posisi,
            'keterangan' => $request->keterangan
        ]);

        $arsip->save();

        $now = date('Y-m-d');

        $case->update(['status' => '21', 'tanggal_status' => $now, 'keterangan' => $request->kasus_posisi ]);

        return redirect(url('/arsip'));
    }
}
