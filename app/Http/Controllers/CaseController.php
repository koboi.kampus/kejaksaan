<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case = \App\Caase::where('status', '!=', '0')->get();  
       
        return view('modules.cases.index', compact('case'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = \App\Type::all();
        $klas = \App\Classification::all();
        $suspect = \App\Suspect::all();
        $law = \App\Lawbook::all();
        $inv = \App\Investigator::all();
        $emp = \App\Employee::all();

        return view('modules.cases.create', compact('type', 'klas', 'suspect', 'law', 'inv', 'emp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $case = new \App\Caase;

        $case->type_id = $request->get('perkara');
        $case->classification_id = $request->get('klasifikasi');
        $case->nomor_spdp = $request->get('spdp');
        $case->tanggal_spdp = $request->get('tanggal_spdp');
        $case->masuk_spdp = $request->get('masuk_spdp');
        $case->suspect_id = $request->get('tersangka');
        $case->lawbook_id = $request->get('undang');
        $case->pasal= $request->get('pasal');
        $case->investigator_id = $request->get('penyidik');
        $case->jaksa_peneliti = $request->get('jp');
        $case->jaksa_berkas= $request->get('jb');

        $case->save();

        return redirect()->route('case.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
