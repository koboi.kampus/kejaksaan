<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
Use App\Profile;
use App\Document;


class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '6')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        //return $case;

        return view('modules.progress.bp.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);

        //return $case;

        return view('modules.progress.bp.create', compact('case'));
    }

    public function store(Request $request, $id)
    {

        $case = Caase::find($id);

        $doc = new Document([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'diterima' => $request->diterima,
            'keterangan' => $request->keterangan
        ]);

        $doc->save();

        $now = date('Y-m-d');

        $case->update(['status' => '6', 'tanggal_status' => $request->diterima, 'keterangan' => $request->keterangan ]);

        return redirect(url('/bp'));
    }

    public function spesifik()
    {
        $userId = Auth::user()->id;
        $profile = Profile::where('user_id', $userId)->get();

        $document= Profile::with('caases')->where('id', $profile->first()->id)->get();
        
        return view('modules.progress.jaksa.document.index', compact('document'))->with('i');
    }
}
