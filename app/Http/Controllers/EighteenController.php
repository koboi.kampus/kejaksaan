<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
Use App\Profile;
use App\Eighteen;


class EighteenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '3')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.p18.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);

        //return $case;

        return view('modules.progress.p18.create', compact('case'));
    }

    public function store(Request $request, $id)
    {

        $case = Caase::find($id);

        $p18 = new Eighteen([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p18->save();

        $now = date('Y-m-d');

        $case->update(['status' => '3', 'tanggal_status' => $request->tanggal, 'keterangan' => $request->keterangan]);

        return redirect(url('/p18'));
    }

    public function spesifik()
    {
        $userId = Auth::user()->id;
        $profile = Profile::where('user_id', $userId)->get();

        $eighteen = Profile::with('caases')->where('id', $profile->first()->id)->get();
        
        return view('modules.progress.jaksa.p18.index', compact('eighteen'))->with('i');
    }
}
