<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
use App\Execution;
use App\Profile;
use App\Device;

class EksekusiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '20')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.eksekusi.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);
        $profile = \App\Profile::all();

        //return $case;

        return view('modules.progress.eksekusi.create', compact('case', 'profile'));
    }

    public function store(Request $request, $id)
    {
        $validateData = $request->validate([
            'nomor_surat' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required'
        ]);

        $case = Caase::find($id);

        if($request->ba17 == 'on') {
            $ba17 = "1";
        } else {
            $ba17 = "0";
        }

        if($request->ba20 == 'on') {
            $ba20 = "1";
        } else {
            $ba20 = "0";
        }

        if($request->ba23 == 'on') {
            $ba23 = "1";
        } else {
            $ba23 = "0";
        }

        $eksekusi = new Execution([
            'caase_id' => $id,
            'nomor' => $request->nomor_surat,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan,
            'ba17' => $ba17,
            'ba20' => $ba20,
            'ba23' => $ba23,
        ]);

        $eksekusi->save();

        $now = date('Y-m-d');

        $case->update(['status' => '20', 'tanggal_status' => $request->tanggal ]);
    
        //$subset = [ "1" ];

        /*foreach($subset as $key=>$c) {
            $device = Device::where('user_id', $subset[$key])->get();

            foreach($device as $key=>$dev) {
                //$de[] = $datae[$key]['playerId'];
                //$de[] = 'Kirim Notifikasi ke '.$datae[$key]['playerId'];
                $notif[] = \OneSignal::sendNotificationToUser(
                    "Saudara ditunjuk sebagai JPU untuk Perkara....",
                    $device[$key]['player_id'], 
                    $url = null, 
                    $data = null, 
                    $buttons = null, 
                    $schedule = null
                );
            }

        } */

        return redirect(url('/eksekusi'));
    }
}
