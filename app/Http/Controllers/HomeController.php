<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Caase;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalSPDP = Caase::all()->count();
        $totaltpul = Caase::where('classification_id', '1')->count();
        $totaltibum = Caase::where('classification_id', '2')->count();
        $totaloharda = Caase::where('classification_id', '3')->count();

        $tpra = Caase::whereBetween('status', array(0, 8))->count();
        $tpenuntutan = Caase::whereBetween('status', array(9, 14))->count();
        $teksekusi = Caase::whereBetween('status', array(15, 20))->count();

        $pra = ($tpra/$totalSPDP)*100;
        $penuntutan = ($tpenuntutan/$totalSPDP)*100;
        $eksekusi = ($teksekusi/$totalSPDP)*100;

        return view('home', compact(
            'totalSPDP', 
            'totaltpul', 
            'totaltibum', 
            'totaloharda',
            'pra',
            'penuntutan',
            'eksekusi'
        ));
    }
}
