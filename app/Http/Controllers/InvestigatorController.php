<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investigator;

class InvestigatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->user()->authorizeRoles(['admin', 'operator', 'kajari', 'kasipidum']);
        
        $investigator = Investigator::all();

        return view('modules.investigator.index', compact('investigator'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.investigator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Disini Validasi
         $validateData = $request->validate([
            'nip' => 'required|unique:investigators',
            'nama' => 'required',
            'instansi' => 'required',
            'phone' => 'required'
        ]);            
       
        // Disini Insert ke Database
        
        /*$jenis = new Type([
            'perkara' => $request->perkara,
            'deskripsi' => $request->deskripsi
          ]);

        $jenis->save(); */

        Investigator::create($request->all());

        return redirect()->route('investigator.index')
          ->with(['success' => 'Data Berhasil disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Investigator $investigator)
    {
        return view('modules.investigator.edit', compact('investigator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Investigator $investigator)
    {
        // Validasi disini
        $investigator->update($request->all());
        
        return redirect()->route('investigator.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Investigator $investigator)
    {
        $investigator->delete();
      
        return redirect()->route('investigator.index');
    }
}
