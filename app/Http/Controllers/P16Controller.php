<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
use App\Sixteen;
use App\Profile;
use App\Device;

class P16Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '1')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.p16.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);
        $profile = \App\Profile::all();

        //return $case;

        return view('modules.progress.p16.create', compact('case', 'profile'));
    }

    public function store(Request $request, $id)
    {
        $validateData = $request->validate([
            'nomor_surat' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required',
            //'profile' => 'required',
        ]);

        //$jaksa = $request->input('profile');
        $jaksa = [
            $request->jaksa1,
            $request->jaksa2
        ];

        $profile = Profile::find($jaksa);

        /*$subset = $profile->map(function ($user) {
            return collect($user->toArray())
                ->only(['user_id'])
                ->all();
        }); */
        
        $case = Caase::find($id);

        $p16 = new Sixteen([
            'caase_id' => $case->id,
            'nomor_surat' => $request->nomor_surat,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p16->save();

        $p16->profiles()->sync($jaksa);
        $case->profiles()->sync($jaksa);

        $now = date('Y-m-d');

        $case->update(['status' => '1', 'tanggal_status' => $now, 'keterangan' => $request->keterangan ]);
    
        //$subset = [ "1" ];

        foreach($jaksa as $key=>$c) {
            $device = Device::where('user_id', $jaksa[$key])->get();

            foreach($device as $key=>$dev) {
                //$de[] = $datae[$key]['playerId'];
                //$de[] = 'Kirim Notifikasi ke '.$datae[$key]['playerId'];
                $notif[] = \OneSignal::sendNotificationToUser(
                            "P-16 : Anda ditunjuk sebagai Jaksa SPDP No. " .$case->nomor_spdp,
                            $device[$key]['player_id'], 
                            $url = null, 
                            $data = null, 
                            $buttons = null, 
                            $schedule = null
                        );
            }

        }

        return redirect(url('/p16'));
    }

    public function spesifik()
    {
        $userId = Auth::user()->id;
        $profile = Profile::where('user_id', $userId)->get();

        $sixteen = Profile::with('sixteens')->where('id', $profile->first()->id)->get();
        
        return view('modules.progress.jaksa.p16.index', compact('sixteen'))->with('i');
    }
}
