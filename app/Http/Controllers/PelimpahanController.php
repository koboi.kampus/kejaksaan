<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
use App\Handover;
use App\Profile;
use App\Device;

class PelimpahanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '11')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.pelimpahan.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);
        $profile = \App\Profile::all();

        //return $case;

        return view('modules.progress.pelimpahan.create', compact('case', 'profile'));
    }

    public function store(Request $request, $id)
    {
        $validateData = $request->validate([
            'nomor_surat' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required'
        ]);

        $case = Caase::find($id);

        if($request->p29 == 'on') {
            $p29 = "1";
        } else {
            $p29 = "0";
        }

        if($request->p31 == 'on') {
            $p31 = "1";
        } else {
            $p31 = "0";
        }

        if($request->p33 == 'on') {
            $p33 = "1";
        } else {
            $p33 = "0";
        }

        if($request->p34 == 'on') {
            $p34 = "1";
        } else {
            $p34 = "0";
        }

        $pelimpahan = new Handover([
            'caase_id' => $id,
            'nomor' => $request->nomor_surat,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan,
            'p29' => $p29,
            'p31' => $p31,
            'p33' => $p33,
            'p34' => $p34,
        ]);

        $pelimpahan->save();

        $now = date('Y-m-d');

        $case->update(['status' => '11', 'tanggal_status' => $request->tanggal, 'keterangan' => $request->keterangan ]);
    
        //$subset = [ "1" ];

        /*foreach($subset as $key=>$c) {
            $device = Device::where('user_id', $subset[$key])->get();

            foreach($device as $key=>$dev) {
                //$de[] = $datae[$key]['playerId'];
                //$de[] = 'Kirim Notifikasi ke '.$datae[$key]['playerId'];
                $notif[] = \OneSignal::sendNotificationToUser(
                    "Saudara ditunjuk sebagai JPU untuk Perkara....",
                    $device[$key]['player_id'], 
                    $url = null, 
                    $data = null, 
                    $buttons = null, 
                    $schedule = null
                );
            }

        } */

        return redirect(url('/pelimpahan'));
    }
}
