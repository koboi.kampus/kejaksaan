<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
use App\Court;
use App\Profile;
use App\Device;

class PersidanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '12')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.persidangan.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);
        $profile = \App\Profile::all();

        //return $case;

        return view('modules.progress.persidangan.create', compact('case', 'profile'));
    }

    public function store(Request $request, $id)
    {
        $validateData = $request->validate([
            'nomor_surat' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required'
        ]);

        /*$subset = $profile->map(function ($user) {
            return collect($user->toArray())
                ->only(['user_id'])
                ->all();
        }); */
        
        $case = Caase::find($id);

        if($request->dakwaan == 'on') {
            $dakwaan = "1";
        } else {
            $dakwaan = "0";
        }

        if($request->p37 == 'on') {
            $p37 = "1";
        } else {
            $p37 = "0";
        }

        if($request->p38 == 'on') {
            $p38 = "1";
        } else {
            $p38 = "0";
        }

        if($request->p42 == 'on') {
            $p42 = "1";
        } else {
            $p42 = "0";
        }

        if($request->putusan == 'on') {
            $putusan = "1";
        } else {
            $putusan = "0";
        }

        if($request->p44 == 'on') {
            $p44 = "1";
        } else {
            $p44 = "0";
        }

        if($request->banding == 'on') {
            $banding = "1";
        } else {
            $banding = "0";
        }

        if($request->kasasi == 'on') {
            $kasasi = "1";
        } else {
            $kasasi = "0";
        }

        $persidangan = new Court([
            'caase_id' => $case->id,
            'nomor' => $request->nomor_surat,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan,
            'dakwaan' => $dakwaan,
            'p37' => $p37,
            'p38' => $p38,
            'p42' => $p42,
            'putusan' => $putusan,
            'P44' => $p44,
            'banding' => $banding,
            'kasasi' => $kasasi

        ]);

        $persidangan->save();

        $now = date('Y-m-d');

        $case->update(['status' => '12', 'tanggal_status' => $request->tanggal, 'keterangan' => $request->keterangan ]);
    
        //$subset = [ "1" ];

        /*foreach($subset as $key=>$c) {
            $device = Device::where('user_id', $subset[$key])->get();

            foreach($device as $key=>$dev) {
                //$de[] = $datae[$key]['playerId'];
                //$de[] = 'Kirim Notifikasi ke '.$datae[$key]['playerId'];
                $notif[] = \OneSignal::sendNotificationToUser(
                    "Saudara ditunjuk sebagai JPU untuk Perkara....",
                    $device[$key]['player_id'], 
                    $url = null, 
                    $data = null, 
                    $buttons = null, 
                    $schedule = null
                );
            }

        } */

        return redirect(url('/persidangan'));
    }

    public function updatedakwaan($id)
    {
        $dakwaan = Court::find($id);
        $dakwaan->update(['dakwaan' => '1']);

        return redirect(url('/persidangan'));
    }

    public function update37($id)
    {
        $p37 = Court::find($id);
        $p37->update(['p37' => '1']);

        return redirect(url('/persidangan'));
    }

    public function update38($id)
    {
        $p38 = Court::find($id);
        $p38->update(['p38' => '1']);

        return redirect(url('/persidangan'));
    }

    public function update42($id)
    {
        $p42 = Court::find($id);
        $p42->update(['p42' => '1']);

        return redirect(url('/persidangan'));
    }

    public function updateputusan($id)
    {
        $putusan = Court::find($id);
        $putusan->update(['putusan' => '1']);

        return redirect(url('/persidangan'));
    }

    public function update44($id)
    {
        $p44 = Court::find($id);
        $p44->update(['P44' => '1']);

        return redirect(url('/persidangan'));
    }

    public function updatebanding($id)
    {
        $banding = Court::find($id);
        $banding->update(['banding' => '1']);

        return redirect(url('/persidangan'));
    }

    public function updatekasasi($id)
    {
        $kasasi = Court::find($id);
        $kasasi->update(['kasasi' => '1']);

        return redirect(url('/persidangan'));
    }
}
