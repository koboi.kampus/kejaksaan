<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Http\Resources\ProfileResource;
use Auth;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::all();

        return view('modules.profile.index', compact('profile'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = \App\Level::all();
        $position = \App\Position::all();

        return view('modules.profile.create', compact('level', 'position'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Disini Validasi
        
        // Disini Insert ke Database
        
        /*$jenis = new Type([
            'perkara' => $request->perkara,
            'deskripsi' => $request->deskripsi
          ]);

        $jenis->save(); */

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->saveFile($request->file('photo'));
        }

        //Profile::create($request->all());
        $profile = Profile::create(
            [
                'nip' => $request->nip, 
                'nama' => $request->nama, 
                'level_id' => $request->level_id,
                'position_id' => $request->position_id,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'kelamin' => $request->kelamin,
                'alamat' => $request->alamat, 
                'phone' => $request->phone,  
                'user_id' => $request->userId
                ]
            );

        return redirect()->route('profile.index')
          ->with(['success' => 'Data Berhasil disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        $level = \App\Level::all();
        $position = \App\Position::all();

        return view('modules.profile.edit', compact('profile', 'level', 'position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        // Validasi disini

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->saveFile($request->file('photo'));
            if ($profile->photo !== '') $this->deletePhoto($profile->photo);

            $profile->update($data);
        }
        //$profile->update($request->all());
        $profiles = Profile::where('id', $profile->id)
            ->update(
                [
                    'nip' => $request->nip, 
                    'nama' => $request->nama, 
                    'level_id' => $request->level_id,
                    'position_id' => $request->position_id,
                    'tempat_lahir' => $request->tempat_lahir,
                    'tanggal_lahir' => $request->tanggal_lahir,
                    'kelamin' => $request->kelamin,
                    'alamat' => $request->alamat, 
                    'phone' => $request->phone,  
                    //'user_id' => $request->userId
                    ]
                );
        
        return redirect()->route('profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();
      
        return redirect()->route('profile.index');
    }

    public function profile()
    {
        $userId = Auth::user()->id;
        $profile = Profile::with('user')->where('user_id', $userId)->get();

        return ProfileResource::collection($profile);
    }

    public function saveFile(UploadedFile $file)
    {
        $fileName = str_random(40) . '.' . $file->guessClientExtension(); 
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/photo';
        $file->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img/photo' 
            . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

}
