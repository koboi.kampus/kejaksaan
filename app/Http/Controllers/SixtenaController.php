<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
use App\Sixtena;
use App\Profile;
use App\Device;

class SixtenaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '10')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.p16a.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);
        $profile = \App\Profile::all();

        //return $case;

        return view('modules.progress.p16a.create', compact('case', 'profile'));
    }

    public function store(Request $request, $id)
    {
        $validateData = $request->validate([
            'nomor_surat' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required',
            'profile' => 'required',
        ]);

        $jaksa = $request->input('profile');
        $profile = Profile::find($jaksa);
        
        $case = Caase::find($id);

        $p16a = new Sixtena([
            'caase_id' => $id,
            'nomor_surat' => $request->nomor_surat,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p16a->save();

        $p16a->profiles()->sync($jaksa);
        //$case->profiles()->sync($jaksa);

        $now = date('Y-m-d');

        $case->update(['status' => '10', 'tanggal_status' => $request->tanggal, 'keterangan' => $request->keterangan ]);
    
        foreach($jaksa as $key=>$c) {
            $device = Device::where('user_id', $jaksa[$key])->get();

            foreach($device as $key=>$dev) {
                //$de[] = $datae[$key]['playerId'];
                //$de[] = 'Kirim Notifikasi ke '.$datae[$key]['playerId'];
                $notif[] = \OneSignal::sendNotificationToUser(
                            "P-16.A : Anda ditunjuk sebagai Jaksa SPDP No. " .$case->nomor_spdp,
                            $device[$key]['player_id'], 
                            $url = null, 
                            $data = null, 
                            $buttons = null, 
                            $schedule = null
                        );
            }

        }

        return redirect(url('/p16a'));
    }

}
