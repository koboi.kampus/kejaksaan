<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Caase;

use App\Role;
use App\Device;

class SpdpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spdp = Caase::where('status', '0')
            ->orderBy('spdp_diterima', 'desc')
            ->get();
        
        return view('modules.spdp.index', compact('spdp'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = \App\Type::all();
        $classification = \App\Classification::all();
        $investigator = \App\Investigator::all();
        $suspect = \App\Suspect::all();

        return view('modules.spdp.create', compact('type', 'classification', 'investigator', 'suspect'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	// Disini Validasi
	
        $validateData = $request->validate([
            'type_id' => 'required',
            'classification_id' => 'required',
            'nomor_spdp' => 'required',
            'tanggal_spdp' => 'required',
            'spdp_diterima' => 'required',
            'pelanggaran' => 'required',
          //  'keterangan' => 'required',
            'investigator' => 'required',
            'suspect' => 'required'
        ]);          
        // Disini Insert ke Database
        
        /*$jenis = new Type([
            'perkara' => $request->perkara,
            'deskripsi' => $request->deskripsi
          ]);

        $jenis->save(); */

        $investigators = $request->input('investigator');
        $suspects = $request->input('suspect');

        //$spdp = Caase::create($request->all());
        $spdp = new Caase([
            'type_id' => $request->type_id, 
            'classification_id' => $request->classification_id,
            'nomor_spdp' => $request->nomor_spdp,
            'tanggal_spdp' => $request->tanggal_spdp,
            'spdp_diterima' => $request->spdp_diterima,
            'case_file_id' => $request->case_file_id,
            'keterangan' => $request->keterangan,
            'pelanggaran' => $request->pelanggaran,
            'status' => 0
        ]);
        $spdp->save();

        $case = Caase::find($spdp->id);
        
        $case->investigators()->sync($investigators);
        $case->suspects()->sync($suspects);

        /*$role = Role::find('4');

        $userId = $role->users->first()->id;

        $device = Device::where('user_id', $userId)->get();

        if($device->count() > 0) {

            $player_id = $device->first()->player_id;

            \OneSignal::sendNotificationToUser(
                "Ini Spesifik Notif dari Laravel, Jika SPDP di entri.....", 
                $player_id, 
                $url = null, 
                $data = null, 
                $buttons = null, 
                $schedule = null
            );
        } */

        return redirect()->route('spdp.index')
          ->with(['success' => 'Data Berhasil disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Caase $spdp)
    {
        $type = \App\Type::all();
        $classification = \App\Classification::all();
        $investigator = \App\Investigator::all();
        $suspect = \App\Suspect::all();

        return view('modules.spdp.edit', compact('spdp', 'type', 'classification', 'investigator', 'suspect'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Caase $spdp)
    {
        // Validasi disini
        $spdp->update($request->all());
        
        return redirect()->route('spdp.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Caase $spdp)
    {
        $spdp->delete();
      
        return redirect()->route('spdp.index');
    }
}
