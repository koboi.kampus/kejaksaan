<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suspect;

class SuspectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->user()->authorizeRoles(['admin','operator', 'kajari', 'kasipidum']);
        
        $suspect = Suspect::all();

        return view('modules.suspect.index', compact('suspect'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.suspect.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Disini Validasi
         $validateData = $request->validate([
            'nama' => 'required|unique:suspects',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'kelamin' => 'required',
            'alamat' => 'required'
        ]);            
       
        // Disini Insert ke Database
        
        /*$jenis = new Type([
            'perkara' => $request->perkara,
            'deskripsi' => $request->deskripsi
          ]);

        $jenis->save(); */

      Suspect::create($request->all());

        return redirect()->route('suspect.index')
          ->with(['success' => 'Data Berhasil disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Suspect $suspect)
    {
        return view('modules.suspect.edit', compact('suspect'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suspect $suspect)
    {
        // Validasi disini
        $suspect->update($request->all());
        
        return redirect()->route('suspect.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suspect $suspect)
    {
        $suspect->delete();
      
        return redirect()->route('suspect.index');
    }
}
