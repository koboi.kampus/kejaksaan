<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
Use App\Profile;
use App\Twenty;

class TwentyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '4')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.p20.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);

        //return $case;

        return view('modules.progress.p20.create', compact('case'));
    }

    public function store(Request $request, $id)
    {

        $case = Caase::find($id);

        $p20 = new Twenty([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p20->save();

        $now = date('Y-m-d');

        $case->update(['status' => '4', 'tanggal_status' => $request->tanggal, 'keterangan' => $request->keterangan ]);

        return redirect(url('/p20'));
    }

    public function spesifik()
    {
        $userId = Auth::user()->id;
        $profile = Profile::where('user_id', $userId)->get();

        $twenty= Profile::with('caases')->where('id', $profile->first()->id)->get();
        
        return view('modules.progress.jaksa.p20.index', compact('twenty'))->with('i');
    }
}
