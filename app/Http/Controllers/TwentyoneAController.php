<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Caase;
Use App\Profile;
use App\TwentyoneA;

class TwentyoneAController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $case = Caase::where('status', '7')
            ->orderBy('spdp_diterima', 'desc')
            ->get();

        return view('modules.progress.p21a.index', compact('case'))->with('i');
    }

    public function create($id)
    {
        $case = Caase::find($id);

        //return $case;

        return view('modules.progress.p21a.create', compact('case'));
    }

    public function store(Request $request, $id)
    {

        $case = Caase::find($id);

        $p21a = new TwentyoneA([
            'caase_id' => $case->id,
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan
        ]);

        $p21a->save();

        $now = date('Y-m-d');

        $case->update(['status' => '7', 'tanggal_status' => $request->tanggal, 'keterangan' => $request->keterangan ]);

        return redirect(url('/p21a'));
    }

    public function spesifik()
    {
        $userId = Auth::user()->id;
        $profile = Profile::where('user_id', $userId)->get();

        $twentyone= Profile::with('caases')->where('id', $profile->first()->id)->get();
        
        return view('modules.progress.jaksa.p21.index', compact('twentyone'))->with('i');
    }
}
