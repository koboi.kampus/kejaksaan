<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\SuspectResource;
use App\Http\Resources\SixteenResource;
use App\Http\Resources\TypeResource;
use App\Http\Resources\ClassificationResource;
use App\Http\Resources\InvestigatorResource;

class ApiMonitoringResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      if($this->status < '10') {
          return [
            'id' => $this->id,
            'spdp' => $this->nomor_spdp,
            'tanggal' => $this->tanggal_spdp,
            'jenis' => $this->type->perkara,
            'klasifikasi' => $this->classification->klasifikasi,
            'pelanggaran' => $this->pelanggaran,
            'diterima' => $this->spdp_diterima,
            'file' => $this->case_file_id,
            'keterangan' => $this->keterangan,
            'penyidik' => InvestigatorResource::collection($this->investigators),
            'tersangka' => SuspectResource::collection($this->suspects),
            'jaksa' => SixteenResource::collection($this->sixteen),
            'status' => $this->StatusPerkara,
            'tanggal_status' => $this->tanggal_status,
            'expire' => $this->TimeLeft,
            'reminder' => $this->TimeReminder,
            'start_ringing' => $this->TimeStart,
            'ringing' => $this->TimeRinging,
          ];
        }
    }

}
