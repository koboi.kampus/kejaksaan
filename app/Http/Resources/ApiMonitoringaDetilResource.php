<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\SuspectResource;
use App\Http\Resources\SixteenResource;
use App\Http\Resources\TypeResource;
use App\Http\Resources\ClassificationResource;
use App\Http\Resources\InvestigatorResource;

class ApiMonitoringaDetilResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'spdp' => $this->nomor_spdp,
        'tanggal' => $this->tanggal_spdp,
        'jenis' => $this->type->perkara,
        'klasifikasi' => $this->classification->klasifikasi,
        'pelanggaran' => $this->pelanggaran,
        'diterima' => $this->spdp_diterima,
        'file' => $this->case_file_id,
        'keterangan' => $this->keterangan,
        'penyidik' => InvestigatorResource::collection($this->investigators),
        'tersangka' => SuspectResource::collection($this->suspects),
        'jaksa' => SixteenResource::collection($this->sixtena),
        'status' => $this->StatusPerkara,
        'tanggal_status' => $this->tanggal_status,
        'expire' => $this->TimeLeft,
        'persidangan' => new PersidanganResource($this->court)
      ];
    }
}
