<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\SuspectResource;
use App\Http\Resources\SixteenResource;
use App\Http\Resources\TypeResource;
use App\Http\Resources\ClassificationResource;
use App\Http\Resources\InvestigatorResource;
use App\Http\Resources\PersidanganResource;

class ApiMonitoringaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->caase->id,
        'spdp' => $this->caase->nomor_spdp,
        'tanggal' => $this->caase->tanggal_spdp,
        'jenis' => $this->caase->type->perkara,
        'klasifikasi' => $this->caase->classification->klasifikasi,
        'pelanggaran' => $this->caase->pelanggaran,
        'diterima' => $this->caase->spdp_diterima,
        'file' => $this->caase->case_file_id,
        'keterangan' => $this->caase->keterangan,
        'penyidik' => InvestigatorResource::collection($this->caase->investigators),
        'tersangka' => SuspectResource::collection($this->caase->suspects),
        'jaksa' => SixteenResource::collection($this->caase->sixtena),
        'status' => $this->caase->StatusPerkara,
        'tanggal_status' => $this->caase->tanggal_status,
        'expire' => $this->caase->TimeLeft,
        'reminder' => $this->caase->TimeReminder,
        'start_ringing' => $this->caase->TimeStart,
        'ringing' => $this->caase->TimeRinging,
        'persidangan' => new PersidanganResource($this->caase->court)
      ];
    }
}
