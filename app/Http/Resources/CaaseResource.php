<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ApiMonitoringResource;

class CaaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'spdp' =>  ApiMonitoringResource::collection($this->caaseStatus)
        ];
    }
}
