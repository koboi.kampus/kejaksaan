<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\SuspectResource;
use App\Http\Resources\SixteenResource;

class MonitoringResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'type' => $this->type_id,
        'classification' => $this->classification_id,
        'spdp' => $this->nomor_spdp.'<br><small class="text-muted"><i>Tanggal: '.$this->tanggal_spdp.'<i></small>',
        'tanggal' => $this->tanggal_spdp,
        'diterima' => "<center>".$this->spdp_diterima."</center>",
        'file' => $this->case_file_id,
        'keterangan' => $this->keterangan,
        'tersangka' => SuspectResource::collection($this->suspects),
        'jaksa' => SixteenResource::collection($this->sixteen),
        'pelanggaran' => $this->pelanggaran,
        'jaksa1' => "<div class='project-members'><a href='javascript:void(0)'><img src='img/avatars/4.png' class='busy'></a> <a href='javascript:void(0)'><img src='img/avatars/female.png' class='online'></a> <a href='javascript:void(0)'><img src='img/avatars/male.png' class='busy'></a></div></div> ",
        'status' => "<center><span class='label label-success'>".$this->StatusPerkara." </span></center>",
        'tracker' => "<span class='onoffswitch'><input type='checkbox' name='start_interval' class='onoffswitch-checkbox' id='st1' checked='checked'><label class='onoffswitch-label' for='st1'><span class='onoffswitch-inner' data-swchon-text='ON' data-swchoff-text='OFF'></span><span class='onoffswitch-switch'></span></label></span>",
        'expire' => "<center><span class='label label-danger'>".$this->TimeLeft." Hari Lagi</span></center>",
        'comments' => "This is a blank comments area, used to add comments and keep notes",
        'action' => "<button class='btn btn-xs'>Detil Perkara</button>"
       
      ];
    }
}
