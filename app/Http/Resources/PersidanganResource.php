<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersidanganResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'dakwaan' => $this->dakwaan,
            'P37' => $this->p37,
            'P38' => $this->p38,
            'P42' => $this->p42,
            'Putusan' => $this->putusan,
            'P44' => $this->P44
        ];
    }
}
