<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigator extends Model
{
    protected $fillable = ['nip', 'nama', 'instansi', 'phone'];

    public function caase()
    {
        return $this->belongsToMany('App\Investigator');
    }
}
