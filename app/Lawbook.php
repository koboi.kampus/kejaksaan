<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lawbook extends Model
{
    protected $fillable = ['kitab', 'tentang', 'tahun', 'deskripsi'];
}
