<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['nip', 'nama', 'level_id', 'position_id', 'tempat_lahir', 'tanggal_lahir', 'kelamin', 'alamat', 'phone', 'user_id', 'photo'];

    public function caases()
    {
        return $this->belongsToMany('App\Caase');
    }

    public function sixteens()
    {
        return $this->belongsToMany('App\Sixteen');
    }

    public function caaseStatus()
    {
        return $this->caases()->statue();
    }

    public function sixtenas()
    {
        return $this->belongsToMany('App\Sixtena');
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function position()
    {
        return $this->belongsTo('App\Position');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getPhotoPathAttribute()
    {
        //$apiAsn = Server::find(2)->url;

        if ($this->photo !== NULL) 
        {
            return  '/img/photo/'.$this->photo;
        } else {
            return 'http://sisroler.rumahprogrammer.com/img/photo/noimage.png';
        }
    }

}
