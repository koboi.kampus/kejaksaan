<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sixteen extends Model
{
    protected $fillable = ['caase_id', 'nomor_surat', 'tanggal', 'keterangan'];

    public function caase()
    {
        return $this->belongsTo('App\Caase');
    }

    public function caaseStatus()
    {
        return $this->caase()->statue();
    }

    public function profiles()
    {
        return $this->belongsToMany('App\Profile');
    }
}
