<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sixtena extends Model
{
    protected $fillable = ['caase_id', 'nomor_surat', 'tanggal', 'keterangan'];

    public function caase()
    {
        return $this->belongsTo('App\Caase');
    }

    public function profiles()
    {
        return $this->belongsToMany('App\Profile');
    }
}
