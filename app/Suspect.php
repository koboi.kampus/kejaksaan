<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suspect extends Model
{
    protected $fillable = ['nik', 'nama', 'tempat_lahir', 'tanggal_lahir', 'kelamin', 'alamat', 'phone', 'kebangsaan', 'pekerjaan', 'pendidikan', 'agama'];

    public function caases()
    {
        return $this->belongsToMany('App\Caase');
    }

    public function getJenkelaminAttribute()
    {
        if($this->kelamin == '1') {
            return 'Laki-laki';
        } else {
            return 'Perempuan';
        }
    }
}
