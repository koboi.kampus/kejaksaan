<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id');
            $table->integer('classification_id');
            $table->string('nomor_spdp', 100);
            $table->date('tanggal_spdp');
            $table->date('spdp_diterima');
            $table->integer('case_file_id')->nullable();
            $table->string('keterangan')->nullable();
            $table->char('status', 1);
            $table->date('tanggal_status')->nullable();
            $table->timestamps();
        });

        Schema::create('caase_suspect', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->integer('suspect_id');
            $table->timestamps();
        });

        Schema::create('caase_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->integer('profile_id');
            $table->timestamps();
        });

        Schema::create('caase_investigator', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->integer('investigator_id');
            $table->timestamps();
        });

        Schema::create('caase_lawbook', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->integer('lawbook_id');
            $table->char('pasal', 10);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cases');
      Schema::dropIfExists('case_suspect');
      Schema::dropIfExists('case_profile');
      Schema::dropIfExists('case_investigator');
      Schema::dropIfExists('case_regulation');
    }
}
