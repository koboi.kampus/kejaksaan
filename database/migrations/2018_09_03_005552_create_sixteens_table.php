<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSixteensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sixteens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->string('nomor_surat', 100);
            $table->date('tanggal');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });

        Schema::create('profile_sixteen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id');
            $table->integer('sixteen_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sixteens');
        Schema::dropIfExists('profile_sixteen');
    }
}
