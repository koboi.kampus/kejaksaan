<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSixtenasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sixtenas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->string('nomor_surat', 100);
            $table->date('tanggal');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });

        Schema::create('profile_sixtena', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id');
            $table->integer('sixtena_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sixtenas');
        Schema::dropIfExists('profile_sixtena');
    }
}
