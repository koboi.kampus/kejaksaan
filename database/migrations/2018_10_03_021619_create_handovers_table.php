<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHandoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handovers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->string('nomor', 100);
            $table->date('tanggal');
            $table->string('keterangan')->nullable();
            $table->char('p29', 1)->default(0);
            $table->char('p31', 1)->default(0);
            $table->char('p33', 1)->default(0);
            $table->char('p34', 1)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handovers');
    }
}
