<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->string('nomor', 100);
            $table->date('tanggal');
            $table->string('keterangan')->nullable();
            $table->char('dakwaan', 1)->default(0);
            $table->char('p37', 1)->default(0);
            $table->char('p38', 1)->default(0);
            $table->char('p42', 1)->default(0);
            $table->char('putusan', 1)->default(0);
            $table->char('P44', 1)->default(0);
            $table->char('banding', 1)->default(0);
            $table->char('kasasi', 1)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courts');
    }
}
