<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caase_id');
            $table->string('dictum')->nullable();
            $table->date('tanggal_dictum')->nullable();
            $table->string('dictum_pidana')->nullable();
            $table->text('dictum_barangbukti')->nullable();
            $table->string('nomor_putusan')->nullable();
            $table->date('tanggal_putusan')->nullable();
            $table->string('amar_pidana')->nullable();
            $table->text('amar_barangbukti')->nullable();
            $table->string('sikap_jpu')->nullable();
            $table->string('sikap_terdakwa')->nullable();
            $table->date('eksekusi_tuntas')->nullable();
            $table->date('bp_arsip')->nullable();
            $table->text('kasus_posisi')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archives');
    }
}
