/*
 Navicat Premium Data Transfer

 Source Server         : pidum.dumaikota
 Source Server Type    : MySQL
 Source Server Version : 100214
 Source Host           : 10.20.51.208:3306
 Source Schema         : dbsisroler

 Target Server Type    : MySQL
 Target Server Version : 100214
 File Encoding         : 65001

 Date: 07/09/2019 07:41:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for archives
-- ----------------------------
DROP TABLE IF EXISTS `archives`;
CREATE TABLE `archives`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `dictum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tanggal_dictum` date NULL DEFAULT NULL,
  `dictum_pidana` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `dictum_barangbukti` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `nomor_putusan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tanggal_putusan` date NULL DEFAULT NULL,
  `amar_pidana` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `amar_barangbukti` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `sikap_jpu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sikap_terdakwa` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `eksekusi_tuntas` date NULL DEFAULT NULL,
  `bp_arsip` date NULL DEFAULT NULL,
  `kasus_posisi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `keterangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of archives
-- ----------------------------
INSERT INTO `archives` VALUES (1, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', NULL, '2018-10-18 03:40:06', '2018-10-18 03:40:06');

-- ----------------------------
-- Table structure for caase_investigator
-- ----------------------------
DROP TABLE IF EXISTS `caase_investigator`;
CREATE TABLE `caase_investigator`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `investigator_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of caase_investigator
-- ----------------------------
INSERT INTO `caase_investigator` VALUES (18, 16, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (19, 16, 7, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (20, 17, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (21, 17, 7, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (25, 21, 23, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (26, 22, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (27, 23, 50, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (28, 24, 61, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (29, 25, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (30, 26, 21, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (31, 27, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (32, 28, 43, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (33, 29, 34, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (34, 30, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (35, 31, 19, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (36, 32, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (37, 33, 29, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (38, 33, 49, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (39, 34, 34, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (40, 35, 49, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (41, 36, 28, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (42, 37, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (43, 38, 49, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (44, 39, 43, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (45, 40, 19, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (46, 41, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (47, 42, 21, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (48, 43, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (49, 44, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (50, 45, 31, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (51, 46, 19, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (52, 47, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (53, 48, 21, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (54, 49, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (55, 50, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (56, 51, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (60, 54, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (61, 54, 52, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (62, 55, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (63, 56, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (64, 56, 52, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (65, 57, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (66, 58, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (67, 59, 65, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (68, 60, 25, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (69, 61, 11, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (70, 62, 22, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (71, 63, 32, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (72, 64, 22, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (73, 65, 60, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (74, 66, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (75, 67, 17, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (76, 68, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (77, 69, 18, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (78, 70, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (79, 71, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (80, 72, 22, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (81, 73, 11, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (82, 74, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (83, 75, 46, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (84, 76, 46, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (85, 77, 11, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (86, 78, 17, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (87, 79, 30, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (88, 80, 49, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (89, 81, 26, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (90, 82, 25, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (91, 83, 25, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (92, 84, 17, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (93, 85, 24, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (94, 86, 60, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (95, 87, 18, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (96, 88, 7, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (97, 89, 21, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (98, 89, 38, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (99, 89, 40, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (100, 89, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (101, 90, 33, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (102, 90, 34, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (103, 90, 36, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (104, 90, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (105, 90, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (106, 91, 84, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (107, 92, 85, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (108, 93, 29, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (109, 94, 25, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (110, 95, 84, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (111, 96, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (112, 97, 50, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (113, 98, 50, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (114, 99, 63, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (115, 100, 63, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (116, 101, 32, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (117, 101, 36, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (118, 102, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (119, 103, 38, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (120, 104, 38, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (121, 105, 17, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (122, 106, 22, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (123, 106, 24, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (124, 107, 32, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (125, 108, 22, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (126, 109, 47, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (127, 109, 48, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (128, 110, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (129, 111, 40, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (130, 112, 86, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (131, 113, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (132, 113, 8, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (133, 114, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (134, 115, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (135, 116, 40, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (136, 117, 40, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (137, 118, 87, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (138, 119, 87, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (139, 120, 52, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (140, 121, 52, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (141, 122, 57, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (142, 123, 57, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (143, 124, 89, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (144, 125, 89, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (145, 126, 18, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (146, 127, 18, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (147, 128, 52, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (148, 129, 24, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (149, 130, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (150, 131, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (151, 132, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (152, 133, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (153, 134, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (154, 135, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (155, 136, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (156, 137, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (157, 138, 19, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (158, 139, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (159, 140, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (160, 141, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (161, 142, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (162, 143, 68, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (163, 144, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (164, 145, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (165, 146, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (166, 147, 6, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (167, 147, 7, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (168, 148, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (169, 149, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (170, 150, 86, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (171, 151, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (172, 152, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (173, 153, 13, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (174, 154, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (175, 155, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (176, 156, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (177, 157, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (178, 158, 37, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (179, 159, 24, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (180, 160, 28, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (181, 161, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (182, 162, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (183, 163, 90, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (184, 164, 84, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (185, 165, 41, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (186, 166, 24, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (187, 167, 11, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (188, 168, 11, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (189, 169, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (190, 170, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (191, 171, 11, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (192, 172, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (193, 173, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (194, 174, 92, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (195, 175, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (196, 176, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (197, 177, 20, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (198, 178, 93, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (199, 179, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (200, 180, 94, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (201, 181, 66, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (202, 182, 88, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (203, 183, 61, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (204, 184, 93, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (205, 185, 95, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (206, 186, 26, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (207, 187, 18, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (208, 188, 90, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (209, 189, 18, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (210, 190, 93, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (211, 191, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (212, 192, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (213, 193, 92, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (214, 194, 10, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (215, 195, 42, NULL, NULL);
INSERT INTO `caase_investigator` VALUES (216, 196, 10, NULL, NULL);

-- ----------------------------
-- Table structure for caase_lawbook
-- ----------------------------
DROP TABLE IF EXISTS `caase_lawbook`;
CREATE TABLE `caase_lawbook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `lawbook_id` int(11) NOT NULL,
  `pasal` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for caase_profile
-- ----------------------------
DROP TABLE IF EXISTS `caase_profile`;
CREATE TABLE `caase_profile`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 291 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of caase_profile
-- ----------------------------
INSERT INTO `caase_profile` VALUES (7, 16, 8, NULL, NULL);
INSERT INTO `caase_profile` VALUES (8, 17, 8, NULL, NULL);
INSERT INTO `caase_profile` VALUES (12, 30, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (13, 30, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (14, 27, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (15, 27, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (16, 41, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (17, 41, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (18, 38, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (19, 38, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (20, 39, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (21, 39, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (22, 47, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (23, 47, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (24, 46, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (25, 46, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (26, 45, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (27, 45, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (28, 42, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (29, 42, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (30, 43, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (31, 43, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (32, 37, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (33, 37, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (34, 35, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (35, 35, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (36, 34, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (37, 34, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (38, 33, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (39, 33, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (40, 31, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (41, 31, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (42, 22, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (43, 22, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (44, 21, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (45, 21, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (49, 54, 11, NULL, NULL);
INSERT INTO `caase_profile` VALUES (50, 54, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (51, 57, 9, NULL, NULL);
INSERT INTO `caase_profile` VALUES (52, 57, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (53, 57, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (54, 62, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (55, 62, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (56, 59, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (57, 59, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (58, 60, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (59, 60, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (60, 66, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (61, 66, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (62, 67, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (63, 67, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (64, 63, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (65, 63, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (66, 65, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (67, 65, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (68, 64, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (69, 64, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (70, 68, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (71, 68, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (72, 61, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (73, 61, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (74, 81, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (75, 81, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (76, 83, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (77, 83, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (78, 84, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (79, 84, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (80, 85, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (81, 85, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (82, 86, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (83, 86, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (84, 88, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (85, 88, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (86, 89, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (87, 89, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (88, 90, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (89, 90, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (90, 72, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (91, 72, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (92, 74, 9, NULL, NULL);
INSERT INTO `caase_profile` VALUES (93, 74, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (94, 73, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (95, 73, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (96, 70, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (97, 70, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (98, 69, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (99, 69, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (100, 79, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (101, 79, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (102, 78, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (103, 78, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (104, 40, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (105, 40, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (106, 97, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (107, 97, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (108, 96, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (109, 96, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (110, 95, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (111, 95, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (112, 94, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (113, 94, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (114, 93, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (115, 93, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (116, 92, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (117, 92, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (118, 91, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (119, 91, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (120, 100, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (121, 100, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (122, 101, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (123, 101, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (124, 80, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (125, 80, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (126, 23, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (127, 23, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (128, 102, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (129, 102, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (130, 103, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (131, 104, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (132, 104, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (133, 105, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (134, 105, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (135, 77, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (136, 77, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (137, 106, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (138, 106, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (139, 107, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (140, 107, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (141, 108, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (142, 108, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (143, 109, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (144, 109, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (145, 110, 19, NULL, NULL);
INSERT INTO `caase_profile` VALUES (146, 111, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (147, 111, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (148, 112, 9, NULL, NULL);
INSERT INTO `caase_profile` VALUES (149, 112, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (150, 113, 19, NULL, NULL);
INSERT INTO `caase_profile` VALUES (151, 29, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (152, 29, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (153, 32, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (154, 32, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (155, 26, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (156, 26, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (157, 26, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (158, 26, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (159, 114, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (160, 114, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (161, 116, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (162, 116, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (163, 118, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (164, 118, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (165, 120, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (166, 120, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (167, 123, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (168, 123, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (169, 124, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (170, 124, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (171, 126, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (172, 126, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (173, 128, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (174, 128, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (175, 129, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (176, 129, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (177, 132, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (178, 132, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (179, 133, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (180, 133, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (181, 134, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (182, 134, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (183, 135, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (184, 135, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (185, 137, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (186, 137, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (187, 138, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (188, 138, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (189, 139, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (190, 139, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (191, 140, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (192, 140, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (193, 136, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (194, 136, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (195, 143, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (196, 143, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (197, 58, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (198, 145, 20, NULL, NULL);
INSERT INTO `caase_profile` VALUES (199, 146, 20, NULL, NULL);
INSERT INTO `caase_profile` VALUES (200, 147, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (201, 147, 19, NULL, NULL);
INSERT INTO `caase_profile` VALUES (202, 148, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (203, 148, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (204, 149, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (205, 149, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (206, 36, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (207, 36, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (208, 150, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (209, 150, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (210, 151, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (211, 151, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (212, 152, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (213, 152, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (214, 153, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (215, 153, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (216, 154, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (217, 154, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (218, 155, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (219, 155, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (220, 156, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (221, 156, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (222, 157, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (223, 157, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (224, 158, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (225, 158, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (226, 159, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (227, 159, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (228, 160, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (229, 160, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (230, 161, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (231, 161, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (232, 162, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (233, 162, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (234, 163, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (235, 163, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (236, 164, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (237, 164, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (238, 165, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (239, 165, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (240, 166, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (241, 166, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (242, 167, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (243, 167, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (244, 168, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (245, 168, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (246, 169, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (247, 169, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (248, 170, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (249, 170, 13, NULL, NULL);
INSERT INTO `caase_profile` VALUES (250, 171, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (251, 171, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (252, 172, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (253, 172, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (254, 48, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (255, 48, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (256, 173, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (257, 173, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (258, 174, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (259, 174, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (260, 181, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (261, 181, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (262, 179, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (263, 177, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (264, 177, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (265, 175, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (266, 175, 12, NULL, NULL);
INSERT INTO `caase_profile` VALUES (267, 184, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (268, 184, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (269, 187, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (270, 187, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (271, 186, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (272, 186, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (273, 185, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (274, 185, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (275, 189, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (276, 189, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (277, 190, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (278, 190, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (279, 183, 14, NULL, NULL);
INSERT INTO `caase_profile` VALUES (280, 183, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (281, 191, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (282, 191, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (283, 192, 10, NULL, NULL);
INSERT INTO `caase_profile` VALUES (284, 192, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (285, 193, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (286, 193, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (287, 194, 15, NULL, NULL);
INSERT INTO `caase_profile` VALUES (288, 194, 17, NULL, NULL);
INSERT INTO `caase_profile` VALUES (289, 195, 16, NULL, NULL);
INSERT INTO `caase_profile` VALUES (290, 195, 15, NULL, NULL);

-- ----------------------------
-- Table structure for caase_suspect
-- ----------------------------
DROP TABLE IF EXISTS `caase_suspect`;
CREATE TABLE `caase_suspect`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `suspect_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 210 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of caase_suspect
-- ----------------------------
INSERT INTO `caase_suspect` VALUES (21, 16, 9, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (22, 17, 9, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (26, 21, 12, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (27, 21, 13, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (28, 22, 11, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (29, 22, 15, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (30, 23, 14, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (31, 24, 16, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (32, 25, 58, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (33, 26, 57, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (34, 27, 55, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (35, 28, 54, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (36, 29, 52, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (37, 30, 51, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (38, 31, 49, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (39, 32, 48, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (40, 33, 47, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (41, 34, 46, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (42, 35, 45, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (43, 36, 43, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (44, 37, 42, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (45, 38, 41, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (46, 39, 40, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (47, 40, 39, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (48, 41, 38, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (49, 42, 37, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (50, 43, 36, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (51, 44, 35, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (52, 45, 34, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (53, 46, 33, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (54, 47, 32, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (55, 48, 31, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (56, 49, 30, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (57, 50, 11, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (58, 51, 30, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (62, 54, 59, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (63, 55, 59, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (64, 56, 59, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (65, 57, 60, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (66, 58, 71, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (67, 59, 70, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (68, 60, 69, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (69, 61, 68, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (70, 62, 67, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (71, 63, 66, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (72, 64, 65, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (73, 65, 64, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (74, 66, 63, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (75, 67, 62, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (76, 68, 29, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (77, 69, 27, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (78, 70, 26, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (79, 71, 24, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (80, 72, 23, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (81, 73, 22, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (82, 74, 21, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (83, 75, 20, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (84, 76, 19, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (85, 77, 18, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (86, 78, 61, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (87, 79, 59, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (88, 80, 17, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (89, 81, 83, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (90, 82, 84, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (91, 83, 88, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (92, 84, 87, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (93, 85, 77, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (94, 86, 86, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (95, 87, 85, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (96, 88, 76, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (97, 89, 75, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (98, 90, 74, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (99, 91, 82, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (100, 92, 81, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (101, 93, 80, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (102, 94, 79, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (103, 95, 78, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (104, 96, 73, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (105, 97, 72, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (106, 98, 72, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (107, 99, 59, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (108, 100, 59, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (109, 100, 93, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (110, 101, 90, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (111, 102, 11, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (112, 103, 96, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (113, 104, 33, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (114, 105, 61, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (115, 106, 95, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (116, 107, 23, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (117, 108, 97, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (118, 109, 94, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (119, 110, 10, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (120, 110, 11, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (121, 110, 12, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (122, 111, 98, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (123, 112, 21, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (124, 113, 97, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (125, 113, 98, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (126, 114, 53, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (127, 115, 53, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (128, 116, 100, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (129, 117, 100, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (130, 118, 101, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (131, 119, 101, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (132, 120, 102, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (133, 121, 102, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (134, 122, 103, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (135, 123, 103, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (136, 124, 104, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (137, 125, 104, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (138, 126, 105, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (139, 127, 105, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (140, 128, 106, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (141, 129, 110, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (142, 130, 109, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (143, 131, 109, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (144, 132, 111, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (145, 133, 108, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (146, 134, 107, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (147, 135, 115, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (148, 136, 115, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (149, 137, 114, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (150, 138, 113, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (151, 139, 112, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (152, 140, 116, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (153, 141, 117, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (154, 142, 117, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (155, 143, 118, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (156, 144, 119, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (157, 145, 15, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (158, 146, 119, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (159, 147, 10, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (160, 147, 11, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (161, 148, 46, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (162, 149, 29, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (163, 150, 124, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (164, 151, 123, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (165, 152, 122, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (166, 153, 121, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (167, 154, 129, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (168, 155, 130, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (169, 156, 131, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (170, 157, 126, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (171, 158, 132, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (172, 159, 127, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (173, 160, 133, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (174, 161, 134, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (175, 162, 135, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (176, 163, 136, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (177, 164, 124, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (178, 165, 137, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (179, 166, 138, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (180, 167, 121, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (181, 168, 139, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (182, 169, 125, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (183, 170, 123, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (184, 171, 140, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (185, 172, 141, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (186, 173, 142, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (187, 174, 144, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (188, 175, 145, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (189, 176, 146, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (190, 177, 147, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (191, 178, 148, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (192, 179, 149, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (193, 180, 150, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (194, 181, 151, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (195, 182, 152, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (196, 183, 155, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (197, 184, 156, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (198, 185, 157, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (199, 186, 158, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (200, 187, 159, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (201, 188, 154, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (202, 189, 159, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (203, 190, 156, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (204, 191, 160, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (205, 192, 161, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (206, 193, 162, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (207, 194, 163, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (208, 195, 164, NULL, NULL);
INSERT INTO `caase_suspect` VALUES (209, 196, 165, NULL, NULL);

-- ----------------------------
-- Table structure for caases
-- ----------------------------
DROP TABLE IF EXISTS `caases`;
CREATE TABLE `caases`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `classification_id` int(11) NOT NULL,
  `nomor_spdp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_spdp` date NOT NULL,
  `spdp_diterima` date NOT NULL,
  `case_file_id` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tanggal_status` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `pelanggaran` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 197 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of caases
-- ----------------------------
INSERT INTO `caases` VALUES (16, 2, 3, 'SPDP/23/VIII/2018/Res Dmi/Sek Dmi Kota', '2018-08-15', '2018-08-20', NULL, '-', '2', '2018-09-25', '2018-09-25 11:19:08', '2018-09-25 11:25:59', 'pasal 114 ayat 1 UU No 35 Thn 2009 Ttg Narkotika');
INSERT INTO `caases` VALUES (17, 2, 1, 'SPDP/23/VIII/2018/Res Dmi/Sek Dmi Kota', '2018-09-25', '2018-08-20', NULL, '-', '5', '2018-09-25', '2018-09-25 11:31:27', '2018-09-25 11:38:17', 'pasal 114 ayat 1 UU No 35 Thn 2009 Ttg Narkotika');
INSERT INTO `caases` VALUES (21, 2, 3, 'SPDP/132/VIII/2018/Reskrim', '2018-08-03', '2018-09-17', NULL, '-', '6', '2018-10-05', '2018-10-02 04:06:26', '2018-10-05 10:01:51', 'Pasal 126 Huruf C UU No. 6 Tahun 2011 tentang Imigrasi');
INSERT INTO `caases` VALUES (22, 2, 1, 'SPDP/15/VIII/2018/Reskrim', '2018-08-01', '2018-09-20', NULL, 'Tindak Pidana Narkotika', '12', '2018-10-15', '2018-10-02 04:13:34', '2018-10-18 06:04:03', 'Pasal 114 ayat (1) Jo Pasal 111 ayat (1) UU RI No. 35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (23, 2, 1, 'SPDP/22/VIII/2018/Reskrim', '2018-08-06', '2018-08-31', NULL, 'Tindak Pidana Narkotika', '12', '2018-10-09', '2018-10-02 04:18:10', '2018-10-18 06:00:57', 'Pasal 112 ayat (1) Jo Pasal 127 ayat (1) Huruf a UU RI No. 35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (24, 2, 1, 'B/355/VII/2018', '2018-07-27', '2018-08-15', NULL, '-', '0', NULL, '2018-10-02 04:23:10', '2018-10-02 04:23:10', 'Pasal 302 ayat (1) Jo Pasal 117 UU RI No. 17 tahun 2008 tentang Pelayaran');
INSERT INTO `caases` VALUES (26, 2, 1, 'SPDP/92/IX/2018/Narkoba', '2018-09-24', '2018-09-27', NULL, 'TindakPidana Narkotika', '1', '2018-10-18', '2018-10-05 08:01:35', '2018-10-18 06:21:34', '114 ayat (1) Jo Pasal 112 ayat (1) UU NO 35/2009 TENTANG NARKOTIKA');
INSERT INTO `caases` VALUES (27, 2, 1, 'B/90/IX/2018/Narkoba', '2018-09-20', '2018-09-25', NULL, '-', '1', '2018-10-05', '2018-10-05 08:06:07', '2018-10-05 09:16:56', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (28, 2, 1, 'SPDP/148/IX/2018/Reskrim', '2018-09-07', '2018-09-19', NULL, '-', '0', NULL, '2018-10-05 08:11:04', '2018-10-05 08:11:04', 'Pasal 50 (3) huruf e UU RI No.42 tahun 1999/Jo 33 ayat (3) UU No.5 tahun 1990');
INSERT INTO `caases` VALUES (29, 2, 1, 'SPDP/87/IX/2018/Narkoba', '2018-09-13', '2018-09-18', NULL, 'Tindak Pidana Narkotika', '5', '2018-10-30', '2018-10-05 08:18:27', '2018-11-02 07:40:27', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (30, 2, 1, 'SPDP/86/IX/2018/Narkoba', '2018-09-11', '2018-09-13', NULL, '-', '1', '2018-10-05', '2018-10-05 08:20:42', '2018-10-05 09:14:37', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (31, 2, 1, 'SPDP/85/IX/2018/Narkoba', '2018-09-08', '2018-09-10', NULL, '-', '1', '2018-10-05', '2018-10-05 08:26:24', '2018-10-05 09:47:59', 'Pasal 114 ayat (1)  Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (32, 2, 1, 'SPDP/34/IX/2018', '2018-09-10', '2018-09-10', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-18', '2018-10-05 08:28:22', '2018-10-18 06:19:19', 'Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (33, 2, 1, 'SPDP/32/IX/2018/Reskrim', '2018-09-10', '2018-09-10', NULL, '-', '1', '2018-10-05', '2018-10-05 08:31:27', '2018-10-05 09:46:52', 'PASAL 112 AYAT (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (34, 1, 1, 'SPDP/87/IX/2018/Res Narkoba', '2018-09-10', '2018-09-10', NULL, '-', '6', '2018-10-05', '2018-10-05 08:33:24', '2018-10-05 10:08:13', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (35, 2, 1, 'SPDP/33/IX/2018/Reskrim', '2018-09-10', '2018-09-10', NULL, 'Tindak Pidana Narkotika', '2', '2018-10-15', '2018-10-05 08:35:24', '2018-10-16 08:13:11', 'Pasal 112 ayat (2) UU RI No.35 tahun 2009 / 62 UU RI No.05 tahun 1997');
INSERT INTO `caases` VALUES (36, 2, 1, 'SPDP/30/IX/2018/Reskrim', '2018-08-31', '2018-09-06', NULL, 'Tindak pidana Narkotika', '1', '2018-10-22', '2018-10-05 08:40:33', '2018-10-22 02:36:03', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (37, 2, 1, 'SPDP/82/IX/2018/Res Narkoba', '2018-09-04', '2018-09-06', NULL, 'Tindak Pidana Narkotika', '2', '2018-10-08', '2018-10-05 08:42:47', '2018-10-16 08:11:23', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (38, 2, 1, 'SPDP/35/VIII/2018/Reskrim', '2018-08-30', '2018-08-30', NULL, 'Tindak Pidana Narkotika', '2', '2018-10-01', '2018-10-05 08:44:49', '2018-10-16 08:07:56', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (39, 2, 1, 'SPDP/29/VIII/2018/Reskrim', '2018-08-30', '2018-08-30', NULL, '-', '6', '2018-10-05', '2018-10-05 08:46:58', '2018-10-05 10:03:29', 'Pasal 108 Jo PASAL 69 HURUF H UU NO.32 TAHUN 2009 JO PASAL 187  JO PASAL 188 KUHP');
INSERT INTO `caases` VALUES (40, 2, 1, 'SPDP/81/VIII/2018/Res Narkoba', '2018-08-29', '2018-09-03', NULL, 'Tindak Pidana Narktoika', '2', '2018-10-04', '2018-10-05 08:48:59', '2018-10-16 08:05:53', 'Pasal 112 ayat (1)  UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (41, 2, 1, 'SPDP/80/VIII/2018/Narkoba', '2018-08-27', '2018-08-29', NULL, '-', '1', '2018-10-05', '2018-10-05 08:50:30', '2018-10-05 09:28:14', 'Pasal 112 ayat (1)  UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (42, 2, 1, 'SPDP/79/VIII/2018/Reskrim', '2018-08-22', '2018-08-28', NULL, '-', '2', '2018-09-28', '2018-10-05 08:52:37', '2018-10-16 07:59:40', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (43, 2, 1, 'SPDP/24/VIII/2018/Reskrim', '2018-08-28', '2018-08-28', NULL, '-', '6', '2018-10-06', '2018-10-05 08:54:02', '2018-10-31 06:06:49', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (45, 2, 1, 'SPDP/27/VIII/2018/Reskrim', '2018-08-15', '2018-08-20', NULL, 'Tindak Pidana Narkotika', '5', '2018-10-29', '2018-10-05 08:57:50', '2018-11-02 07:38:11', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (46, 2, 1, 'SPDP/27/VIII/2018/Reskrim', '2018-08-15', '2018-08-20', NULL, NULL, '10', '2018-10-10', '2018-10-05 08:58:57', '2018-10-12 09:25:33', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (47, 2, 1, 'SPDP/28/VIII/2018/Reskrim', '2018-08-07', '2018-08-20', NULL, 'Tindak Pidana Narkotika', '6', '2018-10-15', '2018-10-05 09:01:31', '2018-10-22 02:22:19', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (48, 2, 1, 'SPDP/75/VIII/2018/Reskrim', '2018-08-08', '2018-08-09', NULL, 'Tindak Pidana Narkotika', '1', '2018-11-02', '2018-10-05 09:03:50', '2018-11-02 07:48:20', 'Pasal 112 ayat (1)  UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (49, 2, 1, 'SPDP/76/VIII/2018/ReskriM', '2018-08-07', '2018-08-08', NULL, '-', '0', NULL, '2018-10-05 09:05:22', '2018-10-05 09:05:22', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (54, 2, 3, 'SPDP/23/VIII/2018/Reskrim', '2018-08-06', '2018-08-06', NULL, '-', '12', '2018-10-15', '2018-10-10 08:37:02', '2018-10-18 05:55:39', 'Pasal 363');
INSERT INTO `caases` VALUES (57, 2, 3, 'SPDP/02B/VI/2018/GAKKUM', '2018-06-25', '2018-07-05', NULL, '-', '12', '2018-09-14', '2018-10-10 09:12:41', '2018-10-10 09:54:00', 'Pasal 480');
INSERT INTO `caases` VALUES (58, 2, 3, 'SPDP/64/IX/2018/Reskrim', '2018-09-30', '2018-10-02', NULL, '-', '5', '2018-10-22', '2018-10-11 09:47:02', '2018-10-21 20:16:17', 'Pasal 368 KUHP');
INSERT INTO `caases` VALUES (59, 2, 3, 'SPDP/21/IX/2018/Reskrim', '2018-10-02', '2018-10-02', NULL, 'Tindak Pidana Pencurian', '5', '2018-10-30', '2018-10-11 09:52:15', '2018-11-02 07:39:20', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (60, 2, 3, 'spdp/161/IX/2018/Reskrim', '2018-09-27', '2018-10-02', NULL, '-', '1', '2018-10-11', '2018-10-11 11:37:49', '2018-10-11 12:33:22', 'Pasal 363 Jo 480 KUHP');
INSERT INTO `caases` VALUES (61, 2, 3, 'spdp/62/IX/2018/Reskrim', '2018-09-29', '2018-10-01', NULL, 'Penganiayaan dan atau penghinaan', '6', '2018-10-25', '2018-10-11 11:40:03', '2018-11-01 07:49:50', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (62, 2, 3, 'SPDP/162/IX/2018/Reskrim', '2018-09-27', '2018-10-01', NULL, 'Tindak Pidana Pencurian', '5', '2018-10-30', '2018-10-11 11:42:00', '2018-11-02 07:35:36', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (63, 2, 3, 'SPDP/152/IX/2018/Reskrim', '2018-09-28', '2018-09-27', NULL, 'Tindak Pidana Pencurian', '6', '2018-11-05', '2018-10-11 11:43:46', '2018-11-05 08:15:44', 'Pasal 362 KUHP');
INSERT INTO `caases` VALUES (64, 2, 3, 'SPDP/141a/IX/2018/Reskrim', '2018-09-24', '2018-09-25', NULL, 'Tindak Pidana Pencurian', '6', '2018-10-25', '2018-10-11 11:46:12', '2018-11-01 08:02:12', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (65, 2, 3, 'SPDP/153/IX/2018/Reskrim', '2018-09-25', '2018-09-25', NULL, 'Tindak Pidana Pencurian', '6', '2018-10-23', '2018-10-11 11:49:09', '2018-11-01 07:55:55', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (66, 2, 3, 'SPDP/150/IX/2018/Reskrim', '2018-09-21', '2018-09-24', NULL, 'Tindak Pidana Pencurian', '5', '2018-09-25', '2018-10-11 11:51:00', '2018-10-22 02:08:00', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (67, 2, 3, 'SPDP/12/IX/2018/Reskrim', '2018-09-18', '2018-09-19', NULL, '-', '6', '2018-09-24', '2018-10-11 11:53:11', '2018-10-11 12:51:11', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (68, 2, 3, 'spdp/60/IX/2018/Reskrim', '2018-09-13', '2018-09-13', NULL, 'Tindak Pidana Pencurian', '12', '2018-09-28', '2018-10-11 11:54:49', '2018-10-22 02:16:50', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (69, 2, 3, 'SPDP/10/IX/2018/Reskrim', '2018-09-06', '2018-09-07', NULL, '-', '6', '2018-09-12', '2018-10-11 11:56:57', '2018-10-12 03:29:52', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (70, 2, 3, 'SPDP/18/IX/2018/Reskrim', '2018-09-03', '2018-09-07', NULL, '-', '1', '2018-10-12', '2018-10-11 11:59:38', '2018-10-12 02:19:01', 'Pasal 372 KUHP');
INSERT INTO `caases` VALUES (71, 2, 3, 'SPDP/58/IX/2018/Reskrim', '2018-09-04', '2018-09-06', NULL, '-', '0', NULL, '2018-10-11 12:01:10', '2018-10-11 12:01:10', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (72, 2, 3, 'SPDP/35/VIII/2018/Reskrim', '2018-08-29', '2018-08-29', NULL, '-', '5', '2018-10-03', '2018-10-11 12:09:11', '2018-10-12 09:52:25', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (73, 2, 3, 'spdp/57/VIII/2018/Reskrim', '2018-08-25', '2018-08-28', NULL, NULL, '6', '2018-09-20', '2018-10-11 12:11:00', '2018-10-12 03:27:22', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (74, 2, 3, 'spdp/18/VIII/2018/Reskrim', '2018-08-01', '2018-08-20', NULL, 'Tindak Pidana Pencurian dan atau Pertolongan Jahat', '3', '2018-10-01', '2018-10-11 12:12:31', '2018-10-16 09:16:18', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (75, 2, 3, 'SPDP/11/VIII/2018/LL', '2018-08-13', '2018-08-15', NULL, '-', '0', NULL, '2018-10-11 12:14:54', '2018-10-11 12:14:54', 'Pasal 310 (4) UU RI No.22 tahun 2009');
INSERT INTO `caases` VALUES (76, 2, 3, 'SPDP/13/VIII/2018/LL', '2018-05-15', '2018-08-15', NULL, '-', '0', NULL, '2018-10-11 12:16:49', '2018-10-11 12:16:49', 'Pasal 310 ayat (3) UU RI nO.22/2009');
INSERT INTO `caases` VALUES (77, 2, 3, 'SPDP/55/VIII/2018/Reskrim', '2018-08-06', '2018-08-07', NULL, '-', '10', '2018-10-01', '2018-10-11 12:18:27', '2018-10-12 09:46:36', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (78, 2, 3, 'SPDP/09/VIII/2018/Reskrim', '2018-08-01', '2018-08-02', NULL, 'Tindak Pidana Pencurian', '12', '2018-10-15', '2018-10-11 12:21:04', '2018-10-18 06:06:16', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (79, 2, 3, 'SPDP/23/VIII/2018/Reskrim', '2018-08-06', '2018-08-06', NULL, '-', '6', '2018-08-31', '2018-10-11 12:22:39', '2018-10-12 03:31:08', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (80, 2, 3, 'spdp/24/XIII/2018/Reskrim', '2018-08-07', '2018-08-07', NULL, '-', '12', '2018-10-11', '2018-10-11 12:24:28', '2018-10-18 05:56:54', 'Pasal 365 KUHP');
INSERT INTO `caases` VALUES (81, 2, 1, 'SPDP/155/IX/2018/Reskrim', '2018-09-26', '2018-09-26', NULL, 'Tindak Pidana Persetubuhan dan Pencabulan terhadap anak dibawah umur', '6', '2018-10-23', '2018-10-11 13:24:45', '2018-11-01 07:54:38', 'Pasal 81,82 UU RI No.35 tahun 2014');
INSERT INTO `caases` VALUES (82, 2, 2, 'SPDP/83/IX/2018/Reskrim', '2018-09-17', '2018-09-28', NULL, '-', '0', NULL, '2018-10-11 13:30:09', '2018-10-11 13:30:09', 'Pasal 170 KUHP');
INSERT INTO `caases` VALUES (83, 2, 3, 'SPDP/157/IX/2018/Reskrim', '2018-09-24', '2018-09-27', NULL, '-', '1', '2018-10-11', '2018-10-11 13:44:35', '2018-10-11 13:45:35', 'Pasal 378 KUHP');
INSERT INTO `caases` VALUES (84, 2, 3, 'SPDP/13/IX/2018/Reskrim', '2018-09-18', '2018-09-19', NULL, '-', '6', '2018-09-24', '2018-10-11 13:49:30', '2018-10-12 03:26:09', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (85, 2, 3, 'SPDP/25/X/2018/Reskrim', '2018-10-02', '2018-10-08', NULL, 'Pencurian dengan kekerasan', '6', '2018-10-29', '2018-10-11 13:50:12', '2018-10-30 04:17:22', '365 KUHP');
INSERT INTO `caases` VALUES (86, 2, 3, 'SPDP/145/IX/2018/Reskrim', '2018-09-14', '2018-09-17', NULL, 'Tindak Pidana Penganiayaan', '2', '2018-09-14', '2018-10-11 13:54:07', '2018-10-16 07:42:29', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (87, 2, 3, 'SPDP/14/X/2018/Reskrim', '2018-10-01', '2018-10-02', NULL, '-', '0', NULL, '2018-10-11 13:57:00', '2018-10-11 13:57:00', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (88, 2, 1, 'SPDP/23/X/2018/Reskrim', '2018-10-01', '2018-10-04', NULL, '-', '1', '2018-10-11', '2018-10-11 13:58:27', '2018-10-11 13:59:29', '114 Ayat (1) Jo Pasal 112 Ayat (1) UU RI No.35 Tahun 2009 Tentang Narkotika');
INSERT INTO `caases` VALUES (89, 2, 1, 'SPDP/95/X/2018/Res.Narkoba', '2018-10-01', '2018-10-02', NULL, '-', '1', '2018-10-11', '2018-10-11 14:09:03', '2018-10-11 14:10:58', '114 Ayat (1) Jo Pasal 112 Ayat (1) UU RI No.35 Tahun 2009 Tentang Narkotika');
INSERT INTO `caases` VALUES (90, 2, 1, 'SPDP/93/IX/2018/Res.Narkoba', '2018-09-25', '2018-09-28', NULL, '-', '1', '2018-10-11', '2018-10-11 14:21:03', '2018-10-11 14:22:12', '114 Ayat (1) Jo Pasal 112 Ayat (1) UU RI No.35 Tahun 2009 Tentang Narkotika');
INSERT INTO `caases` VALUES (91, 2, 3, 'SPDP/23/X/2018/Reskrim', '2018-10-01', '2018-10-02', NULL, '-', '1', '2018-10-12', '2018-10-12 02:34:21', '2018-10-12 03:23:01', 'Pasal 340 KUHP');
INSERT INTO `caases` VALUES (92, 2, 3, 'B/22/X/2018/LL', '2018-10-08', '2018-10-09', NULL, '-', '1', '2018-10-12', '2018-10-12 02:45:11', '2018-10-12 03:21:51', 'Pasal 310 ayat (4) UU RI No.22/2009 tentang Lalu Lintas dan Angkutan Jalan');
INSERT INTO `caases` VALUES (93, 2, 1, 'SPDP/38/X/2018/Reskrim', '2018-10-05', '2018-10-08', NULL, '-', '1', '2018-10-12', '2018-10-12 02:47:31', '2018-10-12 03:20:07', 'Pasal 81 ayat (2) ayat (3) Jo Pasal 82 ayat (1) UU RI No.35/2014');
INSERT INTO `caases` VALUES (94, 2, 3, 'SPDP/158/X/2018/Reskrim', '2018-10-05', '2018-10-08', NULL, '-', '1', '2018-10-12', '2018-10-12 02:49:11', '2018-10-12 03:18:33', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (95, 2, 3, 'SPDP/165/X/2018/Reskrim', '2018-10-05', '2018-10-05', NULL, 'Tindak Pidana Pencurian', '6', '2018-10-29', '2018-10-12 02:52:20', '2018-11-02 07:29:02', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (96, 2, 1, 'SPDP/94/IX/2018/Narkoba', '2018-09-28', '2018-10-01', NULL, '-', '1', '2018-10-12', '2018-10-12 02:54:42', '2018-10-12 03:07:38', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (97, 2, 1, 'SPDP/36/IX/2018/Reskrim', '2018-09-28', '2018-10-01', NULL, '-', '1', '2018-10-12', '2018-10-12 03:01:38', '2018-10-12 03:05:09', 'Pasal 112 ayat (1)  Jo Pasal 127 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (98, 2, 1, 'SPDP/36/IX/2018/Reskrim', '2018-09-28', '2018-10-01', NULL, '-', '0', NULL, '2018-10-12 03:04:06', '2018-10-12 03:04:06', 'Pasal 112 ayat (1)  Jo Pasal 127 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (100, 2, 3, 'SPDP/23/VIII/2018/Res Dmi/Sek Dmi Kota', '2018-08-06', '2018-08-21', NULL, 'Tindak Pidana Pencurian', '12', '2018-10-15', '2018-10-12 08:06:27', '2018-10-18 06:08:43', 'Pasal 363 ayat (1) Ke 3e, 4e, 5e KHUPidana');
INSERT INTO `caases` VALUES (101, 2, 1, 'SPDP/58/VI/2018/Reskrim', '2018-06-06', '2018-08-21', NULL, '-', '12', '2018-10-11', '2018-10-12 08:19:36', '2018-10-18 05:57:45', 'Pasal 114 Ayat (1)  Pasal 112 ayat (1) JoUU No. 35 Tahun 2009 Tentang Narkotika');
INSERT INTO `caases` VALUES (102, 2, 1, 'SPDP/15/VIII/2018/Reskrim', '2018-08-02', '2018-08-02', NULL, NULL, '10', '2018-09-27', '2018-10-12 09:08:18', '2018-10-12 09:16:20', 'Pasal 114 ayat (1) Jo Pasal 111 ayat (1) Jo 127 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (103, 2, 1, 'SPDP/11/I/2018/Res Narkoba', '2018-01-22', '2018-02-01', NULL, '-', '20', '2018-10-08', '2018-10-12 09:15:15', '2018-10-12 09:39:28', 'Pasal 114 Ayat (1) Jo Pasal 112 ayat (1) UU No. 35 Tahun 2009 Tentang Narkotika');
INSERT INTO `caases` VALUES (104, 2, 1, 'SPDP/23/VIII/2018/Reskrim', '2018-08-15', '2018-08-20', NULL, NULL, '6', '2018-09-20', '2018-10-12 09:20:00', '2018-10-12 09:22:52', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (105, 2, 3, 'SPDP/09/VIII/2018/Reskrim', '2018-08-01', '2018-08-02', NULL, '-', '6', '2018-08-20', '2018-10-12 09:29:51', '2018-10-12 09:32:08', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (106, 2, 3, 'SPDP/82/IV/2018/Reskrim', '2018-04-27', '2018-05-08', NULL, '-', '20', '2018-10-08', '2018-10-12 09:47:54', '2018-10-12 09:58:50', 'Pasal 372 Jo 56 KUHP');
INSERT INTO `caases` VALUES (107, 2, 3, 'SPDP/135/VIII/2018/Reskrim', '2018-08-29', '2018-08-29', NULL, '-', '6', '2018-09-17', '2018-10-12 09:50:08', '2018-10-12 09:51:41', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (108, 2, 3, 'SPDP/17/VIII/2018/Reskrim', '2018-07-23', '2018-08-13', NULL, 'Tindak Pidana Pencurian', '10', '2018-09-20', '2018-10-12 09:59:29', '2018-10-12 10:05:41', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (109, 1, 2, 'SPDP/48/VI/2018/Reskrim', '2018-06-25', '2018-06-25', NULL, '-', '21', '2018-10-18', '2018-10-12 10:03:44', '2018-10-18 03:40:06', 'Pasal 351 KUHP Jo UU No. 11 Tahun 2012');
INSERT INTO `caases` VALUES (110, 2, 1, '777', '2018-10-01', '2018-10-03', NULL, 'Tidak ada', '20', '2018-10-19', '2018-10-14 13:10:00', '2018-10-19 06:00:26', 'KUHP Pasal 34');
INSERT INTO `caases` VALUES (111, 2, 1, 'SPDP/72/VII/2018/Res Narkoba', '2018-07-30', '2018-08-03', NULL, 'Tindak Pidana Narkotika', '2', '2018-09-04', '2018-10-16 08:25:09', '2018-10-16 08:37:54', 'Pasal 114 ayat (1) Jo Pasal 111 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (112, 2, 3, 'SPDP/18/VIII/2018/Reskrim', '2018-08-01', '2018-08-20', NULL, 'Tindak Pidana Pencrian dan atau Pertolongan  Jahat', '1', '2018-10-16', '2018-10-16 09:05:21', '2018-10-16 09:06:19', 'Pasal 480 KUHP');
INSERT INTO `caases` VALUES (113, 2, 1, '0909090', '2018-10-08', '2018-10-26', NULL, 'Tidak ada', '1', '2018-10-18', '2018-10-18 02:04:58', '2018-10-18 02:05:16', 'Pasal 21 KUHP');
INSERT INTO `caases` VALUES (114, 2, 1, 'SPDP/88/IX/2018/Res Narkoba', '2018-09-12', '2018-09-18', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-18', '2018-10-18 06:49:02', '2018-10-18 06:50:03', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (115, 2, 1, 'SPDP/88/IX/2018/Res Narkoba', '2018-09-12', '2018-09-18', NULL, 'Tindak Pidana Narkotika', '0', NULL, '2018-10-18 06:49:02', '2018-10-18 06:49:02', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (116, 2, 1, 'SPDP/91/IX/2018/Res Narkoba', '2018-09-19', '2018-09-29', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-18', '2018-10-18 06:59:20', '2018-10-18 07:00:28', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (117, 2, 1, 'SPDP/91/IX/2018/Res Narkoba', '2018-09-19', '2018-09-29', NULL, 'Tindak Pidana Narkotika', '0', NULL, '2018-10-18 06:59:20', '2018-10-18 06:59:20', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (118, 2, 3, 'SPDP/168/X/2018/Reskrim', '2018-08-12', '2018-10-15', NULL, 'Tindak Pidana Pencurian', '1', '2018-10-18', '2018-10-18 07:11:44', '2018-10-18 07:12:51', 'Pasal 363 jo 480 KUHP');
INSERT INTO `caases` VALUES (119, 2, 3, 'SPDP/168/X/2018/Reskrim', '2018-08-12', '2018-10-15', NULL, 'Tindak Pidana Pencurian', '0', NULL, '2018-10-18 07:11:44', '2018-10-18 07:11:44', 'Pasal 363 jo 480 KUHP');
INSERT INTO `caases` VALUES (120, 2, 3, 'SPDP/39/X/2018/Reskrim', '2018-10-11', '2018-10-11', NULL, 'Tindak PIDANA Pencurian', '6', '2018-11-01', '2018-10-18 07:24:02', '2018-11-02 07:29:58', 'Pasal 363 Jo Pasal 480 KUHP');
INSERT INTO `caases` VALUES (121, 2, 3, 'SPDP/39/X/2018/Reskrim', '2018-10-11', '2018-10-11', NULL, 'Jl.Sutomo Gg.Panam Jaya RT.009 Kel.Teluk Binjai  Kec.Dumai Timur Dumai', '0', NULL, '2018-10-18 07:24:02', '2018-10-18 07:24:02', 'Pasal 363 Jo Pasal 480 KUHP');
INSERT INTO `caases` VALUES (123, 2, 3, 'SPDP/161/X/2018/Reskrim', '2018-10-07', '2018-10-09', NULL, 'Tindak Pidana Pencurian', '6', '2018-10-17', '2018-10-18 07:34:30', '2018-10-22 02:29:24', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (124, 2, 3, 'SPDP/26/X/2018/Reskrim', '2018-10-05', '2018-10-09', NULL, 'Tindak Pidana Penggelapan', '1', '2018-10-18', '2018-10-18 07:50:00', '2018-10-18 07:51:04', 'Pasal 372 KUHP');
INSERT INTO `caases` VALUES (125, 2, 3, 'SPDP/26/X/2018/Reskrim', '2018-10-05', '2018-10-09', NULL, 'Tindak Pidana Penggelapan', '0', NULL, '2018-10-18 07:50:00', '2018-10-18 07:50:00', 'Pasal 372 KUHP');
INSERT INTO `caases` VALUES (126, 2, 3, 'SPDP/15/X/2018/Reskrim', '2018-10-12', '2018-10-15', NULL, 'Tindak Pidana Pencurian', '6', '2018-10-25', '2018-10-18 07:59:17', '2018-11-01 07:51:47', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (127, 2, 3, 'SPDP/15/X/2018/Reskrim', '2018-10-12', '2018-10-15', NULL, 'Tindak Pidana Pencurian', '0', NULL, '2018-10-18 07:59:17', '2018-10-18 07:59:17', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (128, 2, 3, 'SPDP/37/X/2018/Reskrim', '2018-10-08', '2018-10-15', NULL, 'Tindak Pidana Pencurian pada hari rabu tanggal 03 Oktober 2018 di Jalan Swadaya Rt.003 Kel. Bukti Batrem Kec. Dumai Timur', '6', '2018-10-29', '2018-10-18 08:18:22', '2018-10-30 04:51:54', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (129, 2, 1, 'SPDP/24/X/2018/Sek Sungai Sembilan', '1997-10-05', '0108-10-11', NULL, 'Tindak pidana Narkotika', '1', '2018-10-21', '2018-10-21 09:45:22', '2018-10-21 09:46:30', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (130, 2, 1, 'SPDP/98/X/2018/Narkoba', '2018-10-08', '2018-10-09', NULL, 'Tindak pidana Narkotika', '0', NULL, '2018-10-21 09:48:40', '2018-10-21 09:48:40', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (131, 2, 1, 'SPDP/98/X/2018/Narkoba', '2018-10-08', '2018-10-09', NULL, 'Tindak pidana Narkotika', '0', NULL, '2018-10-21 09:48:41', '2018-10-21 09:48:41', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (132, 2, 1, 'SPDP/100/X/2018/Narkoba', '2018-10-12', '2018-10-16', NULL, 'Tindak PiDANA NARKOTIKA', '1', '2018-10-21', '2018-10-21 09:50:32', '2018-10-21 09:51:40', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (133, 2, 1, 'SPDP/96/X/2018/Narkotika', '2018-10-05', '2018-10-09', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-21', '2018-10-21 09:54:06', '2018-10-21 09:55:02', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (134, 2, 1, 'SPDP/98/X/2018/Res Narkoba', '2018-10-04', '2018-10-09', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-21', '2018-10-21 09:57:40', '2018-10-21 09:58:50', 'Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (135, 2, 1, 'SPDP/103/X/2018/Res Narkoba', '2018-10-13', '2018-10-18', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-21', '2018-10-21 10:09:38', '2018-10-21 10:10:27', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (136, 2, 1, 'SPDP/103/X/2018/Res Narkoba', '2018-10-13', '2018-10-18', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-21', '2018-10-21 10:09:39', '2018-10-21 11:57:11', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (137, 1, 3, 'SPDP/66/X/2018/Reskrim', '2018-10-10', '2018-10-18', NULL, 'Tindak Pidana Pencurian', '1', '2018-10-21', '2018-10-21 11:29:40', '2018-10-21 11:30:37', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (138, 2, 1, 'SPDP/104/X/2018/Narkoba', '2018-10-15', '2018-10-16', NULL, 'Tindak Pidana Narkotika', '5', '2018-10-29', '2018-10-21 11:32:49', '2018-11-02 07:43:27', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (139, 2, 1, 'SPDP/102/X/2018/Res Narkoba', '2018-10-11', '2018-10-16', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-21', '2018-10-21 11:38:38', '2018-10-21 11:39:34', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (140, 2, 1, 'SPDP/101/X/2018/Res Narkotika', '2018-10-11', '2018-10-18', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-21', '2018-10-21 11:47:08', '2018-10-21 11:48:14', 'Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (141, 2, 1, 'SPDP/67/X/2018/Reskrim', '2018-10-12', '2018-10-16', NULL, 'Tindak Pidana Narkotika', '0', NULL, '2018-10-21 12:01:49', '2018-10-21 12:01:49', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (142, 2, 1, 'SPDP/67/X/2018/Reskrim', '2018-10-12', '2018-10-16', NULL, 'Tindak Pidana Narkotika', '0', NULL, '2018-10-21 12:01:49', '2018-10-21 12:01:49', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (143, 2, 1, 'B/470/X/2018', '2018-10-10', '2018-10-11', NULL, 'Tindak Pidana Pelayaran', '1', '2018-10-21', '2018-10-21 12:08:26', '2018-10-21 12:09:23', 'Pasal 285 Jo Pasal 13 ayat (4) UU RI No.17 tahun 2008 tentang Pelayaran');
INSERT INTO `caases` VALUES (144, 2, 1, 'SPDP/99/X/2018/Narkoba', '2018-10-08', '2018-10-09', NULL, 'Tindak Pidana Narkotika', '0', NULL, '2018-10-21 12:13:38', '2018-10-21 12:13:38', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (145, 2, 2, '001/xx/xx/2018', '2018-10-20', '2018-10-22', NULL, '-', '20', '2018-10-30', '2018-10-21 20:27:55', '2018-10-21 21:24:53', 'pasal 351 ayat 1');
INSERT INTO `caases` VALUES (146, 2, 2, '001/spdp/10/2018', '2018-10-17', '2018-10-22', NULL, '-', '12', '2018-10-22', '2018-10-21 21:13:19', '2018-10-21 21:23:56', '351 ayat 1');
INSERT INTO `caases` VALUES (147, 2, 1, '123/123/123/Reskrim', '2018-10-19', '2018-10-22', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '6', '2018-10-25', '2018-10-21 22:41:39', '2018-11-01 07:52:56', 'Pasal 21 KUHP');
INSERT INTO `caases` VALUES (148, 1, 1, 'SPDP/87/IX/2018/Res Narkoba', '2018-09-10', '2018-10-09', NULL, 'Tindak Pidana Narkotika', '12', '2018-10-04', '2018-10-22 01:44:10', '2018-10-22 01:55:12', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999');
INSERT INTO `caases` VALUES (149, 1, 3, 'SPDP/60/IX/2018/Reskrim', '2018-09-13', '2018-09-13', NULL, 'Tindak Pidana Pencurian', '1', '2018-10-22', '2018-10-22 02:02:35', '2018-10-22 02:03:42', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (150, 2, 1, 'SPDP/24/X/2018/Reskrim', '2018-10-18', '2018-10-22', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-25', '2018-10-25 11:56:02', '2018-10-25 11:57:13', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) No.35 tahun 1999 tentang Narkotika');
INSERT INTO `caases` VALUES (151, 2, 3, 'SPDP/170/X/2018/Reskrim', '2018-10-19', '2018-10-22', NULL, 'TINDAK PIDANA PENGANIAYAAN', '1', '2018-10-25', '2018-10-25 12:16:12', '2018-10-25 12:17:15', 'Pasal 351  KUHP');
INSERT INTO `caases` VALUES (152, 2, 1, 'SPDP/170/X/2018/Res Narkoba', '2018-10-16', '2018-10-22', NULL, 'Tindak Pidana Narkotika', '1', '2018-10-25', '2018-10-25 12:22:07', '2018-10-25 12:23:05', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) No.35 tahun 1999');
INSERT INTO `caases` VALUES (153, 1, 3, 'SPDP/72/X/2018/Reskrim', '2018-10-20', '2018-10-24', NULL, 'Tindak Pidana Penganiayaan', '6', '2018-10-25', '2018-10-25 12:25:21', '2018-11-01 07:50:49', 'Pasal 351  KUHP');
INSERT INTO `caases` VALUES (154, 2, 1, 'SPDP/109/X/2018/Res Narkoba', '2018-02-10', '2018-02-10', NULL, 'Tindak pidana Narkotika', '1', '2018-10-30', '2018-10-30 09:55:11', '2018-10-30 09:56:14', 'Pasal 112 UU RI No.35  tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (155, 2, 1, 'SPDP/III/X/2018/Res Narkoba', '2018-10-24', '2018-10-29', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 07:24:07', '2018-11-01 07:25:04', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (156, 2, 1, 'SPDP/108/X/2018/Res Narkoba', '2018-10-23', '2018-10-29', NULL, 'Tindak Pidana  Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 07:32:46', '2018-11-01 07:33:32', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (157, 2, 1, 'SPDP/110/X/2018/Narkoba', '2018-10-26', '2018-10-29', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:07:04', '2018-11-01 08:07:42', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (158, 2, 1, 'SPDP/112/X/2018/Res Narkoba', '2018-10-25', '2018-10-29', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:12:04', '2018-11-01 08:12:45', 'Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (159, 2, 1, 'SPDP/28/X/2018/Reskrim', '2018-10-27', '2018-10-29', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:18:11', '2018-11-01 08:18:37', 'Pasal 114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (160, 2, 1, 'SPDP/41/X/2018/Reskrim', '2018-10-22', '2018-10-24', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:22:54', '2018-11-01 08:23:43', 'Pasal 112 ayat (1) Jo Pasal 114 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (161, 2, 1, 'SPDP/16/X/2018/Narkoba', '2018-10-18', '2018-10-24', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:29:56', '2018-11-01 08:31:33', 'Pasal 114 Jo Pasal 112 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (162, 2, 1, 'SPDP/105/X/2018/Narkoba', '2018-10-19', '2018-10-22', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:36:20', '2018-11-01 08:36:49', 'Pasal 114 Jo Pasal 112 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (163, 2, 1, 'SPDP/42/X/2018/Reskrim', '2018-10-22', '2018-10-22', NULL, 'Tindak Pidana Kekerasan dalam Rumah tangga', '1', '2018-11-01', '2018-11-01 08:43:55', '2018-11-01 08:44:39', 'Pasal 44 UU RI No.23 tahun 2004');
INSERT INTO `caases` VALUES (164, 2, 1, 'SPDP/24/X/2018/Reskrim', '2018-10-18', '2018-10-22', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:47:39', '2018-11-01 08:48:20', 'Pasal  112  ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (165, 2, 1, 'SPDP/107/X/2018/Res Narkoba', '2018-10-16', '2018-10-22', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:52:49', '2018-11-01 08:53:26', 'Pasal 114 ayat (2) Jo Pasal 112 ayat (2) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (166, 2, 1, 'SPDP/27/X/2018/Reskrim', '2018-10-17', '2018-10-19', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '1', '2018-11-01', '2018-11-01 08:58:30', '2018-11-01 08:59:03', 'Pasal 114 Jo Pasal 112 jo pasal 127 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (167, 1, 3, 'SPDP/72/X/2018/Reskrim', '2018-10-20', '2018-10-24', NULL, 'Tindak Pidana Penganiayan', '1', '2018-11-01', '2018-11-01 09:03:25', '2018-11-01 09:03:57', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (168, 2, 1, 'SPDP/70/X/2018/Reskrim', '2018-10-20', '2018-10-22', NULL, 'Tindak Pidana Pencurian', '1', '2018-11-01', '2018-11-01 09:16:16', '2018-11-01 09:17:00', 'Pasal 363 Jo 53 KUHP');
INSERT INTO `caases` VALUES (169, 2, 3, 'SPDP/25/X/2018/Reskrim', '2018-10-28', '2018-10-29', NULL, 'Tindak Pidana pencurian', '1', '2018-11-01', '2018-11-01 09:20:29', '2018-11-01 09:21:02', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (170, 2, 3, 'SPDP/170/X/2018/Reskrim', '2018-10-19', '2018-10-22', NULL, 'Tindak Pidana Pencurian', '1', '2018-11-01', '2018-11-01 09:25:53', '2018-11-01 09:26:24', 'Pasal 351 KUHP');
INSERT INTO `caases` VALUES (171, 2, 3, 'SPDP//X/2018/Narkoba', '2018-10-22', '2018-10-22', NULL, 'Tindak Pidana Pencurian', '1', '2018-11-01', '2018-11-01 09:41:37', '2018-11-01 09:42:20', 'Pasal 363 Jo Pasal 53 KUHP');
INSERT INTO `caases` VALUES (172, 2, 3, 'SPDP/176/X/2018/Reskrim', '2018-10-27', '2018-10-30', NULL, 'Tindak Pidana Pencurian', '1', '2018-11-01', '2018-11-01 09:45:27', '2018-11-01 09:46:02', 'Pasal 363 Jo 480 KUHP');
INSERT INTO `caases` VALUES (173, 2, 3, 'SPDP/171/X/2018/Reskrim', '2018-10-17', '2018-10-31', NULL, 'Tindak Pidana Penipuan dan atau penggelapan', '1', '2018-11-05', '2018-11-02 05:42:37', '2018-11-05 08:04:42', 'Pasal 378 Jo 372 KUHP');
INSERT INTO `caases` VALUES (174, 2, 1, 'SPDP/178/X/2018/Reskrim', '2018-10-31', '2018-11-05', NULL, 'Tindak Pidana Pencemaran Nama Baik dan atau Penghinaan', '1', '2018-11-06', '2018-11-05 08:13:38', '2018-11-06 08:31:51', 'Pasal 45 ayat 3 UU RI No.19 tahun 2016');
INSERT INTO `caases` VALUES (175, 2, 3, 'SPDP/179/X/2018/Reskrim', '2018-10-31', '2018-11-05', NULL, 'Tindak Pidana Penggelapan', '1', '2018-11-08', '2018-11-06 03:16:01', '2018-11-08 02:31:19', 'Pasal 374 KUHP');
INSERT INTO `caases` VALUES (176, 2, 1, 'SPDP/114/X/2018/Narkoba', '2018-10-30', '2018-11-05', NULL, 'Tindak Pidana Narkotika', '0', NULL, '2018-11-06 03:18:55', '2018-11-06 03:18:55', 'Pasal 114 Jo Pasal 112 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (177, 2, 1, 'SPDP/113/X/2018/Narkoba', '2018-10-30', '2018-11-05', NULL, 'Tindak PIdana Penyalahgunaan Narkotika', '1', '2018-11-08', '2018-11-06 03:21:48', '2018-11-08 02:30:05', 'Pasal 114 Jo Pasal 112 UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (178, 2, 3, 'SPDP/24/X/2018/LL', '2018-10-31', '2018-11-05', NULL, 'Tindak Pidana Kecelakaan lalu lintas dan angkutan jalan', '0', NULL, '2018-11-06 03:26:18', '2018-11-06 03:26:18', 'Pasal 310 (4)(3)(2) UU RI No.22 tahun 2009 tentang Lalu Libtas dan angkutan jalan');
INSERT INTO `caases` VALUES (179, 2, 2, 'SPDP/166/X/2018/Reskrim', '2018-10-11', '2018-11-05', NULL, 'Tindak Pidana Kejahatan terhadap perkawinan', '1', '2018-11-08', '2018-11-06 03:33:55', '2018-11-08 02:28:50', 'Pasal 279 KUHP');
INSERT INTO `caases` VALUES (180, 2, 3, 'SPDP/180/XI/2018/Reskrim', '2018-11-05', '2018-11-06', NULL, 'Tindak Pidana Penggelapan', '0', NULL, '2018-11-06 03:37:36', '2018-11-06 03:37:36', 'Pasal 372 KUHP');
INSERT INTO `caases` VALUES (181, 2, 3, 'SPDP/26/X/2018/Reskirim', '2018-10-01', '2018-11-06', NULL, 'Tindak Pidana Pencurian', '1', '2018-11-08', '2018-11-06 07:15:39', '2018-11-08 02:27:14', 'Pasal 363 KUHP');
INSERT INTO `caases` VALUES (182, 2, 3, 'SPDP/181/XI/2018/Reskrim', '2018-11-07', '2018-11-07', NULL, 'Tindak Pidana Penggelapan', '0', NULL, '2018-11-07 08:36:22', '2018-11-07 08:36:22', 'Pasal 372 KUHP');
INSERT INTO `caases` VALUES (183, 2, 3, 'SPDP/07/I/2019/Reskrim', '2019-12-03', '2019-01-03', NULL, 'Tindak Pidana Pencurian', '1', '2019-01-10', '2019-01-08 05:57:28', '2019-01-10 07:13:39', '363 KUHP');
INSERT INTO `caases` VALUES (184, 1, 3, 'B/28/XII/2018/LL', '2018-12-18', '2018-12-27', NULL, 'Tindak Pidana Kecelakaan Lalu Lintas', '1', '2019-01-08', '2019-01-08 06:02:05', '2019-01-08 06:03:11', '310 (3) UU RI No.22 tahun 2009 tentang Lalu Lintas dan Angkutan Jalan');
INSERT INTO `caases` VALUES (185, 2, 1, 'SPDP/27/XII/2018/Reskrim', '2018-12-21', '2019-01-07', NULL, 'Tindak Pidana yang diduga melakukan persetubuhan dan atau melakukan perbuatan cabul terhadap anak', '1', '2019-01-10', '2019-01-08 06:09:52', '2019-01-10 07:02:08', '81 ayat (1) Jo 82 ayat (1) UU RI No.35 tahun 2014');
INSERT INTO `caases` VALUES (186, 1, 1, 'SPDP/01/I/2019/Reskrim', '2019-01-01', '2019-01-03', NULL, 'Tindak Pidana Persetubuhan terhadap anak dibawah umur', '1', '2019-01-10', '2019-01-08 06:13:39', '2019-01-10 07:01:05', '81,82 UU RI No.35 tahun 2014 Jo UU No.11 tahun 2012');
INSERT INTO `caases` VALUES (187, 2, 3, 'SPDP/19/XII/2018/Reskrim', '2018-12-26', '2018-12-27', NULL, 'Tindak Pidana Pencurian', '1', '2019-01-08', '2019-01-08 06:23:31', '2019-01-08 06:24:09', '363 KUHP');
INSERT INTO `caases` VALUES (188, 2, 1, 'SPDP/01/1/2019/Reskrim', '2019-01-04', '2019-01-07', NULL, 'Tindak pidana kekerasan dalam rumah tangga', '0', NULL, '2019-01-10 06:59:28', '2019-01-10 06:59:28', '44 ayat 1 dan atau ayat 4 UU RI No.23 tahun 2004');
INSERT INTO `caases` VALUES (189, 2, 3, 'SPDP/19/XII/2018/Reskrim', '2018-12-26', '2018-12-27', NULL, 'Tindak Pidana Pencurian', '1', '2019-01-10', '2019-01-10 07:04:07', '2019-01-10 07:04:47', '363 KUHP');
INSERT INTO `caases` VALUES (190, 2, 3, 'B/28/xii/2018/LL', '2018-12-18', '2018-12-27', NULL, 'Tindak Pidana Kecelakaan Lalu Lintas', '1', '2019-01-10', '2019-01-10 07:08:09', '2019-01-10 07:08:43', '310 (3) uu ri nO.22 TAHUN 2009');
INSERT INTO `caases` VALUES (191, 2, 1, 'SPDP/130/XII/2018/Narkoba', '2018-12-28', '2018-12-31', NULL, 'Tindak pidana Narkotika', '1', '2019-01-10', '2019-01-10 07:32:02', '2019-01-10 07:32:40', '114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009 tentang Narkotika');
INSERT INTO `caases` VALUES (192, 2, 1, 'SPDP/80/XII/2018/Reskrim', '2018-12-25', '2018-12-27', NULL, 'Tindak Pidana Narkotika', '1', '2019-01-10', '2019-01-10 07:36:57', '2019-01-10 07:37:50', '114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009');
INSERT INTO `caases` VALUES (193, 2, 3, 'SPDP/201/XII/2018/Reskrim', '2018-12-31', '2019-01-03', NULL, 'Tindak Pidana Pencurian', '1', '2019-01-10', '2019-01-10 07:47:00', '2019-01-10 07:47:31', '363 KUHP');
INSERT INTO `caases` VALUES (194, 2, 1, 'SPDP/02/I/2019/Reskrim', '2019-01-10', '2019-01-10', NULL, 'Tindak Pidana Perlindungan dan Pengelolaan Lingkungan Hidup', '1', '2019-01-16', '2019-01-16 02:47:27', '2019-01-16 02:48:08', 'Pasal 108 Jo 69 UU No.32 tahun 2009  dan atau Pasal 187 Jo 188 KUHP');
INSERT INTO `caases` VALUES (195, 2, 1, 'SPDP/01/1/2019/Reskrim', '2019-01-08', '2019-01-09', NULL, 'Tindak Pidana Narkotika', '1', '2019-01-16', '2019-01-16 02:53:47', '2019-01-16 02:54:35', '114 ayat (1) Jo Pasal 112 ayat (1) UU RI No.35 tahun 2009');
INSERT INTO `caases` VALUES (196, 2, 3, 'SPDP/01/I/2019/Reskrim', '2019-01-01', '2019-01-09', NULL, 'Tindak Pidana Pencurian', '0', NULL, '2019-01-16 02:57:18', '2019-01-16 02:57:18', '363 KUHP');

-- ----------------------------
-- Table structure for case_files
-- ----------------------------
DROP TABLE IF EXISTS `case_files`;
CREATE TABLE `case_files`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomor_berkas` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanngal_berkas` date NOT NULL,
  `rak` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for classifications
-- ----------------------------
DROP TABLE IF EXISTS `classifications`;
CREATE TABLE `classifications`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `klasifikasi` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classifications
-- ----------------------------
INSERT INTO `classifications` VALUES (1, 'TPUL (EUH)', 'TPUL (EUH)', '2018-09-03 02:29:03', '2018-09-03 02:29:03');
INSERT INTO `classifications` VALUES (2, 'KAMTIBUM (EP)', 'KAMTIBUM (EP)', '2018-09-03 02:29:20', '2018-09-03 02:29:20');
INSERT INTO `classifications` VALUES (3, 'OHARDA (EPP)', 'OHARDA (EPP)', '2018-09-03 02:29:39', '2018-09-03 02:29:39');

-- ----------------------------
-- Table structure for courts
-- ----------------------------
DROP TABLE IF EXISTS `courts`;
CREATE TABLE `courts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `dakwaan` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p37` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p38` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p42` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `putusan` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `P44` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `banding` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `kasasi` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of courts
-- ----------------------------
INSERT INTO `courts` VALUES (3, 57, '306/Pid.B/2018/PN Dum', '2018-09-14', '-', '1', '1', '1', '0', '0', '0', '0', '0', '2018-10-10 09:54:00', '2018-10-10 09:54:10');
INSERT INTO `courts` VALUES (4, 103, '146/Pid.B/2018/PN DUM', '2018-05-22', '-', '1', '1', '1', '1', '1', '1', '0', '0', '2018-10-12 09:37:18', '2018-10-12 09:37:18');
INSERT INTO `courts` VALUES (5, 106, '213/Pid.B/2018/PN Dum', '2018-06-04', '-', '1', '1', '1', '1', '1', '1', '0', '0', '2018-10-12 09:57:42', '2018-10-12 09:57:42');
INSERT INTO `courts` VALUES (6, 109, '9/Pid.Pid/2018/PN DUM', '1999-09-13', '-', '1', '1', '1', '1', '1', '1', '0', '0', '2018-10-12 10:21:43', '2018-10-12 10:21:43');
INSERT INTO `courts` VALUES (8, 110, '123456', '2018-10-18', '-', '1', '1', '1', '1', '1', '1', '0', '0', '2018-10-18 02:24:55', '2018-10-19 03:16:34');
INSERT INTO `courts` VALUES (9, 54, '338/Pid.B/2018/PN.Dum.', '2018-10-15', 'pencurian', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 05:55:39', '2018-10-20 15:07:31');
INSERT INTO `courts` VALUES (10, 80, '332/Pen.Pid/2018/PN.Dum', '2018-10-11', 'penadahan', '1', '1', '1', '0', '0', '0', '0', '0', '2018-10-18 05:56:54', '2018-11-07 02:41:36');
INSERT INTO `courts` VALUES (11, 101, '333/Pid.Sus/2018/PN.Dum', '2018-10-11', 'narkotika shabu', '1', '1', '1', '0', '0', '0', '0', '0', '2018-10-18 05:57:45', '2018-11-07 02:41:19');
INSERT INTO `courts` VALUES (12, 23, '329/Pid.Sus/2018/PN.DUM', '2018-10-09', 'Tindak Pidana Narkotika', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:00:57', '2018-10-18 06:00:57');
INSERT INTO `courts` VALUES (13, 23, '329/Pid.Sus/2018/PN.DUM', '2018-10-09', 'Tindak Pidana Narkotika', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:00:57', '2018-10-18 06:00:57');
INSERT INTO `courts` VALUES (14, 78, '339/Pid.B/2018/PN Dum', '2018-10-15', 'Tindak Pidana Pencurian', '0', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:03:56', '2018-10-18 06:03:56');
INSERT INTO `courts` VALUES (15, 22, '340/Pid.Sus/2018/PN.Dum', '2018-10-15', 'Tindak Pidana Narkotika', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:04:03', '2018-10-18 06:04:03');
INSERT INTO `courts` VALUES (16, 22, '340/Pid.Sus/2018/PN.Dum', '2018-10-15', 'Tindak Pidana Narkotika', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:04:57', '2018-10-18 06:04:57');
INSERT INTO `courts` VALUES (17, 22, '340/Pid.Sus/2018/PN.Dum', '2018-10-15', 'Tindak Pidana Narkotika', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:04:58', '2018-10-18 06:04:58');
INSERT INTO `courts` VALUES (18, 78, '339/Pid.B/2018/PN.DUM', '2018-10-15', 'Tindak Pidana Pencurian', '1', '1', '0', '0', '0', '0', '0', '0', '2018-10-18 06:06:16', '2018-10-18 06:06:16');
INSERT INTO `courts` VALUES (19, 78, '339/Pid.B/2018/PN.DUM', '2018-10-15', 'Tindak Pidana Pencurian', '1', '1', '0', '0', '0', '0', '0', '0', '2018-10-18 06:06:16', '2018-10-18 06:06:16');
INSERT INTO `courts` VALUES (20, 100, '338/Pid.B/2018/PN.Dum', '2018-10-15', 'Tindak Pidana Pencurian', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:08:43', '2018-10-18 06:08:43');
INSERT INTO `courts` VALUES (21, 100, '338/Pid.B/2018/PN.Dum', '2018-10-15', 'Tindak Pidana Pencurian', '1', '0', '0', '0', '0', '0', '0', '0', '2018-10-18 06:08:44', '2018-10-18 06:08:44');
INSERT INTO `courts` VALUES (22, 145, '002/zz/2018', '2018-10-22', '-', '0', '0', '0', '0', '0', '0', '0', '0', '2018-10-21 21:23:25', '2018-10-21 21:23:25');
INSERT INTO `courts` VALUES (23, 146, '001/23/2018', '2018-10-22', '-', '0', '0', '0', '1', '1', '1', '0', '0', '2018-10-21 21:23:56', '2018-11-01 04:34:08');
INSERT INTO `courts` VALUES (24, 148, '13/Pi.sus-Anak/2018/PN.Dum', '2018-10-04', 'Tindak Pidana Narkotika', '1', '1', '1', '1', '1', '1', '0', '0', '2018-10-22 01:55:12', '2018-10-22 01:55:12');
INSERT INTO `courts` VALUES (25, 68, '14/Pid.Sus-Anak/2018/PN.Dum', '2018-09-28', 'Tindak Pidana Pencurian', '1', '1', '1', '1', '1', '1', '0', '0', '2018-10-22 02:16:50', '2018-10-22 02:16:50');

-- ----------------------------
-- Table structure for devices
-- ----------------------------
DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `player_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of devices
-- ----------------------------
INSERT INTO `devices` VALUES (13, '06badbf9-900b-426e-b648-2c922ac2c0bc', 9, '2018-10-10 09:06:08', '2018-10-10 09:06:08');
INSERT INTO `devices` VALUES (14, '465fae12-b118-4047-8252-f6f52564bc24', 12, '2018-10-10 09:28:59', '2018-10-10 09:28:59');
INSERT INTO `devices` VALUES (16, 'b8d5e555-78bf-4e0d-bde9-7c67d27ed5cd', 8, '2018-10-11 10:23:49', '2018-10-11 10:23:49');
INSERT INTO `devices` VALUES (30, 'b8d5e555-78bf-4e0d-bde9-7c67d27ed5cd', 16, '2018-10-18 03:46:14', '2018-10-18 03:46:14');
INSERT INTO `devices` VALUES (35, '7cd6ca46-a250-4296-9a0a-fc18f87e1711', 17, '2018-10-30 05:33:55', '2018-10-30 05:33:55');
INSERT INTO `devices` VALUES (36, '43317b8b-3df2-4a3a-b3d7-783fddea3df0', 11, '2018-10-30 08:58:53', '2018-10-30 08:58:53');
INSERT INTO `devices` VALUES (37, 'cef5ef5c-9c42-4d8d-b21d-9bf6d65e3c02', 14, '2018-11-07 02:07:28', '2018-11-07 02:07:28');
INSERT INTO `devices` VALUES (38, 'fc7b9e7b-3162-4860-ae7a-29d5c08b9c2b', 8, '2018-11-07 02:55:54', '2018-11-07 02:55:54');
INSERT INTO `devices` VALUES (39, '4a239cf5-2857-4e21-a416-2b50c8bdaa6f', 16, '2018-11-08 14:24:37', '2018-11-08 14:24:37');

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `diterima` date NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of documents
-- ----------------------------
INSERT INTO `documents` VALUES (7, 17, 'BP/23/IX/2018/Sek Dumai Kota', '2018-09-20', NULL, '-', '2018-09-25 11:34:45', '2018-09-25 11:34:45');
INSERT INTO `documents` VALUES (8, 22, 'B/141/IX/2018/Reskrim', '2018-09-13', NULL, '-', '2018-10-05 09:59:21', '2018-10-05 09:59:21');
INSERT INTO `documents` VALUES (9, 22, 'B/141/IX/2018/Reskrim', '2018-09-13', NULL, '-', '2018-10-05 09:59:41', '2018-10-05 09:59:41');
INSERT INTO `documents` VALUES (10, 22, 'B/141/IX/2018/Reskrim', '2018-09-23', NULL, '-', '2018-10-05 10:00:20', '2018-10-05 10:00:20');
INSERT INTO `documents` VALUES (11, 21, 'B/56/IX/2018/Reskrim', '2018-09-14', NULL, '-', '2018-10-05 10:01:51', '2018-10-05 10:01:51');
INSERT INTO `documents` VALUES (12, 39, 'B/26/IX/2018/Reskrim', '2018-09-20', NULL, '-', '2018-10-05 10:03:29', '2018-10-05 10:03:29');
INSERT INTO `documents` VALUES (13, 46, 'B/206/IX/2018/DmiKota', '2018-09-20', NULL, NULL, '2018-10-05 10:04:43', '2018-10-05 10:04:43');
INSERT INTO `documents` VALUES (14, 34, 'B/IX/2018/Reskrim', '2018-09-20', NULL, '-', '2018-10-05 10:08:13', '2018-10-05 10:08:13');
INSERT INTO `documents` VALUES (20, 54, 'bp/23/vii/2018/rekrim', '2018-08-22', NULL, NULL, '2018-10-10 08:47:51', '2018-10-10 08:47:51');
INSERT INTO `documents` VALUES (21, 57, 'BP/03/VII/2018/Gakkum', '2018-07-12', NULL, 'pencurian Speedboat', '2018-10-10 09:37:01', '2018-10-10 09:37:01');
INSERT INTO `documents` VALUES (22, 57, 'BP-03/VII/2018/Gakkum', '2018-07-12', NULL, NULL, '2018-10-10 09:43:17', '2018-10-10 09:43:17');
INSERT INTO `documents` VALUES (23, 66, 'B/58/IX/2018/Reskrim', '2018-09-27', NULL, '-', '2018-10-11 12:46:53', '2018-10-11 12:46:53');
INSERT INTO `documents` VALUES (24, 67, 'B/228/IX/2018/Reskrim', '2018-09-24', NULL, '-', '2018-10-11 12:51:11', '2018-10-11 12:51:11');
INSERT INTO `documents` VALUES (25, 53, 'B/216/IX/2018/Reskrim', '2018-09-12', NULL, NULL, '2018-10-11 12:52:49', '2018-10-11 12:52:49');
INSERT INTO `documents` VALUES (26, 84, 'B/227/IX/2018/Reskrim', '2018-09-24', NULL, '-', '2018-10-12 03:26:09', '2018-10-12 03:26:09');
INSERT INTO `documents` VALUES (27, 73, 'B/235/IX/2018/Reskrrim', '2018-09-20', NULL, '-', '2018-10-12 03:27:22', '2018-10-12 03:27:22');
INSERT INTO `documents` VALUES (28, 72, 'B/157/IX/2018/Reskrim', '2018-09-17', NULL, '-', '2018-10-12 03:28:08', '2018-10-12 03:28:08');
INSERT INTO `documents` VALUES (29, 69, 'B/261/IX/2018/Reskrim', '2018-09-12', NULL, '-', '2018-10-12 03:29:52', '2018-10-12 03:29:52');
INSERT INTO `documents` VALUES (30, 79, 'B/24/VIII/2018/Reskrim', '2018-08-31', NULL, '-', '2018-10-12 03:31:08', '2018-10-12 03:31:08');
INSERT INTO `documents` VALUES (31, 78, 'B/195/VIII/2018/Reskrim', '2018-08-20', NULL, '-', '2018-10-12 03:33:01', '2018-10-12 03:33:01');
INSERT INTO `documents` VALUES (32, 100, 'BP/25/VIII/2018', '2018-08-22', NULL, '-', '2018-10-12 08:13:03', '2018-10-12 08:13:03');
INSERT INTO `documents` VALUES (33, 101, 'B/972/VIII/2018', '2018-08-28', NULL, '-', '2018-10-12 08:22:22', '2018-10-12 08:22:22');
INSERT INTO `documents` VALUES (34, 80, 'BP/22/VIII/2018', '2018-09-27', NULL, '-', '2018-10-12 08:43:35', '2018-10-12 08:43:35');
INSERT INTO `documents` VALUES (35, 23, 'B/25/IX/2018/Reskrim', '2018-09-07', NULL, '-', '2018-10-12 08:55:23', '2018-10-12 08:55:23');
INSERT INTO `documents` VALUES (36, 102, 'B/141/IX/2018/Reskrim', '2018-09-13', NULL, '-', '2018-10-12 09:12:10', '2018-10-12 09:12:10');
INSERT INTO `documents` VALUES (37, 103, 'B-435/V/2018/ Res Narkoba', '2018-04-16', NULL, '-', '2018-10-12 09:21:56', '2018-10-12 09:21:56');
INSERT INTO `documents` VALUES (38, 104, 'B/206/IX/2018/Reskrim', '2018-09-20', NULL, '-', '2018-10-12 09:22:52', '2018-10-12 09:22:52');
INSERT INTO `documents` VALUES (39, 105, 'B/195/VIII/2018/Reskrim', '2018-08-20', NULL, '-', '2018-10-12 09:32:08', '2018-10-12 09:32:08');
INSERT INTO `documents` VALUES (40, 77, 'B/221/VIII/2018/Reskrim', '2018-08-31', NULL, '-', '2018-10-12 09:42:57', '2018-10-12 09:42:57');
INSERT INTO `documents` VALUES (41, 106, 'BP/27/V/2018/Reskrim', '2018-05-30', NULL, '-', '2018-10-12 09:50:17', '2018-10-12 09:50:17');
INSERT INTO `documents` VALUES (42, 107, 'B/157/IX/2018/Reskrim', '2018-09-17', NULL, '-', '2018-10-12 09:51:41', '2018-10-12 09:51:41');
INSERT INTO `documents` VALUES (43, 108, 'B/36/IX/2018/Reskrim', '2018-09-01', NULL, 'Tindak Pidana Pencurian', '2018-10-12 10:03:01', '2018-10-12 10:03:01');
INSERT INTO `documents` VALUES (44, 109, 'B-205/VIII/2018/Reskrim', '2018-08-10', NULL, '-', '2018-10-12 10:07:49', '2018-10-12 10:07:49');
INSERT INTO `documents` VALUES (45, 110, '123456', '2018-10-15', NULL, '-', '2018-10-14 17:31:24', '2018-10-14 17:31:24');
INSERT INTO `documents` VALUES (46, 74, 'B/37/IX/2018/Reskrim', '2018-09-10', NULL, 'Tindak Pidana Pencurian dan atau Pertolongan Jahat', '2018-10-16 09:14:46', '2018-10-16 09:14:46');
INSERT INTO `documents` VALUES (47, 58, '0908209', '2018-10-22', NULL, '-', '2018-10-21 20:11:54', '2018-10-21 20:11:54');
INSERT INTO `documents` VALUES (48, 145, '001/zz/2018', '2018-10-22', NULL, '-', '2018-10-21 20:30:47', '2018-10-21 20:30:47');
INSERT INTO `documents` VALUES (49, 146, '001/bp/10/2018', '2018-10-22', NULL, '-', '2018-10-21 21:15:11', '2018-10-21 21:15:11');
INSERT INTO `documents` VALUES (50, 148, 'B/1063/IX/2018/Res  Narkoba', '2018-09-19', NULL, 'Tindak Pidana Narkotika', '2018-10-22 01:47:51', '2018-10-22 01:47:51');
INSERT INTO `documents` VALUES (51, 68, 'B-236/IX/2018/Reskrim', '2018-09-19', NULL, 'Tindak Pidana Pencurian', '2018-10-22 02:05:32', '2018-10-22 02:05:32');
INSERT INTO `documents` VALUES (52, 59, 'B/106/X/2018/Reskrim', '2018-10-15', NULL, 'Tindak Pidana Pencurian', '2018-10-22 02:20:11', '2018-10-22 02:20:11');
INSERT INTO `documents` VALUES (53, 47, 'B/29/X/2018/Reskrim', '2018-10-15', NULL, 'Tindak Pidana Narkotika', '2018-10-22 02:22:19', '2018-10-22 02:22:19');
INSERT INTO `documents` VALUES (54, 45, 'B-28/X/2018/Reskrim', '2018-10-12', NULL, 'Tindak Pidana Narkotika', '2018-10-22 02:25:58', '2018-10-22 02:25:58');
INSERT INTO `documents` VALUES (55, 62, 'B-64/X/2018/Reskrim', '2018-10-16', NULL, 'Tindak Pidana Pencurian', '2018-10-22 02:28:28', '2018-10-22 02:28:28');
INSERT INTO `documents` VALUES (56, 123, 'B/63/X/2018/Reskrim', '2018-10-16', NULL, 'Tindak Pidana Pencurian', '2018-10-22 02:29:24', '2018-10-22 02:29:24');
INSERT INTO `documents` VALUES (57, 29, 'B-1212/X/2018/nARKOBA', '2018-10-18', NULL, 'Tindak Pidana Narkotika', '2018-10-22 02:32:08', '2018-10-22 02:32:08');
INSERT INTO `documents` VALUES (58, 85, 'B/232/X/2018', '2018-10-29', NULL, 'Pencurian dengan kekerasan', '2018-10-30 04:17:22', '2018-10-30 04:17:22');
INSERT INTO `documents` VALUES (59, 128, 'B/33/X/2018/Reskrim', '2018-10-29', NULL, 'Tindak Pidana Pencurian pada hari rabu tanggal 03 Oktober 2018 di Jalan Swadaya Rt.003 Kel. Bukti Batrem Kec. Dumai Timur', '2018-10-30 04:51:54', '2018-10-30 04:51:54');
INSERT INTO `documents` VALUES (60, 43, 'BP/24/IX/2018/RES DMI/ SEK DMI KOTA', '2018-10-06', NULL, 'Narkotika splitan dgn Verren alias Rere', '2018-10-31 06:06:49', '2018-10-31 06:06:49');
INSERT INTO `documents` VALUES (61, 138, 'B/1250/X/2018/Narkoba', '2018-10-24', NULL, 'Tindak Pidana Narkotika', '2018-11-01 07:48:22', '2018-11-01 07:48:22');
INSERT INTO `documents` VALUES (62, 61, 'B/256/X/2018/Reskrim', '2018-10-25', NULL, 'Penganiayaan dan atau penghinaan', '2018-11-01 07:49:50', '2018-11-01 07:49:50');
INSERT INTO `documents` VALUES (63, 153, 'B/263/X/2018/Reskrim', '2018-10-24', NULL, 'Tindak Pidana Penganiayaan', '2018-11-01 07:50:49', '2018-11-01 07:50:49');
INSERT INTO `documents` VALUES (64, 126, 'B/252/X/2018/Reskrim', '2018-10-25', NULL, 'Tindak Pidana Pencurian', '2018-11-01 07:51:47', '2018-11-01 07:51:47');
INSERT INTO `documents` VALUES (65, 147, 'B/276/X/2018/Reskrim', '2018-10-25', NULL, 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 07:52:56', '2018-11-01 07:52:56');
INSERT INTO `documents` VALUES (66, 81, 'B/66/X/2018/Reskrim', '2018-10-23', NULL, 'Tindak Pidana Persetubuhan dan Pencabulan terhadap anak dibawah umur', '2018-11-01 07:54:38', '2018-11-01 07:54:38');
INSERT INTO `documents` VALUES (67, 65, 'B/68/X/2018/Reskrim', '2018-10-22', NULL, 'Tindak Pidana Pencurian', '2018-11-01 07:55:55', '2018-11-01 07:55:55');
INSERT INTO `documents` VALUES (68, 64, 'B/70/X/2018/Reskrim', '2018-10-25', NULL, 'Tindak Pidana Pencurian', '2018-11-01 08:02:12', '2018-11-01 08:02:12');
INSERT INTO `documents` VALUES (69, 95, 'B/69/X/2018/Reskrim', '2018-10-26', NULL, 'Tindak Pidana Pencurian', '2018-11-02 07:29:02', '2018-11-02 07:29:02');
INSERT INTO `documents` VALUES (70, 120, 'B/39/XI/2018/Reskrim', '2018-11-01', NULL, 'Tindak PIDANA Pencurian', '2018-11-02 07:29:58', '2018-11-02 07:29:58');
INSERT INTO `documents` VALUES (71, 63, 'B/75/X/2018/Reskrim', '2018-10-31', NULL, 'Tindak Pidana Pencurian', '2018-11-05 08:15:44', '2018-11-05 08:15:44');

-- ----------------------------
-- Table structure for eighteens
-- ----------------------------
DROP TABLE IF EXISTS `eighteens`;
CREATE TABLE `eighteens`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of eighteens
-- ----------------------------
INSERT INTO `eighteens` VALUES (6, 57, 'B-1098/N.4.13/epp.1/07/2018', '2018-07-23', 'belum lengkap', '2018-10-10 09:39:52', '2018-10-10 09:39:52');
INSERT INTO `eighteens` VALUES (7, 110, '12345696', '2018-10-15', '-', '2018-10-14 17:31:48', '2018-10-14 17:31:48');
INSERT INTO `eighteens` VALUES (8, 74, 'B-1628/N.4.13/Epp.1/10/2018', '2018-10-01', 'Tindak Pidana Pencurian dan atau Pertolongan Jahat', '2018-10-16 09:16:18', '2018-10-16 09:16:18');
INSERT INTO `eighteens` VALUES (9, 146, '002/zz/2018', '2018-10-22', '-', '2018-10-21 21:16:16', '2018-10-21 21:16:16');

-- ----------------------------
-- Table structure for executions
-- ----------------------------
DROP TABLE IF EXISTS `executions`;
CREATE TABLE `executions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ba17` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ba20` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ba23` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of executions
-- ----------------------------
INSERT INTO `executions` VALUES (2, 103, 'Print - 1253/N.4.13/Euh/09/2018', '2005-10-08', '-', '1', '1', '1', '2018-10-12 09:39:28', '2018-10-12 09:39:28');
INSERT INTO `executions` VALUES (3, 106, 'PRINT-1254/N.4.13/Epp/09/2018', '2018-10-08', '-', '1', '1', '1', '2018-10-12 09:58:50', '2018-10-12 09:58:50');
INSERT INTO `executions` VALUES (4, 109, 'PRINT-1247/N.4.13/EUH.3/10/2018', '2004-10-03', '-', '1', '1', '1', '2018-10-12 10:22:52', '2018-10-12 10:22:52');
INSERT INTO `executions` VALUES (6, 110, '12356', '2018-10-19', 'Tidak ada', '0', '0', '0', '2018-10-19 06:00:26', '2018-10-19 06:00:26');
INSERT INTO `executions` VALUES (7, 145, '008/zz/2018', '2018-10-30', '-', '0', '0', '0', '2018-10-21 21:24:53', '2018-10-21 21:24:53');

-- ----------------------------
-- Table structure for handovers
-- ----------------------------
DROP TABLE IF EXISTS `handovers`;
CREATE TABLE `handovers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `p29` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p31` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p33` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `p34` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of handovers
-- ----------------------------
INSERT INTO `handovers` VALUES (5, 57, 'B-117/pid.b/09/2018', '2018-09-14', NULL, '1', '1', '1', '1', '2018-10-10 09:49:47', '2018-10-10 09:49:47');
INSERT INTO `handovers` VALUES (6, 54, 'PRINT-1227/N.4.13/Epp.2/09/2018', '2003-09-27', '-', '1', '1', '1', '1', '2018-10-12 05:03:57', '2018-10-12 05:03:57');
INSERT INTO `handovers` VALUES (7, 101, '344/PID.B/2018', '2018-10-10', '-', '1', '1', '1', '1', '2018-10-12 08:30:02', '2018-10-12 08:30:02');
INSERT INTO `handovers` VALUES (8, 80, '345/Pid.B/10/2018', '2018-10-10', '-', '1', '1', '1', '1', '2018-10-12 08:50:05', '2018-10-12 08:50:05');
INSERT INTO `handovers` VALUES (9, 23, '341/Pid.B/09/2018', '2018-10-02', '-', '1', '1', '1', '1', '2018-10-12 09:02:40', '2018-10-12 09:02:40');
INSERT INTO `handovers` VALUES (10, 22, '352/Pid.B/09/2018', '2018-10-11', '-', '1', '1', '1', '1', '2018-10-12 09:17:00', '2018-10-12 09:17:00');
INSERT INTO `handovers` VALUES (11, 103, '152/Pid.B/05/2018', '2018-05-15', '-', '1', '1', '1', '1', '2018-10-12 09:30:05', '2018-10-12 09:30:05');
INSERT INTO `handovers` VALUES (12, 78, '351/Pid.B/10/2018', '2018-10-11', '-', '1', '1', '1', '1', '2018-10-12 09:35:07', '2018-10-12 09:35:07');
INSERT INTO `handovers` VALUES (13, 106, 'B-217/Pid.B/07/2018', '2018-06-02', '-', '1', '1', '1', '1', '2018-10-12 09:56:12', '2018-10-12 09:56:12');
INSERT INTO `handovers` VALUES (14, 109, '309/PID.B/2018', '2002-09-13', '-', '1', '1', '1', '1', '2018-10-12 10:20:05', '2018-10-12 10:20:05');
INSERT INTO `handovers` VALUES (18, 110, '123445', '2018-10-18', '-', '1', '1', '1', '1', '2018-10-18 02:07:06', '2018-10-18 02:07:06');
INSERT INTO `handovers` VALUES (19, 100, '350/Pid.B/10/2018', '2018-10-11', 'Tindak Pidana Pencurian', '1', '1', '1', '1', '2018-10-18 06:07:57', '2018-10-18 06:07:57');
INSERT INTO `handovers` VALUES (20, 145, 'p16a/10.2018', '2018-10-22', '-', '1', '1', '1', '1', '2018-10-21 21:19:53', '2018-10-21 21:19:53');
INSERT INTO `handovers` VALUES (21, 146, 'p16a/10.2018', '2018-10-22', '-', '1', '1', '1', '1', '2018-10-21 21:22:06', '2018-10-21 21:22:06');
INSERT INTO `handovers` VALUES (22, 148, '332/Pid.B/09/2018', '2018-09-27', 'Tindak Pidana Narkotika', '1', '1', '1', '1', '2018-10-22 01:53:57', '2018-10-22 01:53:57');
INSERT INTO `handovers` VALUES (23, 68, '339/Pid.B/09/2018', '2018-09-27', 'Tindak Pidana Pencurian', '1', '1', '1', '1', '2018-10-22 02:14:00', '2018-10-22 02:14:00');

-- ----------------------------
-- Table structure for investigators
-- ----------------------------
DROP TABLE IF EXISTS `investigators`;
CREATE TABLE `investigators`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `instansi` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of investigators
-- ----------------------------
INSERT INTO `investigators` VALUES (6, '94020164', 'ERNANDO PRAYOGA', 'Polsek Kota Dumai', '-', '2018-09-25 11:12:23', '2018-10-02 04:33:47');
INSERT INTO `investigators` VALUES (7, '86030292', 'JEFRIZAL', 'Polsek Kota Dumai', '-', '2018-09-25 11:12:52', '2018-10-02 04:33:51');
INSERT INTO `investigators` VALUES (8, '85121344', 'KASMANDRI,S.Sos', 'Polsek Kota Dumai', '-', '2018-09-26 10:14:08', '2018-10-02 04:33:54');
INSERT INTO `investigators` VALUES (9, '87011390', 'S.B HUTAGAOL,S.Sos', 'Polsek Kota Dumai', '-', '2018-09-26 10:15:04', '2018-10-02 04:34:01');
INSERT INTO `investigators` VALUES (10, '88080681', 'AGUS PRASATYA', 'Polsek Dumai Barat', '-', '2018-09-26 10:16:49', '2018-10-02 04:37:49');
INSERT INTO `investigators` VALUES (11, '84041148', 'ABDI B.K SIREGAR', 'Polsek Dumai Barat', '-', '2018-09-26 10:17:47', '2018-10-02 04:38:10');
INSERT INTO `investigators` VALUES (12, '81030213', 'ALPAJRI', 'Polsek Dumai Barat', '-', '2018-09-26 10:18:17', '2018-10-02 04:37:45');
INSERT INTO `investigators` VALUES (13, '96070370', 'JULIA SIMAMORA', 'Polsek Dumai Barat', '-', '2018-09-26 10:18:49', '2018-10-02 04:40:17');
INSERT INTO `investigators` VALUES (14, '95110519', 'PUTRI NOVIANA', 'Polsek Dumai Barat', '-', '2018-09-26 10:19:17', '2018-10-02 04:38:35');
INSERT INTO `investigators` VALUES (15, '65060568', 'AL AMRAN', 'Polsek Medang Kampai', '-', '2018-09-26 10:21:19', '2018-10-02 04:38:15');
INSERT INTO `investigators` VALUES (16, '81100643', 'AMAN HUTAPEA', 'Polsek Medang Kampai', '-', '2018-09-26 10:21:41', '2018-10-02 04:40:40');
INSERT INTO `investigators` VALUES (17, '84060110', 'HADI HIDAYAT,S.Sos', 'Polsek Medang Kampai', '0899999', '2018-09-26 10:22:17', '2018-09-26 10:22:17');
INSERT INTO `investigators` VALUES (18, '88090324', 'DEDE MUKHSINI', 'Polsek Medang Kampai', '089999', '2018-09-26 10:22:46', '2018-09-26 10:22:46');
INSERT INTO `investigators` VALUES (19, '86070215', 'ARNEBEN PUTRA SILABAN', 'Polres Dumai', '089999', '2018-09-26 10:25:14', '2018-09-26 10:25:14');
INSERT INTO `investigators` VALUES (20, '84091185', 'Y.N.M.HASIBUAN', 'Polres Dumai', '089999', '2018-09-26 10:25:57', '2018-09-26 10:25:57');
INSERT INTO `investigators` VALUES (21, '85051505', 'BOY RAMADHANY.K', 'Polres Dumai', '08999', '2018-09-26 10:26:21', '2018-09-26 10:26:21');
INSERT INTO `investigators` VALUES (22, '79030999', 'AZUAR', 'Polres Dumai', '0899999', '2018-09-26 10:28:06', '2018-09-26 10:28:06');
INSERT INTO `investigators` VALUES (23, '85081543', 'HENGKI', 'Polres Dumai', '0899', '2018-09-26 10:28:24', '2018-09-26 10:28:24');
INSERT INTO `investigators` VALUES (24, '87100788', 'OCTAVIANUS MANIK,S.Sos', 'Polres Dumai', '087777', '2018-09-26 10:28:49', '2018-09-26 10:28:49');
INSERT INTO `investigators` VALUES (25, '90110137', 'PANCA SANJAYA', 'Polres Dumai', '0899999', '2018-09-26 10:29:11', '2018-09-26 10:29:11');
INSERT INTO `investigators` VALUES (26, '93091144', 'RENDI ARISANDI,SH', 'Polres Dumai', '089999', '2018-09-26 10:29:42', '2018-09-26 10:29:42');
INSERT INTO `investigators` VALUES (27, '80080681', 'RAJA JUNAIDI,SH', 'Polsek Dumai Timur', '087777', '2018-09-26 10:31:47', '2018-09-26 10:31:47');
INSERT INTO `investigators` VALUES (28, '83090365', 'B.SIBARANI', 'Polsek Dumai Timur', '09888', '2018-09-26 10:33:08', '2018-09-26 10:33:08');
INSERT INTO `investigators` VALUES (29, '83061187', 'JUMARI', 'Polsek Dumai Timur', '0899999', '2018-09-26 10:33:35', '2018-09-26 10:33:35');
INSERT INTO `investigators` VALUES (30, '84041731', 'S.SEMBIRNG', 'Polsek Dumai Timur', '089999', '2018-09-26 10:33:57', '2018-09-26 10:33:57');
INSERT INTO `investigators` VALUES (31, '95090849', 'WILLYAM FRANS.P', 'Polsek Dumai Timur', '0899999', '2018-09-26 10:34:28', '2018-09-26 10:34:28');
INSERT INTO `investigators` VALUES (32, '90030379', 'NUR RAHIM.S.I.K', 'Polres Dumai', '0899999', '2018-09-26 10:37:05', '2018-09-26 10:37:05');
INSERT INTO `investigators` VALUES (33, '65120242', 'JONER HUTABALIAN', 'Polres Dumai', '0899999', '2018-09-26 10:37:27', '2018-09-26 10:37:27');
INSERT INTO `investigators` VALUES (34, '82110321', 'DANI YOSRIZAL', 'Polres Dumai', '0899999', '2018-09-26 10:37:46', '2018-09-26 10:37:46');
INSERT INTO `investigators` VALUES (35, '83040103', 'ANTON APANDI', 'Polres Dumai', '0899999', '2018-09-26 10:38:05', '2018-09-26 10:38:05');
INSERT INTO `investigators` VALUES (36, '85061563', 'JENFERI PASARIBU', 'Polres Dumai', '0899999', '2018-09-26 10:38:28', '2018-09-26 10:38:28');
INSERT INTO `investigators` VALUES (37, '95060657', 'AGNES GLODYA', 'Polres Dumai', '0899999', '2018-09-26 10:38:58', '2018-09-26 10:38:58');
INSERT INTO `investigators` VALUES (38, '96070904', 'YULIANTI PRATIKA', 'Polres Dumai', '0899999', '2018-09-26 10:39:32', '2018-09-26 10:39:32');
INSERT INTO `investigators` VALUES (39, '76071048', 'H.J.HUTAHAEAN', 'Polres Dumai', '0899999', '2018-09-26 10:40:15', '2018-09-26 10:40:15');
INSERT INTO `investigators` VALUES (40, '83010835', 'JON RIZAL', 'Polres Dumai', '0899999', '2018-09-26 10:40:40', '2018-09-26 10:40:40');
INSERT INTO `investigators` VALUES (41, '92080642', 'FETRO SILABAN', 'Polres Dumai', '0899999', '2018-09-26 10:41:04', '2018-09-26 10:41:04');
INSERT INTO `investigators` VALUES (42, '97020205', 'BAYU AJI NUGROHO', 'Polres Dumai', '0899999', '2018-09-26 10:41:29', '2018-09-26 10:41:29');
INSERT INTO `investigators` VALUES (43, '79041254', 'F.SIMANJUNTAK', 'Polres Dumai', '-', '2018-09-26 10:43:49', '2018-10-02 04:38:58');
INSERT INTO `investigators` VALUES (44, '82060990', 'SANDIMAN ZEBUA', 'Polres Dumai', '0899999', '2018-09-26 10:44:08', '2018-09-26 10:44:08');
INSERT INTO `investigators` VALUES (45, '85121283', 'NOPEL SILITONGA', 'Polres Dumai', '0899999', '2018-09-26 10:44:26', '2018-09-26 10:44:26');
INSERT INTO `investigators` VALUES (46, '96081152', 'KANZI FATHAN,S.Tr.K', 'Polres Dumai', '-', '2018-09-26 10:44:55', '2018-10-02 04:41:31');
INSERT INTO `investigators` VALUES (47, '75100627', 'NOVIA INDRA', '75100627', '-', '2018-09-26 10:45:43', '2018-10-02 04:37:59');
INSERT INTO `investigators` VALUES (48, '65010250', 'DIMPO SINAGA', 'Polsek Dumai Barat', '-', '2018-09-26 10:46:10', '2018-10-02 04:41:25');
INSERT INTO `investigators` VALUES (49, '83040134', 'PRENGKI ADE CANDRA', 'Polsek Dumai Timur', '-', '2018-09-26 10:47:55', '2018-10-02 04:42:16');
INSERT INTO `investigators` VALUES (50, '85061091', 'RISKI RIADY SIREGAR', 'Polsek Dumai Timur', '-', '2018-09-26 10:48:22', '2018-10-02 04:42:28');
INSERT INTO `investigators` VALUES (51, '86031196', 'HENDRA GUNAWAN', 'Polsek Dumai Timur', '-', '2018-09-26 10:48:41', '2018-10-02 04:42:32');
INSERT INTO `investigators` VALUES (52, '87061142', 'IRVAN SIPAHUTAR', 'Polsek Dumai Timur', '-', '2018-09-26 10:48:59', '2018-10-02 04:42:21');
INSERT INTO `investigators` VALUES (53, '71110405', 'JOKO FRASANTA', 'Polres Dumai', '-', '2018-09-26 10:50:46', '2018-10-02 04:38:40');
INSERT INTO `investigators` VALUES (54, '80100705', 'HENDRI', 'Polres Dumai', '-', '2018-09-26 10:51:09', '2018-10-02 04:41:45');
INSERT INTO `investigators` VALUES (55, '79110968', 'ESSARY', 'Polres Dumai', '0899999', '2018-09-26 10:51:29', '2018-09-26 10:51:29');
INSERT INTO `investigators` VALUES (56, '82110890', 'HARINOVA', 'Polres Dumai', '-', '2018-09-26 10:51:43', '2018-10-02 04:40:40');
INSERT INTO `investigators` VALUES (57, '84061035', 'WAN BOBY DHARMAWAN', 'Polres Dumai', '-', '2018-09-26 10:52:06', '2018-10-02 04:40:46');
INSERT INTO `investigators` VALUES (58, '86100378', 'RAHMAD SETIAWADI', 'Polres Dumai', '-', '2018-09-26 10:52:25', '2018-10-02 04:40:30');
INSERT INTO `investigators` VALUES (59, '86091600', 'ROMI SEPTIANDA', 'Polres Dumai', '-', '2018-09-26 10:52:50', '2018-10-02 04:33:21');
INSERT INTO `investigators` VALUES (60, '83040218', 'MUFID ABDILLAH', 'Polres Dumai', '-', '2018-09-26 10:54:51', '2018-10-02 04:40:59');
INSERT INTO `investigators` VALUES (61, '94030223', 'MUHAMMAD YUDA', 'Polres Dumai', '-', '2018-09-26 10:55:07', '2018-10-02 04:40:53');
INSERT INTO `investigators` VALUES (62, '95030197', 'RIO KURNIADI', 'Polres Dumai', '-', '2018-09-26 10:55:27', '2018-10-02 04:40:35');
INSERT INTO `investigators` VALUES (63, '77110189', 'HORAS PASARIBU', 'Polsek Bukit Kapur', '-', '2018-09-26 11:04:14', '2018-10-02 04:40:24');
INSERT INTO `investigators` VALUES (64, '87050563', 'SUTRISNO', 'Polsek Bukit Kapur', '-', '2018-09-26 11:04:47', '2018-10-02 04:38:47');
INSERT INTO `investigators` VALUES (65, '884071532', 'NAFRIDO', 'Polsek Bukit Kapur', '-', '2018-09-26 11:05:21', '2018-10-02 04:38:06');
INSERT INTO `investigators` VALUES (66, '84121818', 'CRISTY.R HUTAPEA', 'Polsek Sungai Sembilan', '-', '2018-10-02 04:10:43', '2018-10-02 04:10:43');
INSERT INTO `investigators` VALUES (67, '13094/P', 'Syaiful Simanjuntak, S.H.,M.H.', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:25:30', '2018-10-02 04:33:03');
INSERT INTO `investigators` VALUES (68, '14677/P', 'Zurahim, S.H., M.H.', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:26:17', '2018-10-02 04:26:17');
INSERT INTO `investigators` VALUES (69, '17653/P', 'Fitriaya C.Ardi, S.E', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:27:34', '2018-10-02 04:27:34');
INSERT INTO `investigators` VALUES (70, '19487/P', 'Ahmad Muzakki', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:28:08', '2018-10-02 04:28:08');
INSERT INTO `investigators` VALUES (71, '21382/P', 'Khairul Amri Sutorus', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:28:33', '2018-10-02 04:28:33');
INSERT INTO `investigators` VALUES (72, '71823/P', 'Agus Harianto', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:29:01', '2018-10-02 04:29:01');
INSERT INTO `investigators` VALUES (73, '16292/P', 'M. Jamil', 'Pangkalan TNI AL Dumai', '-', '2018-10-02 04:29:23', '2018-10-02 04:29:23');
INSERT INTO `investigators` VALUES (75, '17898/P', 'Nano Harsono,S.Sos', 'Pangkalan TNI AL Dumai', '-', '2018-10-10 06:23:19', '2018-10-10 06:23:19');
INSERT INTO `investigators` VALUES (76, '20719/P', 'Arif Prasetya.W, ST.Han,MM', 'Pangkalan TNI AL Dumai', '-', '2018-10-10 06:24:21', '2018-10-10 06:24:21');
INSERT INTO `investigators` VALUES (77, '61040749', 'Ilyas', 'Pol Air', '-', '2018-10-11 10:01:31', '2018-10-11 10:01:31');
INSERT INTO `investigators` VALUES (78, '70040068', 'Aris Marpaung', 'Pol Air', '-', '2018-10-11 10:02:02', '2018-10-11 10:02:02');
INSERT INTO `investigators` VALUES (79, '82120152', 'Donny Arman', 'Pol Air', '-', '2018-10-11 10:02:22', '2018-10-11 10:02:22');
INSERT INTO `investigators` VALUES (80, '86071825', 'Heru Andrias', 'Pol Air', '-', '2018-10-11 10:02:51', '2018-10-11 10:02:51');
INSERT INTO `investigators` VALUES (81, '88010670', 'Ade Yudhi.P', 'Pol Air', '-', '2018-10-11 10:03:29', '2018-10-11 10:03:29');
INSERT INTO `investigators` VALUES (82, '84010856', 'Gunawan', 'Pol Air', '-', '2018-10-11 10:04:38', '2018-10-11 10:04:38');
INSERT INTO `investigators` VALUES (83, '74010091', 'Febri Zuhdi', 'Pol Air', '-', '2018-10-11 10:05:25', '2018-10-11 10:05:25');
INSERT INTO `investigators` VALUES (84, '88020769', 'D.Sibarani', 'Polsek Bukit Kapur', '-', '2018-10-12 02:32:15', '2018-10-12 02:32:15');
INSERT INTO `investigators` VALUES (85, '92010386', 'TONY', 'Polres Dumai (lantas)', '-', '2018-10-12 02:43:14', '2018-10-12 02:43:14');
INSERT INTO `investigators` VALUES (86, '83111290', 'Bernard F.Panggabean', 'Polsek Bukit Kapur', '-', '2018-10-16 09:02:58', '2018-10-16 09:02:58');
INSERT INTO `investigators` VALUES (87, '90070342', 'Harianto,SE', 'Polres`Dumai', '-', '2018-10-18 07:02:02', '2018-10-18 07:02:02');
INSERT INTO `investigators` VALUES (88, '86041978', 'Wahyu Erdianto', 'Polres`Dumai', '-', '2018-10-18 07:35:49', '2018-10-18 07:35:49');
INSERT INTO `investigators` VALUES (89, '90040341', 'Leandris Bob Sinaga', 'Polsek Dumai Kota', '-', '2018-10-18 07:43:44', '2018-10-18 07:43:44');
INSERT INTO `investigators` VALUES (90, '96060497', 'Iis Soleha Razami', 'Polsek Dumai Timur', '-', '2018-11-01 08:38:40', '2018-11-01 08:38:40');
INSERT INTO `investigators` VALUES (91, '99061035', 'William Frans Panjaitan', 'Polsek Dumai Timur', '-', '2018-11-02 05:38:42', '2018-11-02 05:38:42');
INSERT INTO `investigators` VALUES (92, '94070681', 'Yason Gamaliel', 'Polres Dumai', '-', '2018-11-05 08:05:41', '2018-11-05 08:05:41');
INSERT INTO `investigators` VALUES (93, '95040110', 'Rinaldi Eka Surya', 'Polres Dumai', '-', '2018-11-06 03:22:55', '2018-11-06 03:22:55');
INSERT INTO `investigators` VALUES (94, '94020323', 'Muhammad Yuda', 'Polres Dumai', '-', '2018-11-06 03:35:13', '2018-11-06 03:35:13');
INSERT INTO `investigators` VALUES (95, '86120231', 'Ika Wulandari', 'Polsek Bukit Kapur', '-', '2019-01-08 06:04:33', '2019-01-08 06:04:33');

-- ----------------------------
-- Table structure for lawbooks
-- ----------------------------
DROP TABLE IF EXISTS `lawbooks`;
CREATE TABLE `lawbooks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kitab` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tentang` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `golongan` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pangkat` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of levels
-- ----------------------------
INSERT INTO `levels` VALUES (1, 'IV/b', 'Jaksa Utama Pratama', '2018-09-03 09:42:03', '2018-09-26 01:01:57');
INSERT INTO `levels` VALUES (2, 'V/a', 'Jaksa Madya', '2018-09-03 09:42:30', '2018-09-26 01:03:29');
INSERT INTO `levels` VALUES (3, 'III/d', 'Jaksa Muda', '2018-09-21 04:50:26', '2018-09-26 01:03:03');
INSERT INTO `levels` VALUES (4, 'III/c', 'Jaksa Pratama', '2018-09-25 10:04:33', '2018-09-26 01:03:51');
INSERT INTO `levels` VALUES (5, 'III/b', 'Ajun Jaksa', '2018-09-25 10:43:54', '2018-09-26 01:04:28');
INSERT INTO `levels` VALUES (6, 'III/a', 'Ajun Jaksa Madya', '2018-09-25 11:01:26', '2018-09-26 01:04:46');
INSERT INTO `levels` VALUES (7, 'III/b', 'Muda Wira TU', '2018-09-26 01:06:08', '2018-09-26 01:06:08');
INSERT INTO `levels` VALUES (8, 'III/a', 'Yuana Wira TU', '2018-09-26 01:06:31', '2018-09-26 01:06:31');
INSERT INTO `levels` VALUES (9, 'II/d', 'Sena Darma TU', '2018-09-26 01:07:05', '2018-09-26 01:07:05');
INSERT INTO `levels` VALUES (10, 'III/c', 'Madya Dharma TU', '2018-09-26 01:07:58', '2018-09-26 01:07:58');
INSERT INTO `levels` VALUES (11, 'II/b', 'Muda Dharma TU', '2018-09-26 01:08:21', '2018-09-26 01:08:21');
INSERT INTO `levels` VALUES (12, 'II/a', 'Yuana Dharma TU', '2018-09-26 01:08:46', '2018-09-26 01:08:46');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_08_20_235807_create_suspects_table', 1);
INSERT INTO `migrations` VALUES (4, '2018_08_21_000018_create_investigators_table', 1);
INSERT INTO `migrations` VALUES (5, '2018_08_21_000128_create_cases_table', 1);
INSERT INTO `migrations` VALUES (6, '2018_08_21_000200_create_classifications_table', 1);
INSERT INTO `migrations` VALUES (7, '2018_08_21_000327_create_lawbooks_table', 1);
INSERT INTO `migrations` VALUES (8, '2018_08_21_000512_create_positions_table', 1);
INSERT INTO `migrations` VALUES (9, '2018_08_21_003839_create_types_table', 1);
INSERT INTO `migrations` VALUES (10, '2018_08_29_020655_create_levels_table', 1);
INSERT INTO `migrations` VALUES (11, '2018_08_30_073830_create_profiles_table', 1);
INSERT INTO `migrations` VALUES (12, '2018_08_30_075622_create_case_files_table', 1);
INSERT INTO `migrations` VALUES (13, '2018_09_03_005552_create_sixteens_table', 1);
INSERT INTO `migrations` VALUES (14, '2018_09_05_034943_create_roles_table', 2);
INSERT INTO `migrations` VALUES (15, '2016_06_01_000001_create_oauth_auth_codes_table', 3);
INSERT INTO `migrations` VALUES (16, '2016_06_01_000002_create_oauth_access_tokens_table', 3);
INSERT INTO `migrations` VALUES (17, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3);
INSERT INTO `migrations` VALUES (18, '2016_06_01_000004_create_oauth_clients_table', 3);
INSERT INTO `migrations` VALUES (19, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3);
INSERT INTO `migrations` VALUES (20, '2018_09_18_023535_create_devices_table', 4);
INSERT INTO `migrations` VALUES (21, '2018_09_18_150740_create_seventeens_table', 5);
INSERT INTO `migrations` VALUES (22, '2018_09_18_150905_create_eighteens_table', 5);
INSERT INTO `migrations` VALUES (23, '2018_09_18_150934_create_twenties_table', 5);
INSERT INTO `migrations` VALUES (24, '2018_09_18_150945_create_twentyones_table', 5);
INSERT INTO `migrations` VALUES (25, '2018_09_18_204331_create_documents_table', 5);
INSERT INTO `migrations` VALUES (26, '2018_09_27_103953_create_twentyone_as_table', 6);
INSERT INTO `migrations` VALUES (27, '2018_10_02_090825_create_sixtenas_table', 7);
INSERT INTO `migrations` VALUES (28, '2018_10_03_021619_create_handovers_table', 8);
INSERT INTO `migrations` VALUES (29, '2018_10_03_021649_create_courts_table', 8);
INSERT INTO `migrations` VALUES (30, '2018_10_09_235611_create_executions_table', 9);
INSERT INTO `migrations` VALUES (31, '2018_10_13_064830_create_archives_table', 10);

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_access_tokens_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------
INSERT INTO `oauth_access_tokens` VALUES ('005fb19295d6dab0c8ebf9cdf0caa014f83c3e524779c8a11e4b617aa9555c0b2454522121dd03ef', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:20:55', '2018-10-18 03:20:55', '2018-10-25 03:20:55');
INSERT INTO `oauth_access_tokens` VALUES ('01af16a906bb13b393caa1aadf1a6a645b9336e22163eacb1a2962e1989b64741f28c81e8d3d8949', 7, 3, 'Personal Access Token', '[]', 0, '2018-10-30 08:51:52', '2018-10-30 08:51:52', '2018-11-06 08:51:52');
INSERT INTO `oauth_access_tokens` VALUES ('0279c3cb840a7871c52720dd872345dba3bf58c1ba94f28d47899123afba5827f1a473426f6e984b', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:36:53', '2018-09-18 03:36:53', '2018-09-25 03:36:53');
INSERT INTO `oauth_access_tokens` VALUES ('047fb2e218ce8eed6fcc150024de7633e918a9b2a0c3885361ae05b79962cc451be556f7263f1790', 16, 3, 'Personal Access Token', '[]', 0, '2018-11-27 07:47:38', '2018-11-27 07:47:38', '2018-12-04 07:47:38');
INSERT INTO `oauth_access_tokens` VALUES ('05e313c97d3657dd51adeb0fb584112ed2cf4ad85eff34d22bdd22118fbc9528cc7366203021e086', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-13 02:05:44', '2018-10-13 02:05:44', '2018-10-20 02:05:44');
INSERT INTO `oauth_access_tokens` VALUES ('08df2d16f86b1970f4dceebf22252a40cb02c086d4d28d9ec2ba9a2a8a0118b9d0a3457c57650f44', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:41:23', '2018-11-07 02:41:23', '2018-11-14 02:41:23');
INSERT INTO `oauth_access_tokens` VALUES ('09ae9b516467d293a9b8692554a857ca844a09cd321c5eca112b8d807046d627bb696f9d351263ab', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:38:46', '2018-09-18 03:38:46', '2018-09-25 03:38:46');
INSERT INTO `oauth_access_tokens` VALUES ('0a0706ce47b397878e3a64c894d9db6ec0f5950da123f93938a4fbcfad462acc20836764b37428a1', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:46:41', '2018-10-10 03:46:41', '2018-10-17 03:46:41');
INSERT INTO `oauth_access_tokens` VALUES ('0a367b8ac4d04de874accecbf4541ee84ed5727491473713882797c2e91d04f770c3f1d72adfa991', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:44:05', '2018-10-10 03:44:05', '2018-10-17 03:44:05');
INSERT INTO `oauth_access_tokens` VALUES ('0ad95c426c10b6b6dbb9b03b208dbb752c4619fcf8692b3a316a5604f7d17b3db9988894937bd7dd', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:03:40', '2018-10-21 21:03:40', '2018-10-28 21:03:40');
INSERT INTO `oauth_access_tokens` VALUES ('0c95a90c3d4c4091006445cc52cd6502ad94af57be7315900c2cb0bc78bdcffb0cdd33f0041b3b10', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:45:50', '2018-10-10 03:45:50', '2018-10-17 03:45:51');
INSERT INTO `oauth_access_tokens` VALUES ('0d4960b69036498bf3f46df2b80b2004d4cc4079e1b6d2a56254c36abf119c7e571467e159f24dc1', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:45:21', '2018-10-18 03:45:21', '2018-10-25 03:45:21');
INSERT INTO `oauth_access_tokens` VALUES ('0eaf761c9f2de3164f8e1b44b500e2da0a4144264bf441c42e4c4b9ab199d06ee9b9321a6f5ee6c4', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:55:13', '2018-10-10 03:55:13', '2018-10-17 03:55:13');
INSERT INTO `oauth_access_tokens` VALUES ('0f8df757e49f740943ea32dcfd477bbdec1e9bce06894687b2b0634a0ea8f70c421a883e93c161c0', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:55:00', '2018-10-10 03:55:00', '2018-10-17 03:55:00');
INSERT INTO `oauth_access_tokens` VALUES ('1659f9b779f9cb4238e5dc44ea997c35448568d8e83a219ee8a5c17953d6067ac1bd744d5c2071bc', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:45:08', '2018-10-18 03:45:08', '2018-10-25 03:45:08');
INSERT INTO `oauth_access_tokens` VALUES ('185bba3b32eef3b5b19f379069074a5967bbb271ad99bb1012d2d095cf6c40e491e67079e99c3be2', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-14 17:47:42', '2018-10-14 17:47:42', '2018-10-21 17:47:42');
INSERT INTO `oauth_access_tokens` VALUES ('19d4c6ded68c50d87889b0a362a3f658d14c5ae4991a06e8533f24139bc362c0dfb4d36689a3d87c', 24, 3, 'Personal Access Token', '[]', 0, '2018-11-08 14:20:06', '2018-11-08 14:20:06', '2018-11-15 14:20:07');
INSERT INTO `oauth_access_tokens` VALUES ('1cfd87b501da86d7bcd96c80173facc25de93deacbe79a3abeaf466b795d98b9196d1efd28e03e48', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 20:24:08', '2018-10-21 20:24:08', '2018-10-28 20:24:08');
INSERT INTO `oauth_access_tokens` VALUES ('1d37acfeb56be8f862db048b1aa5383943cfb13b694adfda4a3cd4fd9541552cf777ef8492cd83bd', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-18 12:06:15', '2018-10-18 12:06:15', '2018-10-25 12:06:16');
INSERT INTO `oauth_access_tokens` VALUES ('1e912c77e0121f14139a09035b514a9224664e0936c6c55c8bbc82e5516582d48e4572ee082440b8', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 06:30:07', '2018-10-10 06:30:07', '2018-10-17 06:30:07');
INSERT INTO `oauth_access_tokens` VALUES ('1f7cb5cb0bac4e54da0d9b3075d2d65c1823e6a8a75e02afe0f535fcc2e4ff691547c01fca91df3a', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 01:58:05', '2018-11-07 01:58:05', '2018-11-14 01:58:05');
INSERT INTO `oauth_access_tokens` VALUES ('2261e8c9ca44337294428ea6d28b53a856b275e52d642ddb4fd36bf2e7b81c5a2aa557a20f3123ce', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:47:47', '2018-10-18 12:47:47', '2018-10-25 12:47:47');
INSERT INTO `oauth_access_tokens` VALUES ('234fb59573c797d73ed7cf6b534af169e588a9ef89f4628af940214a996164751bea7137beb2a64d', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-20 09:08:23', '2018-10-20 09:08:23', '2018-10-27 09:08:23');
INSERT INTO `oauth_access_tokens` VALUES ('29658bfee9c7524dcf7a42a55ed9c3c4095fd652a728d1e1bbcd89473c80571522e871b9975e2760', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 20:25:03', '2018-10-21 20:25:03', '2018-10-28 20:25:03');
INSERT INTO `oauth_access_tokens` VALUES ('2ba5f64e281080645c298079c17a3a0f5037385411be0ea84bfba322de5ea2dc594a28f439e81045', 15, 1, 'Personal Access Token', '[]', 0, '2018-09-27 09:31:48', '2018-09-27 09:31:48', '2018-10-04 09:31:48');
INSERT INTO `oauth_access_tokens` VALUES ('2bc73c190dcb184b9fafae3a849c18c55f68a6d0ec3ac4fb9d7258d27be131fcd8d29dbf323b8676', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-17 01:53:08', '2018-10-17 01:53:08', '2018-10-24 01:53:08');
INSERT INTO `oauth_access_tokens` VALUES ('2c05507ff6b777de6bc5e341431b89a0ae5efaff68d38b958c6142fbaf35b2dd1814ebb47d0d577a', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-09 17:27:56', '2018-10-09 17:27:56', '2018-10-16 17:27:56');
INSERT INTO `oauth_access_tokens` VALUES ('2c4652a825f29b9f2a6d89268bfa2cea2b128122d7132bbea0200c9d435343c7af639b822e800c0a', 15, 1, 'Personal Access Token', '[]', 0, '2018-09-27 09:45:21', '2018-09-27 09:45:21', '2018-10-04 09:45:21');
INSERT INTO `oauth_access_tokens` VALUES ('2c8c5c58aad3bfdb0c27b182ab5f7f01835121277496f341c557a31f9359f0fe673e13c22de8ad79', 7, 3, 'Personal Access Token', '[]', 0, '2018-10-30 08:59:44', '2018-10-30 08:59:44', '2018-11-06 08:59:44');
INSERT INTO `oauth_access_tokens` VALUES ('2ca9038cde422f428aa5ce95a9c40760eac2b2ab501f3bc619e4490e2e613c631a1b859e46dea3b3', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:05:27', '2018-10-21 21:05:27', '2018-10-28 21:05:27');
INSERT INTO `oauth_access_tokens` VALUES ('2d16c5e3df39df458216ed34f7ec9f150f36e839be5d998879c295d50f0666bbf799625126d122b1', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-10 08:11:06', '2018-10-10 08:11:06', '2018-10-17 08:11:06');
INSERT INTO `oauth_access_tokens` VALUES ('2fcaf4728ed83fffcfeb4250a52353364c176bb9f332f8e27845ccd6d9003f63f310c174e6af1f6d', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:05:28', '2018-10-21 21:05:28', '2018-10-28 21:05:29');
INSERT INTO `oauth_access_tokens` VALUES ('32c37ab9a884128f4c4b54978f4d098658e100b9ad7bd2618f60881abc86f2965acb929c2a083d0c', 27, 3, 'Personal Access Token', '[]', 1, '2018-10-21 20:26:14', '2018-10-21 20:26:14', '2018-10-28 20:26:14');
INSERT INTO `oauth_access_tokens` VALUES ('33f8816116a489d827244fd650a71b3ed25904be7fb2bb667efcfbbeb479f7723840b4e429b23347', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 02:18:37', '2018-09-25 02:18:37', '2018-10-02 02:18:37');
INSERT INTO `oauth_access_tokens` VALUES ('349f09198e0650e47059bcff9de1521521bc827a65b5abb2b3db6b082105ecac56fbfa937311c708', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:45:11', '2018-10-10 03:45:11', '2018-10-17 03:45:11');
INSERT INTO `oauth_access_tokens` VALUES ('34d17f4c8b61284f76985c061b288a69661ccc605060f813abaa55d78768dd2b1a3ab18dffec77cf', 7, 3, 'Personal Access Token', '[]', 0, '2018-10-30 08:59:24', '2018-10-30 08:59:24', '2018-11-06 08:59:24');
INSERT INTO `oauth_access_tokens` VALUES ('356bb5ff8bb421c05efe7532ecb7acf2d07d72e6a5d5a9cefe4383a7df17e9585a02d242a2cb5a4f', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:43:38', '2018-09-18 03:43:38', '2018-09-25 03:43:38');
INSERT INTO `oauth_access_tokens` VALUES ('368b4e14399511734776688e5ecf452e8154f45dd3eea456504fa6084cc8f814aa449c7399b861cd', 15, 1, 'Personal Access Token', '[]', 0, '2018-09-27 09:28:45', '2018-09-27 09:28:45', '2018-10-04 09:28:45');
INSERT INTO `oauth_access_tokens` VALUES ('3b07a4df2157735f1c41b70cea28ac4b083b2c0d0fe7477f1d18b5e2784c5c0baffdfbc9a5d6ee68', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:44:51', '2018-10-10 03:44:51', '2018-10-17 03:44:51');
INSERT INTO `oauth_access_tokens` VALUES ('3b6fd8c9fb5bb616dcbc5b372c4d915225b0bad3fb5f1a6de3e7c963f9af713a58734220bec96f73', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-22 09:22:15', '2018-09-22 09:22:15', '2018-09-29 09:22:15');
INSERT INTO `oauth_access_tokens` VALUES ('3d6202af4649d0c9637c996311123fc2d8ddba321553aa15f1a0bc9295e5d1aa48de9e4446e46e02', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:05:26', '2018-10-21 21:05:26', '2018-10-28 21:05:26');
INSERT INTO `oauth_access_tokens` VALUES ('3d7b86ffa2d0ffef618bd0082757adeeddecd6ffa075e88956d3a3bf04953e94b9015d5380a28aa8', 1, 1, 'Personal Access Token', '[]', 0, '2018-09-16 08:03:44', '2018-09-16 08:03:44', '2018-09-23 08:03:44');
INSERT INTO `oauth_access_tokens` VALUES ('3f1904c131aa095ba7aa64af0cfd6d6e17bffb6eef88d6fab2de787e003ff82e742929e3a1173d6e', 31, 3, 'Personal Access Token', '[]', 0, '2018-11-27 07:58:08', '2018-11-27 07:58:08', '2018-12-04 07:58:08');
INSERT INTO `oauth_access_tokens` VALUES ('401699b18f0c4f70a484fae4b37cf3f5fb11463c0194e74310bc50c9163f8fcc1cde82ce9536dacb', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-14 12:54:19', '2018-10-14 12:54:19', '2018-10-21 12:54:19');
INSERT INTO `oauth_access_tokens` VALUES ('42126f518c0a219f51b4e4440283e085c40bb7f8367e17212884939fc60d68440d8c977ef3cf4c9d', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-14 06:35:38', '2018-10-14 06:35:38', '2018-10-21 06:35:38');
INSERT INTO `oauth_access_tokens` VALUES ('43bdeef5ce1ef7a97b48b159a79e5c11fedbac3e83ec30a0db2ec2873719e7b25c9da3d1c6ffe2b8', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-09 17:26:18', '2018-10-09 17:26:18', '2018-10-16 17:26:18');
INSERT INTO `oauth_access_tokens` VALUES ('45c029dbd0f0588ae80886d62abd1b79e42b113a6ea959769186240b933c5cef59d6cb297f9af3e7', 14, 1, 'Personal Access Token', '[]', 0, '2018-09-25 11:23:31', '2018-09-25 11:23:31', '2018-10-02 11:23:31');
INSERT INTO `oauth_access_tokens` VALUES ('4650c9853e7859ebe883ab3a698ff01eeca5b0b5995a2ba4447eaef66bbf7ef5c1857ed6ec0da5fd', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:00:49', '2018-11-07 02:00:49', '2018-11-14 02:00:49');
INSERT INTO `oauth_access_tokens` VALUES ('47273ac3489529bc753925415d28c592acf53c1a0a37238accd12306e45f666d6da115841518c870', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-14 23:34:09', '2018-10-14 23:34:09', '2018-10-21 23:34:09');
INSERT INTO `oauth_access_tokens` VALUES ('47cc1285e6e351c26e59a738c2734b1a8daad55beea856ea6d2da23efcda62c1c71d4281da38a471', 14, 3, 'Personal Access Token', '[]', 0, '2018-10-11 10:23:48', '2018-10-11 10:23:48', '2018-10-18 10:23:48');
INSERT INTO `oauth_access_tokens` VALUES ('49442945d5ed06772b053634239da2565ee46ae56319347eaaac12ba4644abae0265481cf5004110', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:44:58', '2018-10-18 03:44:58', '2018-10-25 03:44:58');
INSERT INTO `oauth_access_tokens` VALUES ('49ee2f0cee525a45a6c2936f20e81a7d0915e8dce5b63c6c914906b95ebe063209847d2512cf23ba', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:45:02', '2018-10-18 03:45:02', '2018-10-25 03:45:02');
INSERT INTO `oauth_access_tokens` VALUES ('4b7fb059509fa422ac27f67681c302a8fa15554e156aef12cae1acb1853212c95c898cd2dcb6092b', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:03:46', '2018-10-21 21:03:46', '2018-10-28 21:03:46');
INSERT INTO `oauth_access_tokens` VALUES ('4e40dd52361f5496991c0483e52dee9ad8cde592e35d6a0f95f5a50852f7d4e01fbf29ef25addd9a', 18, 3, 'Personal Access Token', '[]', 0, '2018-10-10 09:29:13', '2018-10-10 09:29:13', '2018-10-17 09:29:13');
INSERT INTO `oauth_access_tokens` VALUES ('52c2daea35fc7abbccd1b897355a9e5095a458e39a7c2c8bf4065b5f85b3b61e05620dc75380773e', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:03:48', '2018-10-21 21:03:48', '2018-10-28 21:03:48');
INSERT INTO `oauth_access_tokens` VALUES ('54ba6802047628f4bc488872ffde83c3711a3ccde25ef89015466c1e836558c7050d09c87361957d', 1, 1, 'Personal Access Token', '[]', 0, '2018-09-23 17:34:25', '2018-09-23 17:34:25', '2018-09-30 17:34:25');
INSERT INTO `oauth_access_tokens` VALUES ('553ef372e14cb531cfbd7332d50db52457fd2bfc924a7a7117c714c15c3d99a4891999d38117512e', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:16:48', '2018-09-18 03:16:48', '2018-09-25 03:16:49');
INSERT INTO `oauth_access_tokens` VALUES ('55f9167c6fd388960fc4d2a0a9873f19505699ff90ce538b7d4923ac09b9e4f72bc78eed08671935', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:20:33', '2018-10-18 03:20:33', '2018-10-25 03:20:33');
INSERT INTO `oauth_access_tokens` VALUES ('56305c6ae31fd682fc9616b177224a3c0a10b0b9e63a4a1709142d07f619aa7da67180885eb8d3a4', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:06:09', '2018-10-21 21:06:09', '2018-10-28 21:06:09');
INSERT INTO `oauth_access_tokens` VALUES ('5a4d0ac32769c5d02bce645a26c470f934054cc39da432a57c28ddae8d58761683fe8a0f5ec5d930', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-23 18:27:15', '2018-09-23 18:27:15', '2018-09-30 18:27:15');
INSERT INTO `oauth_access_tokens` VALUES ('5bc0115027d7612880300cf6f2bcb21d1e9d2181d587235a7ec1736f464e33333144a44b86329b7c', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:45:13', '2018-10-18 03:45:13', '2018-10-25 03:45:13');
INSERT INTO `oauth_access_tokens` VALUES ('5c545a7c45e005928ae940b165654b0d4c92f24e1ca2fb46da1c760eec0b1c1941b5b55a3e15af56', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:05:31', '2018-10-21 21:05:31', '2018-10-28 21:05:31');
INSERT INTO `oauth_access_tokens` VALUES ('5cedc3bcebfca21db2c7bec1585c1dcf6fed244bbb882a46a06b73af59680c0ac4f8c4cdc6999e86', 27, 3, 'Personal Access Token', '[]', 1, '2018-10-22 05:22:09', '2018-10-22 05:22:09', '2018-10-29 05:22:09');
INSERT INTO `oauth_access_tokens` VALUES ('5f8581bd952e25950e44b994b30aa109ef79f0cc757724de7079cd492cc5dbc5c94c349eb6f18724', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:25:21', '2018-10-18 12:25:21', '2018-10-25 12:25:21');
INSERT INTO `oauth_access_tokens` VALUES ('6020503b2b4985c93f982c280285964780f07f5daaabf05e1a93e2edae553cedce1e811c1f4f292d', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-14 12:56:15', '2018-10-14 12:56:15', '2018-10-21 12:56:15');
INSERT INTO `oauth_access_tokens` VALUES ('6182b62d8bec1ecc32e059c612b6298a9207b4207d41c21f382ecf06e94dcda24c550137dbae9e5a', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-23 18:25:26', '2018-09-23 18:25:26', '2018-09-30 18:25:26');
INSERT INTO `oauth_access_tokens` VALUES ('62e34faf5766fc9b4739006ae31854ab2a9b0b459a5416c82b5b2108d669f914ce610f9c5019887c', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 04:30:49', '2018-09-18 04:30:49', '2018-09-25 04:30:49');
INSERT INTO `oauth_access_tokens` VALUES ('633e1c2bd6f840e334887fd0f6c54f93de19ab2c195a75d7659d861958e2b1275daa1d140fee61dc', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-14 01:19:29', '2018-10-14 01:19:29', '2018-10-21 01:19:29');
INSERT INTO `oauth_access_tokens` VALUES ('63b8f83f699d02eb9ac9896665ee19d6fe0f91ae610b004d3029e487341a5ee1f9ce80bc507aad2b', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:46:30', '2018-10-18 12:46:30', '2018-10-25 12:46:30');
INSERT INTO `oauth_access_tokens` VALUES ('63daaa4ba8e20b5930d54da217e64907f0d73d9b5f23e04f2fbc1376a550f5f952d6db43d700e5af', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:24:29', '2018-10-18 12:24:29', '2018-10-25 12:24:29');
INSERT INTO `oauth_access_tokens` VALUES ('68b139b3eca8c7c687938240fdac7a1d872b0918e7ea3b8c2d7cfce9d2770e6ddb7b61ba6f0b9dde', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:17:42', '2018-09-18 03:17:42', '2018-09-25 03:17:43');
INSERT INTO `oauth_access_tokens` VALUES ('6a6d53a9d7d9ad84e45fef2f29bfbc7079b26858e415177f62e81f4d9cc973cc23c8f92b3382bf8d', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:35:13', '2018-09-18 03:35:13', '2018-09-25 03:35:13');
INSERT INTO `oauth_access_tokens` VALUES ('6a9805ec1c36d6b77f4e4b2386397e132547e8772d148fa73324aa4850cd69c00e5f05c5c13bd1e1', 15, 1, 'Personal Access Token', '[]', 0, '2018-10-09 16:41:59', '2018-10-09 16:41:59', '2018-10-16 16:41:59');
INSERT INTO `oauth_access_tokens` VALUES ('6bbb64a977b059953a81cac63b9fa81f3dbcb775bb8a8ca0c35e732c2053a9a09e2a3b0a24e4d6ba', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-14 12:55:59', '2018-10-14 12:55:59', '2018-10-21 12:55:59');
INSERT INTO `oauth_access_tokens` VALUES ('6c75a20beaf296cb8ae1169a55dbafd07bbc17e52e4034d526ebfe68798541fc36431548af92bde9', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-17 02:08:47', '2018-09-17 02:08:47', '2018-09-24 02:08:47');
INSERT INTO `oauth_access_tokens` VALUES ('6da274e580a26f60b25a451ddc2f031d54a36299a4b917c2cb0c8948b47301056be40ec17f11628b', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-13 02:05:49', '2018-10-13 02:05:49', '2018-10-20 02:05:49');
INSERT INTO `oauth_access_tokens` VALUES ('6e23f2f036b3f09ba0f34babe82b544dc9ee1d1a115174786a4a1acb082b7d5221c7801f39c0c7df', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:44:46', '2018-10-10 03:44:46', '2018-10-17 03:44:46');
INSERT INTO `oauth_access_tokens` VALUES ('6f2bcc2c4d63e72fcd81bce3388685bfb7e329ea390614756d248be171022e164dbe35f07a65f762', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:50:19', '2018-10-10 03:50:19', '2018-10-17 03:50:19');
INSERT INTO `oauth_access_tokens` VALUES ('7153462f538a928054bc334ff6d6f9ec4de8da7923e1117c5e7ff67a50e48c42d0408e23c6bbea6a', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:03:47', '2018-10-21 21:03:47', '2018-10-28 21:03:47');
INSERT INTO `oauth_access_tokens` VALUES ('74c334e9399b5fff74b234bb35e909650f2891fb26e951af3b32b6bd62934519a17517351500b7a5', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-30 09:00:12', '2018-10-30 09:00:12', '2018-11-06 09:00:12');
INSERT INTO `oauth_access_tokens` VALUES ('751dd781b6a4c27a5fde706fcc1bf1f40fd7b30d4c4bfd053be417bca681eab0e71ef45e549836ff', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 04:04:18', '2018-10-10 04:04:18', '2018-10-17 04:04:18');
INSERT INTO `oauth_access_tokens` VALUES ('78e676aca9c6b2aeb4a7d84cec680d8fc524582f2f8df160d777ef604bce54ac7afc1054da6adf5a', 1, 1, 'Personal Access Token', '[]', 0, '2018-09-17 06:00:44', '2018-09-17 06:00:44', '2018-09-24 06:00:44');
INSERT INTO `oauth_access_tokens` VALUES ('790410a39c20de4c7e8344211c746c44db94a6ca30f9a59087de19608e1eca5a0ed4a48e90c18533', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:46:14', '2018-10-18 03:46:14', '2018-10-25 03:46:14');
INSERT INTO `oauth_access_tokens` VALUES ('791024b5bbf32c1b53a1d0a96610aeedfb404b5aab1f6b951de1398c03107beb8ae725b3cc4a3faf', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 04:38:41', '2018-10-18 04:38:41', '2018-10-25 04:38:41');
INSERT INTO `oauth_access_tokens` VALUES ('79eb627907f7099f94d6efc1f946b329541b6f32bd6f115be4043a554dfbef932dcdf9a7011951a9', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:45:06', '2018-10-10 03:45:06', '2018-10-17 03:45:06');
INSERT INTO `oauth_access_tokens` VALUES ('7aedfa6dbced75f82c98c0af638fe8f50f33f37909149ac1ef5a74308b565f2a4fd314bdc136e8e1', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:44:17', '2018-10-10 03:44:17', '2018-10-17 03:44:18');
INSERT INTO `oauth_access_tokens` VALUES ('7b1fcff2295d9ebfa28b7381bbd9b2d1c72cc77871bf1cc43e3a988ecd35e4651ddab98b8a0dcca3', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-14 12:50:36', '2018-10-14 12:50:36', '2018-10-21 12:50:36');
INSERT INTO `oauth_access_tokens` VALUES ('7d0661f74d2c8fd81ac06cf2db730f2f1bac88fbaedf8a34ebf89bee9033b0e559af3e35228d7959', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-14 17:47:32', '2018-10-14 17:47:32', '2018-10-21 17:47:32');
INSERT INTO `oauth_access_tokens` VALUES ('7d260e5a239f74ad6435fdfdad5d392ac18299ac21e4bb7b22100a2c596c8fcc4b520fe69bcb3e06', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-15 12:29:22', '2018-10-15 12:29:22', '2018-10-22 12:29:22');
INSERT INTO `oauth_access_tokens` VALUES ('7f23b674022760ff4301ab6dcfb59d960736a85430adfb220b4a856ea9757e283973587d6c39730f', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:00:14', '2018-11-07 02:00:14', '2018-11-14 02:00:14');
INSERT INTO `oauth_access_tokens` VALUES ('81e20c23400e06244f8b225c4ffeb1b3acdd71d0d19aea52a4e875b8bf9564890d7e484fc1432488', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:03:44', '2018-10-21 21:03:44', '2018-10-28 21:03:44');
INSERT INTO `oauth_access_tokens` VALUES ('8205b0608cf7eb2d799661bb2c07c9ba3aab398eccfb1f45f6952a7f6e5843c723e473ccafba389e', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:05:30', '2018-10-21 21:05:30', '2018-10-28 21:05:30');
INSERT INTO `oauth_access_tokens` VALUES ('82f962f87b91324fd005608a1952f09ca08d0963a9457b8e4bc62511c68e1f69d9b593c314a6f116', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:03:45', '2018-10-21 21:03:45', '2018-10-28 21:03:45');
INSERT INTO `oauth_access_tokens` VALUES ('8361b5e446d47f5d57b63483d96399306a7e58a5aac426c057ab59188e1274a78fdbf30af07dc369', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:17:19', '2018-09-18 03:17:19', '2018-09-25 03:17:19');
INSERT INTO `oauth_access_tokens` VALUES ('83b72380c9efa2c1b38d8c0ec77576cba32737fac8ac57f2566cc784fbb16650a21893b9375f1c7b', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-15 15:48:22', '2018-10-15 15:48:22', '2018-10-22 15:48:22');
INSERT INTO `oauth_access_tokens` VALUES ('85859280de52478041ded80388b4517ed30eca9e3acce63b6a497b624d9fc2884e941c91afedc4c7', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:45:13', '2018-10-10 03:45:13', '2018-10-17 03:45:13');
INSERT INTO `oauth_access_tokens` VALUES ('86e021496cf28d621f582982e20765f6a156d0c8c762d4b89af20fac44511e7b88a13f8db01268a8', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:31:02', '2018-09-18 03:31:02', '2018-09-25 03:31:02');
INSERT INTO `oauth_access_tokens` VALUES ('87ede0ff74b95f7e531634d53fe6fd57fba0dc6b9e2b39759744ba3a9eab88016073e98eb7698f45', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-10 10:07:10', '2018-10-10 10:07:10', '2018-10-17 10:07:10');
INSERT INTO `oauth_access_tokens` VALUES ('8878b938eedaae06b908e82f974c2d3e3f4b33fbc7b68273f0d7af33faf91f90f252b35c6b17a45a', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-14 12:54:13', '2018-10-14 12:54:13', '2018-10-21 12:54:13');
INSERT INTO `oauth_access_tokens` VALUES ('88ab4a10d133192edb9ed233cacc6848c6abe21bf990d958ed965b5cd8c031a8f97614e9d51de076', 17, 3, 'Personal Access Token', '[]', 0, '2018-10-10 09:06:20', '2018-10-10 09:06:20', '2018-10-17 09:06:20');
INSERT INTO `oauth_access_tokens` VALUES ('88ec514f289f2ee79c3c145edfadde0b0e5d25a7afae62af6d650c1a17aa3bf76bb24326ac7bae78', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:00:47', '2018-10-18 12:00:47', '2018-10-25 12:00:47');
INSERT INTO `oauth_access_tokens` VALUES ('88ef04222543220c244634b71ab4f51f34c5c68d20c907c2e9aacd4ca642f7429b803c02fbaa7671', 19, 3, 'Personal Access Token', '[]', 1, '2018-10-21 16:51:24', '2018-10-21 16:51:24', '2018-10-28 16:51:24');
INSERT INTO `oauth_access_tokens` VALUES ('8944a34dd6e9595025069c399a7fb6f1958ea2cf576d2193198fda8e2d80fec92dcd4da4f53b7021', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:58:24', '2018-09-18 03:58:24', '2018-09-25 03:58:24');
INSERT INTO `oauth_access_tokens` VALUES ('899be90fa8f34a4a5ac8847a905a94208d7c58ea1103034b7ddefa2ae94e488ee892f355b02655ab', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-14 12:37:36', '2018-10-14 12:37:36', '2018-10-21 12:37:36');
INSERT INTO `oauth_access_tokens` VALUES ('8ad52679bdda47603feeaad7db870fb22213695fe4db1fe71ee29d0e4c21ccbdd3c7e286553a5f8b', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-15 12:29:07', '2018-10-15 12:29:07', '2018-10-22 12:29:07');
INSERT INTO `oauth_access_tokens` VALUES ('8b3975069dee13889aae663f5aa69eb4def6df54a0095e65600e382ac5492210918043f8e2b4da06', 24, 3, 'Personal Access Token', '[]', 0, '2018-11-08 14:21:34', '2018-11-08 14:21:34', '2018-11-15 14:21:34');
INSERT INTO `oauth_access_tokens` VALUES ('8bd1104476cb296a107061a8f4b67b98b74213f291eb6f4b98dae1d9fb3de305b4f1e1a1ad60ecfd', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-14 17:49:12', '2018-10-14 17:49:12', '2018-10-21 17:49:12');
INSERT INTO `oauth_access_tokens` VALUES ('8cde3af54c6221b5f4801cbaea7ba4b99c1a1d7233108f9795bc575a54de005d1cfe72963efdbc8c', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 04:03:37', '2018-10-10 04:03:37', '2018-10-17 04:03:37');
INSERT INTO `oauth_access_tokens` VALUES ('8d6e779fdfc22b077d49c797c8738fbf3538e514cae3670b946e3a080b4659224655493b6e0b92d9', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:55:06', '2018-10-10 03:55:06', '2018-10-17 03:55:06');
INSERT INTO `oauth_access_tokens` VALUES ('8fd52add7bc4a156726f3988147514872236a3e0249009a0989390f6f756921ae86bb5ff66e9060e', 7, 3, 'Personal Access Token', '[]', 0, '2018-10-09 17:25:56', '2018-10-09 17:25:56', '2018-10-16 17:25:56');
INSERT INTO `oauth_access_tokens` VALUES ('91e178373f7f132b44bf6a36b0eea074bb523551004bf90ae4c75d0be12728967a0159edb3d6bf4f', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 07:44:20', '2018-10-10 07:44:20', '2018-10-17 07:44:20');
INSERT INTO `oauth_access_tokens` VALUES ('92c42639d6ed4b661c6ed1f950c7f6f7ee988b6df71854e1819fb3f1d8519ea222fbbc6c5769a7bd', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-30 09:00:02', '2018-10-30 09:00:02', '2018-11-06 09:00:02');
INSERT INTO `oauth_access_tokens` VALUES ('92f97832bceb3bedf8258b986db53fb3b78ca7e32e11187b09d118e16f935ed4827819f40eb5d35e', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-15 12:29:26', '2018-10-15 12:29:26', '2018-10-22 12:29:26');
INSERT INTO `oauth_access_tokens` VALUES ('957351618522e6e7167b5a301f228c9a927798aa4ad521530ec13d60180559749653a0b9dde4c078', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 02:18:52', '2018-09-25 02:18:52', '2018-10-02 02:18:52');
INSERT INTO `oauth_access_tokens` VALUES ('97a194092653db02f217793cddbe1a3716f266c747ab5a01d044a6b2597ea429939e496d1f271479', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-23 17:47:38', '2018-09-23 17:47:38', '2018-09-30 17:47:38');
INSERT INTO `oauth_access_tokens` VALUES ('97b141150f3924ee428b363f3c8e05e5c4e17ee0c0e2659b52bfef46711575cb296400f07f9adc59', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-22 05:21:15', '2018-10-22 05:21:15', '2018-10-29 05:21:15');
INSERT INTO `oauth_access_tokens` VALUES ('983988169ac9c828bed6e9f9411960d6415939df13a871b7ede8ef951d6d6684d6dfa04d4237c507', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 01:59:10', '2018-11-07 01:59:10', '2018-11-14 01:59:10');
INSERT INTO `oauth_access_tokens` VALUES ('9923de4ed5fe9c5ead4bc531458060f7fdcf8cfbeab6d37adf72f7be6b939a9e6aff5766465f1fe6', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-15 05:28:27', '2018-10-15 05:28:27', '2018-10-22 05:28:27');
INSERT INTO `oauth_access_tokens` VALUES ('992e93dfb024b88dc7885a8e9c963658ec95ae3d8abe7c6995031d232a2a6fc5b71fcef1ea547738', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-14 01:19:31', '2018-10-14 01:19:31', '2018-10-21 01:19:31');
INSERT INTO `oauth_access_tokens` VALUES ('998c0ba0f099f62adcfc2f75b9e89ad82b38e876f1688d5b8562b4e377d33fb03c9c5c3971be0b1a', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:04:49', '2018-11-07 02:04:49', '2018-11-14 02:04:49');
INSERT INTO `oauth_access_tokens` VALUES ('9a0f42dd8239f9ff975855099e634f588d21cc15a0c26194f85b50feef327743fb6d2e4e99120b2d', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-17 07:38:57', '2018-09-17 07:38:57', '2018-09-24 07:38:57');
INSERT INTO `oauth_access_tokens` VALUES ('9c7269bde6a36a0c29180f4bbaca65cfdf6c99cebefeecf6ff93d4dfca6b1d679499fb03202b2bc8', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-24 18:15:36', '2018-09-24 18:15:36', '2018-10-01 18:15:37');
INSERT INTO `oauth_access_tokens` VALUES ('9da740e1ddbf156006bfb2451febe1e5c88616b3838951734f4e222f6df366e65b243db62ff3bc86', 7, 3, 'Personal Access Token', '[]', 0, '2018-10-09 17:09:05', '2018-10-09 17:09:05', '2018-10-16 17:09:05');
INSERT INTO `oauth_access_tokens` VALUES ('9ea4580975fb2951012d74a93e515c33702ed6d7b4ba46b58d7550d8f5eddc5e6dd7b4f4bb588f30', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 01:57:43', '2018-11-07 01:57:43', '2018-11-14 01:57:43');
INSERT INTO `oauth_access_tokens` VALUES ('9ead101520bd3b8f459452eabf7d88b2ac8c0a3484f5071df470439e12aa4ecb7f9c35ef7bd67a0a', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:55:21', '2018-10-10 03:55:21', '2018-10-17 03:55:22');
INSERT INTO `oauth_access_tokens` VALUES ('a0e3dfcea716178c83aad5a5f72cc3b69d249ceaef141f9fb720ebb52945a9f9ef4f5fc48c91381d', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-30 08:58:52', '2018-10-30 08:58:52', '2018-11-06 08:58:52');
INSERT INTO `oauth_access_tokens` VALUES ('a0f64afca0a35f063f5809113ff0bb1100458ecfa0b80fa16d0861a34863b552996ff58e76f10855', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-22 09:22:04', '2018-09-22 09:22:04', '2018-09-29 09:22:04');
INSERT INTO `oauth_access_tokens` VALUES ('a166b86f656cd122fa1bb663d8730bdb47380c17ef8a10e4178fe7f2fcff24a758a7ed176f1bc5dc', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-14 12:50:31', '2018-10-14 12:50:31', '2018-10-21 12:50:31');
INSERT INTO `oauth_access_tokens` VALUES ('a16e90f121f2fd9cae9f1020710e26cf4511b0142515f94720085108fa8588ad521c6413dfd52a3c', 24, 3, 'Personal Access Token', '[]', 0, '2018-11-08 14:24:37', '2018-11-08 14:24:37', '2018-11-15 14:24:37');
INSERT INTO `oauth_access_tokens` VALUES ('a21bdf36b0346d306868be7cc55092660126c0b4592951cfcc38a90e3267fc15dc12c8d798e88165', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-17 02:10:36', '2018-09-17 02:10:36', '2018-09-24 02:10:36');
INSERT INTO `oauth_access_tokens` VALUES ('a2563386105bd5ad3ef082f7eef2c0780491b3daca047d69edcd2e9661e0b93d41981078aa892438', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:47:06', '2018-10-18 03:47:06', '2018-10-25 03:47:06');
INSERT INTO `oauth_access_tokens` VALUES ('a673a5f28a6af07e4ce762fefb5b1df6429129f526edc9521b465bbc3947532670a7279627672349', 15, 1, 'Personal Access Token', '[]', 0, '2018-09-27 09:52:39', '2018-09-27 09:52:39', '2018-10-04 09:52:39');
INSERT INTO `oauth_access_tokens` VALUES ('a7c09def4972124083b30f9e17604ad141f6a5e163af4254578d1bac5d38705e57cc179060d3a313', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:20:15', '2018-10-18 03:20:15', '2018-10-25 03:20:16');
INSERT INTO `oauth_access_tokens` VALUES ('a9adbf6292123a17156d14e87e103f7076a0c71faa541fade1a931b60e96648b9d6732c0f5f09d92', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 04:30:39', '2018-09-18 04:30:39', '2018-09-25 04:30:39');
INSERT INTO `oauth_access_tokens` VALUES ('ad9e480741cbbce7d9e2c2b14739a649e06449a6ecab161c2b999d7169936a404a4aca305d849b4c', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 01:58:44', '2018-11-07 01:58:44', '2018-11-14 01:58:44');
INSERT INTO `oauth_access_tokens` VALUES ('addb456554c34ef11768cc6afdb1e34df28d4b403e1c8d606d36a1f17cc6447982a84baf0ba03327', 14, 3, 'Personal Access Token', '[]', 0, '2018-10-11 10:23:52', '2018-10-11 10:23:52', '2018-10-18 10:23:52');
INSERT INTO `oauth_access_tokens` VALUES ('af57843fa9ddb830ec2380c222dd3df9a69a6ff3f33d009fd03c0ba3e76261966013c6c4da87a259', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-18 05:27:14', '2018-10-18 05:27:14', '2018-10-25 05:27:14');
INSERT INTO `oauth_access_tokens` VALUES ('b2205556eadb7e4d0b0d29e708977fcc5a5eb19f363f3e72fb1c6db6d8ebf2d6425207abc2aed8a5', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:24:23', '2018-10-18 12:24:23', '2018-10-25 12:24:23');
INSERT INTO `oauth_access_tokens` VALUES ('bba3f599077cd496a9da3f8d2bdbefb6c4d40614a46250c1a891fb47bcfb8fde6457375e779f4f1e', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 01:58:38', '2018-11-07 01:58:38', '2018-11-14 01:58:38');
INSERT INTO `oauth_access_tokens` VALUES ('bd2b76cfacf0c1c72472fafe91c53a82fc61691a05d98890cc3f0be2268332b181515592063049dc', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:41:12', '2018-10-18 12:41:12', '2018-10-25 12:41:12');
INSERT INTO `oauth_access_tokens` VALUES ('bd30761ce57b05c00a43cd812d4483a70b879a98a758848d027fb9f5009a062d7a79e554ccbd6364', 15, 1, 'Personal Access Token', '[]', 0, '2018-09-28 02:21:00', '2018-09-28 02:21:00', '2018-10-05 02:21:00');
INSERT INTO `oauth_access_tokens` VALUES ('bd7948a5d3c2a0f2b565b50f10361362add72e67f859d6e4c94fc0b64835c96d28bbf0c47444fd7f', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-16 06:53:22', '2018-10-16 06:53:22', '2018-10-23 06:53:22');
INSERT INTO `oauth_access_tokens` VALUES ('be0e636afbee2a8c5dbb4b1d17513eaa45c7fee900cefdb884937d4c0d308db9a73b1501269d33f4', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-10 10:06:55', '2018-10-10 10:06:55', '2018-10-17 10:06:55');
INSERT INTO `oauth_access_tokens` VALUES ('bedbba6f19f1dcf935b56d30ee5467a766c12d6f9a864a1aa27868266a0e66d392ce2cbee1af4e9f', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:20:04', '2018-10-18 03:20:04', '2018-10-25 03:20:04');
INSERT INTO `oauth_access_tokens` VALUES ('bffa0f3f3ebc7fed87ebc76c91180a9d563ec4d5c225c4bb0da96aca667ad4485f1764f3629e4e48', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-17 05:15:20', '2018-09-17 05:15:20', '2018-09-24 05:15:20');
INSERT INTO `oauth_access_tokens` VALUES ('c10770de9fd51a43bdf8283a3cd4ea05657f45c49905dbed08cf24154248d9e69e06eb604403dafb', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 04:20:32', '2018-09-18 04:20:32', '2018-09-25 04:20:32');
INSERT INTO `oauth_access_tokens` VALUES ('c53157cc1bb439743206aae81dca1443d8ab41ce6b456c2b8b51e8dd375dc2b9d07b28ee0ffd4055', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:45:28', '2018-10-18 03:45:28', '2018-10-25 03:45:28');
INSERT INTO `oauth_access_tokens` VALUES ('c60e642913216012cab07a637eb84ffa1c9cdbf5a2d5f3d599428dc2bb4d245d6bb98a1d6e4d2264', 19, 3, 'Personal Access Token', '[]', 1, '2018-10-12 04:43:53', '2018-10-12 04:43:53', '2018-10-19 04:43:53');
INSERT INTO `oauth_access_tokens` VALUES ('c9cd9bfcbdc6fb9e7a164912b3a0820a1c9f18e83450cf1976e409d0fbf1df9499b7cdf0ddd583a7', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-18 04:36:09', '2018-10-18 04:36:09', '2018-10-25 04:36:09');
INSERT INTO `oauth_access_tokens` VALUES ('cd81403992bad6e4df000a36ba28618050914e9d071a94cb3f80a2ea40b3171f68e6f6409cb6a764', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 03:12:38', '2018-09-25 03:12:38', '2018-10-02 03:12:38');
INSERT INTO `oauth_access_tokens` VALUES ('cdfc05794725968d374eff1730de45894df95c325b34110902e25bd400ebddaf2efdae941fdaaa76', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 04:44:51', '2018-09-18 04:44:51', '2018-09-25 04:44:51');
INSERT INTO `oauth_access_tokens` VALUES ('cfd28e1c39885b77697eb616f0b6cf453b4019082083fe9040a43d6ecad4b09b1334e92ca6433b11', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-23 18:25:22', '2018-09-23 18:25:22', '2018-09-30 18:25:22');
INSERT INTO `oauth_access_tokens` VALUES ('d0491d90b71f1dd190ffe527c3c11fafb38d69a890d91011ddc433bd8ae9ee0c92d09b012aec1413', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-23 18:32:45', '2018-09-23 18:32:45', '2018-09-30 18:32:45');
INSERT INTO `oauth_access_tokens` VALUES ('d1b1a571a0adb6e539110539d6f27435ea2354488b58f8697dab32fbff5d004e71f6cd3e4a11c7bb', 15, 1, 'Personal Access Token', '[]', 0, '2018-09-27 09:28:52', '2018-09-27 09:28:52', '2018-10-04 09:28:52');
INSERT INTO `oauth_access_tokens` VALUES ('d26725d7aba178593c095d68400fca5dd889f0cbefa16ef7268264afb3e572515f32017c6bb911fc', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-18 12:24:33', '2018-10-18 12:24:33', '2018-10-25 12:24:33');
INSERT INTO `oauth_access_tokens` VALUES ('d28e62e559c6144afc4ab44d5caf4769498368280d6add0b6beafdb7bcfdbf9f1325598e2f0e3ac5', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-09 17:09:49', '2018-10-09 17:09:49', '2018-10-16 17:09:49');
INSERT INTO `oauth_access_tokens` VALUES ('d2ea26f25572f7f80cb55e74b9c0a540d7456257ad14bbe1f68113384d63d5f20dcc6873888377b1', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-16 06:53:25', '2018-10-16 06:53:25', '2018-10-23 06:53:25');
INSERT INTO `oauth_access_tokens` VALUES ('d906c485cf6b729614577c0350caea3c9d3ff8838a094c9febb5663bc1865073b98aa715fcda7864', 24, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:44:30', '2018-10-18 03:44:30', '2018-10-25 03:44:30');
INSERT INTO `oauth_access_tokens` VALUES ('d97d124acdf6f419f2f8c2ced954d6d232c82606e5b36098175df117e27270d8499275206ec8f6c1', 26, 3, 'Personal Access Token', '[]', 0, '2018-10-30 05:33:54', '2018-10-30 05:33:54', '2018-11-06 05:33:54');
INSERT INTO `oauth_access_tokens` VALUES ('d98cb3fa7060d98c49fa66489bf11df4eea996bd7c2a0cc8ae61e61cbb5be3e5cee077ec8029b59c', 1, 1, 'Personal Access Token', '[]', 0, '2018-09-17 06:01:31', '2018-09-17 06:01:31', '2018-09-24 06:01:31');
INSERT INTO `oauth_access_tokens` VALUES ('d9f5f26cc83b16ffd6f6c119c2d5ef30f4db2bfaed0c2a77785f5a270820f7d412217e477ddd3f22', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:07:28', '2018-11-07 02:07:28', '2018-11-14 02:07:28');
INSERT INTO `oauth_access_tokens` VALUES ('d9f7945291b7bc62ba5ae5c655dcf97e68b17f43011b5a95236af012afd8e763f5abc08792d66275', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-16 06:56:50', '2018-10-16 06:56:50', '2018-10-23 06:56:50');
INSERT INTO `oauth_access_tokens` VALUES ('da0525f9b5402a79d8a04a7d2e2b45367e264ad4c5fb8c8092dd019abac589775b78a2f5beb60964', 15, 1, 'Personal Access Token', '[]', 0, '2018-10-09 16:40:22', '2018-10-09 16:40:22', '2018-10-16 16:40:22');
INSERT INTO `oauth_access_tokens` VALUES ('da787e0e50d61eaa5f3ac8a51824422e5b3e2946283c23cdf88732905d26647da25eb15df2db6640', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 03:16:00', '2018-09-25 03:16:00', '2018-10-02 03:16:00');
INSERT INTO `oauth_access_tokens` VALUES ('dab1f521ef0bcea5654bccc484ce942ca107a3971fd5dc67110469f412103ca394c2cc8207515580', 24, 3, 'Personal Access Token', '[]', 0, '2018-11-08 14:21:38', '2018-11-08 14:21:38', '2018-11-15 14:21:38');
INSERT INTO `oauth_access_tokens` VALUES ('dbb2927d7615efc359361f5edf792131f924cac85709983a37a884bff172e64f80197b3689d0753b', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 08:32:02', '2018-09-25 08:32:02', '2018-10-02 08:32:02');
INSERT INTO `oauth_access_tokens` VALUES ('df7b43ed91b304552a07af89893efb8df6ac74e04570aaa1207ad725b2c42450a47c89e41925d05b', 18, 3, 'Personal Access Token', '[]', 0, '2018-10-10 09:28:58', '2018-10-10 09:28:58', '2018-10-17 09:28:58');
INSERT INTO `oauth_access_tokens` VALUES ('e10cf849e6dde6131451505bd7f5c0cbd1f83d681738a7e0970df1aaa76bfe3113bb57d25407722a', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-14 23:34:21', '2018-10-14 23:34:21', '2018-10-21 23:34:21');
INSERT INTO `oauth_access_tokens` VALUES ('e13ef9b858e1c6f63eb9c4ebcac6236be165fda69c4e95cafa9ccc30d16481d0c625437ca0e59844', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-17 01:52:58', '2018-10-17 01:52:58', '2018-10-24 01:52:58');
INSERT INTO `oauth_access_tokens` VALUES ('e3fef151cd4a062a7db80c81813ad5036a537bde8bfd7e8e19fdd0db3721470f4f5391001c4864b9', 29, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:01:28', '2018-11-07 02:01:28', '2018-11-14 02:01:29');
INSERT INTO `oauth_access_tokens` VALUES ('e50b87224fc51680b06babcf881fbd0e1a4900ffc8b9cd65de9b2f49464f96512b1c75c8d8130480', 14, 1, 'Personal Access Token', '[]', 0, '2018-09-25 11:23:37', '2018-09-25 11:23:37', '2018-10-02 11:23:37');
INSERT INTO `oauth_access_tokens` VALUES ('e66b63601625c6c335c7ed386a1f72b4e5ac25ad2253d9fb947f88275a170290920fcd6b95d579cc', 16, 3, 'Personal Access Token', '[]', 1, '2018-10-14 13:50:36', '2018-10-14 13:50:36', '2018-10-21 13:50:36');
INSERT INTO `oauth_access_tokens` VALUES ('e745321b04abd65e17f0905a8da62606592d6646998fef0b0e12a582b9b68b3a0248af824b8ca538', 7, 3, 'Personal Access Token', '[]', 0, '2018-10-30 08:59:43', '2018-10-30 08:59:43', '2018-11-06 08:59:43');
INSERT INTO `oauth_access_tokens` VALUES ('e754b2ae9f99883f9d9cf6611ea93b4a14519829e2d8882d0797271a4b3f7062b99cbcacde413d29', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 03:12:53', '2018-09-25 03:12:53', '2018-10-02 03:12:53');
INSERT INTO `oauth_access_tokens` VALUES ('e8811314a7df3d0dec389c7e6f85c16db0629e308911aa3f910fab30897c25c22620706477ce5253', 27, 3, 'Personal Access Token', '[]', 0, '2018-10-21 21:05:32', '2018-10-21 21:05:32', '2018-10-28 21:05:32');
INSERT INTO `oauth_access_tokens` VALUES ('e8bbe3278828afdcd6cd28ff1eeb9aaac6acf7036e13b93bf9df08043d66299449a88144626acf72', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:46:37', '2018-10-10 03:46:37', '2018-10-17 03:46:37');
INSERT INTO `oauth_access_tokens` VALUES ('e9f6b1741c08e5fbb5ef547225f4b844c9ccac23fcf5db09756d9df99bd619eb681b8f3ed89fb940', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-17 07:40:39', '2018-09-17 07:40:39', '2018-09-24 07:40:40');
INSERT INTO `oauth_access_tokens` VALUES ('eb198ed897203daec8621f63665159fb1e31f842bc04e45859fb3ce5dc7271a047782963af92441e', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-15 05:45:47', '2018-10-15 05:45:47', '2018-10-22 05:45:47');
INSERT INTO `oauth_access_tokens` VALUES ('ebbe57538e6c211813e7d0e7facb3f660465287eceaaf353fa1928310eefa6e87c473b0c382679f0', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:22:27', '2018-10-18 03:22:27', '2018-10-25 03:22:27');
INSERT INTO `oauth_access_tokens` VALUES ('ebdc6bfb5645e20afb0b7b151022ce7613c35a19bfe2daf88e0c635f23e7c9f8b6def5db9319af87', 15, 1, 'Personal Access Token', '[]', 0, '2018-10-09 16:41:49', '2018-10-09 16:41:49', '2018-10-16 16:41:49');
INSERT INTO `oauth_access_tokens` VALUES ('ec6f6cd0ec994ce584fb43d615a5b2727748ac73d5864475f5e9593e73c613809dae333aea0c46ef', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-17 07:56:24', '2018-10-17 07:56:24', '2018-10-24 07:56:24');
INSERT INTO `oauth_access_tokens` VALUES ('ec833489283cc9bf3d8231b366e9f5f02f43e32d468d919f5c3b1370297b3174aa05848894c9f220', 15, 3, 'Personal Access Token', '[]', 0, '2018-10-10 03:50:50', '2018-10-10 03:50:50', '2018-10-17 03:50:51');
INSERT INTO `oauth_access_tokens` VALUES ('ed02004dc8bd52d37d6ef3a0fab380d2bf14aec9e9e267fd4ac9d95bcc66e93eb53882a48f13568d', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-15 05:28:12', '2018-10-15 05:28:12', '2018-10-22 05:28:12');
INSERT INTO `oauth_access_tokens` VALUES ('ed349e95591f3a97f1795b6eb0c7bc7c360123e817e34ec6d25546325795eb6442b5a477225d8ea6', 19, 3, 'Personal Access Token', '[]', 0, '2018-10-18 03:21:32', '2018-10-18 03:21:32', '2018-10-25 03:21:33');
INSERT INTO `oauth_access_tokens` VALUES ('eef6c65f58b7c76a44ad3153b147fa6f0b3a9f32f4ce6f60b08269561e72ddab4e707484ac77884d', 24, 3, 'Personal Access Token', '[]', 0, '2018-11-08 14:19:49', '2018-11-08 14:19:49', '2018-11-15 14:19:49');
INSERT INTO `oauth_access_tokens` VALUES ('f12c05e0442449c268d41175b508fae14403e588ea39a2e3918c3b4f819655f9e985b2e469b0b37c', 3, 1, 'Personal Access Token', '[]', 0, '2018-09-18 03:27:33', '2018-09-18 03:27:33', '2018-09-25 03:27:33');
INSERT INTO `oauth_access_tokens` VALUES ('f3aba9160584b01571e31b162c7cc436917ab8b5acf9a1a50d8bc54b479e046ea8fb94c7db91a01a', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-14 17:38:20', '2018-10-14 17:38:20', '2018-10-21 17:38:20');
INSERT INTO `oauth_access_tokens` VALUES ('f4aca33e404cf6f954e1790fb1acf3926ca58c67d3367901c0cb1bdfd2fa41b98e9407e14a527981', 15, 3, 'Personal Access Token', '[]', 1, '2018-10-20 09:08:35', '2018-10-20 09:08:35', '2018-10-27 09:08:35');
INSERT INTO `oauth_access_tokens` VALUES ('f8039569cfa0637f9b7638b152a2e020b80a3277f9a80ae4da777b85975011f24053063453dccc2b', 17, 3, 'Personal Access Token', '[]', 0, '2018-10-10 09:06:08', '2018-10-10 09:06:08', '2018-10-17 09:06:08');
INSERT INTO `oauth_access_tokens` VALUES ('fa77d055b9381aa6a3db62bf4c3e7bc761e104b511399c3a534f8c199ec908f45a6653d5fb544c81', 26, 3, 'Personal Access Token', '[]', 0, '2018-10-30 05:34:00', '2018-10-30 05:34:00', '2018-11-06 05:34:00');
INSERT INTO `oauth_access_tokens` VALUES ('fad10da91547d4265f3edc7581466b69419daf129abf6483e9f1ca80fd52820a33a446af02632f17', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-25 08:31:43', '2018-09-25 08:31:43', '2018-10-02 08:31:43');
INSERT INTO `oauth_access_tokens` VALUES ('fbab7e2d7f65f989c666dfc0b74ec11933d5064a4397fc51c47e7986b635b71f14bb6b3cf6e79198', 8, 1, 'Personal Access Token', '[]', 0, '2018-09-23 18:44:30', '2018-09-23 18:44:30', '2018-09-30 18:44:30');
INSERT INTO `oauth_access_tokens` VALUES ('fcb0e87fbed8117db9ab140edcbe6abc01f4c9e7d6f099fb8595d1e3e9518f68679b3ff40ee67a5b', 16, 3, 'Personal Access Token', '[]', 0, '2018-10-10 08:11:04', '2018-10-10 08:11:04', '2018-10-17 08:11:04');
INSERT INTO `oauth_access_tokens` VALUES ('fdf8cf35d7fd3a85f946afee15f1249baca38b9023646af63aab44e6f46c7be02abbe919aec052f5', 30, 3, 'Personal Access Token', '[]', 0, '2018-11-07 02:55:54', '2018-11-07 02:55:54', '2018-11-14 02:55:54');

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_clients_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES (1, NULL, 'Laravel Personal Access Client', 'CzfpTXNFLtVkdAQRyrbtAEy5ELSXGgpIdORpb59N', 'http://localhost', 1, 0, 0, '2018-09-16 08:00:07', '2018-09-16 08:00:07');
INSERT INTO `oauth_clients` VALUES (2, NULL, 'Laravel Password Grant Client', '6T5sgZd5TaTC0ABY91tc8Nmx51UcXxMSJKMpR5T4', 'http://localhost', 0, 1, 0, '2018-09-16 08:00:07', '2018-09-16 08:00:07');
INSERT INTO `oauth_clients` VALUES (3, NULL, 'Laravel Personal Access Client', '6GAh20SixHcS7RrFGcPt6qx1xGNbet0Nm8z9VsmP', 'http://localhost', 1, 0, 0, '2018-10-09 17:05:56', '2018-10-09 17:05:56');
INSERT INTO `oauth_clients` VALUES (4, NULL, 'Laravel Password Grant Client', 'MxQvdfdcWRSgjyjv4oJG3MLhNfBtQTt3NBI0AHc1', 'http://localhost', 0, 1, 0, '2018-10-09 17:05:57', '2018-10-09 17:05:57');

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_personal_access_clients_client_id_index`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_personal_access_clients
-- ----------------------------
INSERT INTO `oauth_personal_access_clients` VALUES (1, 1, '2018-09-16 08:00:07', '2018-09-16 08:00:07');
INSERT INTO `oauth_personal_access_clients` VALUES (2, 3, '2018-10-09 17:05:56', '2018-10-09 17:05:56');

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_refresh_tokens_access_token_id_index`(`access_token_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO `positions` VALUES (1, 'Struktural', 'KEPALA KEJAKSAAN NEGERI DUMAI', '2018-09-03 09:41:29', '2018-09-26 01:10:02');
INSERT INTO `positions` VALUES (2, 'Struktural', 'KEPALA SEKSI TINDAK PIDANA UMUM', '2018-09-03 09:42:49', '2018-09-26 01:10:24');
INSERT INTO `positions` VALUES (3, 'Struktural', 'KEPALA SEKSI INTELIJEN', '2018-09-21 04:49:54', '2018-09-26 01:11:23');
INSERT INTO `positions` VALUES (4, 'Struktural', 'KEPALA SEKSI TINDAK PIDANA KHUSUS', '2018-09-25 10:42:59', '2018-09-26 01:12:03');
INSERT INTO `positions` VALUES (5, 'Struktural', 'KEPALA SEKSI PERDATA DAN TATA USAHA NEGARA', '2018-09-25 11:03:42', '2018-09-26 01:12:44');
INSERT INTO `positions` VALUES (6, 'Struktural', 'KEPALA SUB BAGIAN PEMBINAAN', '2018-09-26 01:13:10', '2018-09-26 01:13:10');
INSERT INTO `positions` VALUES (7, 'Struktural', 'KEPALA SEKSI BARANG BUKTI DAN BARANG RAMPASAN', '2018-09-26 01:13:49', '2018-09-26 01:13:49');
INSERT INTO `positions` VALUES (8, 'Struktural', 'KESUBSI PENUNTUTAN', '2018-09-26 01:14:22', '2018-09-26 01:14:22');
INSERT INTO `positions` VALUES (9, 'Struktural', 'PLT. KASUBSI PRAPENUNTUTAN', '2018-09-26 01:14:48', '2018-09-26 01:14:48');
INSERT INTO `positions` VALUES (10, 'Struktural', 'PLT. KASUBSI EKSEKUSI DAN EKSAMINASI', '2018-09-26 01:15:09', '2018-09-26 01:15:09');
INSERT INTO `positions` VALUES (11, 'Fungsional', 'JAKSA', '2018-09-26 01:15:32', '2018-09-26 01:15:32');
INSERT INTO `positions` VALUES (12, 'Adminstrator', 'STAF PIDUM / ADMINSTRATOR', '2018-09-26 01:17:43', '2018-09-26 01:17:43');

-- ----------------------------
-- Table structure for profile_sixteen
-- ----------------------------
DROP TABLE IF EXISTS `profile_sixteen`;
CREATE TABLE `profile_sixteen`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `sixteen_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 334 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of profile_sixteen
-- ----------------------------
INSERT INTO `profile_sixteen` VALUES (25, 8, 14, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (26, 8, 15, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (30, 12, 19, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (31, 15, 19, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (32, 12, 20, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (33, 17, 20, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (34, 13, 21, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (35, 15, 21, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (36, 15, 22, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (37, 16, 22, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (38, 12, 23, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (39, 15, 23, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (40, 14, 24, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (41, 17, 24, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (42, 16, 25, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (43, 17, 25, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (44, 12, 26, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (45, 16, 26, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (46, 15, 27, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (47, 17, 27, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (48, 12, 28, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (49, 15, 28, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (50, 16, 29, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (51, 17, 29, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (52, 16, 30, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (53, 17, 30, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (54, 16, 31, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (55, 17, 31, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (56, 10, 32, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (57, 12, 32, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (58, 10, 33, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (59, 16, 33, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (60, 16, 34, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (61, 17, 34, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (62, 10, 35, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (63, 15, 35, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (67, 11, 38, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (68, 15, 38, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (69, 9, 39, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (70, 12, 39, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (71, 16, 39, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (72, 12, 40, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (73, 16, 40, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (74, 10, 41, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (75, 15, 41, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (76, 10, 42, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (77, 15, 42, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (78, 15, 43, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (79, 16, 43, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (80, 13, 44, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (81, 17, 44, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (82, 16, 45, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (83, 17, 45, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (84, 13, 46, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (85, 16, 46, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (86, 13, 47, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (87, 16, 47, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (88, 15, 48, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (89, 16, 48, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (90, 15, 49, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (91, 17, 49, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (92, 10, 50, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (93, 12, 50, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (94, 15, 51, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (95, 17, 51, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (96, 12, 52, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (97, 16, 52, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (98, 14, 53, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (99, 16, 53, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (100, 14, 54, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (101, 16, 54, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (102, 13, 55, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (103, 17, 55, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (104, 12, 56, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (105, 13, 56, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (106, 13, 57, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (107, 17, 57, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (108, 12, 58, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (109, 13, 58, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (110, 13, 59, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (111, 17, 59, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (112, 13, 60, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (113, 16, 60, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (114, 9, 61, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (115, 12, 61, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (116, 15, 62, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (117, 17, 62, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (118, 16, 63, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (119, 17, 63, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (120, 15, 64, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (121, 17, 64, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (122, 14, 65, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (123, 16, 65, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (124, 14, 66, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (125, 16, 66, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (126, 16, 67, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (127, 17, 67, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (128, 12, 68, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (129, 17, 68, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (130, 12, 69, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (131, 17, 69, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (132, 16, 70, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (133, 17, 70, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (134, 15, 71, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (135, 16, 71, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (136, 15, 72, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (137, 16, 72, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (138, 15, 73, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (139, 17, 73, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (140, 16, 74, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (141, 17, 74, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (142, 16, 75, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (143, 17, 75, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (144, 14, 76, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (145, 16, 76, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (146, 15, 77, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (147, 16, 77, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (148, 15, 78, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (149, 17, 78, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (150, 10, 79, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (151, 12, 79, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (152, 16, 80, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (153, 17, 80, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (154, 12, 81, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (155, 16, 82, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (156, 17, 82, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (157, 14, 83, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (158, 16, 83, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (159, 14, 84, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (160, 16, 84, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (161, 12, 85, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (162, 17, 85, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (163, 13, 86, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (164, 16, 86, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (165, 16, 87, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (166, 17, 87, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (167, 15, 88, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (168, 17, 88, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (169, 19, 89, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (170, 10, 90, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (171, 16, 90, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (172, 9, 91, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (173, 12, 91, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (174, 19, 92, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (175, 13, 93, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (176, 15, 93, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (177, 13, 94, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (178, 15, 94, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (179, 12, 95, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (180, 15, 95, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (181, 15, 96, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (182, 15, 97, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (183, 17, 96, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (184, 17, 97, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (185, 12, 98, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (186, 16, 98, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (187, 12, 99, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (188, 16, 99, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (189, 13, 100, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (190, 15, 100, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (191, 13, 101, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (192, 15, 101, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (193, 15, 102, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (194, 16, 102, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (195, 15, 103, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (196, 16, 103, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (197, 13, 104, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (198, 15, 104, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (199, 15, 105, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (200, 17, 105, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (201, 15, 106, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (202, 17, 106, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (203, 13, 107, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (204, 17, 107, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (205, 13, 108, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (206, 17, 108, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (207, 16, 109, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (208, 17, 109, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (209, 16, 110, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (210, 17, 110, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (211, 16, 111, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (212, 17, 111, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (213, 12, 112, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (214, 15, 112, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (215, 15, 113, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (216, 17, 113, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (217, 15, 114, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (218, 17, 114, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (219, 15, 115, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (220, 16, 115, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (221, 12, 116, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (222, 15, 116, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (223, 13, 117, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (224, 16, 117, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (225, 16, 118, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (226, 17, 118, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (227, 16, 119, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (228, 17, 119, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (229, 16, 120, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (230, 17, 120, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (231, 12, 121, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (232, 13, 121, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (233, 13, 122, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (234, 16, 122, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (235, 13, 123, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (236, 16, 123, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (237, 12, 124, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (238, 16, 124, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (239, 15, 125, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (240, 20, 126, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (241, 20, 127, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (242, 16, 128, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (243, 19, 128, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (244, 16, 129, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (245, 17, 129, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (246, 15, 130, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (247, 17, 130, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (248, 12, 131, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (249, 15, 131, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (250, 12, 132, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (251, 16, 132, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (252, 13, 133, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (253, 17, 133, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (254, 10, 134, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (255, 17, 134, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (256, 13, 135, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (257, 16, 135, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (258, 15, 136, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (259, 16, 136, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (260, 16, 137, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (261, 17, 137, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (262, 12, 138, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (263, 13, 138, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (264, 16, 139, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (265, 17, 139, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (266, 15, 140, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (267, 17, 140, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (268, 12, 141, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (269, 17, 141, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (270, 13, 142, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (271, 15, 142, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (272, 12, 143, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (273, 17, 143, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (274, 10, 144, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (275, 17, 144, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (276, 16, 145, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (277, 17, 145, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (278, 12, 146, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (279, 16, 146, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (280, 10, 147, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (281, 17, 147, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (282, 12, 148, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (283, 17, 148, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (284, 13, 149, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (285, 16, 149, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (286, 16, 150, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (287, 17, 150, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (288, 13, 151, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (289, 17, 151, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (290, 12, 152, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (291, 13, 152, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (292, 16, 153, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (293, 17, 153, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (294, 16, 154, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (295, 17, 154, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (296, 15, 155, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (297, 16, 155, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (298, 16, 156, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (299, 17, 156, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (300, 14, 157, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (301, 15, 157, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (302, 16, 158, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (303, 17, 158, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (304, 16, 159, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (305, 16, 160, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (306, 17, 160, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (307, 10, 161, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (308, 12, 161, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (309, 15, 162, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (310, 16, 162, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (311, 14, 163, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (312, 17, 163, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (313, 17, 164, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (314, 15, 164, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (315, 17, 165, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (316, 15, 165, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (317, 14, 166, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (318, 17, 166, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (319, 15, 167, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (320, 16, 167, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (321, 14, 168, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (322, 16, 168, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (323, 16, 169, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (324, 17, 169, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (325, 10, 170, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (326, 17, 170, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (327, 16, 171, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (328, 17, 171, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (329, 15, 172, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (330, 17, 172, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (331, 16, 173, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (332, 15, 173, NULL, NULL);
INSERT INTO `profile_sixteen` VALUES (333, 16, 174, NULL, NULL);

-- ----------------------------
-- Table structure for profile_sixtena
-- ----------------------------
DROP TABLE IF EXISTS `profile_sixtena`;
CREATE TABLE `profile_sixtena`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `sixtena_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of profile_sixtena
-- ----------------------------
INSERT INTO `profile_sixtena` VALUES (8, 11, 5, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (9, 15, 5, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (10, 9, 6, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (11, 12, 6, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (12, 11, 7, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (13, 15, 7, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (14, 15, 8, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (15, 16, 8, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (18, 15, 10, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (19, 17, 10, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (20, 10, 11, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (21, 12, 11, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (22, 10, 12, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (23, 12, 12, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (24, 16, 13, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (25, 17, 13, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (26, 16, 14, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (27, 17, 14, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (28, 12, 15, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (29, 15, 15, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (30, 11, 16, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (31, 16, 16, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (32, 11, 17, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (33, 16, 17, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (34, 12, 18, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (35, 17, 18, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (36, 16, 19, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (37, 17, 19, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (38, 15, 20, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (39, 17, 20, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (42, 19, 22, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (43, 20, 23, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (44, 20, 24, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (45, 16, 25, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (46, 17, 25, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (47, 15, 26, NULL, NULL);
INSERT INTO `profile_sixtena` VALUES (48, 17, 26, NULL, NULL);

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `kelamin` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `photo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of profiles
-- ----------------------------
INSERT INTO `profiles` VALUES (4, '196606141992031002', 'MAT PERANG YUSUF, SH., MH.', 'ACEH', '1966-06-14', '1', 'Kejaksasaan Negeri Dumai', '081370880338', 1, 1, 28, '2018-09-21 04:48:43', '2018-10-31 11:02:53', NULL);
INSERT INTO `profiles` VALUES (6, '1978082102002121003', 'DELLAN FEBRIYALDY, SH., MH', 'SDAFA', '1980-02-10', '1', 'Kejaksaan Negeri Dumai', '23542', 3, 6, 13, '2018-09-25 10:07:37', '2018-09-26 09:29:38', NULL);
INSERT INTO `profiles` VALUES (7, '197709082002121003', 'ROY MODINO, SH.', 'AFADFASDFADS', '1977-09-08', '1', 'Kejaksaan Negeri Dumai', '082350733553', 3, 3, NULL, '2018-09-25 10:46:14', '2018-09-28 01:50:53', NULL);
INSERT INTO `profiles` VALUES (8, '198103082008121001', 'DENY ALVIANTO, S.H., M.Hum.', 'PEKANBARU', '1978-03-08', '1', 'Kejaksaan Negeri Dumai', '08xxx', 3, 4, 30, '2018-09-25 11:05:19', '2018-11-07 02:54:04', NULL);
INSERT INTO `profiles` VALUES (9, '198406242007121001', 'JONITRIANTO ANDRA, S.H., M.H.', 'PEKANBARU', '1989-05-07', '1', 'Kejaksaan Negeri Dumai', '085263055525', 3, 5, 17, '2018-09-26 09:36:27', '2018-10-10 09:05:46', NULL);
INSERT INTO `profiles` VALUES (10, '198211082007121001', 'NOVRIKA, S.H.', 'PEKANBARU', '1982-11-08', '1', 'Kejaksasaan Negeri Dumai', '082173137633', 3, 7, NULL, '2018-09-26 09:39:56', '2018-09-26 10:10:22', NULL);
INSERT INTO `profiles` VALUES (11, '197806282003121005', 'YUNIUS ZEGA, SH., MH', 'NIAS', '1978-06-28', '1', 'Kejaksaan Negeri Dumai', '081369957599', 3, 2, 16, '2018-09-26 09:42:10', '2018-10-19 00:57:22', '9WcvQTrfSfEFy5ybr1IQb3dzatf8cspOeYoFaoGw.png');
INSERT INTO `profiles` VALUES (12, '198103142007031001', 'HERY SUSANTO,SH', 'LAMPUNG', '1981-03-14', '1', 'Kejaksaan Negeri Dumai', '081388122201', 4, 11, 18, '2018-09-26 09:54:49', '2018-10-10 09:21:14', NULL);
INSERT INTO `profiles` VALUES (13, '198110182007121001', 'MAIMAN LIMBONG, SH', 'MEDAN', '1981-10-18', '1', 'Kejaksaan Negeri Dumai', '081213688933', 4, 11, 25, '2018-09-26 09:55:54', '2018-10-12 07:34:27', NULL);
INSERT INTO `profiles` VALUES (14, '198210152008122001', 'ROSLINA,SH', 'DUMAI', '1982-10-15', '2', 'Kejaksaan Negeri Dumai', '082166831501', 4, 11, 29, '2018-09-26 09:58:17', '2018-11-07 01:52:37', NULL);
INSERT INTO `profiles` VALUES (15, '198504242006041002', 'AGUNG NUGROHO,SH', 'DEPOK', '1985-04-24', '1', 'Kejaksaan Negeri Dumai', '082145944445', 5, 11, 19, '2018-09-26 10:00:31', '2018-10-10 10:06:09', NULL);
INSERT INTO `profiles` VALUES (16, '198905072014031003', 'HENGKY FRANSISCUS MUNTE, SH.', 'PEKANBARU', '1989-05-07', '1', 'Kejaksaan Negeri Dumai', '081266431668', 5, 8, 24, '2018-09-26 10:02:43', '2018-10-12 06:28:02', NULL);
INSERT INTO `profiles` VALUES (17, '198702202014031002', 'YOPENTINU ADI NUGRAHA,SH', 'PEKANBARU', '1987-02-20', '1', 'Kejaksaan Negeri Dumai', '081268201987', 6, 11, 26, '2018-09-26 10:04:11', '2018-10-14 23:53:24', NULL);
INSERT INTO `profiles` VALUES (18, '197406102001122004', 'DEWI IRANA', 'DUMAI', '1974-06-10', '2', 'Jl. Ombak Gg. Sempurna No. 1 Dumai - Kelurahan Ratu sima', '081365241774', 8, 12, 7, '2018-09-26 10:07:25', '2018-09-26 10:07:25', NULL);
INSERT INTO `profiles` VALUES (19, '123456789', 'Taufik', 'Dumai', '1996-10-13', '1', 'Jl. Gunung Merapi, Gg. Mulyo, No. 86', '082284499305', 1, 11, 31, '2018-09-27 09:27:00', '2018-11-27 07:57:47', 'zY1dlyuU4ZG8UEi5Cs0GnvjUNkxhYgLgGhbty4pQ.jpeg');
INSERT INTO `profiles` VALUES (21, '198103082008121001', 'DENY ALVIANTO, SH., M.Hum', 'Payakumbuh', '1981-03-08', '1', '-', '08122813875', 3, 4, NULL, '2018-11-07 02:53:01', '2018-11-07 02:53:01', NULL);

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES (1, 1, 1);
INSERT INTO `role_user` VALUES (2, 5, 2);
INSERT INTO `role_user` VALUES (3, 5, 6);
INSERT INTO `role_user` VALUES (4, 1, 7);
INSERT INTO `role_user` VALUES (5, 5, 8);
INSERT INTO `role_user` VALUES (6, 4, 9);
INSERT INTO `role_user` VALUES (7, 4, 10);
INSERT INTO `role_user` VALUES (8, 4, 12);
INSERT INTO `role_user` VALUES (9, 4, 13);
INSERT INTO `role_user` VALUES (10, 5, 14);
INSERT INTO `role_user` VALUES (11, 5, 15);
INSERT INTO `role_user` VALUES (12, 5, 16);
INSERT INTO `role_user` VALUES (13, 5, 17);
INSERT INTO `role_user` VALUES (14, 5, 18);
INSERT INTO `role_user` VALUES (15, 5, 19);
INSERT INTO `role_user` VALUES (16, 5, 24);
INSERT INTO `role_user` VALUES (17, 5, 25);
INSERT INTO `role_user` VALUES (18, 5, 26);
INSERT INTO `role_user` VALUES (19, 5, 27);
INSERT INTO `role_user` VALUES (20, 3, 28);
INSERT INTO `role_user` VALUES (21, 5, 29);
INSERT INTO `role_user` VALUES (22, 5, 30);
INSERT INTO `role_user` VALUES (23, 5, 31);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'Administrator', NULL, NULL);
INSERT INTO `roles` VALUES (2, 'operator', 'Operator', '2018-09-06 05:07:14', '2018-09-06 05:07:14');
INSERT INTO `roles` VALUES (3, 'kajari', 'Kepala Kejaksaan', '2018-09-06 05:07:30', '2018-09-06 05:07:30');
INSERT INTO `roles` VALUES (4, 'kasipidum', 'Kepala Seksi Pidana Umum', NULL, NULL);
INSERT INTO `roles` VALUES (5, 'jaksa', 'Jaksa', NULL, NULL);

-- ----------------------------
-- Table structure for seventeens
-- ----------------------------
DROP TABLE IF EXISTS `seventeens`;
CREATE TABLE `seventeens`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of seventeens
-- ----------------------------
INSERT INTO `seventeens` VALUES (5, 110, '12345', '2018-10-15', '-', '2018-10-14 17:27:38', '2018-10-14 17:27:38');
INSERT INTO `seventeens` VALUES (6, 86, 'B-1751/N.4.13/Epp.1/10/2018', '2018-09-14', 'Tindak Pidana Penganiayaan', '2018-10-16 07:42:29', '2018-10-16 07:42:29');
INSERT INTO `seventeens` VALUES (7, 42, 'B-1759/N.4.13/Euh.1/09/2018', '2018-09-28', '-', '2018-10-16 07:59:40', '2018-10-16 07:59:40');
INSERT INTO `seventeens` VALUES (8, 40, 'B-1760/N.4.13/Euh.1/10/2018', '2018-10-04', 'Tindak Pidana Narktoika', '2018-10-16 08:05:53', '2018-10-16 08:05:53');
INSERT INTO `seventeens` VALUES (9, 38, 'B-1761/N.4.13/Euh.1/10/2018', '2018-10-01', 'Tindak Pidana Narkotika', '2018-10-16 08:07:56', '2018-10-16 08:07:56');
INSERT INTO `seventeens` VALUES (10, 37, 'B-1762/N.4.13/Euh.1/10/2018', '2018-10-08', 'Tindak Pidana Narkotika', '2018-10-16 08:11:23', '2018-10-16 08:11:23');
INSERT INTO `seventeens` VALUES (11, 35, 'B-1765/N.4.13/Euh.1/10/2018', '2018-10-15', 'Tindak Pidana Narkotika', '2018-10-16 08:13:11', '2018-10-16 08:13:11');
INSERT INTO `seventeens` VALUES (12, 111, 'B-1636/N.4.13/Euh.1/09/2018', '2018-09-04', 'Tindak Pidana Narkotika', '2018-10-16 08:37:54', '2018-10-16 08:37:54');
INSERT INTO `seventeens` VALUES (13, 74, 'B-1586/N.4.13/Epp.1/09/2018', '2018-09-25', 'Tindak Pidana Pencurian dan atau Pertolongan Jahat', '2018-10-16 09:07:54', '2018-10-16 09:07:54');

-- ----------------------------
-- Table structure for sixteens
-- ----------------------------
DROP TABLE IF EXISTS `sixteens`;
CREATE TABLE `sixteens`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor_surat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 175 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sixteens
-- ----------------------------
INSERT INTO `sixteens` VALUES (14, 16, 'PRINT-1040/N.4.13/EUH.1/08/2018', '2018-08-20', NULL, '2018-09-25 11:21:06', '2018-09-25 11:21:06');
INSERT INTO `sixteens` VALUES (15, 17, 'PRINT-1040/N.4.13/EUH.1/08/2018', '2018-08-20', '-', '2018-09-25 11:32:00', '2018-09-25 11:32:00');
INSERT INTO `sixteens` VALUES (19, 30, 'Print-1158/N.4.13/Euh.1/09/2018', '2018-09-17', '-', '2018-10-05 09:14:37', '2018-10-05 09:14:37');
INSERT INTO `sixteens` VALUES (20, 27, 'Print-1207/N.4.13/Euh.1/09/2018', '2018-09-06', '-', '2018-10-05 09:16:55', '2018-10-05 09:16:55');
INSERT INTO `sixteens` VALUES (21, 41, 'Print-1093/N.4.13/Euh.1/08/2018', '2018-08-30', '-', '2018-10-05 09:28:14', '2018-10-05 09:28:14');
INSERT INTO `sixteens` VALUES (22, 38, 'Print-1094/N.4.13/Euh.1/08/2018', '2018-08-31', '-', '2018-10-05 09:29:33', '2018-10-05 09:29:33');
INSERT INTO `sixteens` VALUES (23, 39, 'Print-1069/N.4.13/Euh.1/09/2018', '2018-09-03', '-', '2018-10-05 09:32:01', '2018-10-05 09:32:01');
INSERT INTO `sixteens` VALUES (24, 47, 'Print-1041/N.4.13/Euh.1/08/2018', '2018-08-20', '-', '2018-10-05 09:33:27', '2018-10-05 09:33:27');
INSERT INTO `sixteens` VALUES (25, 46, 'Print-1090/N.4.13/Euh.1/08/2018', '2018-08-20', '-', '2018-10-05 09:34:30', '2018-10-05 09:34:30');
INSERT INTO `sixteens` VALUES (26, 45, 'Print-1039/N.4.13/Euh.1/08/2018', '2018-08-20', '-', '2018-10-05 09:35:38', '2018-10-05 09:35:38');
INSERT INTO `sixteens` VALUES (27, 42, 'Print-1056/N.4.13/Euh.1/08/2018', '2018-08-20', '-', '2018-10-05 09:36:45', '2018-10-05 09:36:45');
INSERT INTO `sixteens` VALUES (28, 43, 'Print-1055/N.4.13/Euh.1/08/2018', '2018-08-28', '-', '2018-10-05 09:37:50', '2018-10-05 09:37:50');
INSERT INTO `sixteens` VALUES (29, 37, 'Print-1089/N.4.13.Euh.1/09/2018', '2018-09-07', '-', '2018-10-05 09:41:11', '2018-10-05 09:41:11');
INSERT INTO `sixteens` VALUES (30, 35, 'Print-1127/N.4.13/Euh.1/09/2018', '2018-09-12', '-', '2018-10-05 09:42:42', '2018-10-05 09:42:42');
INSERT INTO `sixteens` VALUES (31, 34, 'Print-1128/N.4.13/Euh.1/09/2018', '2018-09-12', '-', '2018-10-05 09:45:56', '2018-10-05 09:45:56');
INSERT INTO `sixteens` VALUES (32, 33, 'Print-1129/N.4.13/Euh.1/09/2018', '2018-09-12', '-', '2018-10-05 09:46:52', '2018-10-05 09:46:52');
INSERT INTO `sixteens` VALUES (33, 31, 'Print-1131/N.4.13/Euh.1/09/2018', '2018-09-12', '-', '2018-10-05 09:47:59', '2018-10-05 09:47:59');
INSERT INTO `sixteens` VALUES (34, 22, 'Print-955/N.4.13/Euh.1/08/2018', '2018-08-02', '-', '2018-10-05 09:50:34', '2018-10-05 09:50:34');
INSERT INTO `sixteens` VALUES (35, 21, 'Print-976/N.4.13/Euh.1/08/2018', '2018-08-06', '-', '2018-10-05 09:52:11', '2018-10-05 09:52:11');
INSERT INTO `sixteens` VALUES (38, 54, 'PRINT-975/N.4.13/Euh.1/08/2018', '2018-08-06', '-', '2018-10-10 08:46:32', '2018-10-10 08:46:32');
INSERT INTO `sixteens` VALUES (39, 57, 'PRINT-828/N.4.13/Epp.2/07/2018', '2018-07-05', '-', '2018-10-10 09:14:54', '2018-10-10 09:14:54');
INSERT INTO `sixteens` VALUES (40, 62, 'PRINT-1232/N.4.13/Epp.1/10/2018', '2018-10-01', '-', '2018-10-11 12:29:11', '2018-10-11 12:29:11');
INSERT INTO `sixteens` VALUES (41, 59, 'print-1235/N.4.13/Epp.1/10/2018', '2018-10-03', '-', '2018-10-11 12:31:18', '2018-10-11 12:31:18');
INSERT INTO `sixteens` VALUES (42, 59, 'print-1235/N.4.13/Epp.1/10/2018', '2018-10-03', '-', '2018-10-11 12:32:05', '2018-10-11 12:32:05');
INSERT INTO `sixteens` VALUES (43, 60, 'PRINT-1236/N.4.13/Epp.1/10/2018', '2018-10-02', '-', '2018-10-11 12:33:22', '2018-10-11 12:33:22');
INSERT INTO `sixteens` VALUES (44, 66, 'PRINT-1203/N.4.13/Epp.1/09/2018', '2018-09-25', '-', '2018-10-11 12:36:16', '2018-10-11 12:36:16');
INSERT INTO `sixteens` VALUES (45, 67, 'PRINT-1180/N.4.13/Epp.1/09/2018', '2018-09-20', '=', '2018-10-11 12:38:36', '2018-10-11 12:38:36');
INSERT INTO `sixteens` VALUES (46, 63, 'PRINT-1218/N.4.13/Epp.1/09/2018', '2018-09-27', '-', '2018-10-11 12:39:56', '2018-10-11 12:39:56');
INSERT INTO `sixteens` VALUES (47, 65, 'PRINT-1209/N.4.13/Epp.1/09/2018', '2018-09-26', '-', '2018-10-11 12:41:14', '2018-10-11 12:41:14');
INSERT INTO `sixteens` VALUES (48, 64, 'PRINT-1210/N.4.13/Epp.1/09/2018', '2018-09-26', '-', '2018-10-11 12:42:23', '2018-10-11 12:42:23');
INSERT INTO `sixteens` VALUES (49, 68, 'PRINT-1152/N.4.13/Epp.1/09/2018', '2018-09-17', '-', '2018-10-11 12:43:31', '2018-10-11 12:43:31');
INSERT INTO `sixteens` VALUES (50, 61, 'PRINT-1231/N.4.13/Epp.1/10/2018', '2018-10-01', '-', '2018-10-11 12:45:00', '2018-10-11 12:45:00');
INSERT INTO `sixteens` VALUES (51, 81, 'PRINT-1208/N.4.13/Euh.1/09/2018', '2018-09-26', '-', '2018-10-11 13:25:39', '2018-10-11 13:25:39');
INSERT INTO `sixteens` VALUES (52, 83, 'PRINT-1219/N.4.13/Epp.1/09/2018', '2018-09-27', '-', '2018-10-11 13:45:34', '2018-10-11 13:45:34');
INSERT INTO `sixteens` VALUES (53, 84, 'PRINT-1181/N.4.13/Euh.1/09/2018', '2018-09-20', '-', '2018-10-11 13:50:21', '2018-10-11 13:50:21');
INSERT INTO `sixteens` VALUES (54, 84, 'PRINT-1181/N.4.13/Euh.1/09/2018', '2018-09-20', '-', '2018-10-11 13:52:03', '2018-10-11 13:52:03');
INSERT INTO `sixteens` VALUES (55, 85, 'Nomor : PRINT - 1261 /N.4.13/Epp.1/10/2018', '2018-10-09', '-', '2018-10-11 13:52:46', '2018-10-11 13:52:46');
INSERT INTO `sixteens` VALUES (56, 86, 'PRINT-1161/N.4.13/Epp.1/09/2018', '2018-09-18', '-', '2018-10-11 13:55:08', '2018-10-11 13:55:08');
INSERT INTO `sixteens` VALUES (57, 88, 'Nomor : PRINT- 1260 /N.4.13/Euh.1/10/2018', '2018-10-05', '-', '2018-10-11 13:59:29', '2018-10-11 13:59:29');
INSERT INTO `sixteens` VALUES (58, 89, 'Nomor : PRINT- 1233 /n.4.13/Euh.1/10/2018', '2018-10-02', '-', '2018-10-11 14:10:58', '2018-10-11 14:10:58');
INSERT INTO `sixteens` VALUES (59, 90, 'Nomor : PRINT- 1224 /N.4.13/Euh.1/10/2018', '2018-10-01', '-', '2018-10-11 14:22:11', '2018-10-11 14:22:11');
INSERT INTO `sixteens` VALUES (60, 72, 'PRINT-1089/N.4.13/Epp.1/08/2018', '2018-03-08', '-', '2018-10-12 02:13:13', '2018-10-12 02:13:13');
INSERT INTO `sixteens` VALUES (61, 74, 'PRINT-1038/N.4.13/Epp.1/08/2018', '2018-08-20', '-', '2018-10-12 02:15:16', '2018-10-12 02:15:16');
INSERT INTO `sixteens` VALUES (62, 73, 'PRINT-1053/N.4.13/Epp.1/08/2018', '2018-08-28', '-', '2018-10-12 02:17:07', '2018-10-12 02:17:07');
INSERT INTO `sixteens` VALUES (63, 70, 'PRINT-1092/N.4.13/Epp.1/08/2018', '2018-09-07', '-', '2018-10-12 02:19:01', '2018-10-12 02:19:01');
INSERT INTO `sixteens` VALUES (64, 69, 'PRINT-1101/N.4.13/Epp.1/09.2018', '2018-09-10', '-', '2018-10-12 02:20:36', '2018-10-12 02:20:36');
INSERT INTO `sixteens` VALUES (65, 79, 'PRINT-975/N.4.13/Euh.1/08/2018', '2018-08-06', '-', '2018-10-12 02:22:01', '2018-10-12 02:22:01');
INSERT INTO `sixteens` VALUES (66, 78, 'PRINT-958/N.4.13/Epp.1/08/2018', '2018-08-02', '-', '2018-10-12 02:23:24', '2018-10-12 02:23:24');
INSERT INTO `sixteens` VALUES (67, 40, 'PRIN-1070/N.4.13/Euh.1/09/2018', '2018-09-03', '-', '2018-10-12 02:26:45', '2018-10-12 02:26:45');
INSERT INTO `sixteens` VALUES (68, 97, 'PRINT-1223/N.4.13/Euh.1/10/2018', '2018-10-01', '-', '2018-10-12 03:05:09', '2018-10-12 03:05:09');
INSERT INTO `sixteens` VALUES (69, 97, 'PRINT-1223/N.4.13/Euh.1/10/2018', '2018-09-28', '-', '2018-10-12 03:06:18', '2018-10-12 03:06:18');
INSERT INTO `sixteens` VALUES (70, 96, 'PRINT-1226/N.4.13/Euh.1/10/2018', '2018-10-01', '-', '2018-10-12 03:07:38', '2018-10-12 03:07:38');
INSERT INTO `sixteens` VALUES (71, 95, 'PRINT-1262/N.4.13/Epp.1/10/2018', '2018-10-06', '-', '2018-10-12 03:09:40', '2018-10-12 03:09:40');
INSERT INTO `sixteens` VALUES (72, 94, 'PRINT-1263/N.4.13/Euh.1/10/2018', '2018-10-09', '-', '2018-10-12 03:18:33', '2018-10-12 03:18:33');
INSERT INTO `sixteens` VALUES (73, 93, 'PRINT-1264/N.4.13/Euh.1/10/2018', '2018-10-08', '-', '2018-10-12 03:20:07', '2018-10-12 03:20:07');
INSERT INTO `sixteens` VALUES (74, 92, 'PRINT-1265/N.4.13/Epp.1/10/2018', '2018-10-10', '-', '2018-10-12 03:21:51', '2018-10-12 03:21:51');
INSERT INTO `sixteens` VALUES (75, 91, 'PRINT-1259/N.4.13/Epp.1/10/2018', '2018-10-03', '-', '2018-10-12 03:23:01', '2018-10-12 03:23:01');
INSERT INTO `sixteens` VALUES (76, 100, 'PRINT-975/N.4.13/EUH.1/08/2018', '2018-08-21', '-', '2018-10-12 08:08:12', '2018-10-12 08:08:12');
INSERT INTO `sixteens` VALUES (77, 101, 'PRINT-782/N.4.13/EUH.2/06/2018', '2018-08-26', '-', '2018-10-12 08:20:58', '2018-10-12 08:20:58');
INSERT INTO `sixteens` VALUES (78, 80, 'PRINT-993/N.4.13/EUH.1/08/2018', '2018-08-07', '-', '2018-10-12 08:41:13', '2018-10-12 08:41:13');
INSERT INTO `sixteens` VALUES (79, 23, 'PRINT-974/N.4.13/Euh.1/08/2018', '2018-08-06', '-', '2018-10-12 08:54:28', '2018-10-12 08:54:28');
INSERT INTO `sixteens` VALUES (80, 102, 'PRINT-955/N.4.13/EUH.1/08/2018', '2018-08-02', '-', '2018-10-12 09:11:01', '2018-10-12 09:11:01');
INSERT INTO `sixteens` VALUES (81, 103, 'PRINT-120/N.4.13/EUH.1/02/2018', '2018-02-01', '-', '2018-10-12 09:18:38', '2018-10-12 09:18:38');
INSERT INTO `sixteens` VALUES (82, 104, 'PRINT-1040/N.4.13/EUH.2/08/2018', '2018-08-20', '-', '2018-10-12 09:21:54', '2018-10-12 09:21:54');
INSERT INTO `sixteens` VALUES (83, 105, 'PRINT-958/N.4.13/Epp.2/08/2018', '2018-08-02', '-', '2018-10-12 09:31:08', '2018-10-12 09:31:08');
INSERT INTO `sixteens` VALUES (84, 77, 'PRINT-1192/N.4.13/Epp.2/08/2018', '2018-08-07', '-', '2018-10-12 09:41:59', '2018-10-12 09:41:59');
INSERT INTO `sixteens` VALUES (85, 106, 'PRINT-628/N.4.13/Epp.1/05/2018', '2010-05-08', '-', '2018-10-12 09:49:10', '2018-10-12 09:49:10');
INSERT INTO `sixteens` VALUES (86, 107, 'PRINT-1084/N.4.13/Epp.2/08/2018', '2018-08-30', '-', '2018-10-12 09:50:53', '2018-10-12 09:50:53');
INSERT INTO `sixteens` VALUES (87, 108, 'PRINT-1142/N.4.13/Epp.2/08/2018', '2018-08-13', 'Tindak Pidana Pencurian', '2018-10-12 10:02:08', '2018-10-12 10:02:08');
INSERT INTO `sixteens` VALUES (88, 109, 'PRINT-796/N.4.13/Epp.1/06/2018', '2018-06-25', '-', '2018-10-12 10:04:40', '2018-10-12 10:04:40');
INSERT INTO `sixteens` VALUES (89, 110, '777', '2018-10-04', 'Tidak ada', '2018-10-14 13:10:54', '2018-10-14 13:10:54');
INSERT INTO `sixteens` VALUES (90, 111, 'PRINT-964/N.4.13/EUH.2/08/2018', '2018-08-04', 'Tindak Pidana Narkotika', '2018-10-16 08:34:33', '2018-10-16 08:34:33');
INSERT INTO `sixteens` VALUES (91, 112, 'Print-1038/N.4.13/Epp.1/08/2018', '2018-08-20', 'Tindak Pidana Pencrian dan atau Pertolongan  Jahat', '2018-10-16 09:06:19', '2018-10-16 09:06:19');
INSERT INTO `sixteens` VALUES (92, 113, '123', '2018-10-18', 'Tidak ada', '2018-10-18 02:05:16', '2018-10-18 02:05:16');
INSERT INTO `sixteens` VALUES (93, 29, 'PRINT-1166/N.4.13/EUH.2/09/2018', '2018-09-18', 'Tindak Pidana Narkotika', '2018-10-18 06:16:58', '2018-10-18 06:16:58');
INSERT INTO `sixteens` VALUES (94, 29, 'PRINT-1166/N.4.13/EUH.2/09/2018', '2018-09-18', 'Tindak Pidana Narkotika', '2018-10-18 06:16:59', '2018-10-18 06:16:59');
INSERT INTO `sixteens` VALUES (95, 32, 'PRINT-1130/N.4.13/EUH.2/09/2018', '2018-09-12', 'Tindak Pidana Narkotika', '2018-10-18 06:19:18', '2018-10-18 06:19:18');
INSERT INTO `sixteens` VALUES (96, 26, 'PRINT-1221/N.4.13/EUH.2/09/2018', '2018-09-28', 'TindakPidana Narkotika', '2018-10-18 06:21:34', '2018-10-18 06:21:34');
INSERT INTO `sixteens` VALUES (97, 26, 'PRINT-1221/N.4.13/EUH.2/09/2018', '2018-09-28', 'TindakPidana Narkotika', '2018-10-18 06:21:34', '2018-10-18 06:21:34');
INSERT INTO `sixteens` VALUES (98, 114, 'PRINT-1168/N.4.13/Euh.2/09/2018', '2018-09-18', 'Tindak Pidana Narkotika', '2018-10-18 06:50:03', '2018-10-18 06:50:03');
INSERT INTO `sixteens` VALUES (99, 114, 'PRINT-1168/N.4.13/Euh.2/09/2018', '2018-09-18', 'Tindak Pidana Narkotika', '2018-10-18 06:50:03', '2018-10-18 06:50:03');
INSERT INTO `sixteens` VALUES (100, 116, 'PRINT-1220/N.4.13/EUH.2/09/2018', '2018-09-27', 'Tindak Pidana Narkotika', '2018-10-18 07:00:28', '2018-10-18 07:00:28');
INSERT INTO `sixteens` VALUES (101, 116, 'PRINT-1220/N.4.13/EUH.2/09/2018', '2018-09-27', 'Tindak Pidana Narkotika', '2018-10-18 07:00:28', '2018-10-18 07:00:28');
INSERT INTO `sixteens` VALUES (102, 118, 'PRINT-1290/N.4.13/EUH.2/10/2018', '2018-10-17', 'Tindak Pidana Pencurian', '2018-10-18 07:12:51', '2018-10-18 07:12:51');
INSERT INTO `sixteens` VALUES (103, 118, 'PRINT-1290/N.4.13/EUH.2/10/2018', '2018-10-17', 'Tindak Pidana Pencurian', '2018-10-18 07:12:51', '2018-10-18 07:12:51');
INSERT INTO `sixteens` VALUES (104, 120, 'PRINT-1281/N.4.13/EUH.2/10/2018', '2018-10-15', 'Tindak PIDANA Pencurian', '2018-10-18 07:25:23', '2018-10-18 07:25:23');
INSERT INTO `sixteens` VALUES (105, 123, 'PRINT-1275/N.4.13/Epp.2/10/2018', '2018-10-10', 'Tindak Pidana Pencurian', '2018-10-18 07:39:19', '2018-10-18 07:39:19');
INSERT INTO `sixteens` VALUES (106, 123, 'PRINT-1275/N.4.13/Epp.2/10/2018', '2018-10-10', 'Tindak Pidana Pencurian', '2018-10-18 07:39:19', '2018-10-18 07:39:19');
INSERT INTO `sixteens` VALUES (107, 124, 'PRINT-1274/N.4.13/Epp.2/10/2018', '2018-10-10', 'Tindak Pidana Penggelapan', '2018-10-18 07:51:03', '2018-10-18 07:51:03');
INSERT INTO `sixteens` VALUES (108, 124, 'PRINT-1274/N.4.13/Epp.2/10/2018', '2018-10-10', 'Tindak Pidana Penggelapan', '2018-10-18 07:51:04', '2018-10-18 07:51:04');
INSERT INTO `sixteens` VALUES (109, 126, 'PRINT-1285/N.4.13/Epp.2/10/2018', '2018-10-16', 'Tindak Pidana Pencurian', '2018-10-18 08:00:06', '2018-10-18 08:00:06');
INSERT INTO `sixteens` VALUES (110, 126, 'PRINT-1285/N.4.13/Epp.2/10/2018', '2018-10-16', 'Tindak Pidana Pencurian', '2018-10-18 08:00:06', '2018-10-18 08:00:06');
INSERT INTO `sixteens` VALUES (111, 128, 'PRINT-1287/N.4.13/Epp.1/10/2018', '2018-10-16', 'Tindak Pidana Pencurian', '2018-10-18 08:19:17', '2018-10-18 08:19:17');
INSERT INTO `sixteens` VALUES (112, 129, 'Print-1283/N.4.13/Euh.2/10/2018', '2018-10-15', 'Tindak pidana Narkotika', '2018-10-21 09:46:29', '2018-10-21 09:46:29');
INSERT INTO `sixteens` VALUES (113, 132, 'Print-1292/N.4.13/Euh.1/10/2018', '2018-10-16', 'Tindak PiDANA NARKOTIKA', '2018-10-21 09:51:40', '2018-10-21 09:51:40');
INSERT INTO `sixteens` VALUES (114, 132, 'Print-1292/N.4.13/Euh.1/10/2018', '2018-10-16', 'Tindak PiDANA NARKOTIKA', '2018-10-21 09:51:40', '2018-10-21 09:51:40');
INSERT INTO `sixteens` VALUES (115, 133, 'Print-1276/N.4.13/Euh.1/10/2018', '2018-10-10', 'Tindak Pidana Narkotika', '2018-10-21 09:55:02', '2018-10-21 09:55:02');
INSERT INTO `sixteens` VALUES (116, 134, 'Print-1271/N.4.13/Euh.1/10/2018', '2018-10-10', 'Tindak Pidana Narkotika', '2018-10-21 09:58:49', '2018-10-21 09:58:49');
INSERT INTO `sixteens` VALUES (117, 135, 'Print-1306/N.4.24/Euh.1/10/2018', '2018-10-19', 'Tindak Pidana Narkotika', '2018-10-21 10:10:27', '2018-10-21 10:10:27');
INSERT INTO `sixteens` VALUES (118, 137, 'Print-1307/N.4.13/Epp.1/10/2018', '2018-10-19', 'Tindak Pidana Pencurian', '2018-10-21 11:30:37', '2018-10-21 11:30:37');
INSERT INTO `sixteens` VALUES (119, 138, 'Print-1295/N.4.13/Euh.1/10/2018', '2018-10-17', 'Tindak Pidana Narkotika', '2018-10-21 11:33:50', '2018-10-21 11:33:50');
INSERT INTO `sixteens` VALUES (120, 138, 'Print-1295/N.4.13/Euh.1/10/2018', '2018-10-17', 'Tindak Pidana Narkotika', '2018-10-21 11:33:50', '2018-10-21 11:33:50');
INSERT INTO `sixteens` VALUES (121, 139, 'Print-1294/N.4.13/Euh.1/10/2018', '2018-10-17', 'Tindak Pidana Narkotika', '2018-10-21 11:39:34', '2018-10-21 11:39:34');
INSERT INTO `sixteens` VALUES (122, 140, 'Print-1305/N.4.13/Euh.1/10/2018', '2018-10-19', 'Tindak Pidana Narkotika', '2018-10-21 11:48:14', '2018-10-21 11:48:14');
INSERT INTO `sixteens` VALUES (123, 136, 'Print-1306/N.4.13/Euh.1/10/2018', '2018-10-19', 'Tindak Pidana Narkotika', '2018-10-21 11:57:10', '2018-10-21 11:57:10');
INSERT INTO `sixteens` VALUES (124, 143, 'Print-1282/N.4.13/Euh.1/10/2018', '2018-10-15', 'Tindak Pidana Pelayaran', '2018-10-21 12:09:23', '2018-10-21 12:09:23');
INSERT INTO `sixteens` VALUES (125, 58, '123/xx.yy/2018', '2018-10-22', '-', '2018-10-21 19:57:32', '2018-10-21 19:57:32');
INSERT INTO `sixteens` VALUES (126, 145, '001/p16/2018', '2018-10-22', '-', '2018-10-21 20:29:53', '2018-10-21 20:29:53');
INSERT INTO `sixteens` VALUES (127, 146, '0123/p16/10/2018', '2018-10-23', '-', '2018-10-21 21:14:01', '2018-10-21 21:14:01');
INSERT INTO `sixteens` VALUES (128, 147, '123', '2018-10-22', 'Tidak ada', '2018-10-21 22:42:13', '2018-10-21 22:42:13');
INSERT INTO `sixteens` VALUES (129, 148, 'Print-1128/N.4.13/Euh.1/09/2018', '2018-09-12', 'Tindak Pidana Narkotika', '2018-10-22 01:44:56', '2018-10-22 01:44:56');
INSERT INTO `sixteens` VALUES (130, 149, 'Print-11152/N.4.13/Epp.1/09/2018', '2018-09-17', 'Tindak Pidana Pencurian', '2018-10-22 02:03:42', '2018-10-22 02:03:42');
INSERT INTO `sixteens` VALUES (131, 36, 'Print-1086/N.4.13/Euh.1/09/2018', '2018-09-07', 'Tindak pidana Narkotika', '2018-10-22 02:36:03', '2018-10-22 02:36:03');
INSERT INTO `sixteens` VALUES (132, 150, 'Print-1315/N.4.13/Epp.1/10/2018', '2018-10-22', 'Tindak Pidana Narkotika', '2018-10-25 11:57:13', '2018-10-25 11:57:13');
INSERT INTO `sixteens` VALUES (133, 151, 'Print-1313/N.4.13/Epp.1/10/2018', '2018-10-22', 'TINDAK PIDANA PENGANIAYAAN', '2018-10-25 12:17:15', '2018-10-25 12:17:15');
INSERT INTO `sixteens` VALUES (134, 152, 'Print-1311/N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Narkotika', '2018-10-25 12:23:05', '2018-10-25 12:23:05');
INSERT INTO `sixteens` VALUES (135, 153, 'Print-1318/N.4.13/Epp,1/10/2018', '2018-10-22', 'Tindak Pidana Penganiayaan', '2018-10-25 12:26:19', '2018-10-25 12:26:19');
INSERT INTO `sixteens` VALUES (136, 154, 'Print-1359/N.4.13/Euh.1/10/2018', '2018-03-10', 'Tindak pidana Narkotika', '2018-10-30 09:56:14', '2018-10-30 09:56:14');
INSERT INTO `sixteens` VALUES (137, 155, 'PRINT –  1360/N.4.13/Euh.1/10/2018', '2018-10-30', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 07:25:04', '2018-11-01 07:25:04');
INSERT INTO `sixteens` VALUES (138, 156, 'PRINT – 1361  /N.4.13/Euh.1/10/2018', '2018-10-30', 'Tindak Pidana  Penyalahgunaan Narkotika', '2018-11-01 07:33:32', '2018-11-01 07:33:32');
INSERT INTO `sixteens` VALUES (139, 157, 'PRINT –1365 /N.4.13/Euh.1/10/2018', '2018-10-31', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:07:42', '2018-11-01 08:07:42');
INSERT INTO `sixteens` VALUES (140, 158, 'PRINT –  1366 /N.4.13/Euh.1/10/2018', '2018-10-31', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:12:45', '2018-11-01 08:12:45');
INSERT INTO `sixteens` VALUES (141, 159, 'PRINT –  1363 /N.4.13/Euh.1/10/2018', '2018-10-31', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:18:37', '2018-11-01 08:18:37');
INSERT INTO `sixteens` VALUES (142, 160, 'PRINT – 1339/N.4.13/Epp.1/10/2018', '2018-10-25', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:23:43', '2018-11-01 08:23:43');
INSERT INTO `sixteens` VALUES (143, 161, 'PRINT –1338 /N.4.13/Epp.1/10/2018', '2018-10-24', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:31:33', '2018-11-01 08:31:33');
INSERT INTO `sixteens` VALUES (144, 162, 'PRINT –  1312 /N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:36:49', '2018-11-01 08:36:49');
INSERT INTO `sixteens` VALUES (145, 163, 'PRINT –  1317/N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Kekerasan dalam Rumah tangga', '2018-11-01 08:44:39', '2018-11-01 08:44:39');
INSERT INTO `sixteens` VALUES (146, 164, 'PRINT – 1315  /N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:48:20', '2018-11-01 08:48:20');
INSERT INTO `sixteens` VALUES (147, 165, 'PRINT – 1311 /N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:53:26', '2018-11-01 08:53:26');
INSERT INTO `sixteens` VALUES (148, 166, 'PRINT –1308/N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Penyalahgunaan Narkotika', '2018-11-01 08:59:03', '2018-11-01 08:59:03');
INSERT INTO `sixteens` VALUES (149, 167, 'PRINT –  1318   /N.4.13/Euh.1/10/2018', '2018-10-22', 'Tindak Pidana Penganiayan', '2018-11-01 09:03:57', '2018-11-01 09:03:57');
INSERT INTO `sixteens` VALUES (150, 168, 'PRINT – 1316 /N.4.13/Epp.1/10/2018', '2018-10-22', 'Tindak Pidana Pencurian', '2018-11-01 09:17:00', '2018-11-01 09:17:00');
INSERT INTO `sixteens` VALUES (151, 169, 'PRINT –  1362  /N.4.13/Epp.1/10/2018', '2018-10-31', 'Tindak Pidana pencurian', '2018-11-01 09:21:02', '2018-11-01 09:21:02');
INSERT INTO `sixteens` VALUES (152, 170, 'PRINT –1313   /N.4.13/Epp.1/10/2018', '2018-10-22', 'Tindak Pidana Pencurian', '2018-11-01 09:26:24', '2018-11-01 09:26:24');
INSERT INTO `sixteens` VALUES (153, 171, 'PRINT –1314/N.4.13/Epp.1/10/2018', '2018-10-22', 'Tindak Pidana Pencurian', '2018-11-01 09:42:20', '2018-11-01 09:42:20');
INSERT INTO `sixteens` VALUES (154, 172, 'PRINT –  1364/N.4.13/Epp.1/10/2018', '2018-10-31', 'Tindak Pidana Pencurian', '2018-11-01 09:46:02', '2018-11-01 09:46:02');
INSERT INTO `sixteens` VALUES (155, 48, 'Print-1001/N.4.13/Euh.1/08/2018', '2018-08-13', 'Tindak Pidana Narkotika', '2018-11-02 07:48:19', '2018-11-02 07:48:19');
INSERT INTO `sixteens` VALUES (156, 173, 'Print-1372/N.4.13/Epp.1/10/2018', '2018-11-02', 'Tindak Pidana Penipuan dan atau penggelapan', '2018-11-05 08:04:41', '2018-11-05 08:04:41');
INSERT INTO `sixteens` VALUES (157, 174, 'Print-1380/N.4.13/Euh.1/11/2018', '2018-11-05', 'Tindak Pidana Pencemaran Nama Baik dan atau Penghinaan', '2018-11-06 08:31:50', '2018-11-06 08:31:50');
INSERT INTO `sixteens` VALUES (158, 181, 'Print-1396/N.4.13/Epp.1/11/2018', '2018-11-07', 'Tindak Pidana Pencurian', '2018-11-08 02:27:14', '2018-11-08 02:27:14');
INSERT INTO `sixteens` VALUES (159, 179, 'Print-1395/N.4.13/Epp.1/11/2018', '2018-11-07', 'Tindak Pidana Kejahatan terhadap perkawinan', '2018-11-08 02:28:50', '2018-11-08 02:28:50');
INSERT INTO `sixteens` VALUES (160, 177, 'Print-1394/N.4.13/Euh.1/11/2018', '2018-11-07', 'Tindak PIdana Penyalahgunaan Narkotika', '2018-11-08 02:30:05', '2018-11-08 02:30:05');
INSERT INTO `sixteens` VALUES (161, 175, 'Print-1392/N.4.13/Epp,1/11/2018', '2018-11-07', 'Tindak Pidana Penggelapan', '2018-11-08 02:31:19', '2018-11-08 02:31:19');
INSERT INTO `sixteens` VALUES (162, 184, 'Print-04/N.4.13/Epp,1/01/2019', '2019-01-02', 'Tindak Pidana Kecelakaan Lalu Lintas', '2019-01-08 06:03:11', '2019-01-08 06:03:11');
INSERT INTO `sixteens` VALUES (163, 187, 'Print-01/N.4.13/Epp.1/01/2019', '2019-01-02', 'Tindak Pidana Pencurian', '2019-01-08 06:24:09', '2019-01-08 06:24:09');
INSERT INTO `sixteens` VALUES (164, 186, 'Print-17/N.4.13/Epp.1/01/2019', '2019-01-03', 'Tindak Pidana Persetubuhan terhadap anak dibawah umur', '2019-01-10 07:01:05', '2019-01-10 07:01:05');
INSERT INTO `sixteens` VALUES (165, 185, 'Print-18/N.4.13/Epp.1/01/2019', '2019-01-03', 'Tindak Pidana yang diduga melakukan persetubuhan dan atau melakukan perbuatan cabul terhadap anak', '2019-01-10 07:02:08', '2019-01-10 07:02:08');
INSERT INTO `sixteens` VALUES (166, 189, 'Print-01/N,4,13/Epp.1/01/2019', '2019-01-01', 'Tindak Pidana Pencurian', '2019-01-10 07:04:46', '2019-01-10 07:04:46');
INSERT INTO `sixteens` VALUES (167, 190, 'Print-04/N.4.13/Epp.1/01/2019', '2019-01-02', 'Tindak Pidana Kecelakaan Lalu Lintas', '2019-01-10 07:08:42', '2019-01-10 07:08:42');
INSERT INTO `sixteens` VALUES (168, 183, 'Print-21/N.4.13/Epp.1/01/2018', '2019-01-02', 'Tindak Pidana Pencurian', '2019-01-10 07:13:39', '2019-01-10 07:13:39');
INSERT INTO `sixteens` VALUES (169, 191, 'Print-03/N.4.13/Euh.1/01/2019', '2019-01-02', 'Tindak pidana Narkotika', '2019-01-10 07:32:39', '2019-01-10 07:32:39');
INSERT INTO `sixteens` VALUES (170, 192, 'Print-02/N.4..13/Euh.1/01/2019', '2019-01-02', 'Tindak Pidana Narkotika', '2019-01-10 07:37:50', '2019-01-10 07:37:50');
INSERT INTO `sixteens` VALUES (171, 193, 'Print-19/N.4.13/Epp.1/01/2019', '2019-01-03', 'Tindak Pidana Pencurian', '2019-01-10 07:47:31', '2019-01-10 07:47:31');
INSERT INTO `sixteens` VALUES (172, 194, 'Print-38/N.4.13/Euh.1/01/2019', '2019-01-10', 'Tindak Pidana Perlindungan dan Pengelolaan Lingkungan Hidup', '2019-01-16 02:48:08', '2019-01-16 02:48:08');
INSERT INTO `sixteens` VALUES (173, 195, 'Print-35/N.4.13/Euh./01/2019', '2019-01-10', 'Tindak Pidana Narkotika', '2019-01-16 02:54:35', '2019-01-16 02:54:35');
INSERT INTO `sixteens` VALUES (174, 196, 'Print-36/N.4.13/Epp.1/01/2019', '2019-01-10', 'Tindak Pidana Pencurian', '2019-01-16 02:58:25', '2019-01-16 02:58:25');

-- ----------------------------
-- Table structure for sixtenas
-- ----------------------------
DROP TABLE IF EXISTS `sixtenas`;
CREATE TABLE `sixtenas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor_surat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sixtenas
-- ----------------------------
INSERT INTO `sixtenas` VALUES (5, 54, 'PRINT-1217/N.4.13/Epp.2/09/2018', '2018-09-27', '-', '2018-10-10 09:00:24', '2018-10-10 09:00:24');
INSERT INTO `sixtenas` VALUES (6, 57, 'PRINT-1143/N.4.13/Epp.2/09/2018', '2018-09-13', '-', '2018-10-10 09:47:17', '2018-10-10 09:47:17');
INSERT INTO `sixtenas` VALUES (7, 100, 'PRINT-1227/N.4.13/Epp.2/09/2018', '2018-09-27', '-', '2018-10-12 08:16:30', '2018-10-12 08:16:30');
INSERT INTO `sixtenas` VALUES (8, 101, 'PRINT-1230/N.4.13/EUH.1/10/2018', '2018-10-02', '-', '2018-10-12 08:25:48', '2018-10-12 08:25:48');
INSERT INTO `sixtenas` VALUES (10, 80, 'PRINT-1213/N.4.13/Epp.2/09/2018', '2018-09-27', '-', '2018-10-12 08:49:02', '2018-10-12 08:49:02');
INSERT INTO `sixtenas` VALUES (11, 22, 'PRINT-1192/N.4.13/EUH.2/09/2018', '2018-09-25', '-', '2018-10-12 08:59:33', '2018-10-12 08:59:33');
INSERT INTO `sixtenas` VALUES (12, 23, 'PRINT-1192/N.4.13/EUH.2/09/2018', '2019-09-25', '-', '2018-10-12 09:01:14', '2018-10-12 09:01:14');
INSERT INTO `sixtenas` VALUES (13, 102, 'PRINT-1216/N.4.13/EUH.2/09/2018', '2018-09-27', '-', '2018-10-12 09:16:20', '2018-10-12 09:16:20');
INSERT INTO `sixtenas` VALUES (14, 46, 'PRINT-1256/N.4.13/EUH.2/10/2018', '2018-10-10', '-', '2018-10-12 09:25:32', '2018-10-12 09:25:32');
INSERT INTO `sixtenas` VALUES (15, 103, 'PRINT-637/N.4.13/EUH.2/05/2018', '2018-05-09', '-', '2018-10-12 09:29:06', '2018-10-12 09:29:06');
INSERT INTO `sixtenas` VALUES (16, 78, 'PRINT-1214/N.4.13/Epp.2/09/2018', '2018-09-27', '-', '2018-10-12 09:34:22', '2018-10-12 09:34:22');
INSERT INTO `sixtenas` VALUES (17, 77, 'PRINT-1925/N.4.13/Epp.2/102018', '2018-10-01', '-', '2018-10-12 09:46:36', '2018-10-12 09:46:36');
INSERT INTO `sixtenas` VALUES (18, 106, 'PRINT-767/N.4.13/Epp.2/06/2018', '2018-06-21', '-', '2018-10-12 09:54:24', '2018-10-12 09:54:24');
INSERT INTO `sixtenas` VALUES (19, 108, 'PRINT-1184/N.4.13/Epp.2/09/2018', '2018-09-20', 'Tindak Pidana Pencurian', '2018-10-12 10:05:41', '2018-10-12 10:05:41');
INSERT INTO `sixtenas` VALUES (20, 109, 'PRINT-1116/N.4.13/Euh.2/09/2018', '2018-09-12', '-', '2018-10-12 10:19:22', '2018-10-12 10:19:22');
INSERT INTO `sixtenas` VALUES (22, 110, '8787878787', '2018-10-18', 'Tidak ada', '2018-10-18 02:06:27', '2018-10-18 02:06:27');
INSERT INTO `sixtenas` VALUES (23, 145, 'xx/zz/123/2018', '2018-10-24', '-', '2018-10-21 20:34:23', '2018-10-21 20:34:23');
INSERT INTO `sixtenas` VALUES (24, 146, 'p16a/10.2018', '2018-10-23', '-', '2018-10-21 21:21:00', '2018-10-21 21:21:00');
INSERT INTO `sixtenas` VALUES (25, 148, 'Print-1189/N.4.13/Euh.2/09/2018', '2018-09-24', 'Tindak Pidana Narkotika', '2018-10-22 01:53:09', '2018-10-22 01:53:09');
INSERT INTO `sixtenas` VALUES (26, 68, 'Print-1315/N.4.13/Epp.2/09/2018', '2018-09-27', 'Tindak Pidana Pencurian', '2018-10-22 02:13:00', '2018-10-22 02:13:00');

-- ----------------------------
-- Table structure for suspects
-- ----------------------------
DROP TABLE IF EXISTS `suspects`;
CREATE TABLE `suspects`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `kelamin` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `kebangsaan` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pekerjaan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pendidikan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `agama` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 166 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suspects
-- ----------------------------
INSERT INTO `suspects` VALUES (10, NULL, 'Dedi Satria Als Dedi Bin Masran', 'Air Tiris', '1988-05-03', '1', 'Jl. Swadaya, RT 001, Kel. Bukti Batrem, Kota Dumai', '-', '2018-10-01 08:49:33', '2018-10-02 04:00:00', 'Indonesia', 'Wiraswasta', NULL, 'Islam');
INSERT INTO `suspects` VALUES (11, NULL, 'Suwandi Als Montil Bin Masri (alm)', 'Bangsal Aceh Dumai', '1997-03-15', '1', 'Jl. Industri, RT 004, Kel. Bangsal Aceh, Kota Dumai', '-', '2018-10-01 08:51:47', '2018-10-02 04:00:49', 'Indonesia', '-', '-', 'Islam');
INSERT INTO `suspects` VALUES (12, '-', 'Jonaidi Alias JON Bin Wahidin', 'Koto Tuo', '1983-04-30', '1', 'Jl. Simpang Tetap Darul Ihsan, Kec. Dumai Barat, Kota Dumai', '-', '2018-10-02 03:48:42', '2018-10-02 03:48:42', 'Indonesia', 'Wiraswasta', 'SMA - Tamat', 'Islam');
INSERT INTO `suspects` VALUES (13, '-', 'Dodi Bin (Alm) Rusdi', 'Medan', '1977-10-20', '1', 'Jl. Jawa, RT. 011, Kel. Bumi Ayu, Kec. Dumai Selatan Kota Dumai', '-', '2018-10-02 03:51:23', '2018-10-02 03:51:23', 'Indonesia', 'Wiraswasta', 'SMA - Tamat', 'Islam');
INSERT INTO `suspects` VALUES (14, NULL, 'Febio Julius Saputra Als Purta Bin Erizal', 'Dumai', '1993-11-29', '1', 'Jl. Bintan Gg Satria, RT 014, Kel. Suka Jadi, Kec. Dumai Kota, Kota Dumai', '-', '2018-10-02 03:54:13', '2018-10-02 03:54:13', 'Indonesia', 'Belum Bekerja', 'SMP - Tidak Tamat', 'Islam');
INSERT INTO `suspects` VALUES (15, NULL, 'Yanto Bin Bujang', 'Bangsal Aceh Dumai', '1987-05-03', '1', 'Jl. Sepakat, RT 020, Kel. Bukit Kayu Kapur, Kec. Bukit Kapur, Kota Dumai', '-', '2018-10-02 03:56:11', '2018-10-02 04:03:38', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (16, NULL, 'Nopendra Bin Sudarmadi', 'Selat Panjang', '1977-11-19', '1', 'Jl. SMEA, RT 002/RW 002, Kel. Selat Panjang Timur, Kec. Tebing Tinggi, Kab. Meranti', '-', '2018-10-02 03:59:14', '2018-10-02 04:03:57', 'Indonesia', 'Nahkoda', '-', 'Islam');
INSERT INTO `suspects` VALUES (17, '-', 'DANIEL HOTMAN SIMANUNGKALIT', 'Dumai', '1955-10-05', '1', 'Gg.Manggis RT.011 Kel.Bintan Kec.Dumai  Kota Kota Dumai', '-', '2018-10-02 08:08:21', '2018-10-02 08:08:21', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (18, '-', 'HERIYANTO ALS HERI BIN SUDARO', 'Dumai', '1987-08-05', '1', 'Jl..Prof M.Yamin RT.01 Kel. Laksamana Kec.Dumai Kota Kota Dumai', '-', '2018-10-02 08:18:02', '2018-10-02 08:18:02', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (19, '-', 'Ruslianto Bin Abdul Gani', 'Bengkalis', '1978-02-08', '1', 'Jl. Belimbing Gg.binjai  II Kel. Rimba Sekampung Kota Dumai', '-', '2018-10-02 08:33:01', '2018-10-02 08:33:01', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (20, '-', 'Wisnu Tri  Budi Bin Raili', 'Aceh', '1997-05-02', '1', 'Jl.Panam RT.018 Kel. Bukit Kayu Kapur Kota Dumai', '-', '2018-10-02 08:37:34', '2018-10-02 08:37:34', 'Indonesia', NULL, '-', 'Islam');
INSERT INTO `suspects` VALUES (21, '-', 'Engki Meryanto Bin Ermeli', 'Dumai', '1989-03-10', '1', 'Jl.Sepakat No.015 Kel.Teluk Binjai Kec.Bukit Kapur Kota Dumai', '-', '2018-10-02 08:41:34', '2018-10-02 08:41:34', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (22, '-', 'Rafli Ferdian Saputra Bin Razali', 'Dumai', '1999-02-09', '1', 'Jl.Cut Nyak Dien Kel.Bangsal Aceh Kec.Dumai Barat Kota Dumai', '-', '2018-10-02 08:48:33', '2018-10-02 08:48:33', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (23, '-', 'Febri Ibnu Mulia Bin Deny Frengky, dkk', 'Bengkalis', '1998-02-21', '1', 'JL.Gunung bromo Gg.Panca Karya Dumai', '-', '2018-10-02 08:54:42', '2018-10-02 08:54:42', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (24, '-', 'Herman als Herman Belando Bin Masri (alm)', 'Teluk Lancar', '1977-03-04', '1', 'Jl..M.H.Husni Tamrin Gg.Pare RT.04 Kel.ratu Sima Kec.Dumai Selatan Kota Dumai', NULL, '2018-10-02 09:10:54', '2018-10-02 09:10:54', 'Indonesia', NULL, '-', 'Islam');
INSERT INTO `suspects` VALUES (25, '-', 'Herman als Herman Belando Bin Masri (alm)', 'Teluk Lancar', '1977-03-04', '1', 'Jl..M.H.Husni Tamrin Gg.Pare RT.04 Kel.ratu Sima Kec.Dumai Selatan Kota Dumai', NULL, '2018-10-02 09:10:54', '2018-10-02 09:10:54', 'Indonesia', NULL, '-', 'Islam');
INSERT INTO `suspects` VALUES (26, '-', 'Jaya Putra Pasaribu', 'Naga Mas', '1997-10-23', '1', 'Jl.Lestari RT.01 Kel.Kesumbo Ampai Kec.Mandau Kab.Bengkalis', '-', '2018-10-02 09:16:15', '2018-10-02 09:16:15', 'Indonesia', 'Tidak bekerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (27, '-', 'Dedi Bin Yusuf', 'Sumut', '1981-04-14', '1', 'Jl.Parit Purba RT.09 Kel.Pelintung Kec.Medang Kampai Kota Dumai', '-', '2018-10-02 09:23:25', '2018-10-02 09:23:25', 'Indonesia', 'Mandor Alat Berat Perkebunan', '-', 'Islam');
INSERT INTO `suspects` VALUES (28, '-', 'Tian Ha als Apo', 'Rupat', '1966-12-26', '1', 'Jl.Rambutan Gg.Gandum RT.II Kel.Rimba Sekampung Kec.Dumai Kota Kota Dumai', '-', '2018-10-02 09:26:14', '2018-10-02 09:26:14', 'Indonesia', 'Wiraswasta', '-', 'Budha');
INSERT INTO `suspects` VALUES (29, '-', 'Andi Kurniawan Als Andi Bin Hendri,dkk', 'Dumai', '2001-03-27', '1', 'Jl.Batu Bintang Kel. Purnama Kec.Dumai Barat Kota Dumai', '-', '2018-10-02 09:31:42', '2018-10-02 09:31:42', 'Indonesia', 'Buruh', '-', 'Budha');
INSERT INTO `suspects` VALUES (30, '-', 'Yulius.L Febrisen', 'Palembang', '1973-02-05', '1', 'Jl.Pemuda Darat No.40 Kel.Pangkalan Sesai Kec.Dumai BARAT Dumai', NULL, '2018-10-05 06:21:16', '2018-10-05 06:21:16', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (31, '-', 'Harno Siagian als rnold Bin (alm) Udin Siagian', 'Padang Pulau', '1976-10-06', '1', 'Jl.Perjuangan RT.010 Kel.Bukit Batrem Kec.Dumai Timur Dumai', NULL, '2018-10-05 06:29:31', '2018-10-05 06:29:31', 'Indonesia', 'PNS', '-', 'Islam');
INSERT INTO `suspects` VALUES (32, '-', 'Edi Julianto als Edi Bin Warjono', 'Dumai', '1986-07-10', '1', 'Jl.Perintis Gg.Amir RT.02 Kel.Bumi Ayu Dumai', NULL, '2018-10-05 06:31:30', '2018-10-05 06:31:30', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (33, '-', 'Ari Priambodo Bin (alm) Sukarni', 'Dumai', '1993-08-12', '1', 'Jl.Sempurna Gg.Fajarsari No.25 RT.09 Kel.Ratu Sima Dumai', NULL, '2018-10-05 06:34:59', '2018-10-05 06:34:59', 'Indonesia', 'Tidak bekerja', NULL, 'Islam');
INSERT INTO `suspects` VALUES (34, '-', 'Renold Maulana als Renol Bin Muslim', 'Dumai', '1985-02-02', '1', 'Jl.Jawa I RT.05 Kel.Bumi Ayu Kec.Dumai Selatan Dumai', NULL, '2018-10-05 06:36:48', '2018-10-05 06:36:48', 'Indonesia', 'Swasta', NULL, 'Islam');
INSERT INTO `suspects` VALUES (35, '-', 'Sodri Bin Syamsunil Syam', 'Dumai', '1998-09-20', '1', 'Jl.Gatot Subroto KM.14 Kel.BANGSAL ACEH KOTA DUMAI', NULL, '2018-10-05 06:38:45', '2018-10-05 06:38:45', 'Indonesia', 'Petani', NULL, 'Islam');
INSERT INTO `suspects` VALUES (36, '-', 'Erlandia Sahputra als Engku Bin Efendi, dkk', 'Dumai', '1996-06-03', '1', 'Jl.GUNUNG KEL.GUNTUNG KEC.MEDANG KAMPAI DUMAI', NULL, '2018-10-05 06:40:37', '2018-10-05 06:40:37', NULL, 'Supir', NULL, 'Islam');
INSERT INTO `suspects` VALUES (37, '-', 'Taufik Hidayat als Dayat BIN Ruslan', 'Bagan Siapi-api', '1975-05-11', '1', 'Jl.Sei Teras Gg.Durian II Kel.STDI Kec.Dumai Barat Kota Dumai', NULL, '2018-10-05 06:42:53', '2018-10-05 06:42:53', 'Indonesia', 'Wiraswasta', NULL, 'Islam');
INSERT INTO `suspects` VALUES (38, '-', 'Denny Hariadi als Deni Bin Ellyan Sutikno', 'Dumai', '1982-10-22', '1', 'Jl.Teladan Gg.Melati No.01 Kel.Jaya Mukti Kec.Dumai Timur Dumai', NULL, '2018-10-05 06:45:10', '2018-10-05 06:45:10', 'Indonesia', 'Wiraswasta', NULL, 'Islam');
INSERT INTO `suspects` VALUES (39, '-', 'Imam Maradona als Donald Bin (Alm) Syamsuar', 'Dumai', '1986-06-30', '1', 'Jl.Pangkalan Sena Gg.Tanjung RT.03 Kel.STDI Dumai', NULL, '2018-10-05 06:47:11', '2018-10-05 06:47:11', 'Indonesia', 'Supir', NULL, 'Islam');
INSERT INTO `suspects` VALUES (40, '-', 'Ali Kuto Harahap als Harahap Bin (alm) Rahman Harahap', 'Tapsel', '1967-02-12', '1', 'Jl.Soekarno Hatta RT.03 Kel.Bukit Batrem Dumai', NULL, '2018-10-05 06:50:07', '2018-10-05 06:50:07', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (41, '-', 'Syahroni als Ipan Bin alm Hasan Basri', 'Bengkalis', '1986-08-10', '1', 'Jl.Villa Guru No.02 Klel.Bukit Timah Kec.Dumai Selatan Dumai', NULL, '2018-10-05 06:52:13', '2018-10-05 06:52:13', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (42, '-', 'Nanang Ardiansyah als Nanang Bin Amin', 'Dumai', '1988-11-10', '1', 'Jl.Parit Tugu Gg.Atan Dires Kel.Mundam Kec.Medang Kampai Dumai', NULL, '2018-10-05 06:54:39', '2018-10-05 06:54:39', 'Indonesia', 'Buruh', NULL, 'Islam');
INSERT INTO `suspects` VALUES (43, '-', 'Andri als Andri Bin Suhariyadi, dkk', 'Dumai', '2000-04-16', '1', 'Jl.Bukit Datuk lama RT.003 Kel.Bukit Datuk Kec.Dumai Selatan Dumai', NULL, '2018-10-05 06:56:53', '2018-10-05 06:56:53', 'Indonesia', 'Belum berkerja', NULL, 'Islam');
INSERT INTO `suspects` VALUES (44, '-', 'Eri Gunawan als Heri Bin Ridjan', 'Dumai', '1978-04-17', '1', 'Jl.Pemuda Darat Gg.Wakaf No.8 RT.II Kel.Pangkalan Sesai Kec.Dumai Barat Dumai', NULL, '2018-10-05 07:03:14', '2018-10-05 07:03:14', 'Indonesia', 'Nellayan', NULL, 'Islam');
INSERT INTO `suspects` VALUES (45, '-', 'Riatun Mariana als Silvi Binti Bither Pardede', 'Duri', '1997-05-16', '1', 'Jl.Teratai Gg.Teratai I Kel.Dumai Kota Dumai', NULL, '2018-10-05 07:07:16', '2018-10-05 07:07:16', 'Indonesia', 'Belum berkerja', NULL, 'Protestan');
INSERT INTO `suspects` VALUES (46, '-', 'Wan Muhammaf hafiz Nawawi Bin alm Wan Edi', 'Dumai', '2002-04-30', '1', 'Jl.Raja Wali Ujung RT.003 Kel.Laksamana Kec,Dumai Kota Dumai', NULL, '2018-10-05 07:10:07', '2018-10-05 07:10:07', 'Indonesia', 'Tidak bekerja', NULL, 'Islam');
INSERT INTO `suspects` VALUES (47, '-', 'Laila Novellia als Lela Binti Shahrom', 'Teluk Rhu Rupat Utara', '1996-11-17', '1', 'Jl.Teratai Gg.Teratai Kel.Dumai  Kota Dumai', NULL, '2018-10-05 07:12:39', '2018-10-05 07:12:39', 'Indonesia', 'Mengurus Rumah tangga', '-', 'Islam');
INSERT INTO `suspects` VALUES (48, '-', 'Wahyudi als Yudi Bin alm Kabotinggi', 'Dumai', '1980-06-06', '1', 'Jl.Kelapa Tiga Kel.Purnama Kec.Dumai Barat Dumai', NULL, '2018-10-05 07:14:40', '2018-10-05 07:14:40', 'Indonesia', 'Tani', NULL, 'Islam');
INSERT INTO `suspects` VALUES (49, '-', 'Yossi Syafitra Dewi als Uci Lesut Binti Syafril M', 'Padang', '1989-10-24', '1', 'Jl.CENDRAWASIH RT.03 KEL.LAKSAMANA KEC.DUMAI KOTA DUMAI', NULL, '2018-10-05 07:18:12', '2018-10-05 07:18:12', 'Indonesia', 'Tidak bekerja', NULL, 'Islam');
INSERT INTO `suspects` VALUES (50, '-', 'Ramadeni als Deni Cilax Bin alm Rostami', 'Dumai', '1982-07-14', '1', 'Jl.Teratai Gg.Dahlia Kel.Dumai Kota Dumai', NULL, '2018-10-05 07:20:05', '2018-10-05 07:20:05', 'Indonesia', 'Swasta', NULL, 'Islam');
INSERT INTO `suspects` VALUES (51, '-', 'Fris Flyangga', 'Jakarta', '1969-02-06', '1', 'Jl.Cempedak Gg.Kelapa Kel.Rimba Sekampung Kec.Dumai Kota Dumai', NULL, '2018-10-05 07:21:51', '2018-10-05 07:21:51', 'Indonesia', NULL, NULL, 'Islam');
INSERT INTO `suspects` VALUES (52, '-', 'Izhar als Har Bin alm H.Ismail', 'Bengkalis', '1972-12-31', '1', 'Jl.Benteng No.07 RT.06 Kel.Pangkalan Sesai Kec.Dumai Barat Dumai', NULL, '2018-10-05 07:23:54', '2018-10-05 07:23:54', 'Indonesia', 'Tidak bekerja', NULL, NULL);
INSERT INTO `suspects` VALUES (53, '-', 'Marahadi Ritonggga als Adi  Bin alm H,Mansur Ritonga', 'Dumai', '1980-03-01', '1', 'Jl.Tegalega No.57 Kel.Bukit Datuk Kec.Dumai Selatan Dumai', NULL, '2018-10-05 07:27:52', '2018-10-05 07:27:52', 'Indonesia', 'PNS Satpol PP', NULL, 'Islam');
INSERT INTO `suspects` VALUES (54, '-', 'Samsir Posmauli Simatupang', 'Surau', '1980-09-05', '1', 'Jl.Utama Karya RT.012 Kel.Bukit Batrem Kec.Dumai Timur Kota Dumai', NULL, '2018-10-05 07:30:06', '2018-10-05 07:30:06', 'Indonesia', 'Buruh Harian Lepas', NULL, 'Protestan');
INSERT INTO `suspects` VALUES (55, '-', 'Oddy Harvinas als odi Bin alm Darvis', 'Dumai', '1989-05-21', '1', 'BTN Taman Mitra A1 /15 Kel.Bukit Timah Kec.Dumai Selatan Dumai', NULL, '2018-10-05 07:33:21', '2018-10-05 07:33:21', 'Indonesia', 'Wiraswasta', NULL, 'Islam');
INSERT INTO `suspects` VALUES (56, '-', 'Hendri als Awi bin alm Effendi', 'Dumai', '1977-02-04', '1', 'Jl.Gajah Mada No.25 RT.13 Kel.Buluh Kasap Kec.Dumai Timur Dumai', NULL, '2018-10-05 07:36:23', '2018-10-05 07:36:23', 'Indonesia', 'Wiraswasta', NULL, 'Budha');
INSERT INTO `suspects` VALUES (57, '-', 'Prayogi  Tegar Wibawa als Yogi Bin Serianto', 'Dumai', '1984-09-02', '1', 'Jl.Berembang Gg.Kelapa RT.04 Kel.Rimba Sekampung Dumai Kota Dumai', NULL, '2018-10-05 07:39:37', '2018-10-05 07:39:37', 'Indonesia', NULL, NULL, 'Islam');
INSERT INTO `suspects` VALUES (58, '-', 'Anang Indra Priyanto als Anang Bin Arief Rahmanto, dkk', 'Dumai', '1996-12-12', '1', 'Jl.Janur Kuning Gg.Kapua RT.005 kEL.Jaya Mukti Kec.Dumai Timur Kota Dumai', NULL, '2018-10-05 07:46:18', '2018-10-05 07:46:18', 'Indonesia', NULL, NULL, 'Islam');
INSERT INTO `suspects` VALUES (59, '-', 'Ferdinan Rycoboy Sitompul alias Boy bin (Alm) Jokeri Sitompul dkk', 'Dumai', '1997-10-06', '1', 'Jl. Merdeka Baru No. 10 RT. 027 Kel. Teluk Binjai - Dumai Timur Dumai', '-', '2018-10-10 08:34:26', '2018-10-10 08:34:26', 'Indonesia', 'Belum Bekerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (60, '-', 'Zulhermanto bin M. Ja\'far alias Herman Legop', 'Dumai', '1979-09-30', '1', 'Jl. Patimura No. 44 Kel. Dumai Kota - Dumai', '-', '2018-10-10 09:10:17', '2018-10-10 09:10:17', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (61, '-', 'Muhamad Farok als Ayup', 'Pelintung', '1995-07-04', '1', 'Jl.Telapak Sakti RT.01 Kel.Pelintung Kec.Medang KAMPAI Dumai', NULL, '2018-10-11 09:08:04', '2018-10-11 09:08:04', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (62, '-', 'Jefri Silaban Bin Edison Silaban', 'Medan', '2001-08-11', '1', 'Jl.Utama Karya Gg.Sadar RT,03 Bukit Batrem III Dumai', NULL, '2018-10-11 09:15:33', '2018-10-11 09:15:33', 'Indonesia', 'Tidak bekerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (63, '-', 'Muhammad`Faisal als Faisal Bin H.Muhammad Yusuf', 'Dumai', '1995-06-07', '1', 'Jl.Melati No.15 RT.011 Kel.Datuk Laksamana Kec.Dumai Kota Dumai', '-', '2018-10-11 09:19:04', '2018-10-11 09:19:04', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (64, '-', 'M.Fahroni', 'Dumai', '1978-01-01', '1', 'Jl.Kebun Kel.Bukit Kayu Kapur Kec.Bukit Kapur Dumai', NULL, '2018-10-11 09:20:56', '2018-10-11 09:20:56', 'Indonesia', 'Kary PT.IBP', NULL, 'Islam');
INSERT INTO `suspects` VALUES (65, '-', 'Fetri Kumala als Fetri Bin Fadly Yahya', 'Dumai', '1964-04-02', '1', 'Jl.Tunas Mekar No.09 B Kel.Bukit Datuk Kec.Dumai Selatan Dumai', NULL, '2018-10-11 09:22:56', '2018-10-11 09:22:56', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (66, '-', 'Muhammad Adib als Adib Bin Tamsil', 'Dumai', '1995-09-09', '1', 'Jl.Perjuangan Kel.Teluk Binjai Dumai', '-', '2018-10-11 09:34:59', '2018-10-11 09:34:59', 'Indonesia', 'Mahasiswa', '-', 'Islam');
INSERT INTO `suspects` VALUES (67, '-', 'Endang Abu Bakar als Endang Bin Umar Seman', 'Dumai', '1983-04-15', '1', 'Jl.Sabar Menanti Gg.Sabda  RT.013 Kel.Bumi Ayu Dumai', NULL, '2018-10-11 09:36:56', '2018-10-11 09:36:56', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (68, '-', 'Surya Ramadhan Harahap als Siboy Bin H.Darwis Harahap', 'MEDAN', '1984-06-22', '1', 'Jl.Mitra Seruni Ratu Kel.Bukit Timah Kec.Dumai Selatan Dumai', NULL, '2018-10-11 09:40:12', '2018-10-11 09:40:12', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (69, '-', 'Wendi Haryadi als Aneh Bin Hamzah', 'Dumai', '1995-09-01', '1', 'Jl.Siak Kel.Pangkalan Sesai Kec.Dumai Barat Dumai', NULL, '2018-10-11 09:41:33', '2018-10-11 09:41:33', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (70, '-', 'Arif Prima Silalahi Bin M.Silalahi', 'Balam', '1991-08-31', '1', 'Jl.Pemuda Laut Gg.Teladan RT.006 Kel.Pangkalan Sesai Kec.DUMAI BARAT Dumai', '-', '2018-10-11 09:43:24', '2018-10-11 09:43:24', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (71, '-', 'Agustino Sigalingging Bin Binsar Sigalingging', 'Dumai', '1999-08-01', '1', 'Jl.Yaktapena Kel.Bukit Timah Kec.Dumai Selatan Dumai', NULL, '2018-10-11 09:45:12', '2018-10-11 09:45:12', 'Indonesia', 'Belum berkerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (72, '-', 'Anang Indra Priyanto Als Anang Bin Arief Rahmanto.Dkk', 'Dumai', '1996-12-17', '1', 'Jl.Janur Kuning Gg.Kapua RT.005 Kel.Jaya Mukti Kec.Dumai Timur - Kota Dumai', '-', '2018-10-11 10:12:55', '2018-10-11 10:12:55', 'Indonesia', 'Belum Bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (73, '-', 'Dudi Putra Als Dudi Bin Junaidi', 'Medan', '1983-04-14', '1', 'Jl.Soekarno Hatta Gg.Tengkorak Kel.Bukit Kayu Kappur Kec.Bukit Kapur - Kota Dumai', '-', '2018-10-11 10:14:39', '2018-10-11 10:14:39', 'Indonesia', 'Sopir', '-', 'Islam');
INSERT INTO `suspects` VALUES (74, '-', 'Junaidi Als Jon Baron Bin (Alm) H.Alwi', 'Batam', '1978-08-12', '1', 'Jl.Teduh No.18 RT.011 Kel.Pangkalan Sesai Kec.Dumai Barat - Kota Dumai', '-', '2018-10-11 10:25:59', '2018-10-11 10:25:59', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (75, '-', 'Ronal Hidayat Als Dayat Bin Pikin', 'Koto Baru', '1982-03-02', '1', 'Jl.Syeh Umar Kel.Pangkalan Sesai Kec.Dumai Barat - Kota Dumai', '-', '2018-10-11 10:27:26', '2018-10-11 10:27:26', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (76, '-', 'Hilal Al Hafizh Als Hilal Bin Amril', 'Dumai', '1998-11-26', '1', 'Jl.Simpang Tetap RT.002 Kel.STDI Kec.Dumai Barat - Kota Dumai', '-', '2018-10-11 10:29:15', '2018-10-11 10:29:15', 'Indonesia', 'Mahasiswa', '-', 'Islam');
INSERT INTO `suspects` VALUES (77, '-', 'Harosman Nainggolan Als Horas Bin Lamentar Nainggolan', 'Sei.Juragan - Sumut', '1994-08-20', '1', 'Jl.Batu Bintang RT.005 Kel.Bukit Batrem Kec.Dumai Timur - Kota Dumai', '-', '2018-10-11 10:31:36', '2018-10-11 10:31:36', 'Indonesia', 'Buruh', '-', 'Protestan');
INSERT INTO `suspects` VALUES (78, '-', 'Mahadi Maulana Als Adi Bin Misno.Dkk', 'Bagan Besar', '1979-04-04', '1', 'Jl.Inpres I RT.17 Kel.Bagan Besar Kec.Bukit Kapur Kota Dumai', '-', '2018-10-11 10:35:51', '2018-10-11 10:35:51', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (79, '-', 'Susanto Als Santo Bin Parto Akad.Dkk', 'Nganjuk', '1975-05-21', '1', 'Jl.Siderejo Gg.Ikhlas Kosan No.11 Dumai / Desa Purba Bangun Labuhan Batu Bara Utara Rantau Prapat', '-', '2018-10-11 10:37:37', '2018-10-11 10:37:37', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (80, '-', 'Syamsul Kumar Als Isul Bin Saharuddin', 'Dumai', '1981-10-18', '1', 'Jl.Setia Bakti No.29 RT.005 Kel.Tanjung Palas Kec.Dumai Timur - Kota Dumai', '-', '2018-10-11 10:38:56', '2018-10-11 10:38:56', 'Indonesia', 'Karyawan Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (81, '-', 'Fahmi Efendi Bin Kusno', '-', '0001-01-01', '1', 'Jl.Tendri Sangka RT.06 Kel.Bangsal Aceh Kec.Sungai Sembilan KOta Dumai', '-', '2018-10-11 10:46:02', '2018-10-11 10:46:02', 'Indonesia', 'Pelajar', '-', 'Islam');
INSERT INTO `suspects` VALUES (82, '-', 'Sopan Sofiyan Bin Tukirin', 'Rantau Prapat', '1995-09-06', '1', 'Jl.Pinang Merah RT.014 Kel.Bukit Kapur Kec. Bukit Kapur - Kota Dumai', '-', '2018-10-11 10:47:42', '2018-10-11 10:47:42', 'Indonesia', 'Karyawan Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (83, '-', 'Muhammad Rafi als Rafi Bin Syahrial Hasibuan', 'Dumai', '2018-09-23', '1', 'Jl.Soekarno hatta Bagan Besar Kel.Bagan Besar Dumai', '-', '2018-10-11 13:22:23', '2018-10-11 13:22:23', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (84, '-', 'Reza Paisal als Ntoy Bin Rahmad,dkk', 'Sei Pakning', '1992-11-15', '1', 'Jl.Sei Siak No.17 Kel.Buluh Kasap Kota Dumai', '-', '2018-10-11 13:28:18', '2018-10-11 13:28:18', 'Indonesia', 'wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (85, '-', 'Ramlan Als Epet Bin Nasrun', 'Pelintung', '1986-07-09', '1', 'Jl.Arifin Ahmad Kel,Pelintung Dumai', NULL, '2018-10-11 13:32:20', '2018-10-11 13:32:20', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (86, '-', 'Yudi Rangga', 'Dumai', '0001-01-01', '1', 'Jl.Sultan Syarif Kasim Kel.Buluh Kasap Dumai', NULL, '2018-10-11 13:34:31', '2018-10-11 13:34:31', 'Indonesia', 'wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (87, '-', 'Lamsehat Silaban Bin Albinus Silaban, dkk', 'Dumai', '1995-12-26', '1', 'Jl.Danau Bukit Batrem II Kel.Bukit Batrem Dumai', NULL, '2018-10-11 13:40:31', '2018-10-11 13:40:31', 'Indonesia', NULL, '-', 'Protestan');
INSERT INTO `suspects` VALUES (88, '-', 'Helen Nora,dkk', '-', '0001-01-01', '2', 'Jl.Tunas Muda Gg.Tiara Kec.Dumai Timur Dumai', '-', '2018-10-11 13:42:23', '2018-10-11 13:42:23', 'Indonesia', '-', '-', 'Islam');
INSERT INTO `suspects` VALUES (90, '-', 'Muhamad Yoggi als Yogi Bin Dasril', 'DUMAI', '1993-09-30', '1', 'Jl. Sudirman Gg. Datuk Tabano Kel. Teluk Binjai Kec. Dumai Timur Kota Dumai.', '-', '2018-10-12 07:38:18', '2018-10-12 07:38:18', 'indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (91, '-', 'Daniel Hotman Simanungkalit Bin M. Simanungkalit', 'DUMAI', '1995-05-10', '1', 'Gg. Manggis RT.011 Kel. Bintan Kec. Dumai Kota Dumai', '-', '2018-10-12 07:44:56', '2018-10-12 07:44:56', 'indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (92, '-', 'Ferdinan Rycboy Sitompul Als Boy Bin (alm) Jokeri', 'DUMAI', '1977-10-06', '1', 'Jl. Merdeka Baru No. 10 RT. 027 Kel. teluk Binjai Kec. Dumai Timur Kota Dumai.', '-', '2018-10-12 07:50:05', '2018-10-12 07:50:05', 'indonesia', 'Tidak bekerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (93, '-', 'RIkson Fernando Siregar Als Ando Bin Rudy Hartono Siregar', 'DUMAI', '1988-10-27', '1', 'Jl. Pengeran Diponegoro RT. 006 Kel. Dumai Kota Kec. Kota Dumai - Kota Dumai.', '-', '2018-10-12 07:59:44', '2018-10-12 07:59:44', 'indonesia', 'Tidak bekerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (94, '-', 'Aldi Rifaldi Als Aldi Bin Sofyan', 'DUMAI', '2000-09-21', '1', 'Jl. Nelayan Laut Kel. Pangkalan Sesi Kec. Dumai Barat Kota Dumai.', '-', '2018-10-12 09:00:11', '2018-10-12 09:00:11', 'Indonesia', 'Tidak Ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (95, '-', 'Muliadi Als Ateng Als Adi Bin (alm) Muhammad', 'Mangkai Baru (sumut)', '1977-03-29', '1', 'Dusun V Desa Mangkai Baru Kec. Lima Puluh Kab. Batu Bara Provinsi Sumatera Utara', '-', '2018-10-12 09:03:33', '2018-10-12 09:03:33', 'Indonesia', 'Sopir', '-', 'Islam');
INSERT INTO `suspects` VALUES (96, '-', 'Asnur Memory als Mory Bin (alm) M. Nur', 'DUMAI', '1982-03-17', '2', 'Jl. Hayam Wuruk No. 34 Kel. Buluh Kasap Kec. Dumai Timur Kota Dumai', '-', '2018-10-12 09:09:44', '2018-10-12 09:09:44', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (97, '-', 'Agus Rizal als Ijal Bin Syahril', 'Dumai', '1987-08-17', '1', 'Jl.Soekarno Hatta RT.010 Kel.Bukit Nenas Dumai', '-', '2018-10-12 09:55:09', '2018-10-12 09:55:09', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (98, '-', 'Mishar Okafali als Mishar Bin Ali Muddin', 'Tanjung Balai Karimun', '2018-10-27', '1', 'Jl.Dermaga Gg.Baruna I No.03 RT.09 Kel.PURNAMA Kec.Dumai Barat Kota Dumai', NULL, '2018-10-16 08:22:54', '2018-10-16 08:22:54', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (99, '-', 'Seger Bin Sukardi', 'Aek Nabara', '2018-12-23', '1', 'Kampung Bagan Baru Desa Sei meranti Kecamatan Torgambah Kab.Labuhan Batu Selatan Sumut', NULL, '2018-10-18 06:36:49', '2018-10-18 06:36:49', 'Indonesia', 'Supir', '-', 'Katolik');
INSERT INTO `suspects` VALUES (100, '-', 'Handri als Awi Bin (Alm) Effendi', 'Dumai', '2018-02-04', '1', 'Jl.Gajah Mada N0.25 RT.13 kEL.Buluh Kasap Kec.Dumai Timur Kota Dumai', NULL, '2018-10-18 06:53:39', '2018-10-18 06:53:39', 'Indonesia', 'Wiraswasta', '-', 'Budha');
INSERT INTO `suspects` VALUES (101, '-', 'Swanto Ricardo Ompusunggu als Ricardo Bin Marulak Ompusunggu', 'Dumai', '1991-06-22', '1', 'Jl.Siliwangi RT.008 Kel.Tanjung Palas Kec.Dumai Timur Kota Dumai', '-', '2018-10-18 07:05:57', '2018-10-18 07:05:57', 'Indonesia', 'Karyawan Swasta', '-', 'Protestan');
INSERT INTO `suspects` VALUES (102, '-', 'Haryono Saputra alias KambaBin Agusman', 'Dumai', '2018-09-19', '1', 'Jl.Sutomo Gg.Panam Jaya RT.009 Kel.Teluk Binjai Kec.Dumai Timur Dumai', '-', '2018-10-18 07:20:30', '2018-10-18 07:20:30', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (103, '-', 'Gusti  Indra als Ambun Bin Mansyurdin,cs', 'Dumai', '1989-08-13', '1', 'Jl.Tanjung Jaya RT.005 Kel.Tanjung Palas Kec.Dumai Barat  Dumai', '-', '2018-10-18 07:32:04', '2018-10-18 07:32:04', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (104, '-', 'Sonydra als Indra', '-', '0001-01-01', '1', 'Jl.Puskesmas RT.002 Kel.Bumi Ayu Kec.Dumai Selatan Kota Dumai', '-', '2018-10-18 07:45:44', '2018-10-18 07:45:44', 'Indonesia', 'Karyawan Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (105, '-', 'Misiriayanto als Misran Bin H.Tamsis', 'Kebumen', '1966-08-08', '1', 'Jl.Pelajar RT.06 Kel.Purnama Dumai', '-', '2018-10-18 07:55:30', '2018-10-18 07:55:30', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (106, '-', 'Revo Nusantara Ilham alias Tobing Bin Hanafi', 'Dumai', '2018-05-20', '1', 'Jl.Merdeka Baru RT.20 Kel.Teluk Binjai Kec.Dumai Timur Dumai', '-', '2018-10-18 08:15:42', '2018-10-18 08:15:42', 'Indonesia', 'Tidak ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (107, '-', 'Muhammad Faisal als Apit', 'Dumai', '1995-06-16', '1', 'Jl.Semangka No.05 Kel.Rimba Sekampung Dumai', '-', '2018-10-21 09:30:34', '2018-10-21 09:30:34', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (108, '-', 'Andri Sinaga als Andri Bin Alon Ace Sinaga,dkk', 'Dumai', '1986-12-07', '1', 'Jl.Jend. Sudirman Gg,Sri Langgam Kel.Teluk Binjai Dumai', NULL, '2018-10-21 09:33:08', '2018-10-21 09:33:08', 'Indonesia', 'Buruh', NULL, 'Protestan');
INSERT INTO `suspects` VALUES (109, '-', 'Kasriman als Man Bin Amiruddin', 'Dumai', '1989-09-21', '1', 'Jl.Meranti darat Gg.Kasturi Kec.Dumai Selatan Dumai', NULL, '2018-10-21 09:35:06', '2018-10-21 09:35:06', 'Indonesia', 'Wiraswasta', '-', 'Protestan');
INSERT INTO `suspects` VALUES (110, '-', 'Rozi als Oji Bin Alan Metalina,dkk', 'Lubuk Gaung', '1997-10-07', '1', 'Jl,Pribumi RT.02 Kel.Bnagsal Aceh Kec.Sungai Sembilan Dumai', NULL, '2018-10-21 09:37:27', '2018-10-21 09:37:27', 'Indonesia', 'Tidak ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (111, '-', 'Irawan als Iwan Bin Rian', 'Dumai', '1988-03-03', '1', 'Jl.Rajawali RT.03 Kel.Laksamana Kec.Dumai Kota Dumai', NULL, '2018-10-21 09:41:13', '2018-10-21 09:41:13', 'Indonesia', 'Tidak ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (112, '-', 'Muhammad Asri als Amat Bin Bambang Sugiarto', 'Dumai', '1995-07-17', '1', 'Kamar Kost No.B3 Jl.Nenas Kel,Pangkalan Sesai Dumai', '-', '2018-10-21 10:01:58', '2018-10-21 10:01:58', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (113, '-', 'Muhammad Haieqal als Haikal Bin Marwan, dkk', 'Dumai', '2002-09-16', '1', 'Jl.Rambutan Gg.Gandum Kel.Rimba Sekampung Kec.Dumai Kota Dumai', NULL, '2018-10-21 10:04:01', '2018-10-21 10:04:01', 'Indonesia', NULL, '-', 'Islam');
INSERT INTO `suspects` VALUES (114, '-', 'Muhammad Wahyu Lubis als Yu Bin Kristiadi', 'Medan', '2001-06-21', '1', 'Jl.Ratu Sima Kel.Simpang Tetap Darul Ikhsan Dumai', NULL, '2018-10-21 10:05:46', '2018-10-21 10:05:46', 'Indonesia', 'Tidak ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (115, '-', 'Ismaya als Maya Binti  (alm) Abdul Muis', 'Tanjung Balai', '1975-03-12', '2', 'Jl.Said Umar Gg.Mina Sari Kel.Simpang Tetap Darul Ikhsan Dumai', '-', '2018-10-21 10:07:48', '2018-10-21 10:07:48', 'Indonesia', 'Ibu Rumah Tangga', '-', 'Islam');
INSERT INTO `suspects` VALUES (116, '-', 'Surya  Fajri als Surya Bin Ahmad Ansori', 'Pekanbaru', '1997-11-10', '1', 'Jl.Bintan GG.Satria Kel.Sukajadi Kec.Dumai Kota Dumai', '-', '2018-10-21 11:42:53', '2018-10-21 11:42:53', 'Indonesia', 'Tidak ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (117, '-', 'Asnan als Cek Su Bin Umar (Alm)', 'Siajam', '2018-04-28', '1', 'Jl.Simpang Panti Gg.Sidodadi Kel.Bagan KeladiDumai', '-', '2018-10-21 11:59:46', '2018-10-21 11:59:46', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (118, '-', 'Zulfahyani Bin Zaini', 'Tembilah', '2018-08-17', '1', 'Jl.Grelia Lorong Cendana RT.03 /RW.016 Kel.Desa Tembilahan Hulu Kec.Tembilahan Hulu', '-', '2018-10-21 12:06:07', '2018-10-21 12:06:07', 'Indonesia', 'Nakhoda KM Rena GT.25', '-', 'Islam');
INSERT INTO `suspects` VALUES (119, '-', 'Jonny als Jon Bin (Alm) Ajismar', 'Dumai', '1975-04-19', '1', 'Jl.Bintan Kel.Sukajadi Kec.Dumai Kota Dumai', NULL, '2018-10-21 12:11:46', '2018-10-21 12:11:46', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (120, '-', 'Wan Muhammad Hafiz Nawawi als Hafiz Bin (Alm) Wan Edi', 'Dumai', '2002-04-30', '1', 'Jl.Rajawali Ujung RT.003 Kel.Laksamana Kec.Dumai Kota Dumai', '-', '2018-10-22 01:42:11', '2018-10-22 01:42:11', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (121, '-', 'Joel Mamito Simamora als Joel Bin Bustamin', 'Dumai', '2001-11-20', '1', 'jl.M.Husni Thamrin gg.GerejaKel.Bukit Datuk Dumai', '-', '2018-10-25 11:39:44', '2018-10-25 11:39:44', 'Indonesia', 'Tidak bekerja', '=', 'Protestan');
INSERT INTO `suspects` VALUES (122, '-', 'Asdi als Lelek als Edi, dkk', 'Titi Akar', '1984-07-17', '1', 'Jl.Nerbit Besar RT.016 kEL.Lubuk Gaung Dumai', NULL, '2018-10-25 11:43:20', '2018-10-25 11:43:20', 'Indonesia', 'Petani', '-', 'Budha');
INSERT INTO `suspects` VALUES (123, '-', 'Winarto als Awi Bin Kong Tong', 'Bengkalis', '1994-12-02', '1', 'Jl.Jenderal Sudirman No.276 RT.001 Kel.Teluk Binjai Dumai', '-', '2018-10-25 11:45:15', '2018-10-25 11:45:15', 'Indonesia', 'Tidak bekerja', '-', 'Budha');
INSERT INTO `suspects` VALUES (124, '-', 'Nurul Huda Siagian alias Irul Bin Marihot Siagian', 'Islam', '1984-01-12', '1', 'Jl.KUD RT.003 Kelurahan Bagan Besar Kec.Bukit Kapur Dumai', NULL, '2018-10-25 11:52:40', '2018-10-25 11:52:40', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (125, '-', 'Nurul Efendi Als Kutat Bin Iran (Alm)', 'Lubuk Gaung', '1998-11-05', '1', 'Jl. Rimbun Jaya Rt. 004 Kel Lubuk Gaung Kec. Sei Sembilan Kota Dumai', '-', '2018-10-30 04:57:07', '2018-10-30 04:57:07', 'Indonesia', 'Tidak Ada', '-', 'Islam');
INSERT INTO `suspects` VALUES (126, '-', 'Irfan Efendi Harahap Als Kadek Bin Sangkot Harahap', 'Medan', '1993-07-11', '1', 'Jl. Rambutan Gg. Tobat Kel. Purnama Kec. Dumai Barat - Kota Dumai', '-', '2018-10-30 05:06:31', '2018-10-30 05:06:31', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (127, '-', 'Ikbal Bin Erwin Efendi', 'DUMAI', '1998-12-03', '1', 'Jl. Bintan Gg. Mata Air RT.015 Kel. Bintan Kec. Dumai - Kota Dumai', '-', '2018-10-30 05:15:05', '2018-10-30 05:15:05', 'Indonesia', 'Supir / bengkel', '-', 'Islam');
INSERT INTO `suspects` VALUES (128, '-', 'Moh. Sukano Als Karno Als Wak Gaul Bin (Alm) Mas\'un Sonjaya', 'Cirebon (Jawa Barat)', '1973-05-30', '1', 'BTN Bukti Nenas Permai Jl. Cemara Blok A2 No. 18 Kel. Bukit Nenas Kec. Bukti Kapur - Kota Dumai.', '-', '2018-10-30 05:19:14', '2018-10-30 05:19:14', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (129, '-', 'Rhaynal Ikhwan als Romi Bin (Als) Anwar', 'Pekanbaru', '1983-11-12', '1', 'Jl.Gunung Merbabu RT.001 Kel.Bumi Ayu Dumai', '-', '2018-10-30 09:52:35', '2018-10-30 09:52:35', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (130, '-', 'Romandhani Efendi als Madan Bn (alm) Gemiel', 'Dumai', '1997-12-30', '1', 'Jl.Jend.Sudirman Gg.Becek Kel.Bintan  Kec.Dumai Kota  Kota Dumai', '-', '2018-11-01 07:22:03', '2018-11-01 07:22:03', 'Indonesia', NULL, NULL, 'Islam');
INSERT INTO `suspects` VALUES (131, '-', 'Ikbal Rosidi als Ikbal Bin Khairul,dkk', 'Dumai', '1985-05-11', '1', 'Jl.Mangga RT.10 Kel.Rimba Sekampung Kec.Dumai Kota Dumai', '-', '2018-11-01 07:28:48', '2018-11-01 07:28:48', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (132, '-', 'Moh.Sukarno als Karno als Wak Galau Bin (Alm) Mas’un Sonjaya', 'Cirebon', '1973-05-30', '1', 'BTN Bukit Nenas Permai Jl.Cemara Blok A2 No.18 Kel.Bukit Nenas Kec.Bukit Kapur Dumai', '-', '2018-11-01 07:36:17', '2018-11-01 07:36:17', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (133, '-', 'Dedi Robi alias Robi Bin (Alm) Darunsyah, dkk', 'Dumai', '1992-11-06', '1', 'Jl.Sultan Syarif Kasim Gg.Duku Kel.Rimba Sekampung Kec.Dumai Kota Kota Dumai', '-', '2018-11-01 08:20:36', '2018-11-01 08:20:36', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (134, '-', 'Romi Ardian als Romi Bin Herman', 'Dumai', '1979-08-07', '1', 'Jl.Garuda Kel.Kampung Baru Kec.Bukit Kapur Kota Dumai', '-', '2018-11-01 08:25:58', '2018-11-01 08:25:58', 'Indonesia', 'Juru Parkir', '-', 'Islam');
INSERT INTO `suspects` VALUES (135, '-', 'Gustiawan Tarigan als Iwan Bin Sadakata Tarigan', 'Buluh Duri', '2018-08-14', '1', 'Jl.KUD RT.021 Kel.Lubuk Gaung Kec.Sungai Sembilan    Kota Dumai', '-', '2018-11-01 08:33:13', '2018-11-01 08:33:13', 'Indonesia', 'Swasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (136, '-', 'Jhonson Barimbing alias Jon Bin (Alm) M.Barimbing', 'Tanjung Balai', '2018-05-15', '1', 'Jl.Kesuma Gg.Nauli RT.017 Kel.jaya Mukti Kec.Dumai Timur    Kota Dumai', '-', '2018-11-01 08:40:23', '2018-11-01 08:40:23', 'Indonesia', 'Wiraswasta', '-', 'Protestan');
INSERT INTO `suspects` VALUES (137, '-', 'Asdi als Lelek  als Edi,dkk', 'Titi Akar', '1984-07-17', '1', 'Jl.Nerbit Besar RT.016 Kel.Lubuk Gaung Kec.Sungai Sembilan Kota Dumai', NULL, '2018-11-01 08:50:57', '2018-11-01 08:50:57', 'Indonesia', 'Petani', '-', 'Budha');
INSERT INTO `suspects` VALUES (138, '-', 'Rinaldi als Pak Ben Bin (Alm) Acang Sasmita', 'Pekanbaru', '1968-10-06', '1', 'Jl.Bintan Gg.Takdir RT.005 Kel.Bintan Kec.Dumai Kota    Kota Dumai', '-', '2018-11-01 08:56:11', '2018-11-01 08:56:11', 'Indonesia', NULL, '-', 'Islam');
INSERT INTO `suspects` VALUES (139, '-', 'Syahputra als Putra Bin Irsan Hasanudin', 'Bangun Sari', '1992-11-15', '1', 'Jl.Jenderal Sudirman Gg.Sejahtera Dumai/ Perum GBI Cluster Dahlia Blok  G No.21 Batam', '-', '2018-11-01 09:14:30', '2018-11-01 09:14:30', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (140, '-', 'Nafi Derdi als Nafi Bin Amirudin,dkk', 'Dumai', '1994-01-07', '1', 'Jl.Nelayan Laut Gg.Budi Utama RT.006 Kel.Pangkalan Sesai Kec.Dumai Barat Kota Dumai', NULL, '2018-11-01 09:28:24', '2018-11-01 09:28:24', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (141, '-', 'Herlian Dalimunte  Als Lian Bin Agus Salim Dalimunte,Cs', 'Babussalam', '2018-06-05', '1', 'Dusun Babussalam Kel.Pasir Tuntung Kec.Kota Pinang Prov.Sumut', '-', '2018-11-01 09:43:57', '2018-11-01 09:43:57', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (142, '-', 'Yanto als Lim Acong', '-', '0001-01-01', '1', 'Jl.Diponegoro Kel.Sukajadi Kec.Dumai Kota Kota Dumai', '-', '2018-11-02 05:40:37', '2018-11-02 05:40:37', 'Indonesia', 'Wiraswasta', '-', 'Budha');
INSERT INTO `suspects` VALUES (143, '-', 'Yetty Petro Nella', '-', '0001-01-01', '2', 'Jl.Sudirman Gg.Sejahtera Gg.Galon 2 Kel.Teluk Binjai Dumai', '-', '2018-11-05 08:07:32', '2018-11-05 08:07:32', 'Indonesia', 'Ibu Rumah Tangga', '-', NULL);
INSERT INTO `suspects` VALUES (144, '-', 'Mega Sinaga als Mega Silvi', '-', '0001-01-01', '2', 'Jl.Sudirman Gg.Sejahtera Gg.Galon 2 Kel.Teluk Binjai Kec.Dumai Timur Dumai', '-', '2018-11-05 08:11:01', '2018-11-05 08:11:01', 'Indonesia', 'Ibu Rumah Tangga', '-', NULL);
INSERT INTO `suspects` VALUES (145, '-', 'Jujun Juliandra Bin Maman Rahman (Alm), dkk', 'Dumai', '1972-07-19', '1', 'Jl.Pangkalan Sena Gg.Srikandi  No.4 A RT.003 Kel.Simpang Tetap Darul Ichsan Kec.Dumai Barat Kota Dumai', '-', '2018-11-06 03:14:35', '2018-11-06 03:14:35', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (146, '-', 'Fadhil Eko Syaputra als Ata Bin Edi Saputra', 'Dumai', '1992-04-12', '1', 'Jl,Jenderal Sudirman Gg.Terikat RT.004 Kel.Teluk Binjai Kec.Dumai Timur Dumai', '-', '2018-11-06 03:17:46', '2018-11-06 03:17:46', 'Indonesia', 'Sopir', '-', 'Islam');
INSERT INTO `suspects` VALUES (147, '-', 'Dodoi Harianto als Dodoi Bin Sarial', 'Pekanbaru', '1982-02-02', '1', 'Jl.Tapian Nauli RT.005 Kel.Teluk Binjai Kec.Dumai Timur Dumai', '-', '2018-11-06 03:20:42', '2018-11-06 03:20:42', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (148, '-', 'Romi Syahputra', '-', '0001-01-01', '2', 'Tindak Pidana kecelakaan lalu lintas yang mengakibatkan kerusakan kendaraan', '-', '2018-11-06 03:24:29', '2018-11-06 03:24:29', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (149, '-', 'Arny Renta Uli Napitupulu,cs', '-', '0001-01-01', '2', '-', '-', '2018-11-06 03:27:57', '2018-11-06 03:27:57', 'Indonesia', '-', '-', NULL);
INSERT INTO `suspects` VALUES (150, '-', 'Drs.Frans E Hutabarat,dkk', '-', '0001-01-01', '1', '-', '-', '2018-11-06 03:36:14', '2018-11-06 03:36:14', NULL, '-', '-', NULL);
INSERT INTO `suspects` VALUES (151, '-', 'Musliadi als Doni Bin Ponirin (Alm)', 'Pematang Siantar', '1979-03-20', '1', 'Jl.Saeitindo RT.009 Kel.Basilam Baru Kec.Sungai Sembilan Kota Dumai', '-', '2018-11-06 07:14:04', '2018-11-06 07:14:04', 'Indonesia', 'Petani / Pekebun', '-', 'Islam');
INSERT INTO `suspects` VALUES (152, '-', 'Rima Yunita Marsilia', '-', '0001-01-01', '1', 'Jl.Gunung Merapi RT.004 Kel.Bumi Ayu Kec.Dumai Selatan Dumai', '-', '2018-11-07 08:35:01', '2018-11-07 08:35:01', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (153, '-', 'Nanang als Nang Bin (Alm) Rusli', 'Bagan Siapiapi', '1984-09-16', '1', 'Jl.Cendrawasih RT.003 Kel.Laksamana Kec.Dumai Kota Kota Dumai', '-', '2018-11-14 02:54:50', '2018-11-14 02:54:50', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (154, '-', 'Tommi Hutapea alias Tommi Bin (Alm) Togar Manorus Hutapea', 'Dumai', '1981-10-09', '1', 'Jl.Merdeka Baru No.106 RT.006 Kel.Teluk Binjai Kec.Dumai Timur', NULL, '2019-01-08 05:49:29', '2019-01-08 05:49:29', 'Indonesia', 'Buruh', '-', 'Protestan');
INSERT INTO `suspects` VALUES (155, '-', 'Romi Devianra Bin Sapri', 'Dumai', '1996-12-10', '1', 'Jl.Bintan Gg,Jadid RT.001 Kel.Bintan Dumai', '-', '2019-01-08 05:55:59', '2019-01-08 05:55:59', 'Indonesia', 'Belum bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (156, '-', 'Parlindungan Nainggolan', 'Dumai', '2001-05-11', '1', 'Jl.PU Kel.Basilam Kec.Sungai Sembilan Dumai', '-', '2019-01-08 06:00:11', '2019-01-08 06:00:11', 'Indonesia', NULL, '-', 'Protestan');
INSERT INTO `suspects` VALUES (157, '-', 'Robi Firmanto Siahaan anak dari J Siahaan', 'Duri XIII', '1998-01-24', '1', 'Jl.Baru Duri XIII Desa Kesumbo Ampai Kec.Mandau Kab.Bengkalis', '-', '2019-01-08 06:06:42', '2019-01-08 06:06:42', 'Indonesia', 'Belum bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (158, '-', 'Rival Juwir Ananda als Nanda Bin Nasrul', 'Padang', '2001-07-27', '1', 'Jl.Soekarno Hatta Kel.Bagan Besar Kec.Bukit Kapur Kota Dumai', '-', '2019-01-08 06:11:40', '2019-01-08 06:11:40', 'Indonesia', 'Pelajar', '-', 'Islam');
INSERT INTO `suspects` VALUES (159, '-', 'Rusli Siregar als Regar Bin Imam Mahdi Siregar', 'Batang Julu', '1968-05-25', '1', 'Jl.Makmur Gg.Mawar RT.13 Kel.Tanjung Palas Kec.Dumai Timur Dumai', '-', '2019-01-08 06:20:28', '2019-01-08 06:20:28', 'Indonesia', 'Buruh', '-', 'Islam');
INSERT INTO `suspects` VALUES (160, '-', 'Sukarno Muhammad Nur als Karno,dkk', 'Dumai', '1997-08-04', '1', 'Tindak Pidana Narkotika', '-', '2019-01-10 07:29:46', '2019-01-10 07:29:46', 'Indonesia', 'Tidak bekerja', '-', 'Protestan');
INSERT INTO `suspects` VALUES (161, '-', 'Naruliyan als Uli Bin AGUS sALIM (aLM)', 'Bengkalis', '1981-08-04', '1', 'Jl.Mastari Desa Sukarejo Mesin Rupat', '-', '2019-01-10 07:34:52', '2019-01-10 07:34:52', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (162, '-', 'Eduar Panggabean als Gabe, dkk', 'Rantau Prapat', '2019-01-03', '1', 'Jl.Utama Karya RT.014 Kel.Bukit Batrem Kec.Dumai Timur', '-', '2019-01-10 07:44:36', '2019-01-10 07:44:36', 'Indonesia', 'Swasta', '-', 'Protestan');
INSERT INTO `suspects` VALUES (163, '-', 'Muhammad sabri als sabri Bin Amir Hamzah', 'Dumai', '1969-10-12', '1', 'Dusun Baru Selatan RT.016 RW.007 Kel.Baru Kec.Manggar Kab.Belitung Timur', '-', '2019-01-16 02:45:21', '2019-01-16 02:45:21', 'Indonesia', 'Wiraswasta', '-', 'Islam');
INSERT INTO `suspects` VALUES (164, '-', 'Abdul Rauf als Acok Bin Abbas M', 'Bukit Timah', '1985-03-23', '1', 'Jl.Tuanku Tambusai Gg.Putra 07 Kel.Bukit Timah Dumai', '-', '2019-01-16 02:52:18', '2019-01-16 02:52:18', 'Indonesia', 'Tidak bekerja', '-', 'Islam');
INSERT INTO `suspects` VALUES (165, '-', 'Daniel Krissanto als Ijon Bin Monang Pasaribu, dkk', 'Dumai', '1992-11-16', '1', 'Jl.Garuda Simpang Murini RT.11 Kel.Bukit Nenas Dumai', '-', '2019-01-16 02:56:10', '2019-01-16 02:56:10', 'Indonesia', 'Buruh', '-', 'Katolik');

-- ----------------------------
-- Table structure for twenties
-- ----------------------------
DROP TABLE IF EXISTS `twenties`;
CREATE TABLE `twenties`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of twenties
-- ----------------------------
INSERT INTO `twenties` VALUES (2, 110, '173883', '2018-10-15', '-', '2018-10-14 17:32:08', '2018-10-14 17:32:08');

-- ----------------------------
-- Table structure for twentyone_as
-- ----------------------------
DROP TABLE IF EXISTS `twentyone_as`;
CREATE TABLE `twentyone_as`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of twentyone_as
-- ----------------------------
INSERT INTO `twentyone_as` VALUES (3, 145, '001/zz/2018', '2018-10-24', '-', '2018-10-21 20:32:38', '2018-10-21 20:32:38');
INSERT INTO `twentyone_as` VALUES (4, 146, '001/p21a/10/2018', '2018-10-23', '-', '2018-10-21 21:18:24', '2018-10-21 21:18:24');

-- ----------------------------
-- Table structure for twentyones
-- ----------------------------
DROP TABLE IF EXISTS `twentyones`;
CREATE TABLE `twentyones`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caase_id` int(11) NOT NULL,
  `nomor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of twentyones
-- ----------------------------
INSERT INTO `twentyones` VALUES (2, 17, '4556', '2018-09-25', NULL, '2018-09-25 11:38:17', '2018-09-25 11:38:17');
INSERT INTO `twentyones` VALUES (5, 54, 'b-1263/N.4.13/Epp.1/09/2018', '2018-09-14', NULL, '2018-10-10 08:50:14', '2018-10-10 08:50:14');
INSERT INTO `twentyones` VALUES (6, 57, 'B-1426/n.4.13/epp.1/08/2018', '2018-08-30', NULL, '2018-10-10 09:44:41', '2018-10-10 09:44:41');
INSERT INTO `twentyones` VALUES (7, 100, 'B-1706/N.4.13/EPP.1/09/2018', '2018-09-14', '-', '2018-10-12 08:14:51', '2018-10-12 08:14:51');
INSERT INTO `twentyones` VALUES (8, 101, 'B-1591/N.4.13/EUH.1/09/2018', '2018-09-11', '-', '2018-10-12 08:23:51', '2018-10-12 08:23:51');
INSERT INTO `twentyones` VALUES (9, 80, 'B-15723/N.4.13/EPP.1/09/2018', '2018-09-11', '-', '2018-10-12 08:47:06', '2018-10-12 08:47:06');
INSERT INTO `twentyones` VALUES (10, 22, 'B-1610/N.4.13/Euh.1/09/2018', '2016-09-26', '-', '2018-10-12 08:51:12', '2018-10-12 08:51:12');
INSERT INTO `twentyones` VALUES (11, 23, 'B-1531/N.4.13/Euh.1/09/2018', '2018-09-18', '-', '2018-10-12 08:56:15', '2018-10-12 08:56:15');
INSERT INTO `twentyones` VALUES (12, 102, 'B/1610/N.4.13/Euh.1/09/2018', '2018-09-26', '-', '2018-10-12 09:13:08', '2018-10-12 09:13:08');
INSERT INTO `twentyones` VALUES (13, 46, 'B/1711/X/2018/Reskrim', '2018-09-10', '-', '2018-10-12 09:24:41', '2018-10-12 09:24:41');
INSERT INTO `twentyones` VALUES (14, 103, 'B-705/N.4.13/Euh.1/05/2018', '2018-05-07', '-', '2018-10-12 09:27:55', '2018-10-12 09:27:55');
INSERT INTO `twentyones` VALUES (15, 78, 'B-1666/N.4.13/Epp.1/09/2018', '2018-09-01', '-', '2018-10-12 09:32:59', '2018-10-12 09:32:59');
INSERT INTO `twentyones` VALUES (16, 77, 'B-1665/n.4.13/Epp.1/09/2018', '2017-09-12', '-', '2018-10-12 09:43:46', '2018-10-12 09:43:46');
INSERT INTO `twentyones` VALUES (17, 72, 'B-1667/N.4.13/Epp.1/10/2018', '2018-10-03', '-', '2018-10-12 09:52:25', '2018-10-12 09:52:25');
INSERT INTO `twentyones` VALUES (18, 106, 'B-991/N.4.13/EPP.1/06/2018', '2018-06-21', '-', '2018-10-12 09:52:32', '2018-10-12 09:52:32');
INSERT INTO `twentyones` VALUES (19, 108, 'B/1549/N.4.13/Epp.1/09/2018', '2018-09-19', 'Tindak Pidana Pencurian', '2018-10-12 10:04:02', '2018-10-12 10:04:02');
INSERT INTO `twentyones` VALUES (20, 109, 'B-1341/N.4.13/EPP.1/08/2018', '2018-08-20', '-', '2018-10-12 10:17:33', '2018-10-12 10:17:33');
INSERT INTO `twentyones` VALUES (21, 110, '173883', '2018-10-16', '-', '2018-10-14 17:32:24', '2018-10-14 17:32:24');
INSERT INTO `twentyones` VALUES (22, 58, '090820911', '2018-10-22', '-', '2018-10-21 20:16:17', '2018-10-21 20:16:17');
INSERT INTO `twentyones` VALUES (23, 145, '001/zz/2018', '2018-10-22', '-', '2018-10-21 20:31:43', '2018-10-21 20:31:43');
INSERT INTO `twentyones` VALUES (24, 146, '001/p21/10/2018', '2018-10-22', '-', '2018-10-21 21:16:59', '2018-10-21 21:16:59');
INSERT INTO `twentyones` VALUES (25, 148, 'B-1609/N.4.13/Euh.1/09/2018', '2018-09-24', 'Tindak Pidana Narkotika', '2018-10-22 01:48:47', '2018-10-22 01:48:47');
INSERT INTO `twentyones` VALUES (26, 66, 'B-1611/N.4.13/Epp.1/09/2018', '2018-09-25', 'Tindak Pidana Pencurian', '2018-10-22 02:08:00', '2018-10-22 02:08:00');
INSERT INTO `twentyones` VALUES (27, 68, 'B-1611/N.4.13/Epp.1/09/2018', '2018-09-25', 'Tindak Pidana Pencurian', '2018-10-22 02:11:14', '2018-10-22 02:11:14');
INSERT INTO `twentyones` VALUES (28, 62, 'B-1893/N.4.13/Epp.1/10/2018', '2018-10-30', 'Tindak Pidana Pencurian', '2018-11-02 07:35:36', '2018-11-02 07:35:36');
INSERT INTO `twentyones` VALUES (29, 45, 'B-1860/N.4.13/Euh.1/10/2018', '2018-10-29', 'Tindak Pidana Narkotika', '2018-11-02 07:38:11', '2018-11-02 07:38:11');
INSERT INTO `twentyones` VALUES (30, 59, 'B-N.4.13/Epp.1/10/2018', '2018-10-30', 'Tindak Pidana Pencurian', '2018-11-02 07:39:20', '2018-11-02 07:39:20');
INSERT INTO `twentyones` VALUES (31, 29, 'B-1889/N.4.13/Euh.1/10/2018', '2018-10-30', 'Tindak Pidana Narkotika', '2018-11-02 07:40:27', '2018-11-02 07:40:27');
INSERT INTO `twentyones` VALUES (32, 138, 'B-1855/N.4.13/Euh.1/10/2018', '2018-10-29', 'Tindak Pidana Narkotika', '2018-11-02 07:43:27', '2018-11-02 07:43:27');

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `perkara` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, 'Anak', 'Perkara Anak', '2018-09-03 02:21:22', '2018-09-03 02:21:22');
INSERT INTO `types` VALUES (2, 'Dewasa', 'Perkara Dewasa', '2018-09-03 02:21:34', '2018-09-03 02:21:34');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (7, 'DEWI IRIANA', 'dewi.iriana@gmail.com', '$2y$10$MODcP7uWVUUqXF4mV0CFgusR7NH8zeDC8hRjhFNAFO7uMy/bGLevK', 'Icq2PyH0z9TeeSQmgxa8oQ0bZPEsxvgdP6wB80ge4VnAq8qhklWCD3z11Vcc', '2018-09-21 04:52:54', '2018-09-21 04:52:54');
INSERT INTO `users` VALUES (16, 'YUNIUS ZEGA, SH., MH', 'yuniuszega14@gmail.com', '$2y$10$npjUS3TvexDzDBIiHxtRdeoohkYVXdxKgR/zeQfuPe3OsYYCP.LVu', 'RU62mPhfqDNAEmF5CtKiCurV6Cx5tblvO7YE0AFSgvtv3SRkDtzUNOoLQQji', '2018-10-10 07:58:01', '2018-10-16 06:53:48');
INSERT INTO `users` VALUES (17, 'JONITRIANTO ANDRA, S.H., M.H.', 'kasidatundumai@gmail.com', '$2y$10$xrmhVbTab38Ok4GRwYY77eUbUqhOw6cJw.gpBwo.pInEW32EMmb4C', NULL, '2018-10-10 09:05:46', '2018-10-10 09:05:46');
INSERT INTO `users` VALUES (18, 'HERY SUSANTO,SH', 'herysusantodisini@gmail.com', '$2y$10$3Ch6OnS1cnKkACQEMlox6OfoIOHysLmMK5mCCaQEk0Drum4v0iRqe', NULL, '2018-10-10 09:21:14', '2018-10-10 09:21:14');
INSERT INTO `users` VALUES (19, 'AGUNG NUGROHO,SH', 'ook.nugroho24@gmail.com', '$2y$10$kBUfd1x0t9jgKZ6bMdRy.eBJMhKxh2tmwH9iCfT6lvnpnx7jOgLnG', NULL, '2018-10-10 10:06:09', '2018-10-10 10:06:09');
INSERT INTO `users` VALUES (24, 'HENGKY FRANSISCUS MUNTE, SH.', 'hf.munthe@gmail.com', '$2y$10$lUtnIjrsCI68ooeWohlXnOEO1dsWvWvc8vV8xuKm0N6NgVHh4Viam', 'mS2Hezfwl8n42seJ92fRqWIYntC59ndZyGSlmtWANgdguT5bxBESfnmUI1Du', '2018-10-12 06:28:02', '2018-10-12 06:28:02');
INSERT INTO `users` VALUES (25, 'MAIMAN LIMBONG,SH', 'limbong.maiman@gmail.com', '$2y$10$xHc.SsKVD2SPP2f8kJV1n.2P3K78moSYsNyeaKWTux0t5Zzhi56SC', NULL, '2018-10-12 07:32:38', '2018-10-12 07:32:38');
INSERT INTO `users` VALUES (26, 'YOPENTINU ADI NUGRAHA,SH', 'yopentinu20@gmail.com', '$2y$10$wv1c50.azDZlgVYSE4IVG.sQKcx7jN/rpr6vpJKYbIZ53jKVjPitG', NULL, '2018-10-14 23:53:24', '2018-10-14 23:53:24');
INSERT INTO `users` VALUES (28, 'MAT PERANG YUSUF, SH., MH.', 'kajaridumai@gmail.com', '$2y$10$sXtfPUFmOtWKuqsGwQvTMeI5M9zQow.D.YG4ddaDkmou473FDes0a', 'fhTQoAos7tf7IBCgnRGnKsl1oYMEL1xCKcJnquFKHhx25LANg4YrMbHj0O9Y', '2018-10-31 11:02:53', '2018-10-31 11:02:53');
INSERT INTO `users` VALUES (29, 'ROSLINA,SH', 'roslina.monica82@gmail.com', '$2y$10$3GhkLtJztJO1DgEtwaMLJO923qBwYZHjbQHoKf7D.xJ1EYJ/M4w.u', NULL, '2018-11-07 01:52:37', '2018-11-07 01:52:37');
INSERT INTO `users` VALUES (30, 'DENY ALVIANTO, S.H., M.Hum.', 'alviantodeny@gmail.com', '$2y$10$l4e3R7xZSa1XJoXEuxUL7eBz5I7hNeRTsSfvqr67/.mHgMXoJFQqC', NULL, '2018-11-07 02:54:04', '2018-11-07 02:54:04');
INSERT INTO `users` VALUES (31, 'Taufik', 'taufik@mail.com', '$2y$10$gQAoMZ6HIG9ipdZcHhxxCukoWUXmYo3v3rmz7eWPE96Tuqzk6hXaW', NULL, '2018-11-27 07:57:47', '2018-11-27 07:57:47');

SET FOREIGN_KEY_CHECKS = 1;
