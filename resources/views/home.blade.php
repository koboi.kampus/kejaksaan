@extends('layouts.master')

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Beranda</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Beranda</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- CONTENT DISINI -->
                {{--<div class="row">
                    <div class="col-lg-12">
                        <div class="card  bg-light no-card-border">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="m-r-10">
                                        <img src="../../assets/images/users/2.jpg" alt="user" width="60" class="rounded-circle" />
                                    </div>
                                    <div>
                                        <h3 class="m-b-0">Selamat Datang!</h3>
                                        <span>Monday, 9 March 2019</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{ $totalSPDP }}</h3>
                                    <h6 class="card-subtitle">Total SPDP</h6>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                            aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{ number_format($pra, 2, '.', ',') }}%</h3>
                                    <h6 class="card-subtitle">Pra Penuntutan</h6>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ $pra }}%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                            aria-valuemax="{{ $pra }}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{ number_format($penuntutan, 2, '.', ',') }}%</h3>
                                    <h6 class="card-subtitle">Penuntutan</h6>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $penuntutan }}%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                            aria-valuemax="{{ $penuntutan }}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h3>{{ number_format($eksekusi, 2, '.', ',') }}%</h3>
                                    <h6 class="card-subtitle">Eksekusi</h6>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-inverse" role="progressbar" style="width: {{ $eksekusi }}%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                            aria-valuemax="{{ $eksekusi }}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- place order / Exchange -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card bg-danger text-white">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white">
                                            <i class="ti-pie-chart"></i>
                                        </h1>
                                    </div>
                                    <div>
                                        <h4 class="card-title">TPUL - EUH</h4>
                                        <h6 class="text-white op-5">2018</h6>
                                    </div>

                                </div>

                                <div class="row m-t-20 align-items-center">
                                    <div class="col-4">
                                        <h3 class="font-light text-white">{{ $totaltpul }}</h3>
                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="bandwidth"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-warning text-white">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white">
                                            <i class="ti-pie-chart"></i>
                                        </h1>
                                    </div>
                                    <div>
                                        <h4 class="card-title">KAMTIBUM - EP</h4>
                                        <h6 class="text-white op-5">2018</h6>
                                    </div>

                                </div>

                                <div class="row m-t-20 align-items-center">
                                    <div class="col-4">
                                        <h3 class="font-light text-white">{{ $totaltibum }}</h3>
                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="spark-count"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-success text-white">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        <h1 class="text-white">
                                            <i class="ti-pie-chart"></i>
                                        </h1>
                                    </div>
                                    <div>
                                        <h4 class="card-title">OHARDA - EPP</h4>
                                        <h6 class="text-white op-5">2018</h6>
                                    </div>

                                </div>

                                <div class="row m-t-20 align-items-center">
                                    <div class="col-4">
                                        <h3 class="font-light text-white">{{ $totaloharda }}</h3>
                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="spark-count"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- project of the month -->
                    <!-- ============================================================== -->
                    <div class="col-lg-9">
                      <iframe src="http://sipp.pn-dumai.go.id/list_perkara/type/Tlkva0tVQ1pNZlQ3dXBXR2xyaDF1d3F4a2pQcjB5ZEVkOEs4bXNqTXd6elFtd3RYTU00akNYS3BTVm5zR0tROC9XcFc5NEhLZ2M2dGhsamtKVlZ0Y2c9PQ==" 
                          scrolling="yes" 
                          width="100%" 
                          height="495px" 
                          frameborder="1">
                      </iframe>
                    </div>
                    
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

