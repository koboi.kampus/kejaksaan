<!DOCTYPE html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SisRoler') }}</title>

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/libs/chartist/dist/chartist.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/extra-libs/c3/c3.min.css') }}">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/libs/morris.js/morris.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('dist/css/style.min.css') }}">

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

    @stack('styles')
</head>
  
  <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

   <!-- #MAIN PANEL -->
    <div id="main-wrapper">
    
    <!-- #HEADER -->
    @include('layouts.header')

    <!-- #SIDE MENU -->
    @include('layouts.left-panel')

    <!-- #MAIN CONTENT -->
    @yield('content')

    <!-- end row -->
    <!-- END #MAIN CONTENT -->

    </div>
    <!-- END #MAIN PANEL -->

    <!-- Footer -->
    
    @include('layouts.footer')

    @include('layouts.scripts') 
    @stack('scripts')
   
  </body>
</html>
