<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="#">SisRoler.Draft</a>  
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/home">Beranda</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('monitor.index') }}">Kontrol Perkara</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('case.index') }}">SPDP</a>
            </li>
            
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                Tahapan 
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Tahap I</a>
                <a class="dropdown-item" href="#">Tahap II</a>
                <a class="dropdown-item" href="#">Persidangan</a>
                <a class="dropdown-item" href="#">Upaya Hukum</a>
                <a class="dropdown-item" href="#">Eksekusi</a>
              </div>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                Referensi
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Jenis Perkara</a>
                <a class="dropdown-item" href="#">Klasifikasi</a>
                <a class="dropdown-item" href="#">Kitab Undang-undang</a>
                <a class="dropdown-item" href="#">Data Tersangka</a>
              </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Data Pegawai</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Konfigurasi</a>
            </li>
        </ul>
    </div>
    
    <div class="mx-auto order-0">
    </div>
    
    <div class="navbar-collapse collapse w-80 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link">
              {{ Auth::user()->name }}
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link">
              |
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
          </li>
        </ul>
    </div>
</nav>
