@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endpush

@section('content')

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <div class="d-flex align-items-center">
                    <h5>Arsip</h5>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#"> Arsip</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"> P-53</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- CONTENT DISINI -->

        <!-- Row -->
        <div class="row">
       
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-warning">
                        <h4 class="m-b-0 text-white">Informasi Perkara</h4>
                    </div>
                    <div class="card-body">

                        <dl>
                            <div class="row">
                                <div class="col-md-12">
                                    <dt>Nomor SPDP : {{ $case->nomor_spdp }} </dt>
                                    <dd>
                                        <i class="mdi mdi-chevron-double-right"></i> Tanggal : {{ $case->tanggal_spdp }}, <font color="red"> Diterima :</font> {{ $case->spdp_diterima }}
                                    </dd>
                                    <hr>
                                    <dt>Jenis / Klasifikasi</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->type->perkara }} / {{ $case->classification->klasifikasi }}</dd>
                                    <dt>Pelanggaran</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->pelanggaran }}</dd>
                                    <dt>Status</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->StatusPerkara }}</dd>
                                    <hr>
                                    <dt>Penyidik</dt>
                                    <dd>
                                        @foreach($case->investigators as $inv)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $inv->nama }} <br>
                                        @endforeach
                                    </dd>
                                    <hr>
                                    <dt>Jaksa</dt>
                                    <dd>
                                        @foreach($case->sixtena as $cs)
                                            @foreach($cs->profiles as $jks)
                                                <i class="mdi mdi-chevron-double-right"></i> {{ $jks->nama }} <br>
                                            @endforeach
                                        @endforeach
                                    </dd>
                                    <hr>
                                    <dt>Terdakwa</dt>
                                    <dd>
                                        @foreach($case->suspects as $sus)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $sus->nama }} <br>
                                        @endforeach
                                    </dd>
                                </div>
                            </div>
                            <hr>
                            <dt>Kasus Posisi</dt>
                            <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->keterangan }}</dd>
                        </dl>

                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">Arsip</h4>
                    </div>
                    <form action="{{ url('/arsippost', $case->id) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="card-body">
                                <div class="row p-t-20">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('dictum') ? 'has-error' : '' }}"> Dictum </label>
                                            <input type="text" class="form-control" name="dictum" placeholder="Nomor Dictum">
                                            <span>{{ $errors->first('dictum') }}</span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('tanggal_dictum') ? 'has-error' : '' }}">Tanggal Dictum</label>
                                            <input type="date" class="form-control" name="tanggal_dictum">
                                            <span>{{ $errors->first('tanggal_dictum') }}</span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('dictum_pidana') ? 'has-error' : '' }}"> Dictum Pidana Penjara</label>
                                            <input type="text" class="form-control" name="dictum_pidana" placeholder="Pidana Pejara">
                                            <span>{{ $errors->first('dictum_pidana') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('dictum_barangbukti') ? 'has-error' : '' }}">Dictum Barang Bukti</label>
                                            <textarea name="dictum_barangbukti" class="form-control" rows="4"></textarea>
                                            <span>{{ $errors->first('dictum_barangbukti') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('nomor_putusan') ? 'has-error' : '' }}"> Nomor Putusan</label>
                                            <input type="text" class="form-control" name="nomor_putusan" placeholder="Nomor Putusan">
                                            <span>{{ $errors->first('nomor_putusan') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('tanggal_putusan') ? 'has-error' : '' }}">Tanggal Putusan</label>
                                            <input type="date" class="form-control" name="tanggal_putusan">
                                            <span>{{ $errors->first('tanggal_putusan') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('amar_pidana') ? 'has-error' : '' }}"> Amar Pidana Penjara</label>
                                            <input type="text" class="form-control" name="amar_pidana" placeholder="Pidana Pejara">
                                            <span>{{ $errors->first('amar_pidana') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('amar_barangbukti') ? 'has-error' : '' }}">Amar Barang Bukti</label>
                                            <textarea name="amar_barangbukti" class="form-control" rows="4"></textarea>
                                            <span>{{ $errors->first('amar_barangbukti') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('sikap_jpu') ? 'has-error' : '' }}"> Sikap JPU</label>
                                            <input type="text" class="form-control" name="sikap_jpu" placeholder="Sikap JPU">
                                            <span>{{ $errors->first('sikap_jpu') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('sikap_terdakwa') ? 'has-error' : '' }}"> Sikap Terdakwa</label>
                                            <input type="text" class="form-control" name="sikap_terdakwa" placeholder="Sikap Terdakwa">
                                            <span>{{ $errors->first('sikap_terdakwa') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('eksekusi_tuntas') ? 'has-error' : '' }}">Eksekusi Tuntas</label>
                                            <input type="date" class="form-control" name="eksekusi_tuntas">
                                            <span>{{ $errors->first('eksekusi_tuntas') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('bp_arsip') ? 'has-error' : '' }}">BP Masuk Arsip</label>
                                            <input type="date" class="form-control" name="bp_arsip">
                                            <span>{{ $errors->first('bp_arsip') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('kasus_posisi') ? 'has-error' : '' }}">Kasus Posisi</label>
                                            <textarea name="kasus_posisi" class="form-control" rows="4">{{ $case->keterangan }}</textarea>
                                            <span>{{ $errors->first('kasus_posisi') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('keterangan') ? 'has-error' : '' }}">Keterangan Tambahan</label>
                                            <textarea name="keterangan" class="form-control" rows="4"></textarea>
                                            <span>{{ $errors->first('keterangan') }}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                    <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->

<!-- Disini Footer -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endpush
