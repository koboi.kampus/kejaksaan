@extends('layouts.master')

@push('styles')

<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">

@endpush

@section('content')

    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Daftar Arsip Perkara</h4>
                    <div class="d-flex align-items-center">

                    </div>
                </div>
                <div class="col-7 align-self-center">
                    <div class="d-flex no-block justify-content-end align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#">Arsip</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">P - 53</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">

            <!-- scroll horizontal & vertical -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="scroll_ver_hor" class="table table-striped table-bordered display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>SPDP - Tanggal</th>
                                            <th>Klasifikasi</th>
                                            <th>Tersangka</th>
                                            <th>Detil</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($case as $m)
                                        <tr>
                                            <td align="center">{{ ++$i }}</td>
                                            <td>
                                                {{ $m->nomor_spdp }} <br>
                                                {{ $m->tanggal_spdp }}, <u><font color="#e60000">Diterima</font></u> : {{ $m->spdp_diterima }}
                                            </td>
                                            {{--<td>
                                            @foreach($m->sixtena as $ms)
                                                @foreach($ms->profiles as $jaksa)
                                                    {{ $jaksa->nama  }} <br>
                                                @endforeach
                                            @endforeach
                                            </td> --}}
                                            <td>
                                                {{ $m->type->perkara }} <br>
                                                {{ $m->classification->klasifikasi }}
                                            </td>
                                            <td>
                                            @foreach($m->suspects as $tersangka)
                                                {{ $tersangka->nama  }} <br>
                                            @endforeach
                                            </td>
                                            <td align="center">
                                                <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg{{ $m->id }}" class="btn btn-primary btn-xs"><i class="fa fa-align-justify"></i> </button>
                                            </td>
                                            {{--<td align="center">
                                                <form action="{{ route('spdp.destroy', $m->id) }}" method="post">
                                                    <a href="{{ route('spdp.edit', $m->id) }}" type="button" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> </a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </button>
                                                </form>
                                            </td> --}}
                                        </tr>

                                        <!-- sample modal content -->
                                        <div class="modal fade bs-example-modal-lg{{ $m->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">

                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Detil Perkara</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="row">

                                                        <div>
                                                            <!-- Nav tabs -->
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#proses" role="tab"><span class="hidden-sm-up"><i class="ti-settings"></i></span> <span class="hidden-xs-down"> Detil Arsip</span></a> </li>
                                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#kembali" role="tab"><span class="hidden-sm-up"><i class="ti-back-right"></i></span> <span class="hidden-xs-down"> Pra Penuntutan</span></a> </li>
                                                            </ul>

                                                            <!-- Tab panes -->
                                                            <div class="tab-content tabcontent-border">
                                                                
                                                                <div class="tab-pane active" id="proses" role="tabpanel">
                                                                    <div class="p-20">
                                                                        .....
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane p-20" id="kembali" role="tabpanel">
                                                                    <div class="p-20">
                                                                        .....
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>

                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tutup</button>
                                                    </div>

                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
   </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->

    <!-- Disini Footer -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
@endpush
