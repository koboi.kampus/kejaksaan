@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><h4>Input Data SPDP</h4></li>
      </ol>
    </nav>

<div class="container">
                
      <form method="post" action="{{ route('case.store') }}" enctype="multipart/form-data">
      @csrf
      
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group col-md-12">
              <label for="Name">Perkara</label>
              <select class="form-control"  name="perkara">
                @foreach($type as $t)
                <option value="{{ $t->id }}">{{ $t->perkara }}</option>
                @endforeach`
              </select>
            </div>
            <div class="form-group col-md-12">
              <label for="Name">Klasifikasi</label>
              <select name="klasifikasi" class="form-control">
                @foreach($klas as $k)
                <option value="{{ $k->id }}">{{ $k->klasifikasi }}</option>
                @endforeach
              </select> 
            </div> 
            <div class="form-group col-md-12">
              <label for="Name">No. SPDP</label>
              <input type="text" class="form-control" name="spdp">
            </div>
            <div class="form-group col-md-12">
              <label for="Name">Tanggal SPDP</label>
              <input type="text" class="form-control" name="tanggal_spdp">
            </div>
            <div class="form-group col-md-12">
              <label for="Name">Tgl Masuk SPDP</label>
              <input type="text" class="form-control" name="masuk_spdp">
            </div>
            <div class="form-group col-md-12">  
              <label for="Name">Nama Tersangka</label>
              <select name="tersangka" class="form-control">
                @foreach($suspect as $s)
                <option value="{{ $s->id }}">{{ $s->nama }}</option>  
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="col-sm-6">
            <div class="form-group col-md-12">
              <label for="Name">Undang-Undang</label>
              <select name="undang" class="form-control">
                @foreach($law as $w)
                  <option value="{{ $w->id }}">{{ $w->kitab }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-12">
              <label for="Name">Pasal</label>
              <input type="text" class="form-control" name="pasal">
            </div>
            <div class="form-group col-md-12">
              <label for="Name">Penyidik</label>
              <select name="penyidik" class="form-control">
                @foreach($inv as $i)
                <option value="{{ $i->id }}">{{ $i->nama }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-12">  
              <label for="Name">Jaksa Peneliti</label>
              <select name="jp" class="form-control">
               @foreach($emp as $j)
                <option value="{{ $j->id }}">{{ $j->nama }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-12">
              <label for="Name">Jaksa Pemegang Berkas</label>
              <select name="jb" class="form-control">
                @foreach($emp as $k)
                <option value="{{ $k->id }}">{{ $k->nama }}</option>
                @endforeach
              </select> 
            </div>

            <div class="form-group col-md-12" style="margin-top:20px">
              <button type="submit" class="btn btn-success">Simpan</button>
              <button type="submit" class="btn btn-danger">Batal</button>
            </div>
          </div>
        </div> 

      </form>
        
</div>
@endsection
