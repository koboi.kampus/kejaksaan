@extends('layouts.app')

@push('scripts')

<script>
  $(document).ready(function() {
    $('#example').DataTable();
  });
</script>
@endpush

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><h4>SPDP dan Berkas Perkara</h4></li>
      </ol>
      <a class="btn btn-primary btn-sm" href="{{ route('case.create') }}" role="button">Input SPDP</a>
      <hr>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-12">
        
          <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Perkara</th>
                <th>Klasifikasi</th>
                <th>SPDP</th>
                <th>SPDP diTerima</th>
                <th>Tersangka</th>
                <th>Pasal</th>
                <th>Masuk Berkas</th>
                <th>Penyidik</th>
                <th>Jaksa Peneliti</th>
                <th>Jaksa Berkas</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($case as $c)
            <tr>
                <td>{{ $c->id }}</td>
                <td>{{ $c->type->perkara }}</td>
                <td>{{ $c->classification->klasifikasi}}</td>
                <td>{{ $c->nomor_spdp }}</td>
                <td>{{ $c->tanggal_spdp }}</td>
                <td>{{ $c->masuk_spdp }}</td>
                <td>{{ $c->suspect->nama }}</td>
                <td>{{ $c->lawbook->kitab}} - {{ $c->pasal }}</td>
                <td>{{ $c->investigator->nama}}</td>
                <td>{{ $c->peneliti->nama }}</td>
                <td>{{ $c->berkas->nama }}</td>
                <td></td>
            </tr>
            @endforeach
        </tbody>
    </table>        

        </div>
    </div>
</div>
@endsection

