@extends('layouts.master')

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">SPDP</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Penyidik</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- CONTENT DISINI -->

                 <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-white">Ubah Data Penyidik</h4>
                            </div>
                            <form action="{{ route('investigator.update', $investigator->id) }}" method="post">
                                @csrf
                                @method('PUT')
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('nip') ? 'has-error' : '' }}">NRP</label>
                                                    <input type="text" class="form-control" name="nip" value="{{ $investigator->nip }}" placeholder="NRP Penyidik">
                                                    <span>{{ $errors->first('nip') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('nama') ? 'has-error' : '' }}">Nama</label>
                                                    <input type="text" id="firstName" class="form-control" name="nama" value="{{ $investigator->nama }}" placeholder="Nama Penyidik">
                                                    <span>{{ $errors->first('nama') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <br>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('instansi') ? 'has-error' : '' }}">Instansi</label>
                                                    <input type="text" class="form-control" name="instansi" value="{{ $investigator->instansi }}" placeholder="Nama Instansi Penyidik">
                                                    <span>{{ $errors->first('instansi') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('phone') ? 'has-error' : '' }}">Telepon/HP</label>
                                                    <input type="text" class="form-control" name="phone" value="{{ $investigator->phone }}" placeholder="No. Telepon Penyidik">
                                                    <span>{{ $errors->first('phone') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                      
                                    </div>

                                    <div class="form-actions">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                            <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
           
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection