{{ csrf_field() }}
<header>
  <strong>Tambah</strong> atau <strong>Ubah</strong>, Data Jenis Perkara 
</header>

<fieldset>

  <section>
    <label class="label">Jenis Perkara</label>
    <label class="input">
      <input type="text" name="perkara" maxlength="100">
    </label>
    <!--<div class="note">
      <strong>Maxlength</strong> is automatically added via the "maxlength='#'" attribute
    </div> -->
  </section>

  <section>
    <label class="label">Deskripsi</label>
    <label class="textarea textarea-resizable"> 										
      <textarea name="deskripsi" rows="3" class="custom-scroll"></textarea> 
    </label>
  </section>

</fieldset>

<footer>
  <button type="submit" class="btn btn-primary">
    Simpan  
  </button>
  <button type="button" class="btn btn-default" onclick="window.history.back();">
    Batal
  </button>
</footer>

