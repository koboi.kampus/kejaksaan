@extends('layouts.master')

@section('content')

  <!-- RIBBON -->
  <div id="ribbon">

    <span class="ribbon-button-alignment"> 
      <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
        <i class="fa fa-refresh"></i>
      </span> 
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <li>Jabatan</li><li>Input Data Baru</li>
    </ol>
    <!-- end breadcrumb -->

  </div>
  <!-- END RIBBON -->


<!-- MAIN CONTENT -->
<div id="content">

  <!-- START ROW -->
  <div class="row">

    <!-- NEW COL START -->
    <article class="col-sm-12 col-md-12 col-lg-12">

      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

        <header>
          <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
          <h2>Referensi Jabatan</h2>
        </header>

        <!-- widget div-->
        <div>
          <!-- widget content -->
          <div class="widget-body no-padding">
            <!-- Form Disini -->
            <form class="smart-form" action="{{ route('position.store') }}" method="POST">
              {{ csrf_field() }}
              <header>
                <strong>Tambah</strong> Data Jabatan 
              </header>

              <fieldset>

                <section>
                  <label class="label">Jenis</label>
                  <label class="input">
                    <input type="text" name="jenis" maxlength="100">
                  </label>
                  <!--<div class="note">
                    <strong>Maxlength</strong> is automatically added via the "maxlength='#'" attribute
                  </div> -->
                </section>
                <section>
                  <label class="label">Jabatan</label>
                  <label class="input">
                    <input type="text" name="jabatan" maxlength="100">
                  </label>
                  <!--<div class="note">
                    <strong>Maxlength</strong> is automatically added via the "maxlength='#'" attribute
                  </div> -->
                </section>


              </fieldset>

              <footer>
                <button type="submit" class="btn btn-primary">
                  Simpan  
                </button>
                <button type="button" class="btn btn-default" onclick="window.history.back();">
                  Batal
                </button>
              </footer>

            </form>
          </div>
          <!-- end widget content -->
        </div>
        <!-- end widget div -->
      </div>
      <!-- end widget -->

    </article>
    <!-- END COL -->
  </div>
</div>

@endsection

