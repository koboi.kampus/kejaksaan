@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endpush

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Kepegawaian</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ubah Data Pegawai</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- CONTENT DISINI -->

                 <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-white">Ubah Data Pegawai</h4>
                            </div>
                            <form action="{{ route('profile.update', $profile->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="row p-t-20">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">NIP</label>
                                                    <input type="text" id="firstName" class="form-control" value="{{ $profile->nip }}" name="nip" placeholder="NIP Pegawai">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nama</label>
                                                    <input type="text" id="firstName" class="form-control" value="{{ $profile->nama }}" name="nama" placeholder="Nama Pegawai">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label">HP</label>
                                                    <input type="text" id="firstName" class="form-control" value="{{ $profile->phone }}" name="phone" placeholder="Nomor HP">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <!--/row-->
                                        <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tempat Lahir</label>
                                                    <input type="text" id="firstName" class="form-control" value="{{ $profile->tempat_lahir }}" name="tempat_lahir" placeholder="Tempat Lahir">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Tanggal Lahir</label>
                                                    <input type="date" value="{{ $profile->tanggal_lahir }}" name="tanggal_lahir" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('kelamin') ? 'has-error' : '' }}">Jenis Kelamin</label>
                                                    <select class="form-control custom-select" name="kelamin">
                                                        <option value="">Pilih Jenis Kelamin</option>
                                                        @if ($profile->kelamin == 1)
                                                          <option value="1" selected>Laki-laki</option>
                                                          <option value="2">Perempuan</option>
                                                        @else 
                                                          <option value="1">Laki-laki</option> 
                                                          <option value="2" selected>Perempuan</option>
                                                        @endif
                                                    </select>
                                                    <span>{{ $errors->first('kelamin') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Pangkat/Gol</label>
                                                    <select class="form-control custom-select" name="level_id">
                                                        <option value="">Pilih Pangkat/Gol</option>
                                                          @foreach($level as $t)
                                                            <option {{ $t->id == $profile->level_id ? 'selected':'' }} value="{{ $t->id }}">{{$t->pangkat}} / ({{ $t->golongan }})</option>
                                                            {{--@if($profile->level_id == $t->id)
                                                              <option value="{{ $t->id }}" selected>{{ $t->pangkat }} / {{ $t->golongan }}</option>
                                                            @endif --}}
                                                          @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Jabatan</label>
                                                    <select class="form-control custom-select" name="position_id">
                                                        <option value="">Pilih Jabatan</option>
                                                        @foreach($position as $c)
                                                            <option {{ $c->id == $profile->position_id ? 'selected':'' }} value="{{ $c->id }}">{{$c->jabatan}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Alamat</label>
                                                    <textarea name="alamat" class="form-control" rows="4">{{ $profile->alamat }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                          <label>Upload Photo</label>
                                          <div class="input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text">Upload</span>
                                              </div>
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" name="photo" id="inputGroupFile01">
                                                  <label class="custom-file-label" for="inputGroupFile01"></label>
                                              </div>
                                          </div>
                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                            <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
           
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endpush
