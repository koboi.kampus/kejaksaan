@extends('layouts.master')

@push('styles')

<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">

@endpush

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Daftar Pegawai</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Kepegawaian</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Pegawai</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- CONTENT DISINI -->
                <div align="right" style="margin-bottom: 10px">
                    <a href="{{ route('profile.create') }}" class="btn btn-primary btn-rounded btn-xs"><i class="fa fa-pencil-alt"></i> INPUT PEGAWAI</a>
                </div>

                <!-- scroll horizontal & vertical -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="scroll_ver_hor" class="table table-striped table-bordered display nowrap" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIP</th>
                                                <th>Nama Pegawai</th>
                                                <th>Pangkat/Gol</th>
												<th>Jabatan</th>
                                                <th>No. HP</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($profile as $m)
                                            <tr>
                                                <td align="center">{{ ++$i }}</td>
                                                <td align="center">{{ $m->nip }}</td>
                                                <td>{{ $m->nama }}</td>
                                                <td align="center">
                                                    {{ $m->level->pangkat }}<br>
                                                    ({{ $m->level->golongan }})
                                                </td>
                                                <td align="center">{{ $m->position->jabatan }}</td>
                                                <td align="center">{{ $m->phone }}</td>
                                                <td align="center">
                                                    <form action="{{ route('profile.destroy', $m->id) }}" method="post">
                                                        <a href="{{ route('profile.edit', $m->id) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> </a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
@endpush
