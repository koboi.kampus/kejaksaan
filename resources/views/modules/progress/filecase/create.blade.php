@extends('layouts.master')

@section('content')

  <!-- RIBBON -->
  <div id="ribbon">

    <span class="ribbon-button-alignment"> 
      <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
        <i class="fa fa-refresh"></i>
      </span> 
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <li>Pra Penuntutan</li><li>Berkas Perkara</li>
    </ol>
    <!-- end breadcrumb -->

  </div>
  <!-- END RIBBON -->


<!-- MAIN CONTENT -->
<div id="content">

<!-- widget grid -->
<section id="widget-grid" class="">

<!-- row -->
<div class="row">

  <!-- NEW WIDGET ROW START -->
  <div class="col-sm-6">

    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
      <!-- widget options:
      usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

      data-widget-colorbutton="false"
      data-widget-editbutton="false"
      data-widget-togglebutton="false"
      data-widget-deletebutton="false"
      data-widget-fullscreenbutton="false"
      data-widget-custombutton="false"
      data-widget-collapsed="true"
      data-widget-sortable="false"

      -->
      <header>
        <h2>#Data Berkas Perkara</h2>
      </header>

      <!-- widget div-->

      <div>
        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
          <!-- This area used as dropdown edit box -->
          <input class="form-control" type="text">
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body">
            <fieldset>
              <legend>
                Deti Berkas Perkara
              </legend>
              
            </fieldset>

            <table id="user" class="table table-bordered table-striped" style="clear: both">
											<tbody>
												<tr>
													<td style="width:35%;">Nomor SPDP</td>
													<td style="width:65%">{{ $case->nomor_spdp }}</td>
												</tr>
												<tr>
													<td>Tanggal SPDP</td>
													<td>{{ $case->tanggal_spdp }} </td>
												</tr>
                        <tr>
													<td>Jenis/Klasifikasi</td>
													<td>{{ $case->type->perkara }} / {{$case->classification->klasifikasi}} </td>
												</tr>
												<tr>
													<td>Penyidik</td>
													<td>
                            @foreach($case->investigators as $inv)
                              {{ $inv->nama }} <br>
                            @endforeach
                          </td>
												</tr>
												<tr>
													<td>Tersangka</td>
													<td>
                            @foreach($case->suspects as $sus)
                              {{ $sus->nama }} <br>
                            @endforeach
                          </td>
												  </tr>
													<tr>
													<td>Jaksa</td>
													<td>
                            @foreach($case->sixteen as $sx)
                              {{ $sx->profiles->nama }} <br>
                            @endforeach
                          </td>
												</tr>
											<tr>
													<td>Pasal Dilanggar</td>
													<td></td>
												</tr>
                        <tr>
													<td>Keterangan</td>
													<td>{{ $case->keterangan }}</td>
												</tr>
                        </tbody>
                    </table>

        </div>
        <!-- end widget content -->

      </div>
      <!-- end widget div -->

    </div>
    <!-- end widget -->

  </div>
  
  <!-- NEW WIDGET ROW START -->
  <div class="col-sm-6">

    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
      <!-- widget options:
      usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

      data-widget-colorbutton="false"
      data-widget-editbutton="false"
      data-widget-togglebutton="false"
      data-widget-deletebutton="false"
      data-widget-fullscreenbutton="false"
      data-widget-custombutton="false"
      data-widget-collapsed="true"
      data-widget-sortable="false"

      -->
      <header>
        <h2>#Penunjukan Jaksa </h2>
      </header>

      <!-- widget div-->

      <div>
        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
          <!-- This area used as dropdown edit box -->
          <input class="form-control" type="text">
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body">

          <form id="Form01" action="{{ url('/p16post', $case->id) }}" method="post">
            {{ csrf_field() }}
            <fieldset>
              <legend>
                Buat P-16
              </legend>
              
            </fieldset>

            <fieldset>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-12 col-md-8">
                    <label class="control-label">Nomor P-16</label>
                    <input type="text" class="form-control" name="nomor_surat" />
                  </div>

                  <div class="col-sm-12 col-md-4">
                    <label class="control-label">Tanggal</label>
                    <input type="text" class="form-control" id="surat" name="tanggal" />
                  </div>

                </div>
              </div>
            </fieldset>

            <fieldset>
              <div class="form-group">
                <label class="control-label">Keterangan</label>
                <textarea class="form-control" name="keterangan" rows="4"></textarea>
              </div>
            </fieldset>

            <legend>
              Data Jaksa
            </legend>

            <fieldset>
              <div class="form-group">
								<select name="profile[]" multiple style="width:100%" class="select2">
									@foreach($profile as $p)
                    <option value="{{ $p->id }}">{{ $p->nama }}</option>
									@endforeach
								</select>
                <div class="note">
									<strong>Catatan:</strong> Jaksa dapat dipilih lebih dari satu
								</div>
							</div>
            </fieldset>

            <legend></legend>

            <footer>
              <button type="submit" class="btn btn-primary">
                Simpan  
              </button>
              <button type="button" class="btn btn-default" onclick="window.history.back();">
                Batal
              </button>
            </footer>

          </form>

        </div>
        <!-- end widget content -->

      </div>
      <!-- end widget div -->

    </div>
    <!-- end widget -->

  </div>

  </section>
</div>

@endsection

@push('scripts')

		<!-- JQUERY VALIDATE -->
		<script src="{{ asset('js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

    <!-- JQUERY SELECT2 INPUT -->
		<script src="{{ asset('js/plugin/select2/select2.min.js') }}"></script>

		<script>
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
		
			// START AND FINISH DATE
			$('#surat').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#finishdate').datepicker('option', 'minDate', selectedDate);
				}
			});

      // START AND FINISH DATE
			$('#diterima').datepicker({
				dateFormat : 'dd.mm.yy',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#finishdate').datepicker('option', 'minDate', selectedDate);
				}
			});
		})
    
		</script>

    <script>
		
		$(document).ready(function() {
			
			pageSetUp();
	
		})

    </script>
@endpush
