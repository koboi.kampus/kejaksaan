@extends('layouts.master')

@section('content')

  <!-- RIBBON -->
  <div id="ribbon">

    <span class="ribbon-button-alignment"> 
      <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
        <i class="fa fa-refresh"></i>
      </span> 
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <li>SPDP</li><li>Ubah Data</li>
    </ol>
    <!-- end breadcrumb -->

  </div>
  <!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

  <!-- START ROW -->
  <div class="row">

    <!-- NEW COL START -->
    <article class="col-sm-12 col-md-12 col-lg-12">

      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

        <header>
          <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
          <h2>Daftar SPDP</h2>
        </header>

        <!-- widget div-->
        <div>
          <!-- widget content -->
          <div class="widget-body no-padding">
            <!-- Form Disini -->
            <form class="smart-form" action="{{ route('spdp.update', $spdp->id) }}" method="POST">
            
              @csrf
              @method('PUT')
              
              <header>
                <strong>Ubah</strong> Data SPDP 
              </header>

              <fieldset>

                <section>
                  <label class="label">Jenis Perkara</label>
                  <label class="input">
                    <input type="text" name="type_id" maxlength="100" value="{{ $spdp->type_id }}">
                  </label>
                </section>
                <section>
                  <label class="label">Klasifikasi</label>
                  <label class="input">
                    <input type="text" name="classification_id" maxlength="100" value="{{ $spdp->classification_id }}">
                  </label>
                </section>
                <section>
                  <label class="label">Nomor SPDP</label>
                  <label class="input">
                    <input type="text" name="nomor_spdp" maxlength="100" value="{{ $spdp->nomor_spdp }}">
                  </label>
                </section>
                <section>
                  <label class="label">Tanggal SPDP</label>
                  <label class="input">
                    <input type="text" name="tanggal_spdp" maxlength="100" value="{{ $spdp->tanggal_spdp }}">
                  </label>
                </section>
                <section>
                  <label class="label">SPDP diterima</label>
                  <label class="input">
                    <input type="text" name="spdp_diterima" maxlength="100" value="{{ $spdp->spdp_diterima }}">
                  </label>
                </section>
                <section>
                  <label class="label">Berkas Perkara</label>
                  <label class="input">
                    <input type="text" name="case_file_id" maxlength="100" value="{{ $spdp->case_file_id }}">
                  </label>
                </section>
                 <section>
                  <label class="label">Keterangan</label>
                  <label class="input">
                    <input type="text" name="keterangan" maxlength="100" value="{{ $spdp->keterangan }}">
                  </label>
                </section>
              </fieldset>

              <footer>
                <button type="submit" class="btn btn-primary">
                  Simpan  
                </button>
                <button type="button" class="btn btn-default" onclick="window.history.back();">
                  Batal
                </button>
              </footer>
            </form>
          </div>
          <!-- end widget content -->
        </div>
        <!-- end widget div -->
      </div>
      <!-- end widget -->

    </article>
    <!-- END COL -->
  </div>
</div>

@endsection

