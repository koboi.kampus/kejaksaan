<div class="form-body">
    <div class="card-body">

        <div class="row p-t-20">
            <div class="col-md-12">
                <div class="form-group has-feedback {!! $errors->has('judul') ? 'has-error' : '' !!}"> 
                    {!! Form::label('judul', 'Judul Informasi') !!}
                    {!! Form::text('judul', null, ['class'=>'form-control', 'placeholder' => 'Judul Informasi']) !!}
                    {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group has-feedback {!! $errors->has('informasi') ? 'has-error' : '' !!}"> 
                    {!! Form::label('informasi', 'Isi Informasi') !!}
                    {!! Form::textarea('informasi', null, ['class'=>'form-control', 'placeholder' => 'Isi Informasi']) !!}
                    {!! $errors->first('informasi', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group has-feedback {!! $errors->has('keterangan') ? 'has-error' : '' !!}"> 
                    {!! Form::label('keterangan', 'Keterangan') !!}
                    {!! Form::textarea('keterangan', null, ['class'=>'form-control', 'placeholder' => 'Keterangan']) !!}
                    {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {!! $errors->has('lampiran') ? 'has-error' : '' !!} btn btn-success"> 
                    <span class="la la-cloud-upload ks-icon"></span>
                    <span class="ks-text">Unggah Lampiran</span>
                    {!! Form::file('lampiran') !!}
                    {!! $errors->first('lampiran', '<p class="help-block">:message</p>') !!}
                    @if (isset($model) && $model->lampiran !== '')
                        <div class="row">
                            <div class="col-md-12">
                            <p>:</p>
                                <div class="thumbnail">
                                    <img src="{{ url('/img/' . $model->lampiran) }}" class="img-rounded"> 
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="card-body" align="right">
                {!! Form::submit(isset($model) ? 'Update' : 'Kirim', ['class'=>'btn btn-primary']) !!}
                <a class="btn btn-danger" href="{{ route('regulasi.index') }}"><span class="la la-times"></span> Batal</a>
            </div>
        </div>

    </div>
</div>
