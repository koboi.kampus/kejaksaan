@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endpush

@section('content')

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <div class="d-flex align-items-center">
                    <h5>Input P - 18</h5>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#"> Progress</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"> Input P - 18</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- CONTENT DISINI -->

        <!-- Row -->
        <div class="row">
       
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-warning">
                        <h4 class="m-b-0 text-white">Informasi Perkara (SPDP)</h4>
                    </div>
                    <div class="card-body">

                        <dl>
                            <div class="row">
                                <div class="col-md-12">
                                    <dt>Nomor SPDP</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->nomor_spdp }} <br> 
                                        <i class="mdi mdi-chevron-double-right"></i> Tanggal : {{ $case->tanggal_spdp }}
                                    </dd>
                                    <dt>Jenis / Klasifikasi</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->type->perkara }} / {{ $case->classification->klasifikasi }}</dd>
                                    <dt>Pelanggaran</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->pelanggaran }}</dd>
                                    <dt>Status</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->StatusPerkara }}</dd>
                                    <hr>
                                    <dt>Penyidik</dt>
                                    <dd>
                                        @foreach($case->investigators as $inv)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $inv->nama }} <br>
                                        @endforeach
                                    </dd>
                                    <hr>
                                    <dt>Jaksa</dt>
                                    <dd>
                                        @foreach($case->profiles as $jks)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $jks->nama }} <br>
                                        @endforeach
                                    </dd>
                                    <hr>
                                    <dt>Tersangka</dt>
                                    <dd>
                                        @foreach($case->suspects as $sus)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $sus->nama }} <br>
                                        @endforeach
                                    </dd>
                                </div>
                            </div>
                            <hr>
                            <dt>Keterangan</dt>
                            <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->keterangan }}</dd>
                        </dl>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">Input P-18</h4>
                    </div>
                    <form action="{{ url('/p18post', $case->id) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="card-body">
                                <div class="row p-t-20">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('nomor') ? 'has-error' : '' }}"> Nomor P - 18</label>
                                            <input type="text" class="form-control" name="nomor" placeholder="Nomor Surat Pengantar">
                                            <span>{{ $errors->first('nomor') }}</span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('tangal') ? 'has-error' : '' }}">Tanggal</label>
                                            <input type="date" class="form-control" name="tanggal">
                                            <span>{{ $errors->first('tanggal') }}</span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Kasus Posisi</label>
                                                    <textarea name="keterangan" class="form-control" rows="4">{{ $case->keterangan }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>

                            </div>

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                    <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->

<!-- Disini Footer -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endpush
