@extends('layouts.master')

@push('styles')

<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">

@endpush

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Daftar P-21</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Pra Penuntutan</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">P-21</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- scroll horizontal & vertical -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="scroll_ver_hor" class="table table-striped table-bordered display nowrap" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>SPDP - Tanggal</th>
                                                <th>Jaksa</th>
                                                <th>Klasifikasi</th>
                                                <th>Tersangka</th>
                                                <th>Countdown</th>
                                                <th>P-21.A</th>
                                                {{--<th>P-16.A</th>--}}
                                                <th>Detil</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($case as $m)
                                            <tr>
                                                <td align="center">{{ ++$i }}</td>
                                                <td>
                                                    {{ $m->nomor_spdp }} <br>
                                                    {{ $m->tanggal_spdp }}, <u><font color="#e60000">Diterima</font></u> : {{ $m->spdp_diterima }}
                                                </td>
                                                <td>
                                                @foreach($m->profiles as $jaksa)
                                                    {{ $jaksa->nama  }} <br>
                                                @endforeach
                                                </td>
                                                <td>
                                                    {{ $m->type->perkara }} <br>
                                                    {{ $m->classification->klasifikasi }}
                                                </td>
                                                <td>
                                                @foreach($m->suspects as $tersangka)
                                                    {{ $tersangka->nama  }} <br>
                                                @endforeach
                                                </td>
                                                <td align="center">
                                                    <span class="badge badge-pill badge-danger"><strong>{{ $m->TimeLeft }} Hari Lagi</strong></span>
                                                </td>
                                                <td align="center">
                                                    <a href="{{ url('createp21a', $m->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil-alt"></i> </a>
                                                </td>
                                                {{--<td align="center">
                                                    <a href="{{ url('createp16a', $m->id) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil-alt"></i> </a>
                                                </td> --}}
                                                <td align="center">
                                                    <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg{{ $m->id }}" class="btn btn-primary btn-xs"><i class="fa fa-align-justify"></i> </button>
                                                </td>
                                            </tr>
                                            <!-- sample modal content -->
                                            <div class="modal fade bs-example-modal-lg{{ $m->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Detil Perkara</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <dl>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <dt>Nomor SPDP</dt>
                                                                        <dd><i class="mdi mdi-chevron-double-right"></i> {{ $m->nomor_spdp }} <br> 
                                                                            <i class="mdi mdi-chevron-double-right"></i> Tanggal : {{ $m->tanggal_spdp }}
                                                                        </dd>
                                                                        <dt>Jenis / Klasifikasi</dt>
                                                                        <dd><i class="mdi mdi-chevron-double-right"></i> {{ $m->type->perkara }} / {{ $m->classification->klasifikasi }}</dd>
                                                                        <dt>Pelanggaran</dt>
                                                                        <dd><i class="mdi mdi-chevron-double-right"></i> {{ $m->pelanggaran }}</dd>
                                                                        <dt>Status</dt>
                                                                        <dd><i class="mdi mdi-chevron-double-right"></i> {{ $m->StatusPerkara }}</dd>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <dt>Penyidik</dt>
                                                                        <dd>
                                                                            @foreach($m->investigators as $inv)
                                                                                <i class="mdi mdi-chevron-double-right"></i> {{ $inv->nama }} <br>
                                                                            @endforeach
                                                                        </dd>
                                                                        </hr>
                                                                        <dt>Jaksa</dt>
                                                                        <dd>
                                                                            @foreach($m->profiles as $jks)
                                                                                <i class="mdi mdi-chevron-double-right"></i> {{ $jks->nama }} <br>
                                                                            @endforeach
                                                                        </dd>
                                                                        </hr>
                                                                        <dt>Tersangka</dt>
                                                                        <dd>
                                                                            @foreach($m->suspects as $sus)
                                                                                <i class="mdi mdi-chevron-double-right"></i> {{ $sus->nama }} <br>
                                                                            @endforeach
                                                                        </dd>
                                                                    </div>
                                                                </div>
                                                                <dt>Kasus Posisi</dt>
                                                                <dd><i class="mdi mdi-chevron-double-right"></i> {{ $m->keterangan }}</dd>
                                                            </dl>
                                                            <!-- SKILL BARS -->
                                                            <div class="progress ">
                                                                <div class="progress-bar bg-danger wow animated progress-animated" style="width: {{ $m->TimeLeft }}%; height:6px;" role="progressbar"> <span class="sr-only"></span> </div>
                                                            </div>
                                                            <blockquote>
                                                                <p>Saat ini Status Perkara berada pada <span class="badge badge-pill badge-warning">{{ $m->StatusPerkara }}</span>, dengan Sisa Waktu pada Status ini selama <span class="badge badge-pill badge-danger">{{ $m->TimeLeft }}</span> Hari Lagi.</p>
                                                                <small>- Kepala Seksi Pidana Umum <cite title="Source Title">Kejaksaan Negeri Dumai</cite></small>
                                                            </blockquote>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tutup</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
@endpush
