@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endpush

@section('content')

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <div class="d-flex align-items-center">
                    <h5>Persidangan</h5>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#"> Penuntutan</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"> Persidangan</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- CONTENT DISINI -->

        <!-- Row -->
        <div class="row">
       
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-warning">
                        <h4 class="m-b-0 text-white">Informasi Perkara (SPDP)</h4>
                    </div>
                    <div class="card-body">

                        <dl>
                            <div class="row">
                                <div class="col-md-12">
                                    <dt>Nomor SPDP : {{ $case->nomor_spdp }} </dt>
                                    <dd>
                                        <i class="mdi mdi-chevron-double-right"></i> Tanggal : {{ $case->tanggal_spdp }}
                                    </dd>
                                    <hr>
                                    <dt>Jenis / Klasifikasi</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->type->perkara }} / {{ $case->classification->klasifikasi }}</dd>
                                    <dt>Pelanggaran</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->pelanggaran }}</dd>
                                    <dt>Status</dt>
                                    <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->StatusPerkara }}</dd>
                                    <hr>
                                    <dt>Penyidik</dt>
                                    <dd>
                                        @foreach($case->investigators as $inv)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $inv->nama }} <br>
                                        @endforeach
                                    </dd>
                                    <hr>
                                    <dt>Jaksa</dt>
                                    <dd>
                                        @foreach($case->sixtena as $cs)
                                            @foreach($cs->profiles as $jks)
                                                <i class="mdi mdi-chevron-double-right"></i> {{ $jks->nama }} <br>
                                            @endforeach
                                        @endforeach
                                    </dd>
                                    <hr>
                                    <dt>Tersangka</dt>
                                    <dd>
                                        @foreach($case->suspects as $sus)
                                            <i class="mdi mdi-chevron-double-right"></i> {{ $sus->nama }} <br>
                                        @endforeach
                                    </dd>
                                </div>
                            </div>
                            <hr>
                            <dt>Kasus Posisi</dt>
                            <dd><i class="mdi mdi-chevron-double-right"></i> {{ $case->keterangan }}</dd>
                        </dl>

                    </div>
                </div>
            </div>
            
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">Persidangan Perkara</h4>
                    </div>
                    <form action="{{ url('/persidanganpost', $case->id) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="card-body">
                                <div class="row p-t-20">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('nomor_surat') ? 'has-error' : '' }}"> Nomor </label>
                                            <input type="text" class="form-control" name="nomor_surat" placeholder="Nomor Surat Pelimpahan">
                                            <span>{{ $errors->first('nomor_surat') }}</span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label {{ $errors->has('tanggal') ? 'has-error' : '' }}">Tanggal</label>
                                            <input type="date" class="form-control" name="tanggal">
                                            <span>{{ $errors->first('tanggal') }}</span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('keterangan') ? 'has-error' : '' }}">Kasus Posisi</label>
                                                    <textarea name="keterangan" class="form-control" rows="4">{{ $case->keterangan }}</textarea>
                                                    <span>{{ $errors->first('keterangan') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <!--/span-->
                                </div>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="dakwaan" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1"> Dakwaan</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="p37" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2"> P - 37</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="p38" id="customCheck3">
                                    <label class="custom-control-label" for="customCheck3"> P - 38</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="p42" id="customCheck4">
                                    <label class="custom-control-label" for="customCheck4"> P - 42</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="putusan" id="customCheck5">
                                    <label class="custom-control-label" for="customCheck5"> Putusan</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="p44" id="customCheck6">
                                    <label class="custom-control-label" for="customCheck6"> P - 44</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="banding" id="customCheck7">
                                    <label class="custom-control-label" for="customCheck7"> Banding</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="kasasi" id="customCheck8">
                                    <label class="custom-control-label" for="customCheck8"> Kasasi</label>
                                </div>

                            </div>

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                    <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->

<!-- Disini Footer -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endpush
