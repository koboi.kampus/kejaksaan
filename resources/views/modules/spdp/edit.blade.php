@extends('layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endpush

@section('content')

    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <div class="d-flex align-items-center">

                    </div>
                </div>
                <div class="col-7 align-self-center">
                    <div class="d-flex no-block justify-content-end align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#">SPDP</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Ubah SPDP</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">

            <!-- CONTENT DISINI -->

                <!-- Row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header bg-info">
                            <h4 class="m-b-0 text-white">Ubah Data SPDP</h4>
                        </div>
                        <form action="{{ route('spdp.store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="card-body">
                                    <div class="row p-t-20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nomor SPDP</label>
                                                <input type="text" id="firstName" class="form-control" value="{{ $spdp->nomor_spdp }}" name="nomor_spdp" placeholder="Nomor SPDP">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Tanggal SPDP</label>
                                                <input type="date" name="tanggal_spdp" value="{{ $spdp->tanggal_spdp }}" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Tanggal Diterima</label>
                                                <input type="date" name="spdp_diterima" value="{{ $spdp->spdp_diterima }}" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group has-success">
                                                <label class="control-label">Jenis Perkara</label>
                                                <select class="form-control custom-select" name="type_id">
                                                    <option value="">Pilih Jenis Perkara</option>
                                                    @foreach($type as $t)
                                                        <option {{ $t->id == $spdp->type_id ? 'selected':'' }} value="{{ $t->id }}">{{$t->perkara}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group has-success">
                                                <label class="control-label">Klasifikasi</label>
                                                <select class="form-control custom-select" name="classification_id">
                                                    <option value="">Pilih Klasifikasi</option>
                                                    @foreach($classification as $c)
                                                        <option {{ $c->id == $spdp->classification_id ? 'selected':'' }} value="{{ $c->id }}">{{$c->klasifikasi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Pelanggaran</label>
                                                <input name="pelanggaran" type="text" id="firstName" value="{{ $spdp->pelanggaran }}" class="form-control" placeholder="Pasal Pelanggaran"> 
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Kasus Posisi</label>
                                                <textarea name="keterangan" class="form-control" rows="4">{{ $spdp->keterangan }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label">Penyidik</label>
                                            <select name="investigator[]" class="select2 form-control" multiple="multiple" style="height: 36px;width: 100%;">
                                                @foreach($investigator as $i)
                                                    <option {{ $i->id == $spdp->investigator_id ? 'selected':'' }} value="{{ $i->id }}">{{$i->nama}} - {{ $i->instansi }}</option>
                                                    {{--<option value="{{ $i->id }}">{{ $i->nama }} - {{ $i->instansi }}</option>--}}
                                                @endforeach
                                            </select>
                                            <small class="form-control-feedback"> Pilih Penyidik (Dapat lebih dari satu) </small>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label">Tersangka</label>
                                            <select name="suspect[]" class="select2 form-control" multiple="multiple" style="height: 36px;width: 100%;">
                                                @foreach($suspect as $s)
                                                    <option value="{{ $s->id }}">{{ $s->nama }} - (Tanggal Lahir: {{ $s->tanggal_lahir }})</option>
                                                                    @endforeach
                                            </select>
                                            <small class="form-control-feedback"> Pilih Tersangka (Dapat lebih dari satu) </small>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-actions">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->

    <!-- Disini Footer -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/forms/select2/select2.init.js') }}"></script>
@endpush