@extends('layouts.master')

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">SPDP</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Input Tersangka</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- CONTENT DISINI -->

                 <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-white">Input Data Tersangka</h4>
                            </div>
                            <form action="{{ route('suspect.store') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">NIK</label>
                                                    <input type="text" class="form-control" name="nik" placeholder="NIK Tersangka">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('nama') ? 'has-error' : '' }}">Nama Lengkap</label>
                                                    <input type="text" id="firstName" class="form-control" name="nama" placeholder="Nama Lengkap dengan Bin/Binti">
                                                    <span>{{ $errors->first('nama') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('tempat_lahir') ? 'has-error' : '' }}">Tempat Lahir</label>
                                                    <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
                                                    <span>{{ $errors->first('tempat_lahir') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('tanggal_lahir') ? 'has-error' : '' }}">Tanggal Lahir</label>
                                                    <input type="date" class="form-control" name="tanggal_lahir" placeholder="Tanggal Lahir">
                                                    <span>{{ $errors->first('tanggal_lahir') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('kelamin') ? 'has-error' : '' }}">Jenis Kelamin</label>
                                                    <select class="form-control custom-select" name="kelamin">
                                                        <option value="">Pilih Jenis Kelamin</option>
                                                        <option value="1">Laki-laki</option>
                                                        <option value="2">Perempuan</option>
                                                    </select>
                                                    <span>{{ $errors->first('kelamin') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('kebangsaan') ? 'has-error' : '' }}">Kebangsaan</label>
                                                    <input type="text" class="form-control" name="kebangsaan" placeholder="Kebangsaan">
                                                    <span>{{ $errors->first('kebangsaan') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('agama') ? 'has-error' : '' }}">Agama</label>
                                                    <select class="form-control custom-select" name="agama">
                                                        <option value="">Pilih Jenis Agama</option>
                                                        <option value="Islam">Islam</option>
                                                        <option value="Katolik">Katolik</option>
                                                        <option value="Protestan">Protestan</option>
                                                        <option value="Hindu">Hindu</option>
                                                        <option value="Budha">Budha</option>
                                                    </select>
                                                    <span>{{ $errors->first('agama') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('pendidikan') ? 'has-error' : '' }}">Pendidikan</label>
                                                    <input type="text" class="form-control" name="pendidikan" placeholder="Pendidikan">
                                                    <span>{{ $errors->first('pendidikan') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('pekerjaan') ? 'has-error' : '' }}">Pekerjaan</label>
                                                    <input type="text" class="form-control" name="pekerjaan" placeholder="Pekerjaan">
                                                    <span>{{ $errors->first('tempat_lahir') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label {{ $errors->has('phone') ? 'has-error' : '' }}">Telepon</label>
                                                    <input type="text" class="form-control" name="phone" placeholder="Telepon/HP">
                                                    <span>{{ $errors->first('phone') }}</span>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Alamat</label>
                                                    <textarea name="alamat" class="form-control" rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Simpan</button>
                                            <button type="button" class="btn btn-dark" onclick="window.history.back();">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
           
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection