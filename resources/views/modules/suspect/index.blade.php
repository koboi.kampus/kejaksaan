@extends('layouts.master')

@push('styles')

<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">

@endpush

@section('content')

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Daftar Tersangka</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Pra Penuntutan</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Tersangka</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- CONTENT DISINI -->
                <div align="right" style="margin-bottom: 10px">
                    <a href="{{ route('suspect.create') }}" class="btn btn-primary btn-rounded btn-xs"><i class="fa fa-pencil-alt"></i> INPUT TERSANGKA </a>
                </div>

                <!-- scroll horizontal & vertical -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="scroll_ver_hor" class="table table-striped table-bordered display nowrap" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="20%">Nama</th>
                                                <th width="10%">Tempat Lahir</th>
                                                <th width="10%">Tgl.Lahir</th>
                                                <th width="10%">J.Kelamin</th>
                                                <th width="20%">Alamat</th>
                                                <th width="10%">Telepon</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($suspect as $m)
                                            <tr>
                                                <td align="center">{{ ++$i }}</td>
                                                <td>{{ $m->nama }}</td>
                                                <td>{{ $m->tempat_lahir }}</td>
												<td align="center">{{ $m->tanggal_lahir }}</td>
                                                <td align="center">{{ $m->jenkelamin }}</td>
                                                <td>{{ $m->alamat }}</td>
                                                <td align="center">{{ $m->phone }}</td>
                                                <td align="center">
                                                    <form action="{{ route('suspect.destroy', $m->id) }}" method="post">
                                                        <a href="{{ route('suspect.edit', $m->id) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> </a>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
       </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

        <!-- Disini Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/datatable/datatable-basic.init.js') }}"></script>
@endpush
