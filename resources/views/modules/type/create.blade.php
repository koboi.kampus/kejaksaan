@extends('layouts.master')

@section('content')

  <!-- RIBBON -->
  <div id="ribbon">

    <span class="ribbon-button-alignment"> 
      <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
        <i class="fa fa-refresh"></i>
      </span> 
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <li>Jenis Perkara</li><li>Input Data Baru</li>
    </ol>
    <!-- end breadcrumb -->

  </div>
  <!-- END RIBBON -->


<!-- MAIN CONTENT -->
<div id="content">

  <!-- START ROW -->
  <div class="row">

    <!-- NEW COL START -->
    <article class="col-sm-12 col-md-12 col-lg-12">

      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

        <header>
          <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
          <h2>Referensi Jenis Perkara</h2>
        </header>

        <!-- widget div-->
        <div>
          <!-- widget content -->
          <div class="widget-body no-padding">
            <!-- Form Disini -->
            <form class="smart-form" action="{{ route('type.store') }}" method="POST">
              @include('modules.type._form')
            </form>
          </div>
          <!-- end widget content -->
        </div>
        <!-- end widget div -->
      </div>
      <!-- end widget -->

    </article>
    <!-- END COL -->
  </div>
</div>

@endsection

