@extends('layouts.master')

@section('content')

  <!-- RIBBON -->
  <div id="ribbon">

    <span class="ribbon-button-alignment"> 
      <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
        <i class="fa fa-refresh"></i>
      </span> 
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <li>Pegawai</li><li>Ubah Data</li>
    </ol>
    <!-- end breadcrumb -->

  </div>
  <!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

  <!-- START ROW -->
  <div class="row">

    <!-- NEW COL START -->
    <article class="col-sm-12 col-md-12 col-lg-12">

      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">

        <header>
          <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
          <h2>Daftar Pegawai</h2>
        </header>

        <!-- widget div-->
        <div>
          <!-- widget content -->
          <div class="widget-body no-padding">
            <!-- Form Disini -->
            <form class="smart-form" action="{{ route('profile.update', $profile->id) }}" method="POST">
            
              @csrf
              @method('PUT')
              
              <header>
                <strong>Ubah</strong> Data Pegawai 
              </header>

              <fieldset>

                <section>
                  <label class="label">NIP</label>
                  <label class="input">
                    <input type="text" name="nip" maxlength="100" value="{{ $profile->nip }}">
                  </label>
                </section>
                <section>
                  <label class="label">Nama Pegawai</label>
                  <label class="input">
                    <input type="text" name="nama" maxlength="100" value="{{ $profile->nama }}">
                  </label>
                </section>
                <section>
                  <label class="label">Pangkat</label>
                  <label class="input">
                    <input type="text" name="level_id" maxlength="100" value="{{ $profile->level_id }}">
                  </label>
                </section>
                <section>
                  <label class="label">Jabatan</label>
                  <label class="input">
                    <input type="text" name="position_id" maxlength="100" value="{{ $profile->position_id }}">
                  </label>
                </section>
                <section>
                  <label class="label">Tempat Lahir</label>
                  <label class="input">
                    <input type="text" name="tempat_lahir" maxlength="100" value="{{ $profile->tempat_lahir }}">
                  </label>
                </section>
                <section>
                  <label class="label">Tanggal Lahir</label>
                  <label class="input">
                    <input type="text" name="tanggal_lahir" maxlength="100" value="{{ $profile->tanggal_lahir }}">
                  </label>
                </section>
                 <section>
                  <label class="label">Jenis Kelamin</label>
                  <label class="input">
                    <input type="text" name="kelamin" maxlength="100" value="{{ $profile->kelamin }}">
                  </label>
                </section>
                <section>
                  <label class="label">Alamat</label>
                  <label class="input">
                    <input type="text" name="alamat" maxlength="100" value="{{ $profile->alamat }}">
                  </label>
                </section>

                 <section>
                  <label class="label">Telepon</label>
                  <label class="input">
                    <input type="text" name="phone" maxlength="100" value="{{ $profile->phone }}">
                  </label>
                </section>

              </fieldset>

              <footer>
                <button type="submit" class="btn btn-primary">
                  Simpan  
                </button>
                <button type="button" class="btn btn-default" onclick="window.history.back();">
                  Batal
                </button>
              </footer>
            </form>
          </div>
          <!-- end widget content -->
        </div>
        <!-- end widget div -->
      </div>
      <!-- end widget -->

    </article>
    <!-- END COL -->
  </div>
</div>

@endsection

