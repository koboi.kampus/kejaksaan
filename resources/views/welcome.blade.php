<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!--><html class="no-js" lang="en"><!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Sisroler</title>
	<meta name="keywords" content="" >
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="{{ asset('landing/css/base.css') }}"/>
	<link rel="stylesheet" href="{{ asset('landing/css/skeleton.css') }}"/>
	<link rel="stylesheet" href="{{ asset('landing/css/layout.css') }}"/>
	<link rel="stylesheet" href="{{ asset('landing/css/colorbox.css') }}"/>
	<link rel="stylesheet" href="{{ asset('landing/css/font-awesome.css" ') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/jquery.custombox.css') }}">
	{{--<link rel="stylesheet" href="{{ asset('landing/css/retina.css') }}"/>--}}

	<link rel="alternate stylesheet" type="text/css" href="{{ asset('landing/css/colors/color-orange.css') }}" title="orange">
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('landing/css/colors/color-green.css') }}" title="green">
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('landing/css/colors/color-red.css') }}" title="red">
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('landing/css/colors/color-blue.css') }}" title="blue">
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('landing/css/colors/color-yellow.css') }}" title="yellow">
	
	
	
    <!--[if lte IE 8]>
        <script src="js/html5.js"></script>
    <![endif]-->		
		
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.png">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
</head>
<body>


	
	<!-- Primary Page Layout
	================================================== -->
    <div id="preloader"></div>
	
	<div id="menu-wrap">
		<div class="logo"></div>
		<ul class="slimmenu">
			<li> 
				<a href="/login">Login</a>
			</li>
		</ul>
	</div>	
	

	<section id="content">
	
		<article id="home" class="active"> 
			<div id="wrapper-slider">
				<div id="controls">
					<div class="background-grid"></div>
					<div class="prev"></div>
					<div class="next"></div>
				</div>
			</div>
			<div class="home-text">
				{{--<p><span>Kejaksaan Negeri Dumai - Sisroler - Seksi Pidana Umum</span></p>
				<div class="rotator">
					<div><h1>DISINI <span>VISI</span></h1></div>
					<div><h1>DISINI  <span>MOTTO</span></h1></div>
					<div><h1>DISINI <span>TARGET</span></h1></div>
				</div>
				{{--<div class="link-home"><div class="cl-effect-14"><a href="javascript:goTo('about');">start here</a></div></div>--}}
		 	</div>
		</article>
      
	</section>
	
	
    <div id="modal" style="display: none;" class="modal-example-content">
		<button type="button" class="close" onclick="$.fn.custombox('close');">&times;</button>
		<div class="container">
			<div class="sixteen columns">
				<div class="services-wrap">  
					<div class="services-wrap-icon">
						<img src="images/ser1.png" alt=""/> 
					</div>		
				</div>
			<h5>Responsive Design</h5>
			</div>
			<div class="sixteen columns">
				<div class="modal-example-body">  
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.</p>
				</div>	
			</div>
		</div>
    </div>	

    <!-- Switch Panel -->
    <div id="switch">
        <div class="content-switcher" >        
			<p>Color Options</p>
			<ul class="header">            
				<li><a href="#" onClick="setActiveStyleSheet('orange'); return false;" class="button color switch" style="background-color:#d35400"></a></li>
				<li><a href="#" onClick="setActiveStyleSheet('green'); return false;" class="button color switch" style="background-color:#2ecc71"></a></li>
				<li><a href="#" onClick="setActiveStyleSheet('red'); return false;" class="button color switch" style="background-color:#e74c3c"></a></li>
				<li><a href="#" onClick="setActiveStyleSheet('blue'); return false;" class="button color switch" style="background-color:#3498db"></a></li>
				<li><a href="#" onClick="setActiveStyleSheet('yellow'); return false;" class="button color switch" style="background-color:#f1c40f"></a></li>
			</ul>        
			<div class="clear"></div>        
			<p>Page Templates:</p> 
			<div class="home-options">
				<a href="http://ivang-design.com/vulcan/video/">Multi-Page Video</a>
				<a href="http://ivang-design.com/vulcan/sliderop/">One-Page Slider</a>
				<a href="http://ivang-design.com/vulcan/videoop/">One-Page Video</a>
				<a href="http://ivang-design.com/vulcan/animated/">Animated Layout</a>
				<a href="http://ivang-design.com/vulcan/animated2/">Animated Layout 2</a>
				<a href="http://ivang-design.com/vulcan/parallax/">Parallax Version</a>
				<a href="http://ivang-design.com/vulcan/showcaseslider/">Showcase Slider</a>
				<a href="http://ivang-design.com/vulcan/splash/">Splash Intro</a>
				<a href="http://ivang-design.com/vulcan/rotatingwords/">Rotating Words</a>
			</div> 
			<div id="hide">
				<img  src="images/close.png" alt="" /> 
			</div>
        </div>
	</div>
	<div id="show" style="display: block;">
        <div id="setting"></div>
    </div>
    <!-- Switch Panel -->
	

	
	<!-- JAVASCRIPT
    ================================================== -->
<script type="text/javascript" src="{{ asset('landing/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/styleswitcher.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/pace.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('landing/js/retina-1.1.0.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('landing/js/modernizr.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.slimmenu.js') }}"></script> 
<script type="text/javascript" src="{{ asset('landing/js/mb.bgndGallery.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.easing.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.custombox.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.colorbox.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.transit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.isotope.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.fitvids.js') }}"></script>	
{{--<script type="text/javascript" src="{{ asset('landing/js/gmaps.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/contact.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('landing/js/template.js') }}"></script>  	  
<!-- End Document
================================================== -->
</body>
</html>
